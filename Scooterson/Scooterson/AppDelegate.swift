//
//  AppDelegate.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 3/8/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMaps
import Intents
import SwiftTheme
import FirebaseCore
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Alamofire
import Solar
import ZendeskCoreSDK
import SupportProvidersSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let notificationCenter = UNUserNotificationCenter.current()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Check if launched from notification
        let notificationOption = launchOptions?[.remoteNotification]
        
        notificationCenter.delegate = self
        if let notification = notificationOption as? [String: AnyObject]{
            //todo handle category action depends on the type of notification
            print("Notification arrived  \(notification)")
            
        }
        
        GMSServices.provideAPIKey("AIzaSyCmxz1zWYNA1hP8OjS875axXEQe_e4-AKk")
        
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.theme_setStatusBarStyle([.lightContent, .darkContent], animated: true)
            ThemeManager.setTheme(index: UserDefaults.standard.integer(forKey: ContantsKey.darkModeIos13.rawValue) == ConstantValues.DARKMODE_ON ? 0 : 1)
        }else{
            UIApplication.shared.theme_setStatusBarStyle([.lightContent, .default], animated: true)
            ThemeManager.setTheme(index: UserDefaults.standard.bool(forKey: "darkMode") ? 0 : 1)
        }
        
        
        let navBarApperance = UINavigationBar.appearance()
        navBarApperance.theme_barTintColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        navBarApperance.isTranslucent = false
        navBarApperance.theme_tintColor = [UIColor.dodgerBlue.toHexString(), UIColor.charcoalGrey.toHexString()]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Splash")
        window?.rootViewController = controller
        FirebaseApp.configure()
        //        application.beginBackgroundTask(withName: "showNotification", expirationHandler: nil)
        Reachability.startNetworkReachabilityObserver()
        
        // Zendesk
        Zendesk.initialize(appId: "c6f08bc79a34b82a3707f41b5c3b604bba1c2d38aa8258d7",
                           clientId: "mobile_sdk_client_ca3cfbd55e93a9137727",
                           zendeskUrl: "https://scooterson.zendesk.com")
        let ident = Identity.createAnonymous()
        Zendesk.instance?.setIdentity(ident)
        Support.initialize(withZendesk: Zendesk.instance)
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //Theme check for  ios 13 automatically set
        if(UserDefaults.standard.integer(forKey: ContantsKey.darkModeIos13.rawValue) == ConstantValues.DARKMODE_AUTOMATIC){
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    ThemeManager.setTheme(index: 0)
                    print("Theme is dark")
                } else {
                    ThemeManager.setTheme(index: 1)
                    print("Theme is light")
                }
            }
        }
        
        if API.shared.authorized{
            if UserDefaults.standard.bool(forKey: "IS_NOT_FIRSR_RUN")  {
                registerForPushNotifications()
            }
            else {
                UserDefaults.standard.set(true, forKey: "IS_NOT_FIRSR_RUN")
                //BTElfService.shared.chargerReminderIsOn = true
            }
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: Pushes
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Notification tap \(userInfo.keys)")
        
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newPush"), object: userInfo["aps"])
        guard let deviceName = userInfo["deviceName"] as? NSString else { return }
        print("Notification tap device name: \(deviceName)")
        
        
        if let aps = userInfo["aps"] as? NSDictionary {
            if let category = aps["category"] as? NSString {
                print("Notification tap category \(category)")  
                if category == "SHARING_REQUEST" {
                    let presentViewController = self.window!.rootViewController?.presentedViewController
                    if (presentViewController?.presentedViewController as? SCTabBarController) != nil{
                        PLBleService.getInstance()?.disconnect()
                    }
                    let controller: LaunchViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Splash") as! LaunchViewController
                    window?.rootViewController = controller
                    controller.doAction(deviceMac: deviceName as String)
                }
            }
        }
        
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge, .criticalAlert]) {
                [weak self] granted, error in
                print("Notification permission granted \(granted)")
                UserDefaults.standard.set(granted, forKey: ContantsKey.settingsNotificationPermission.rawValue)
                UserDefaults.standard.synchronize()
                guard granted else {return}
                self?.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Notification Device APN Token: \(token)")
        Messaging.messaging().apnsToken = deviceToken
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Notification Device APN Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Notification Device APN firebase instance ID token: \(result.token)")
                let firebaseToken = result.token
                let storedToken = UserDefaults.standard.string(forKey: ContantsKey.pushNotificationToken.rawValue)
                if storedToken == nil || storedToken != firebaseToken{
                    //Firebase Cloud Messaging setup
                    //Firebase token fetch
                    API.shared.sendPushToken(deviceToken: firebaseToken) { (success) in
                        if success{
                            //save Device token to make sure we sync with our server whenever it changes
                            UserDefaults.standard.set(firebaseToken, forKey: ContantsKey.pushNotificationToken.rawValue)
                            UserDefaults.standard.set(true, forKey: ContantsKey.settingsNotification.rawValue)
                            UserDefaults.standard.synchronize()
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Notification Device APN Failed to register: \(error)")
    }
    
    public func scheduleNotification(notificationData:[String:String]) {
        let content = UNMutableNotificationContent() // Содержимое уведомления
        
        content.title = notificationData["title"]!
        content.body = notificationData["message"]!
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if isNotification(){
            let category = notification.request.content.categoryIdentifier
            print("Notification arrived \(category)")
            completionHandler([.alert, .sound])
        }else{
            print("Notification disabled by user")
        }
    }
}

