//
//  ConfirmationCodeViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 3/19/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import SnapKit

class ConfirmationCodeViewController: BaseViewController, UITextFieldDelegate {
    public var phone: String = "1"
    public var countrySelected: String = ""
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet var codeLabels: [UILabel]!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var labelsStackView: UIStackView!
    private let waitTime = 40
    private var timerCount = 0
    private var timer: Timer!
    @IBOutlet weak var logoImageView: UIImageView!
    public var isModify: Bool = false
    
    
    fileprivate func buildTheUi() {
        let tapGestureRecogniser = UITapGestureRecognizer(target: self, action:#selector(tapOnCode))
        codeTextField.becomeFirstResponder()
        codeTextField.textContentType = .oneTimeCode
        codeTextField.delegate = self
        codeTextField.theme_keyboardAppearance = [.dark, .light]
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        descriptionLabel.text = "We have sent you an SMS with a code\nto the number:  +\(phone)"
        descriptionLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        timerLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        logoImageView.theme_image = ["scootersonWhite","scooterson"]
        
        
        for label in codeLabels {
            label.addGestureRecognizer(tapGestureRecogniser)
            
            let indicatorView = UIView()
            indicatorView.backgroundColor = UIColor.dodgerBlue
            indicatorView.alpha = 0.5
            indicatorView.layer.cornerRadius = 4.0
            indicatorView.tag = 111
            label.addSubview(indicatorView)
            indicatorView.snp.makeConstraints { (make) in
                make.width.height.equalTo(16.0)
                make.center.equalTo(label)
            }
        }
        resendButton.layer.cornerRadius = 10.0
        resendButton.alpha = 0.5
        resendButton.isEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.theme_barTintColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        timerCount = waitTime
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    @objc func tapOnCode() {
        codeTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buildTheUi()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        startTimer()
    }
    
    private func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 0.40, repeats: true) { [weak self] (timer) in
            if self?.timerCount != 0 {
                DispatchQueue.main.async {
                    self!.timerCount -= 1
                    let timeValue = self!.timerCount > 40 ? "minutes" : "seconds"
                    let (_,m,s) = self!.secondsToHoursMinutesSeconds(seconds: self!.timerCount)
                    self?.timerLabel.text = String(format: "Available in %d:%02d %@", m, s, timeValue)
                }
            }
            else {
                DispatchQueue.main.async {
                    self!.timerLabel.text = "Resend is available"
                    self!.resendButton.isEnabled = true
                    self!.resendButton.alpha = 1.0
                }
                timer.invalidate()
            }
        }
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("Range found is \(range.location)")
        if (range.location < codeLabels.count || string.isEmpty) {
            codeLabels[range.location].text = string
            codeLabels[range.location].viewWithTag(111)?.alpha = 0.0
            if range.location < codeLabels.count - 1 {
                codeLabels[range.location + 1].viewWithTag(111)?.alpha =  1.0
            }
            if string.isEmpty {
                codeLabels[range.location].viewWithTag(111)?.alpha =  1.0
                if range.location < codeLabels.count - 1 {
                    codeLabels[range.location+1].viewWithTag(111)?.alpha =  0.5
                }
            }
            if !(codeLabels.last!.text?.isEmpty)! {
                if let text = textField.text,
                    let textRange = Range(range, in: text) {
                    let updatedText = text.replacingCharacters(in: textRange,
                                                               with: string)
                    print(updatedText)
                    checkCode(codeString: updatedText)
                }
            }
            return true
        }
        else {
            return false
        }
    }
    
    private func resetCodeLabel(){
        for index in 0...5{
            codeLabels[index].text = ""
            codeLabels[index].viewWithTag(111)?.alpha =  0.5
            codeTextField.text = ""
        }
    }
    
    
    private func checkCode(codeString:String) {
        loaderShow(withDelegate: nil)
        API.shared.verifyOTP(phone: phone, code: codeString) {[weak self] (success, statusCode, errorCode, hasSharedDevice) in
            self?.loaderHide()
            if success {
                if var userData = SCUser.reStore() {
                    userData.phone = self!.phone
                    userData.country = self!.countrySelected
                    userData.store()
                }
                else {
                    #if DEBUG
                    print("Country wasn't stored!")
                    #endif
                }
                let devicesVC = AddNameViewController()
                devicesVC.setHasSharedDevice(hasShared: hasSharedDevice)
                let navVC = UINavigationController(rootViewController: devicesVC)
                navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self?.present(navVC, animated: true)
            }
            else {
                self!.resetCodeLabel()
                switch statusCode{
                case 0:
                    self?.showAlert(title: "noNetwork".localized(), message:"noNetworkDefaultMessage".localized())
                case 500:
                    if (errorCode != nil && ((errorCode?.contains("TBSM02")) == true)){
                        self?.showAlert(title: "expired".localized(), message: "expiredMessage".localized())
                    }else{
                        self?.showAlert(title: "error".localized(), message:API.shared.errorDictionarySignUp[statusCode] ?? "networkErrorMessage".localized())
                    }
                case 401:
                    self?.showAlert(title: "invalidOtp".localized(), message:API.shared.errorDictionarySignUp[statusCode] ?? "networkErrorMessage".localized())
                case 409:
                    let alert: UIAlertController = UIAlertController(title: "error".localized(), message: "phoneRegisteredWithDifferentUser".localized(), preferredStyle: .alert)
                    let action: UIAlertAction = UIAlertAction.init(title: "okay".localized(), style: UIAlertAction.Style.cancel) { (UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    }
                    let contactSupport: UIAlertAction = UIAlertAction.init(title: "contactSupport".localized(), style: .default) { (UIAlertAction) in
                        //todo add ticket creation
                        alert.dismiss(animated: true, completion: nil)
                    }
                    alert.addAction(action)
                    alert.addAction(contactSupport)
                    self?.present(alert, animated: true, completion: nil)
                    
                    
                default:
                    self?.showAlert(title: "error".localized(), message:API.shared.errorDictionarySignUp[statusCode] ?? "networkErrorMessage".localized())
                }
                
            }
        }        
    }
    
    @IBAction func resendCodeButtonTapped(_ sender: UIButton) {
        timerCount = waitTime
        resendButton.isEnabled = false
        resetCodeLabel()
        for label in codeLabels {
            label.text = ""
        }
        startTimer()
        API.shared.authorizeByPhone(phone: phone, isModifyingPhone: isModify) { [weak self] (success, status, errorCode)  in
            if success {
                if status == 200 {
                    self?.timerLabel.text = "Code resended"
                }
                else {
                    self?.showAlert(title: "phoneErrorTitle".localized(), message:API.shared.errorDictionarySignUp[status!] ?? "unknown")
                }
            }
            else {
                if status == 0 {
                    self?.showAlert(title: "noNetwork".localized(), message:"noNetworkDefaultMessage".localized())
                }else{
                    self?.showAlert(title: "phoneErrorTitle".localized(), message:API.shared.errorDictionarySignUp[status!] ?? "unknown")
                }
            }
        }
    }
}
