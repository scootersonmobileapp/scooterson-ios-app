//
//  ViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 3/8/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class MainViewController: BaseViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var phoneField: FPNTextField!
    @IBOutlet weak var goButton: UIButton!
    var validatedPhone: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.theme_barTintColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        self.navigationController?.navigationBar.theme_tintColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        
        logoImageView.theme_image = ["scootersonWhite","scooterson"]
        titleLabel.theme_textColor  = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        descriptionLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        phoneField.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        phoneField.backgroundColor = UIColor.clear
        phoneField.becomeFirstResponder()
        
        setUpView()
        var countryName = ""
        if let countryCode = Utils.firstCarrier()?.isoCountryCode {
            countryName = getCountryName(countryCode: countryCode.uppercased()) ?? ""
            
            phoneField.attributedPlaceholder = NSAttributedString(string: "00000", attributes:[NSAttributedString.Key.foregroundColor: UIColor.coolGrey])
            phoneField.setFlag(countryCode: FPNCountryCode(rawValue:countryCode.uppercased())!)
        }
        else {
            if let countryCode = Locale.current.regionCode {
                countryName = getCountryName(countryCode: (countryCode.uppercased())) ?? ""
                phoneField.attributedPlaceholder = NSAttributedString(string: "0000", attributes:[NSAttributedString.Key.foregroundColor: UIColor.coolGrey])
                phoneField.setFlag(countryCode: FPNCountryCode(rawValue:countryCode.uppercased())!)
            }
        }
        if let _ = SCUser.reStore() {}
        else {
            let userData = SCUser(firstName: "", lastName: "", addressLine1: "", addressLine2: nil, city: "", country: countryName, state: "", zipcode: "", email: "", phone: "")
            userData.store()
        }
    }
    
    func getCountryName(countryCode: String) -> String? {
        let current = Locale(identifier: "en_US")
        return current.localizedString(forRegionCode: countryCode)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setUpView() {        
        phoneField.theme_keyboardAppearance = [.dark, .light]        
        #if DEBUG
        let testButton = UIButton(type: .custom)
        self.view.addSubview(testButton)        
        testButton.backgroundColor = .red
        testButton.setTitleColor(.white, for: .normal)
        testButton.setTitle("TEST", for: .normal)
        testButton.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.width.equalTo(60)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
        testButton.addAction(for: .touchUpInside) { [weak self] in
            //            let devicesVC = DeviceSelectionViewController()
            //            let navVC = UINavigationController(rootViewController: devicesVC)
            //            devicesVC.navigationController?.navigationBar.shadowImage = UIImage()
            //            navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            //            self?.present(navVC, animated: true)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let confirmVC = storyboard.instantiateViewController(withIdentifier: "checkCode") as! ConfirmationCodeViewController
            confirmVC.phone = "+14156047124"
            confirmVC.countrySelected = "USA"
            let backItem = UIBarButtonItem()
            backItem.title = "editPhoneTitle".localized()
            backItem.tintColor = .dodgerBlue
            self?.navigationItem.backBarButtonItem = backItem
            confirmVC.navigationController?.navigationBar.isHidden = false
            self?.navigationController?.pushViewController(confirmVC, animated: true)
            
            
        }
        #endif
        goButton.alpha = 0.0
        goButton.layer.cornerRadius = 10.0
        phoneField.autocorrectionType = .no
        //        phoneField.parentViewController = self
    }
    @IBAction func sendButtonTapped(_ sender: Any) {
        let phoneToSend = self.validatedPhone
        if phoneToSend!.count == 0 {
            showAlert(title: "incorrectNumberTitle".localized(), message:"incorrectNumberMessage".localized())
        }else{
            let selectedCountry = self.phoneField.selectedCountry?.name
            signUp(phoneToSend!, selectedCountry, isModifyingPhone: false)
        }
    }
    
    fileprivate func signUp(_ phoneToSend: String, _ selectedCountry: String?, isModifyingPhone: Bool) {
        loaderShow(withDelegate: nil)
        API.shared.authorizeByPhone(phone: phoneToSend, isModifyingPhone: isModifyingPhone) { [weak self] (success, status, errorCode)  in
            self?.loaderHide()
            if success {
                if status == 200 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let confirmVC = storyboard.instantiateViewController(withIdentifier: "checkCode") as! ConfirmationCodeViewController
                    confirmVC.phone = self!.validatedPhone
                    confirmVC.isModify = isModifyingPhone
                    confirmVC.countrySelected = selectedCountry!
                    let backItem = UIBarButtonItem()
                    backItem.title = "editPhoneTitle".localized()
                    backItem.tintColor = .dodgerBlue
                    self?.navigationItem.backBarButtonItem = backItem
                    confirmVC.navigationController?.navigationBar.isHidden = false
                    self?.navigationController?.pushViewController(confirmVC, animated: true)
                }
                else if status == 404 {
                    self?.showAlertForChangingPhone()
                }
                else if status == 409 {
                    self?.showAlert(title: "smsErrorTitle".localized(), message:"smsErrorMessage".localized())
                }
                else if status == 401 {
                    self?.showAlert(title: "unauthorisedTitle".localized(), message:"unauthorisedMessage".localized())
                }
                else if status == 403 {
                    self?.showAlert(title: "forbiddenTitle".localized(), message:"forbiddenMessage".localized())
                }
                else if status == 500 {
                    if errorCode != nil && (errorCode?.contains("TWER34") == true) {
                        self?.showAlert(title: "incorrectNumberTitle".localized(), message:"incorrectNumberOrCountryNotSupportedMessage".localized())
                    }else{
                        self?.showAlert(title: "error".localized(), message:API.shared.errorDictionary[status!] ?? "unknown".localized())
                    }
                    
                }
                else {
                    if(errorCode != nil && (errorCode?.contains("SCER84")) == true){
                        self?.showAlert(title: "smsSentNeedToWait".localized(), message:"smsSentNeedToWaitBody".localized())
                    }else{
                        self?.showAlert(title: "phoneErrorTitle".localized(), message:API.shared.errorDictionary[status!] ?? "unknown".localized())
                    }
                }
            }
            else {
                self?.showAlert(title: "networkErrorTitle".localized(), message: "networkErrorMessage".localized())
            }
        }
    }
    
    
}

extension MainViewController {
    func showPhoneAlert(title: String, message: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "cancel".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let actionTurnOn: UIAlertAction = UIAlertAction.init(title: "contactSupport".localized(), style: .default) { (UIAlertAction) in
            let email = "support@scooterson.com"
            if let url = URL(string: "mailto:\(email)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        alert.addAction(action)
        alert.addAction(actionTurnOn)
        self.present(alert, animated: true, completion: nil)
    }
}

extension MainViewController: FPNTextFieldDelegate {
    func fpnDisplayCountryList() {
        
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
        DispatchQueue.main.async {
            self.phoneField.becomeFirstResponder()
            self.phoneField.text = ""
            self.validatedPhone = ""
        }
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            self.validatedPhone = String((textField.getFormattedPhoneNumber(format: .E164)?.dropFirst())!)
        }else{
            self.validatedPhone = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string.count > 1 && string.hasPrefix("+")) {
            self.phoneField.set(phoneNumber: string)
            return false
        }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            self.goButton.alpha = updatedText == "" ? 0.0 : 1.0
            if string.count > 1 && updatedText.hasPrefix("+") {
                return true
            }
        }
        
        return string.rangeOfCharacter(from: CharacterSet(charactersIn: "+_-*#")) == nil
    }
    
    private func showAlertForChangingPhone() {
        let alert: UIAlertController = UIAlertController(title: "phoneErrorTitle".localized(), message:"phoneErrorMessage".localized(), preferredStyle: .alert)
        let remindLater: UIAlertAction = UIAlertAction.init(title: "cancel".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let action: UIAlertAction = UIAlertAction.init(title: "update".localized(), style: .default) { (UIAlertAction) in
            guard let phoneToSend = self.validatedPhone else {
                return
            }
            let selectedCountry = self.phoneField.selectedCountry?.name
            self.signUp(phoneToSend, selectedCountry, isModifyingPhone: true)
            
        }
        alert.addAction(remindLater)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIView {
    func setBottomBorder() {
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.charcoalGrey.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
