//
//  ArcaProgressVC.swift
//  SampeArcaProgressBar
//
//  Created by Anton Umnitsyn on 5/28/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit

class ArcaProgressView: UIView {
    
    public var threeArcs:Bool = true
    public var progressBackgoundColor = UIColor.lightGray
    public var speedProgressForegroundColor = UIColor.red
    public var powerProgressForegroundColor = UIColor.blue
    public var batteryProgressForegroundColor = UIColor.green
    public var fontForTitleLabels = UIFont.boldSystemFont(ofSize: 15.0)
    public var lastBatteryState: String = ""
    public var lastPowerState: String = ""
    public var showSpeedOnLabel: Bool!
    public var showPowerOnLabel: Bool = false
    
    public var lineWidth:CGFloat = 30 {
        didSet{
            foregroundLayerSpeed.lineWidth = lineWidth
            backgroundLayerSpeed.lineWidth = lineWidth
            foregroundLayerPower.lineWidth = lineWidth
            backgroundLayerPower.lineWidth = lineWidth
            foregroundLayerBat.lineWidth = lineWidth
            backgroundLayerBat.lineWidth = lineWidth
        }
    }
    
    public var duration: Double = 0.5
    
    func isDarkMode() -> Bool {
        return UserDefaults.standard.bool(forKey: "darkMode")
    }
    
    public func toggleBetweenLabelAndImageView(showLock: Bool){
        if(showLock){
            lockImageView.isHidden = false
            mainLabel.isHidden = true
            mainDescriptionLabel.isHidden = true
            UIView.animate(withDuration: 1.0,
                    delay: 0,
                  options: [.repeat, .autoreverse],
                  animations: { self.lockImageView.alpha = 0.3 }
            )
        }else{
            lockImageView.isHidden = true
            mainLabel.isHidden = false
            mainDescriptionLabel.isHidden = false
            
        }
    }
    
    public func updateTapOntheMainLable(){
        if showSpeedOnLabel {
            mainLabel.theme_textColor = [UIColor.pastelOrange.toHexString(), UIColor.pastelOrange.toHexString()]
            mainDescriptionLabel.theme_textColor = [UIColor.pastelOrange.toHexString(), UIColor.pastelOrange.toHexString()]
            self.mainLabel.text = "0"
            self.mainDescriptionLabel.text = isImperial() ? "mph".localized() : "kph".localized()
        }else if showPowerOnLabel{
            mainLabel.theme_textColor = [UIColor.dodgerBlue.toHexString(), UIColor.dodgerBlue.toHexString()]
            mainDescriptionLabel.theme_textColor = [UIColor.dodgerBlue.toHexString(), UIColor.dodgerBlue.toHexString()]
            self.mainLabel.text = "0"
            self.mainDescriptionLabel.text  = "W"
        }else{
            mainLabel.theme_textColor = [UIColor.greenblue.toHexString(), UIColor.greenblue.toHexString()]
            mainDescriptionLabel.theme_textColor = [UIColor.greenblue.toHexString(), UIColor.greenblue.toHexString()]
            self.mainLabel.text = self.lastBatteryState
            self.mainDescriptionLabel.text = "%"
        }
        self.configLabel()
    }
    
    public func updateBatteryOnMainLable(){
        if !showPowerOnLabel && !showSpeedOnLabel{
            self.mainLabel.text = self.lastBatteryState
            self.configLabel()
        }
    }
    
    
    public var labelSize: CGFloat = 20.0 {
        didSet {
            mainLabel.font = .bold(size: labelSize)
            mainLabel.theme_textColor = [UIColor.pastelOrange.toHexString(), UIColor.pastelOrange.toHexString()]
            
            mainDescriptionLabel.font = .regular(size: 14)
            mainDescriptionLabel.theme_textColor = [UIColor.pastelOrange.toHexString(), UIColor.pastelOrange.toHexString()]
            configLabel()
        }
    }
    
    public func setProgressSpeed(to progressConstant: Double, withAnimation: Bool, maxSpeed: Double) {
        var progress: Double {
            get {
                if progressConstant > 1 { return 1 }
                else if progressConstant < 0 { return 0 }
                else { return progressConstant }
            }
        }
        foregroundLayerSpeed.strokeEnd = CGFloat(progress == 0 ? 0.01 : progress)
        
        if showSpeedOnLabel {
            let unitsStr = isImperial() ? "mph".localized() : "kph".localized()
            let speed = Double((maxSpeed/100) * progress*100)
            if(isImperial()){
                self.mainLabel.text = "\(toMphFromKmph(speed: speed))"
            }else{
                self.mainLabel.text = "\(Int(speed))"
            }
            self.mainDescriptionLabel.text = unitsStr
        }
        else if showPowerOnLabel {
            self.mainLabel.text = self.lastPowerState
            self.mainDescriptionLabel.text = "W"
        }else{
            self.mainLabel.text = self.lastBatteryState
            self.mainDescriptionLabel.text = "%"
        }
        self.configLabel()
        
        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = 0
            animation.toValue = progress
            animation.duration = duration
            foregroundLayerSpeed.add(animation, forKey: "foregroundAnimation")
        }
        
        var currentTime:Double = 0
        let timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { (timer) in
            if currentTime >= self.duration{
                timer.invalidate()
            } else {
                currentTime += 0.05
                self.setForegroundLayerColorForSpeed()
            }
        }
        timer.fire()
    }
    
    public func setProgressPower(to progressConstant: Double, withAnimation: Bool) {
        var progress: Double {
            get {
                if progressConstant > 1 { return 1 }
                else if progressConstant < 0 { return 0 }
                else { return progressConstant }
            }
        }
        foregroundLayerPower.strokeEnd = CGFloat(progress == 0 ? 0.01 : progress)
        
        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = 0
            animation.toValue = progress
            animation.duration = duration
            foregroundLayerPower.add(animation, forKey: "foregroundAnimation")
        }
        
        var currentTime:Double = 0
        let timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { (timer) in
            if currentTime >= self.duration{
                timer.invalidate()
            } else {
                currentTime += 0.05
                self.setForegroundLayerColorForPower()
            }
        }
        timer.fire()
    }
    
    public func setProgressBattary(to progressConstant: Double, withAnimation: Bool) {
        var progress: Double {
            get {
                if progressConstant > 1 { return 1 }
                else if progressConstant < 0 { return 0 }
                else { return progressConstant }
            }
        }
        foregroundLayerBat.strokeEnd = CGFloat(progress == 0 ? 0.01 : progress)
        
        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = 0
            animation.toValue = progress
            animation.duration = self.duration
            foregroundLayerBat.add(animation, forKey: "foregroundAnimation")
        }
        
        var currentTime:Double = 0
        let timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { (timer) in
            if currentTime >= self.duration{
                timer.invalidate()
            } else {
                currentTime += 0.05
                self.setForegroundLayerColorForBattery()
            }
        }
        timer.fire()
    }
    
    
    public var lockImageView = UIImageView()
    public var mainLabel = UILabel()
    public var mainDescriptionLabel = UILabel()
    public var speedLabelDescription = UILabel()
    //MARK: Private
    private let foregroundLayerSpeed = CAShapeLayer()
    private let backgroundLayerSpeed = CAShapeLayer()
    
    private let foregroundLayerPower = CAShapeLayer()
    private let backgroundLayerPower = CAShapeLayer()
    
    private let foregroundLayerBat = CAShapeLayer()
    private let backgroundLayerBat = CAShapeLayer()
    
    private var radius: CGFloat {
        get{
            return (min(self.frame.width, self.frame.height) - lineWidth)/2
        }
    }
    
    private var pathCenter: CGPoint{ get{ return self.convert(self.center, from:self.superview) } }
    private func makeBar(){
        self.layer.sublayers = nil
        drawBackgroundLayerSpeed()
        drawForegroundLayerSpeed()
        drawBackgroundLayerPower()
        drawForegroundLayerPower()
        if threeArcs {
            drawBackgroundLayerBat()
            drawForegroundLayerBat()
        }
    }
    
    
    // MARK: Speed bar
    private let speedStartAngle = CGFloat.pi*2.6/3
    private let speedEndAngle = CGFloat.pi*4.8/3
    
    private func drawBackgroundLayerSpeed(){
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: speedStartAngle , endAngle: speedEndAngle, clockwise: true)
        self.backgroundLayerSpeed.path = path.cgPath
        self.backgroundLayerSpeed.strokeColor = progressBackgoundColor.cgColor
        self.backgroundLayerSpeed.lineWidth = lineWidth
        self.backgroundLayerSpeed.lineCap = CAShapeLayerLineCap.round
        self.backgroundLayerSpeed.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(backgroundLayerSpeed)
    }
    
    private func drawForegroundLayerSpeed(){
        let startAngle = speedStartAngle
        let endAngle = speedEndAngle
        
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        foregroundLayerSpeed.lineCap = CAShapeLayerLineCap.round
        foregroundLayerSpeed.path = path.cgPath
        foregroundLayerSpeed.lineWidth = lineWidth
        foregroundLayerSpeed.fillColor = UIColor.clear.cgColor
        foregroundLayerSpeed.strokeColor = speedProgressForegroundColor.cgColor
        foregroundLayerSpeed.strokeEnd = 0.01
        
        self.layer.addSublayer(foregroundLayerSpeed)
        
    }
    
    private func makeLabel(withText text: String) -> UILabel {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        label.text = text
        label.font = UIFont.systemFont(ofSize: labelSize)
        label.sizeToFit()
        label.center = pathCenter
        return label
    }
    
    private func makeSpeedTitleLabel(withText text: String) -> UILabel {
        var rect:CGRect!
        if threeArcs {
            rect = CGRect(x: lineWidth + 15, y: self.frame.height - lineWidth - 95, width: 50, height: 40)
        }
        else {
            rect = CGRect(x: lineWidth + 5, y: self.frame.height - lineWidth - 55, width: 50, height: 40)
        }
        let label = UILabel(frame: rect)
        label.text = text
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = fontForTitleLabels
        label.textColor = speedProgressForegroundColor
        return label
    }
    
    private func configLabel(){
        mainLabel.sizeToFit()
        mainLabel.center = pathCenter
        mainDescriptionLabel.sizeToFit()
        mainDescriptionLabel.center = CGPoint(x: pathCenter.x, y: pathCenter.y + mainLabel.frame.height/2)
    }
    
    private func configImageView(){
        let image = UIImage(named: "AuthenticateLock")?.withRenderingMode(.alwaysTemplate)
        lockImageView = UIImageView(image: image)
        lockImageView.isHidden = true
        lockImageView.center = pathCenter
    }
    
    private func setForegroundLayerColorForSpeed(){
        self.foregroundLayerSpeed.strokeColor = speedProgressForegroundColor.cgColor
    }
    
    // MARK: Power bar
    private let powerStartAngle = CGFloat.pi*0.4/3
    private let powerEndAngle = CGFloat.pi*5.1/3
    
    private func drawBackgroundLayerPower(){
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: powerStartAngle, endAngle: powerEndAngle, clockwise: false)
        self.backgroundLayerPower.path = path.cgPath
        self.backgroundLayerPower.strokeColor = progressBackgoundColor.cgColor
        self.backgroundLayerPower.lineWidth = lineWidth
        self.backgroundLayerPower.lineCap = CAShapeLayerLineCap.round
        self.backgroundLayerPower.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(backgroundLayerPower)
        
    }
    
    private func drawForegroundLayerPower(){
        let startAngle = powerStartAngle
        let endAngle = powerEndAngle
        
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
        
        foregroundLayerPower.lineCap = CAShapeLayerLineCap.round
        foregroundLayerPower.path = path.cgPath
        foregroundLayerPower.lineWidth = lineWidth
        foregroundLayerPower.fillColor = UIColor.clear.cgColor
        foregroundLayerPower.strokeColor = powerProgressForegroundColor.cgColor
        foregroundLayerPower.strokeEnd = 0.01
        
        self.layer.addSublayer(foregroundLayerPower)
        
    }
    
    private func makePowerTitleLabel(withText text: String) -> UILabel {
        var rect: CGRect!
        if threeArcs {
            rect = CGRect(x: self.frame.width - 95, y: self.frame.height - lineWidth - 95, width: 50, height: 40)
        }
        else {
            rect = CGRect(x: self.frame.width - 85, y: self.frame.height - lineWidth - 55, width: 50, height: 40)
        }
        let label = UILabel(frame: rect)
        label.text = text
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = fontForTitleLabels
        label.textColor = powerProgressForegroundColor
        return label
    }
    
    private func setForegroundLayerColorForPower(){
        self.foregroundLayerPower.strokeColor = powerProgressForegroundColor.cgColor
    }
    
    // MARK: Battery bar
    private let batteryStartAngle = CGFloat.pi*2.3/3
    private let batteryEndAngle = CGFloat.pi*0.7/3
    
    private func drawBackgroundLayerBat(){
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: batteryStartAngle, endAngle: batteryEndAngle, clockwise: false)
        self.backgroundLayerBat.path = path.cgPath
        self.backgroundLayerBat.strokeColor = progressBackgoundColor.cgColor
        self.backgroundLayerBat.lineWidth = lineWidth
        self.backgroundLayerBat.lineCap = CAShapeLayerLineCap.round
        self.backgroundLayerBat.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(backgroundLayerBat)
        
    }
    
    private func drawForegroundLayerBat(){
        let startAngle = batteryStartAngle
        let endAngle = batteryEndAngle
        
        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
        
        foregroundLayerBat.lineCap = CAShapeLayerLineCap.round
        foregroundLayerBat.path = path.cgPath
        foregroundLayerBat.lineWidth = lineWidth
        foregroundLayerBat.fillColor = UIColor.clear.cgColor
        foregroundLayerBat.strokeColor = batteryProgressForegroundColor.cgColor
        foregroundLayerBat.strokeEnd = 0.01
        
        self.layer.addSublayer(foregroundLayerBat)
        
    }
    
    private func makeBatTitleLabel(withText text: String) -> UILabel {
        let label = UILabel(frame: CGRect(x: self.frame.width/2 - 35, y: self.frame.height - lineWidth - 30, width: 70, height: 20))
        label.text = text
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = fontForTitleLabels
        label.textColor = batteryProgressForegroundColor
        return label
    }
    
    private func setForegroundLayerColorForBattery(){
        self.foregroundLayerBat.strokeColor = batteryProgressForegroundColor.cgColor
    }
    
    private func setupView() {
        makeBar()
        self.configImageView()
        self.addSubview(lockImageView)
        self.addSubview(mainLabel)
        self.addSubview(mainDescriptionLabel)
        self.addSubview(makeSpeedTitleLabel(withText: "speed".localized()))
        if threeArcs {
            self.addSubview(makePowerTitleLabel(withText: "power".localized()))
            self.addSubview(makeBatTitleLabel(withText: "battery".localized()))
        }
        else {
            self.addSubview(makePowerTitleLabel(withText: "battery".localized()))
        }
        
    }
    
    //Layout Sublayers
    private var layoutDone = false
    override func layoutSublayers(of layer: CALayer) {
        if !layoutDone {
            let tempText = mainLabel.text
            let temp2text = mainDescriptionLabel.text
            setupView()
            mainLabel.text = tempText
            mainDescriptionLabel.text = temp2text
            layoutDone = true
        }
    }
}
