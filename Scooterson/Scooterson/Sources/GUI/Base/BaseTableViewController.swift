//
//  BaseTableViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 6/13/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import SwiftTheme

class BaseTableViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    public var mainTable: UITableView!
    public var titleString: String!
    public var cells:[[SCTableViewCell]] = [[]]
    private var segments: [UISegmentedControl] = []
    private var segmentFontcolors: [UIColor] = [.white, .charcoalGrey]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        setupCells()
        self.setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(themeSwitched), name: NSNotification.Name(rawValue: "ThemeUpdateNotification"), object: nil)
    }
    
    @objc private func themeSwitched() {
        for s in segments {
            s.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.regular(size: 16), NSAttributedString.Key.foregroundColor:segmentFontcolors[ThemeManager.currentThemeIndex]], for: .normal)
        }
    }
    
    private func setUpViews() {
        let headerView = viewForTableHeader()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        
        mainTable = UITableView()
        mainTable.separatorInset = .zero
        mainTable.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        mainTable.theme_separatorColor = [UIColor.paleGrey.toHexString(), UIColor.charcoalGrey.toHexString()]
        self.view.addSubview(mainTable)
        mainTable.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view.safeAreaLayoutGuide)
            make.top.equalTo((headerView?.snp.bottom)!)
        }
        
        mainTable.tableHeaderView = UIView()//
        mainTable.tableFooterView = UIView()
        
        mainTable.delegate = self
        mainTable.dataSource = self
        
        mainTable.layoutIfNeeded()
    }
    
    func setupCells() {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func viewForTableHeader() -> UIView? {
        let titleView = UIView()
        
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 30)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = titleString.localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    
    func setupDescriptionCell(withTitle title: String, withDescription: Bool) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.cellHeight = 90.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.barbiePink.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView.snp.centerX).offset(-10.0)
            if withDescription {
                make.top.equalTo(cell.contentView).offset(10.0)
            }
            else {
                make.centerY.equalTo(cell.contentView)
            }
            make.height.equalTo(30.0)
        }
        
        cell.valueLabel = UILabel()
        cell.valueLabel.font = .regular(size: 20)
        cell.valueLabel.textColor = .dodgerBlue
        cell.valueLabel.textAlignment = .left
        cell.contentView.addSubview(cell.valueLabel)
        cell.valueLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView.snp.centerX).offset(10)
            make.right.equalTo(cell.contentView).offset(-20)
            if withDescription {
                make.top.equalTo(cell.contentView).offset(10.0)
            }
            else {
                make.centerY.equalTo(cell.contentView)
            }
            make.height.equalTo(30.0)
        }
        
        cell.descriptionLabel = UILabel()
        cell.descriptionLabel.font = .regular(size: 14)
        cell.descriptionLabel.textAlignment = .left
        cell.descriptionLabel.numberOfLines = 2
        cell.descriptionLabel.lineBreakMode = .byWordWrapping
        cell.descriptionLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        cell.contentView.addSubview(cell.descriptionLabel)
        cell.descriptionLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.bottom.equalTo(cell.contentView.snp.bottom).offset(-10.0)
            make.height.equalTo(40.0)
        }
        
        return cell
    }
    
    func setupButtonCell(withTitle title:String, buttonTitle: String, handler: @escaping ()->()) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.cellHeight = 70.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-60.0)
            make.centerY.equalTo(cell.contentView)
            make.height.equalTo(30.0)
        }
        let button = UIButton()
        button.backgroundColor = .dodgerBlue
        button.setTitleColor(.white, for: .normal)
        button.setTitle(buttonTitle, for: .normal)
        button.layer.cornerRadius = 20.0
        cell.contentView.addSubview(button)
        button.addAction(for: .touchUpInside, handler)
        button.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.right.equalTo(cell.contentView).offset(-20)
            make.left.equalTo(cell.contentView.snp.centerX).offset(10)
            make.height.equalTo(40)
        }
        button.addAction(for: .valueChanged, handler)
        return cell
    }
    
    func setupSwitchCellWithSubTitle(withTitle title:String, subtitle: String ,initValue: Bool, handler: @escaping ()->()) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.cellHeight = 80.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(25.0)
        }
        let subTitleLabel = UILabel()
        subTitleLabel.font = .regular(size: 14.0)
        subTitleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        subTitleLabel.textAlignment = .left
        subTitleLabel.text = subtitle
        cell.contentView.addSubview(subTitleLabel)
        subTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-60.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(10.0)
            make.height.equalTo(15.0)
        }
        cell.switchControl  =  UISwitch()
        let switcher = cell.switchControl
        switcher!.onTintColor = .dodgerBlue
        switcher!.theme_tintColor = [UIColor.dodgerBlue.toHexString(), UIColor.charcoalGrey.toHexString()]
        switcher!.isOn = initValue
        cell.contentView.addSubview(switcher!)
        switcher!.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.right.equalTo(cell.contentView).offset(-20)
        }
        switcher!.addAction(for: .valueChanged, handler)
        return cell
    }
    
    func setupSwitchCell(withTitle title:String, initValue: Bool, handler: @escaping ()->()) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.cellHeight = 70.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-60.0)
            make.centerY.equalTo(cell.contentView)
            make.height.equalTo(30.0)
        }
        let switcher = UISwitch()
        cell.switchControl = switcher
        cell.switchControl.onTintColor = .dodgerBlue
        cell.switchControl.theme_tintColor = [UIColor.dodgerBlue.toHexString(), UIColor.charcoalGrey.toHexString()]
        cell.switchControl.isOn = initValue
        cell.contentView.addSubview(cell.switchControl)
        cell.switchControl.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.right.equalTo(cell.contentView).offset(-20)
        }
        cell.switchControl.addAction(for: .valueChanged, handler)
        return cell
    }
    
    func setupSegmentCell(withTitle title:String, values: [String], initValue: Int, handler: @escaping ()->()) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.cellHeight = 70.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView.snp.centerX).offset(-10.0)
            make.centerY.equalTo(cell.contentView)
            make.height.equalTo(30.0)
        }
        let segment = UISegmentedControl(items: values)
        segment.theme_selectedSegmentTintColor = [UIColor.dodgerBlue.toHexString(), UIColor.dodgerBlue.toHexString()]
        segment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.regular(size: 16), NSAttributedString.Key.foregroundColor:segmentFontcolors[ThemeManager.currentThemeIndex]], for: .normal)
        segment.selectedSegmentIndex = initValue
        cell.contentView.addSubview(segment)
        segment.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.left.equalTo(cell.contentView.snp.centerX).offset(10)
            make.right.equalTo(cell.contentView).offset(-20)
        }
        segment.addAction(for: .valueChanged, handler)
        segments.append(segment)
        return cell
    }
    
    func setupPickerCellWithSubTitle(withTitle title:String, subTitle: String) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.expandable = true
        cell.selectionStyle = .blue
        cell.cellHeight = 80.0
        cell.cellHeightExpanded = 170.0
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView.snp.centerX).offset(-10.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(25.0)
        }
        
        cell.valueLabel = UILabel()
        cell.valueLabel.font = .regular(size: 20)
        cell.valueLabel.textColor = .dodgerBlue
        cell.valueLabel.textAlignment = .right
        cell.contentView.addSubview(cell.valueLabel)
        cell.valueLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView.snp.centerX).offset(64.00)
               make.right.equalTo(cell.contentView).offset(-20.0)
               make.top.equalTo(cell.contentView).offset(25.0)
               make.height.equalTo(25.0)
           }
           
           let subTitleLabel = UILabel()
           subTitleLabel.font = .regular(size: 14.0)
           subTitleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
           subTitleLabel.textAlignment = .left
           subTitleLabel.text = subTitle
           cell.contentView.addSubview(subTitleLabel)
           subTitleLabel.snp.makeConstraints { (make) in
               make.left.equalTo(cell.contentView).offset(20.0)
               make.right.equalTo(cell.contentView).offset(-60.0)
               make.top.equalTo(titleLabel.snp.bottom).offset(10.0)
               make.height.equalTo(15.0)
           }
           
           
           cell.picker = UIPickerView()
           cell.picker.delegate = self
           cell.picker.dataSource = self

           cell.expandedView = cell.picker
           cell.contentView.addSubview(cell.expandedView)
           cell.picker.snp.makeConstraints { (make) in
               make.top.equalTo(subTitleLabel.snp.bottom)
               make.left.equalTo(cell.contentView)
               make.right.equalTo(cell.contentView)
               make.height.equalTo(0)
           }
           return cell
       }
    
    
    func setupPickerCell(withTitle title:String) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.expandable = true
        cell.selectionStyle = .blue
        cell.cellHeight = 70.0
        cell.cellHeightExpanded = 150.0
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView.snp.centerX).offset(-10.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(30.0)
        }
        
        cell.valueLabel = UILabel()
        cell.valueLabel.font = .regular(size: 20)
        cell.valueLabel.textColor = .dodgerBlue
        cell.valueLabel.textAlignment = .left
        cell.contentView.addSubview(cell.valueLabel)
        cell.valueLabel.snp.makeConstraints { (make) in
            make.right.equalTo(cell.contentView).offset(-20)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(30.0)
        }
        
        cell.picker = UIPickerView()
        cell.picker.delegate = self
        cell.picker.dataSource = self
        //        DispatchQueue.main.async {
        //            cell.picker.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        //            cell.picker.theme_tintColor = [UIColor.paleGrey.toHexString(), UIColor.charcoalGrey.toHexString()]
        //        }
        
        cell.expandedView = cell.picker
        cell.contentView.addSubview(cell.expandedView)
        cell.picker.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.equalTo(cell.contentView)
            make.right.equalTo(cell.contentView)
            make.height.equalTo(0)
        }
        return cell
    }
    
    func setupOneValuedCell(withTtile title:String, andText text:String) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.cellHeight = 70.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(20.0)
        }
        
        let textLabel = UILabel()
        textLabel.font = .regular(size: 14.0)
        textLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        textLabel.textAlignment = .left
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.text = text
        textLabel.numberOfLines = 0
        cell.contentView.addSubview(textLabel)
        textLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(5.0)
            //make.bottom.equalTo(cell.contentView).offset(-20.0)
        }
        textLabel.sizeToFit()
        return cell
    }
    
    func setupProfileValueCell(withTtile title:String, andText text:String) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.cellHeight = 100.0
        cell.selectionStyle = .none
        cell.isTapable = true
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.pastelOrange.toHexString(), UIColor.pastelOrange.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(20.0)
        }
        
        cell.infomationLabel = UILabel()
        cell.infomationLabel.font = .regular(size: 20.0)
        cell.infomationLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        cell.infomationLabel.textAlignment = .left
        cell.infomationLabel.lineBreakMode = .byWordWrapping
        cell.infomationLabel.text = text
        cell.infomationLabel.numberOfLines = 0
        cell.contentView.addSubview(cell.infomationLabel)
        cell.infomationLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(15.0)
            //make.bottom.equalTo(cell.contentView).offset(-20.0)
        }
        cell.infomationLabel.sizeToFit()
        return cell
    }
    
    func setupJustHeaderCell(withTitle title:String, tag: Int) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.cellHeight = 70.0
        cell.tag = tag
        cell.isTapable = true
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(20.0)
        }
        
        return cell
    }
    
    func setupDefaultSettingsCell(withTitle title:String, subTitle: String, tag: Int) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.titleLabel = titleLabel
        cell.cellHeight = 80.0
        cell.tag = tag
        cell.isTapable = true
        cell.selectionStyle = .none
        
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(25.0)
        }
        
        let subTitleLabel = UILabel()
        cell.infomationLabel = subTitleLabel
        subTitleLabel.font = .regular(size: 14.0)
        subTitleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        subTitleLabel.textAlignment = .left
        subTitleLabel.text = subTitle
        cell.contentView.addSubview(subTitleLabel)
        cell.contentView.addSubview(subTitleLabel)
        subTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-60.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(10.0)
            make.height.equalTo(15.0)
        }
        
        cell.cellImageView  =  UIImageView()
        let arrowNextImageView = cell.cellImageView
        arrowNextImageView?.image = UIImage(named: "ArrowNext")
        cell.contentView.addSubview(arrowNextImageView!)
        arrowNextImageView!.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.right.equalTo(cell.contentView).offset(-20)
        }
        
        
        return cell
    }
    
    
    func setupDescriptionCellTypeBig(withTitle title: String, withDescription: Bool) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.cellHeight = 120.0
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.textColor = UIColor.pastelOrange
        titleLabel.textAlignment = .left
        titleLabel.text = title
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(30.0)
        }
        
        cell.descriptionLabel = UILabel()
        cell.descriptionLabel.font = .regular(size: 14)
        cell.descriptionLabel.textAlignment = .left
        cell.descriptionLabel.numberOfLines = 2
        cell.descriptionLabel.lineBreakMode = .byWordWrapping
        cell.descriptionLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        cell.contentView.addSubview(cell.descriptionLabel)
        cell.descriptionLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.bottom.equalTo(cell.contentView.snp.bottom).offset(-10.0)
            make.height.equalTo(40.0)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = cells[indexPath.section][indexPath.row]
        if cell.expandable {
            return cell.expanded ? cell.cellHeightExpanded : cell.cellHeight
        }
        return cell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cells[indexPath.section][indexPath.row]
        return cell
    }
    
    // MARK: Picker view methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {}
}
