//
//  BaseViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 3/12/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import SwiftTheme
import LocalAuthentication
import ZendeskCoreSDK
import SupportProvidersSDK
import FirebaseRemoteConfig


protocol AuthenticationDelgate {
    func success()
    func failed()
}

extension Date {
    func currentTimeMillis() -> Double {
        return self.timeIntervalSince1970
    }
}

class BaseViewController: UIViewController {
    
    private var loader: LoaderView!
    private var mqtt = API.shared.mqtt
    private var infoPopUpIsPresent: Bool!
    private var authicationDelgate: AuthenticationDelgate!
    private var remoteConfig: RemoteConfig!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame!.origin.y
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomConstraint?.constant = 20.0
            } else {
                self.bottomConstraint?.constant = endFrame?.size.height ?? 20.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    /**
     Function tell is the device is small screen
     */
    public func isSmallScreen() ->Bool{
        let smallScreen = 300 > self.view.frame.height/2 - 40
        return smallScreen
    }
    
    func showAlertForPermissionNotFound(title: String, message: String, actionButtoTitle: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "cancel".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let actionTurnOn: UIAlertAction = UIAlertAction.init(title: actionButtoTitle, style: .default) { (UIAlertAction) in
            let url = URL(string: UIApplication.openSettingsURLString)
            let app = UIApplication.shared
            app.open(url!, options: [:], completionHandler: nil)
        }
        alert.addAction(action)
        alert.addAction(actionTurnOn)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func createSupportTicket(subject: String, message: String, tags: [String]){
        let userData = SCUser.reStore()
        if !Reachability.isConnectedToInternet{
            self.showAlert(title: "noNetwork".localized(), message: "noNetworkBodyForContactSupport".localized())
            return
        }else if userData?.email == nil || userData?.email == ""{
            showAlert(title: "emailMissingSupportRequestTitle".localized(), message: "emailMissingSupportRequestBody".localized())
            return
        }
        self.loaderShow(withDelegate: nil)
        let identity =  Identity.createAnonymous(name: userData?.firstName, email: userData?.email)
        Zendesk.instance?.setIdentity(identity)
        let requestProvider = ZDKRequestProvider()
        let request = ZDKCreateRequest()
        request.subject = subject
        request.requestDescription = message
        request.tags = tags
        requestProvider.createRequest(request) { (result, error) in
            DispatchQueue.main.async {
                self.loaderHide()
                if result != nil {
                    print(result ?? "unknown response")
                    self.showAlert(title: "", message: "We have received the request, our support team will contact you shortly")
                }
                else if error != nil {
                    print(error!.localizedDescription)
                }
            }
            
        }
    }
    
    
    
    func isDarkMode() -> Bool {
        return UserDefaults.standard.bool(forKey: "darkMode")
    }
    
    func darkModeIos13SetValue() -> Int{
        let valueSet = UserDefaults.standard.integer(forKey: ContantsKey.darkModeIos13.rawValue)
        
        return valueSet
        
        
    }
    
    func updateDarkMode(valueSet: Int){
        switch valueSet {
        case ConstantValues.DARKMODE_AUTOMATIC:
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    ThemeManager.setTheme(index: 0)
                    print("Theme is dark")
                } else {
                    ThemeManager.setTheme(index: 1)
                    print("Theme is light")
                }
            }
        case ConstantValues.DARKMODE_ON:
            ThemeManager.setTheme(index: 0)
        default:
            ThemeManager.setTheme(index: 1)
            
        }
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.theme_setStatusBarStyle([.lightContent, .darkContent], animated: true)
        }else{
            UIApplication.shared.theme_setStatusBarStyle([.lightContent, .default], animated: true)
        }
        UserDefaults.standard.set(valueSet, forKey: ContantsKey.darkModeIos13.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    
    func switchDarkMode() {
        let isDark = !UserDefaults.standard.bool(forKey: "darkMode")
        ThemeManager.setTheme(index: isDark ? 0 : 1)
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.theme_setStatusBarStyle([.lightContent, .darkContent], animated: true)
        }else{
            UIApplication.shared.theme_setStatusBarStyle([.lightContent, .default], animated: true)
        }
        
        UserDefaults.standard.set(isDark, forKey: "darkMode")
        UserDefaults.standard.synchronize()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoPopUpIsPresent = false
        self.view.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        //        if !Reachability.isConnectedToInternet {
        //            showNoNetworkStatus()
        //        }
        NotificationCenter.default.addObserver(self, selector: #selector(showModeSuggestion(_:)), name: NSNotification.Name(rawValue: "MODE_SUGGESTION"), object: nil)
    }
    
    @objc func showModeSuggestion(_ notification: NSNotification) {
        guard let title: String = notification.userInfo?["title"] as? String else { return }
        guard let message: String = notification.userInfo?["message"] as? String else { return }
        self.showAlert(title: title, message: message)
    }
    
    @objc private func showNoNetworkStatus() {
        if !infoPopUpIsPresent {
            let infoView = setupInfoView(title: "noNetwork".localized())
            infoView.tag = 7171
            infoPopUpIsPresent = true
            infoView.snp.makeConstraints({ (update) in
                update.top.equalTo(self.view.snp.bottom).offset(-40)
            })
            UIView.animate(withDuration: 0.3) {
                self.view.layoutSubviews()
            }
        }
    }
    
    @objc private func showWeakNetworkStatus() {
        if !infoPopUpIsPresent {
            let infoView = setupInfoView(title: "weakNetwork".localized())
            infoView.tag = 7171
            infoPopUpIsPresent = true
            infoView.snp.makeConstraints({ (update) in
                update.top.equalTo(self.view.snp.bottom).offset(-40)
            })
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutSubviews()
            }) { (success) in
                Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { (timer) in
                    self.hideNetworkStatus()
                })
            }
        }
    }
    
    @objc private func hideNetworkStatus() {
        if infoPopUpIsPresent {
            let infoView = self.view.viewWithTag(7171)
            infoView!.snp.updateConstraints({ (update) in
                update.top.equalTo(self.view.snp.bottom)
            })
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [], animations: {
                self.view.layoutIfNeeded()
            }) { (success) in
                infoView?.removeFromSuperview()
                self.infoPopUpIsPresent = false
            }
        }
    }
    
    private func setupInfoView(title: String) -> UIView {
        let infoView = UIView()
        infoView.backgroundColor = .barbiePink
        self.view.addSubview(infoView)
        infoView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.height.equalTo(40.0)
            make.top.equalTo(self.view.snp.bottom)
        }
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = .bold(size: 26)
        infoView.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.edges.equalTo(infoView)
        }
        label.text = title
        view.layoutSubviews()
        return infoView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPushData(_:)), name: NSNotification.Name(rawValue: "newPush"), object: [AnyHashable : Any].self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showNoNetworkStatus), name: NSNotification.Name(rawValue: "NETWORK_IS_DOWN"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showWeakNetworkStatus), name: NSNotification.Name(rawValue: "NETWORK_IS_WEAK"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideNetworkStatus), name: NSNotification.Name(rawValue: "NETWORK_IS_UP"), object: nil)
        initializeRemoteConfig()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newPush"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NETWORK_IS_DOWN"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NETWORK_IS_WEAK"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NETWORK_IS_UP"), object: nil)
    }
    
    @objc private func showPushData(_ notification: Notification) {
        print("From BaseVC: \(notification)")
        //        showAlert(title: "New Push", message: "\(notification.userInfo?["aps"]?["alert"] ?? [:])")
    }
    
    // MARK: Utilits
    public func showAlert(title: String, message: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "okay".localized(), style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)        
        self.present(alert, animated: true, completion: nil)        
    }
    
    public func loaderShow(withDelegate delegate: LoaderViewDelegate?) {
        let windowCount = UIApplication.shared.windows.count
        loader = LoaderView(frame: UIApplication.shared.windows[windowCount-1].bounds, spinnerColor: .pastelOrange, isSmallScreen: isSmallScreen() ,withDelegate: delegate)
        //LoaderView(frame: UIApplication.shared.windows[windowCount-1].bounds, title: "Sending ...", font: UIFont.bold(size: 24), titleColor: .charcoalGrey, spinnerColor: .barbiePink)
        UIApplication.shared.windows[windowCount-1].addSubview(loader)
        
    }
    
    public func showloaderOnSecondWindow(withDelegate delegate: LoaderViewDelegate?, withTitle title: String) {
        let windowCount = UIApplication.shared.windows.count
        loader = LoaderView(frame: UIApplication.shared.windows[windowCount-1].bounds, spinnerColor: .pastelOrange, isSmallScreen: isSmallScreen() ,withDelegate: delegate)
        
        UIApplication.shared.windows[windowCount-2].addSubview(loader)
        
    }
    
    public func loaderHide () {
        if loader != nil {
            loader.removeFromSuperview()
        }
    }
    
    public func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func sendToMQTT(message: String) {
        if mqtt?.connState == .connected {
            mqtt?.publish("v1/devices/me/telemetry", withString: message, qos: .qos1)
        }
    }
    
    //Mark: Added Remote Config from Firebase
    private func initializeRemoteConfig(){
        remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        #if DEBUG
        settings.minimumFetchInterval = 0
        #endif
        remoteConfig.configSettings = settings
    }
    
    public func checkForAppVersion(){
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                print("App version Config fetched!")
                self.remoteConfig.activate() { (changed, error) in
                    let latestVersion = self.remoteConfig["app_version"].stringValue
                    print("App version changed value \(String(describing: latestVersion))")
                    if latestVersion != appVersion{
                        DispatchQueue.main.async {
                            //todo
                            self.showAlertForAppUpdate()
                        }
                        
                    }
                }
            } else {
                print("App version Config not fetched")
                print("App version Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
    }
    
    
    public func checkForOTAUpdate(completionHandler: @escaping (_ displayVersion: String, _ displayFileUrl: String, _ processorVersion: String, _ processotFileURL: String) -> Void){
        
        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                print("OTA update version Config fetched!")
                self.remoteConfig.activate() { (changed, error) in
                    let displayVersion = self.remoteConfig["display_version"].dataValue
                    let displayOBJ = try? JSONSerialization.jsonObject(with: displayVersion, options: []) as? [String: Any]
                    let processorVersion = self.remoteConfig["processor_version"].dataValue
                    let processorOBJ = try? JSONSerialization.jsonObject(with: processorVersion, options: []) as? [String: Any]
                    print("OTA update display version info \(String(describing: displayOBJ)) \(String(describing: processorOBJ))")
                    completionHandler(displayOBJ!["version"] as! String, displayOBJ?["filePath"] as! String,processorOBJ?["version"] as! String,processorOBJ?["filePath"] as! String)
                    
                }
            } else {
                print("App version Config not fetched")
                print("App version Error: \(error?.localizedDescription ?? "No error available.")")
                completionHandler("NA","NA","NA","NA")
            }
        }
    }
    
    
    
    
    private func showAlertForAppUpdate() {
        let alert: UIAlertController = UIAlertController(title: "appUpdateTitle".localized(), message: "appUpdateMessage".localized(), preferredStyle: .alert)
        let remindLater: UIAlertAction = UIAlertAction.init(title: "later".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let action: UIAlertAction = UIAlertAction.init(title: "appUpdateButton".localized(), style: .default) { (UIAlertAction) in
            //todo fix the URL
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id..."),
                UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url, options: [:]) { (opened) in
                    if(opened){
                        print("App Store Opened")
                    }
                }
            }
        }
        alert.addAction(remindLater)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showToast(title: String, message: String){
        let alertDisapperTimeInSeconds = 10.0
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        self.present(alert, animated: true) {
            // Enabling Interaction for Transperent Full Screen Overlay
            alert.view.superview?.subviews.first?.isUserInteractionEnabled = true
            // Adding Tap Gesture to Overlay
            alert.view.superview?.subviews.first?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
            alert.dismiss(animated: true)
        }
    }
    @objc private func alertControllerBackgroundTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
}

class ClosureSleeve {
    let closure: ()->()
    
    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }
    
    @objc func invoke () {
        closure()
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControl.Event, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}

extension String {
    func validateEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}

extension UIFont {
    class func black(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-black", size: size)!
    }
    
    class func regular(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-regular", size: size)!
    }
    
    class func bold(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-bold", size: size)!
    }
    
    class func semiBold(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-semibold", size: size)!
    }
    
    class func heavy(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-heavy", size: size)!
    }
    
    class func light(size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-light", size: size)!
    }
}

func isNotification() -> Bool {
    return UserDefaults.standard.bool(forKey: ContantsKey.settingsNotification.rawValue) && UserDefaults.standard.bool(forKey: ContantsKey.settingsNotificationPermission.rawValue)
}
func isImperial() -> Bool {
    return UserDefaults.standard.bool(forKey: "imperial")
}

func setNotification(){
    let isSet = !UserDefaults.standard.bool(forKey: ContantsKey.settingsNotification.rawValue)
    UserDefaults.standard.set(isSet, forKey: ContantsKey.settingsNotification.rawValue)
}
func setImperial(){
    let isSet = !UserDefaults.standard.bool(forKey: "imperial")
    UserDefaults.standard.set(isSet, forKey: "imperial")
}
/**
 Get Speed in MPH
 */
func toMphFromKmph(speed: Double)-> Int{
    return Int(0.6214 * speed)
}
func toMileFromKm(distance: Double)-> Double{
    return (0.6214 * distance)
}

/**
 Key Feature Authenitcation check.
 */
func authenticationWithBiometricOrPhonePin(authicationDelgate: AuthenticationDelgate) {
    let localAuthenticationContext = LAContext()
    localAuthenticationContext.localizedFallbackTitle = "autheticationFallBack".localized()
    
    var authorizationError: NSError?
    let reason = "autheticationRequest".localized()
    
    
    
    if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
        
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
            if success {
                authicationDelgate.success()
                
            } else {
                // Failed to authenticate
                guard let error = evaluateError else {
                    return
                    
                }
                showLAError(laError: error)
                authicationDelgate.failed()
                print(error)
                
            }
        }
    } else {
        guard let error = authorizationError else {
            return
        }
        authicationDelgate.failed()
        print(error)
    }
}
// function to detect an error type
func showLAError(laError: Error) -> Void {
    
    var message = ""
    
    switch laError {
        
    case LAError.appCancel:
        message = "Authentication was cancelled by application"
        
    case LAError.authenticationFailed:
        message = "The user failed to provide valid credentials"
        
    case LAError.invalidContext:
        message = "The context is invalid"
        
    case LAError.passcodeNotSet:
        message = "Passcode is not set on the device"
        
    case LAError.systemCancel:
        message = "Authentication was cancelled by the system"
        
    case LAError.userCancel:
        message = "The user did cancel"
        
    case LAError.userFallback:
        message = "The user chose to use the fallback"
        
    case LAError.biometryNotAvailable:
        message = "Biometry is not available"
        
    case LAError.biometryNotEnrolled:
        message = "Authentication could not start, because biometry has no enrolled identities"
        
    case LAError.biometryLockout:
        message = "Biometry is locked. Use passcode."
        
    default:
        message = "Did not find error code on LAError object"
        
    }
    
    //return message
    print("Key feature LAError message - \(message)")
    
}

public func getStringForDateWithDay(time: Double) -> String{
    let date = Date(timeIntervalSince1970: time)
    let calendar = Calendar.current
    let dateFormatterPrint = DateFormatter()
    dateFormatterPrint.dateFormat = "MMM dd,yyyy"
    var timeString = ""
    if(calendar.isDateInToday(date)){
        timeString = "today".localized()
    }else{
        timeString = dateFormatterPrint.string(from: date)
    }
    dateFormatterPrint.dateFormat = "h:mm a"
    timeString.append(" • ")
    timeString.append(dateFormatterPrint.string(from: date))
    
    return timeString
}

