//
//  Constants.swift
//  Scooterson
//
//  Created by deepansh jain on 6/12/20.
//  Copyright © 2020 Anton Umnitsyn. All rights reserved.
//

import Foundation

enum ContantsKey: String {
    case keyOnBoardingStageAddName
    case activaatedDevicesCount
    case prouctRolleyCounter
    case settingsNotification
    case settingsNotificationPermission
    case darkModeIos13
    case tbPhoneDeviceId
    case pushNotificationToken
    case autorized
}

enum ConstantInt: Int {
    case maxFirstNameChar = 10
}

struct ConstantValues {
    static let PRODUCT_TYPE_ROLLEY = 1
    static let PRODUCT_TYPE_ELF = 2
    static let PRODUCT_TYPE_ROLLEY_PLUS = 3
    
    //Dark mode setting for ios 13
    static let DARKMODE_AUTOMATIC = 0
    static let DARKMODE_OFF = 2
    static let DARKMODE_ON = 1
    
}

