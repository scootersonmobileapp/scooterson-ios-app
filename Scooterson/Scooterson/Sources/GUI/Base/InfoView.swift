//
//  InfoView.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 7/10/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import Foundation

class InfoView: UIView {
    
    private var labels:[String]?
    private var mainColor:UIColor?
    private var image:UIImage?
    private var onImage:UIImage?
    
    public var isOn:Bool = false    
    public var button: UIButton!
    public var topLabel: UILabel!
    public var bottomLabel: UILabel!
    public var mainLabel:UILabel!
    public var images:[String]?
    public var isWalkAssitOn: Bool = false
    private var timer : Timer?
    
    init(image:UIImage, onImage: UIImage?, labelsOnOff:[String], and color:UIColor, isOn: Bool) {
        self.image = image
        self.onImage = onImage
        self.labels = labelsOnOff
        self.mainColor = color
        self.isOn = isOn
        super.init(frame: CGRect(x: 0, y: 0, width: 104, height: 104))
        setupView()
    }
    
    init(labels:[String], and color:UIColor) {
        self.labels = labels
        self.mainColor = color
        super.init(frame: CGRect(x: 0, y: 0, width: 104, height: 104))
        setupView()
    }
    
    init (labels:[String], imageOnOFF: [String], and color:UIColor){
        self.labels = labels
        self.mainColor = color
        self.images = imageOnOFF
        super.init(frame: CGRect(x: 0, y: 0, width: 104, height: 104))
        setupView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setLabels(top: String?, main: String?, bottom: String?) {
        if top != nil {
            topLabel.text = top
        }
        if main != nil {
            mainLabel.text = main
        }
        if bottom != nil {
            bottomLabel.text = bottom
        }
    }
    
    public func setState(stateOn:Bool) {
        let bottomLabel = self.viewWithTag(101) as! UILabel
        let imageView = self.viewWithTag(100) as! UIImageView
        isOn = stateOn
        if !stateOn {
            self.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
            bottomLabel.textColor = mainColor
            bottomLabel.text = labels![1]
            imageView.image = image
        }
        else {
            self.theme_backgroundColor = [UIColor.dodgerBlue.toHexString(), UIColor.dodgerBlue.toHexString()]
            bottomLabel.textColor = .white
            bottomLabel.text = labels![0]
            let imageOn = onImage != nil ? onImage : imageView.image?.withRenderingMode(.alwaysTemplate)
            imageView.image = imageOn
            imageView.tintColor = .white
        }
    }
    
    public func setStateForLevel(isWalkingAssitanceSet: Bool, walkingAssistanceOn: Bool){
        let bottomLabel = self.viewWithTag(203) as! UILabel
        let topLabel = self.viewWithTag(201) as! UILabel
        let mainLabel = self.viewWithTag(202) as! UILabel
        let imageView = self.viewWithTag(200) as! UIImageView
        isWalkAssitOn = walkingAssistanceOn
        if !isWalkAssitOn {
            stopTimer()
        }else{
            startTimer(imageView: imageView)
        }
        
        if isWalkingAssitanceSet{
            bottomLabel.text = labels![3]
            topLabel.isHidden = false
            mainLabel.isHidden = true
            imageView.isHidden = false
            if(!isWalkAssitOn){
                imageView.theme_image = [images![0],images![1]]
            }
        }else{
            
            bottomLabel.text = labels![3]
            topLabel.isHidden = true
            mainLabel.isHidden = false
            imageView.isHidden = true
            
        }
        
    }
    
    func startTimer (imageView: UIImageView) {
        guard timer == nil else { return }
        timer =  Timer.scheduledTimer(withTimeInterval: 0.8, repeats: true) { [weak self] (timer) in self?.updateAssist(imageView: imageView)}
    }
    
    
    private func updateAssist(imageView: UIImageView){
        print("WalkingAssistanceOn status \(isWalkAssitOn)")
        if isWalkAssitOn{
            imageView.theme_image = [self.images![2],self.images![3]]
            self.delay(0.4) {
                imageView.theme_image = [self.images![0],self.images![1]]
            }
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    //    public func reverse () {
    //        let bottomLabel = self.viewWithTag(101) as! UILabel
    //        let imageView = self.viewWithTag(100) as! UIImageView
    //        if isOn {
    //            self.backgroundColor = .paleGrey
    //            bottomLabel.textColor = mainColor
    //            imageView.image = image
    //
    //        }
    //        else {
    //            self.backgroundColor = mainColor
    //            bottomLabel.textColor = .white
    //            let imageOn = imageView.image?.withRenderingMode(.alwaysTemplate)
    //            imageView.image = imageOn
    //            imageView.tintColor = .white
    //        }
    //        isOn = !isOn
    //    }
    
    private func setupView() {
        
        layer.cornerRadius = 20
        
        clipsToBounds = true
        
        if labels?.count == 3 {
            topLabel = UILabel()
            topLabel.text = labels![0]
            topLabel.font = UIFont.regular(size:14)
            topLabel.theme_textColor = [UIColor.white.toHexString(), mainColor!.toHexString()]
            topLabel.textAlignment = .center
            self.addSubview(topLabel)
            let isLabelPresent = labels![0].count >= 0
            topLabel.snp.makeConstraints { (make) in
                make.height.equalTo(isLabelPresent ? 16 : 0)
                make.top.equalTo(self).offset(10)
                make.left.equalTo(self).offset(20)
                make.right.equalTo(self).offset(-20)
            }
            
            mainLabel = UILabel()
            mainLabel.text = labels![1]
            mainLabel.font = UIFont.bold(size:44)
            mainLabel.theme_textColor = [UIColor.white.toHexString(), mainColor!.toHexString()]
            mainLabel.textAlignment = .center
            self.addSubview(mainLabel)
            mainLabel.snp.makeConstraints { (make) in
                make.height.equalTo(44)
                make.centerY.equalTo(self).offset(isLabelPresent ? 0 : -16)
                make.left.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-10)
            }
            
            bottomLabel = UILabel()
            bottomLabel.text = labels![2]
            bottomLabel.font = UIFont.regular(size:14)
            bottomLabel.theme_textColor = [UIColor.white.toHexString(), mainColor!.toHexString()]
            bottomLabel.textAlignment = .center
            self.addSubview(bottomLabel)
            bottomLabel.snp.makeConstraints { (make) in
                make.height.equalTo(16)
                make.bottom.equalTo(self).offset(-10)
                make.left.equalTo(self).offset(20)
                make.right.equalTo(self).offset(-20)
            }
        }
        else if labels?.count == 4{
            topLabel = UILabel()
            topLabel.text = labels![0]
            topLabel.isHidden = true
            topLabel.tag = 201
            topLabel.font = UIFont.regular(size:14)
            topLabel.theme_textColor = [UIColor.white.toHexString(), mainColor!.toHexString()]
            topLabel.textAlignment = .center
            self.addSubview(topLabel)
            let isLabelPresent = labels![0].count >= 0
            topLabel.snp.makeConstraints { (make) in
                make.height.equalTo(isLabelPresent ? 16 : 0)
                make.top.equalTo(self).offset(10)
                make.left.equalTo(self).offset(20)
                make.right.equalTo(self).offset(-20)
            }
            
            mainLabel = UILabel()
            mainLabel.text = labels![1]
            mainLabel.tag = 202
            mainLabel.font = UIFont.bold(size:44)
            mainLabel.theme_textColor = [UIColor.white.toHexString(), mainColor!.toHexString()]
            mainLabel.textAlignment = .center
            self.addSubview(mainLabel)
            mainLabel.snp.makeConstraints { (make) in
                make.height.equalTo(44)
                make.centerY.equalTo(self).offset(isLabelPresent ? 0 : -16)
                make.left.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-10)
            }
            
            let imageView = UIImageView()
            self.addSubview(imageView)
            imageView.isHidden = true
            imageView.contentMode = .scaleAspectFit
            imageView.tag = 200
            imageView.snp.makeConstraints { (make) in
                make.height.equalTo(40.0)
                make.centerY.equalTo(self).offset(isLabelPresent ? 0 : -16)
                make.left.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-10)
            }
            
            
            bottomLabel = UILabel()
            bottomLabel.text = labels![3]
            bottomLabel.tag = 203
            bottomLabel.font = UIFont.regular(size:14)
            bottomLabel.theme_textColor = [UIColor.white.toHexString(), mainColor!.toHexString()]
            bottomLabel.textAlignment = .center
            self.addSubview(bottomLabel)
            bottomLabel.snp.makeConstraints { (make) in
                make.height.equalTo(16)
                make.bottom.equalTo(self).offset(-10)
                make.left.equalTo(self).offset(20)
                make.right.equalTo(self).offset(-20)
            }
            
            
            
        }else {
            let imageView = UIImageView(image: image)
            self.addSubview(imageView)
            imageView.contentMode = .scaleAspectFit
            imageView.tag = 100
            imageView.snp.makeConstraints { (make) in
                make.width.height.equalTo(40.0)
                make.top.equalTo(self).offset(18)
                make.centerX.equalTo(self)
            }
            
            bottomLabel = UILabel()
            bottomLabel.text = labels![0]
            bottomLabel.font = UIFont.regular(size:24)
            bottomLabel.textColor = mainColor
            bottomLabel.textAlignment = .center
            bottomLabel.tag = 101
            self.addSubview(bottomLabel)
            bottomLabel.snp.makeConstraints { (make) in
                make.height.equalTo(28)
                make.bottom.equalTo(self).offset(-10)
                make.left.equalTo(self).offset(20)
                make.right.equalTo(self).offset(-20)
            }
        }
        button = UIButton(type: .custom)
        button.backgroundColor = .clear
        self.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
}
