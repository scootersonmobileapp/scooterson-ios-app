//
//  SCTabBarController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/31/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit

class SCTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    private var name:String!
    public var device:SCDevice?
    public var peripheral: CBPeripheral?
    private var type: SCDeviceType!
    private var dashboardVC: SCBaseDashboardViewController!
    private var deviceListController: DeviceSelectionViewController!
    public static var isDeviceAuthenticate = true
    public var devicesPlaceHolderVC: PlaceHolderViewController!
    private var isAppDisconnected = false
    private var settingsVC: RolleySettingsViewContoller!
    
    init(with name:String, for device: SCDevice?, peripheral: CBPeripheral?, deviceType: SCDeviceType, deviceListController: DeviceSelectionViewController?) {
        self.type = deviceType
        self.device = device
        self.name = name
        self.deviceListController = deviceListController
        self.peripheral = peripheral
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        self.tabBar.theme_barTintColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]        
        
        devicesPlaceHolderVC =  PlaceHolderViewController()//DeviceSelectionViewController()
        devicesPlaceHolderVC.tabBarItem = UITabBarItem(title: "Devices", image: UIImage(named: "Devices"), selectedImage: UIImage(named: "devices".localized())?.maskWithColor(color: UIColor.blue))
        devicesPlaceHolderVC.tabBarItem.tag = 0
        
        var insightVc: UIViewController!
        
        switch type! {
        case .SCDeviceTypeRolley:
            insightVc = RolleyInsightsViewController()
            
        case .SCDeviceTypeELF:
            insightVc = InsightsViewController()
        default:
            return
        }
        
        let analitycsNavVC = UINavigationController(rootViewController: insightVc)
        insightVc.navigationController?.navigationBar.theme_barTintColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        insightVc.navigationController?.navigationBar.shadowImage = UIImage()
        let imgAnalitycs = UIImage(named: "Analitycs")?.withRenderingMode(.alwaysTemplate)
        analitycsNavVC.tabBarItem = UITabBarItem(title: "insights".localized(), image: imgAnalitycs, selectedImage: imgAnalitycs?.maskWithColor(color: UIColor.blue))
        analitycsNavVC.tabBarItem.tag = 1
        
        
        switch type! {
        case .SCDeviceTypeRolley:
            dashboardVC = DashBoardViewController()
            dashboardVC.device = self.device
            dashboardVC.deviceName = self.name
            dashboardVC.tabBarItem = UITabBarItem(title: "dashboard".localized(), image: UIImage(named: "Dashboard")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage(named: "Dashboard"))
            dashboardVC.tabBarItem.tag = 2
        case .SCDeviceTypeELF:
            dashboardVC = DashBoardViewControllerELF()
            dashboardVC.device = self.device
            dashboardVC.deviceName = self.name
            dashboardVC.tabBarItem = UITabBarItem(title: "dashboard".localized(), image: UIImage(named: "Dashboard")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage(named: "Dashboard"))
            dashboardVC.tabBarItem.tag = 2
        default:
            return
        }
        
        
        settingsVC = RolleySettingsViewContoller()
        let settingsNavVC = UINavigationController(rootViewController: settingsVC)
        settingsVC.navigationController?.navigationBar.theme_barTintColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        settingsVC.navigationController?.navigationBar.shadowImage = UIImage()
        let imgSettings = UIImage(named: "Settings")?.withRenderingMode(.alwaysTemplate)
        settingsNavVC.tabBarItem = UITabBarItem(title: "settings".localized(), image: imgSettings, selectedImage: imgSettings?.maskWithColor(color: UIColor.blue))
        settingsNavVC.tabBarItem.tag = 3
        
        let profileVC = ProfileViewController()
        let profileNavVC = UINavigationController(rootViewController: profileVC)
        profileVC.navigationController?.navigationBar.theme_barTintColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        profileVC.navigationController?.navigationBar.shadowImage = UIImage()
        let imgProfile = UIImage(named: "Profile")?.withRenderingMode(.alwaysTemplate)
        profileNavVC.tabBarItem = UITabBarItem(title: "profile".localized(), image: imgProfile, selectedImage: imgProfile?.maskWithColor(color: UIColor.blue))
        profileNavVC.tabBarItem.tag = 4
        
        self.setViewControllers([devicesPlaceHolderVC, analitycsNavVC, dashboardVC, settingsNavVC, profileNavVC], animated: false)
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.selectedIndex = 2
    }
    
    func moveToDeviceTab(){
        isAppDisconnected = true
        _ = tabBarController(self, shouldSelect: devicesPlaceHolderVC)
    }
    
    func forceUpdateTabAction(){
        settingsVC.isForceUpdate = true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.isKind(of: PlaceHolderViewController.self) {
            //Mark: Trip satus
            dashboardVC.markTripStatus(isTripStarted: false)
            
            switch type! {
            case .SCDeviceTypeRolley:
                PLBleService.getInstance()?.disconnect()
            default:
                if let peripheralToDisconnect = peripheral {
                    BTElfService.shared.manager.cancelPeripheralConnection(peripheralToDisconnect)
                }
            }
            
            
            if deviceListController != nil{
                viewController.dismiss(animated: true, completion: nil)
                deviceListController.viewWillAppear(true)
                if isAppDisconnected{
                    deviceListController.appDisconneted(deviceType: device!.type)
                }
            }else{
                //
                viewController.dismiss(animated: true, completion: {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = UINavigationController(rootViewController: DeviceSelectionViewController())
                    
                })
            }
        }
        
        if SCTabBarController.isDeviceAuthenticate{
            if viewController.tabBarItem.tag == 1 {
                if self.device!.isOwner{
                    return true
                }else{
                    self.showAlert(title: "", message: "insightNotAvailableInShare".localized())
                    return false
                }
            }else{
                return true
            }
        }else{
            return false
        }
        
    }
    private func showAlert(title: String , message: String){
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "okay".localized(), style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}
