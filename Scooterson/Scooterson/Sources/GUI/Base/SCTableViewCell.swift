//
//  SCTableViewCell.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 6/4/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit

class SCTableViewCell: UITableViewCell {
    
    public var cellHeight: CGFloat!
    public var titleString: String!
    public var valueLabel: UILabel!
    public var valueString: String! {
        didSet {
            valueLabel.text = self.valueString
        }
    }
    public var descriptionLabel: UILabel!
    public var descriptionString: String! {
        didSet {
            descriptionLabel.text = self.descriptionString
        }
    }
    
    public var textField: UITextField!
    public var segmetControl: UISegmentedControl!
    public var switchControl: UISwitch!
    public var expandedView: UIView!
    public var picker: UIPickerView!
    public var cellHeightExpanded: CGFloat!
    public var expanded: Bool!
    public var expandable: Bool!
    public var isTapable = false
    public var infomationLabel: UILabel!
    public var titleLabel : UILabel!
    public var cellImageView: UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        expanded = false
        expandable = false
        super .init(style: style, reuseIdentifier: reuseIdentifier)
        self.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        self.theme_tintColor = [UIColor.orange.toHexString(), UIColor.orange.toHexString()]
        contentView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if (self.isTapable){
            if highlighted {
                contentView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
            }else{
                contentView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
            }
        }
    }
}
