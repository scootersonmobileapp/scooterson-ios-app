//
//  AlertPickerView.swift
//  Scooterson
//
//  Created by deepansh jain on 6/20/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import SwiftTheme

public enum UIButtonBorderSide {
    case Top, Bottom, Left, Right
}
extension UIButton {
    
    public func addBorder(side: UIButtonBorderSide, color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        switch side {
        case .Top:
            border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: width)
        case .Bottom:
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        case .Left:
            border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        case .Right:
            border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        }
        
        self.layer.addSublayer(border)
    }
}


class AlertPickerView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var numberPicker: UIPickerView!
    @IBOutlet weak var okayButton: UIButton!
    
    var delegate: AlertPickerViewDelegate?
    private var pickerData: [String]!
    private var titleString = ""
    private var messageBodyString = ""
    private var extraInfo: String!
    private let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
        cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
        cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1)
        okayButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
        numberPicker.delegate = self
        numberPicker.dataSource = self
        
    }
    
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        alertView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        titleLabel.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        messageLabel.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        messageLabel.font = .light(size: 14)
        messageLabel.numberOfLines = 2
        titleLabel.font = .bold(size: 18)
        
        titleLabel.text = titleString
        messageLabel.text = messageBodyString
        
    }
    
    public func setData(data: [String], title: String, message: String, extraInfo: String){
        self.pickerData = data
        self.titleString = title
        self.messageBodyString = message
        self.extraInfo = extraInfo
    }
    
    
    @IBAction func onTapCancelButton(_ sender: Any) {
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapOkButton(_ sender: Any) {
        delegate?.okButtonTapped(selectedItemName: extraInfo,selectedValue: pickerData[numberPicker.selectedRow(inComponent: 0)])
        print("selected value: \(pickerData[numberPicker.selectedRow(inComponent: 0)])")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = .regular(size: 18)
        label.text =  pickerData[row]
        label.textAlignment = .center
        return label
    }
    
}






