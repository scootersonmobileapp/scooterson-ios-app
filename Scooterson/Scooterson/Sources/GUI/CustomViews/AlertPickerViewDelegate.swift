//
//  AlertPickerViewDelegate.swift
//  Scooterson
//
//  Created by deepansh jain on 6/20/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

protocol AlertPickerViewDelegate: class {
    func okButtonTapped(selectedItemName: String,selectedValue: String)
    func cancelButtonTapped()
}

