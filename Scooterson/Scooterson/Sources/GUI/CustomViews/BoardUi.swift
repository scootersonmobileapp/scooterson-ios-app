//
//  DeviceConnectBoard.swift
//  Scooterson
//
//  Created by deepansh jain on 7/17/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import Foundation
import BLTNBoard
import SwiftTheme

protocol BroadUIDelegate {
    func cancelButtonTapped()
    func otherActionButtonTapped ()
}
extension BroadUIDelegate {
    func otherActionButtonTapped() {}
}

protocol BroadUIOTADelegate {
    func cancelButtonTapped()
    func isVisible(fileURL: String)
}

enum BoardUi {
    
    
    public static func getDeviceManger(item: BLTNItem) -> BLTNItemManager{
        let deviceScanManager  = BLTNItemManager(rootItem: item)
        deviceScanManager.backgroundViewStyle = .dimmed
        if ThemeManager.currentThemeIndex == 0  { //DarkMode
            deviceScanManager.backgroundColor = .charcoalGrey
        }else{
            deviceScanManager.backgroundColor = .white
        }
        deviceScanManager.cardCornerRadius = 20
        deviceScanManager.statusBarAppearance = .automatic
        return deviceScanManager
    }
    
    
    static func showLocationIntro(name: String,deviceType: Int, delegate: BroadUIDelegate)-> FeedbackPageBLTNItem{
        let page = FeedbackPageBLTNItem(title: "locationSharedTitle".localized())
        let messageBody = String(format: NSLocalizedString("locationSharedMesssages", comment: ""), arguments: [name])
        page.image = UIImage(named: "location_permission_small")
        page.isDismissable = false
        page.next = showDeviceConnectIntro(deviceType: deviceType, delegate: delegate)
        page.descriptionText = messageBody
        page.appearance = getAppearance()
        page.actionButtonTitle = "okay".localized()
        page.alternativeButtonTitle = "cancel".localized()
        page.actionHandler = { item in
            item.manager?.displayNextItem()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let deviceScanPage = page.next as! FeedbackPageBLTNItem
                deviceScanPage.titleLabel.blink()
                delegate.otherActionButtonTapped()
            }
        }
        page.alternativeHandler = { item in
            item.manager?.dismissBulletin(animated: true)
        }
        
        return page
    }
    
    
    static func showDeviceConnectIntro(deviceType: Int, delegate: BroadUIDelegate) -> FeedbackPageBLTNItem {
        var page: FeedbackPageBLTNItem!
        switch (deviceType){
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            page = FeedbackPageBLTNItem(title: "Rolley+")
            page.image = UIImage(named: "rolley_display_3")
        case ConstantValues.PRODUCT_TYPE_ROLLEY:
            page = FeedbackPageBLTNItem(title: "Rolley")
            page.image = UIImage(named: "rolley_display_3")
        default :
            page = FeedbackPageBLTNItem(title: "ELF")
            page.image = UIImage(named: "ELRE")
        }
        
        page.appearance = getAppearance()
        
        page.descriptionText = "turnOnMessage".localized()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            page.titleLabel.blink()
        }
        
        page.isDismissable = false
        page.actionButtonTitle = "cancel".localized()
        page.actionHandler = { item in
            delegate.cancelButtonTapped()
        }
        page.next = showDeviceAlreadyAdded(deviceType: deviceType)
        
        return page
    }
    
    
    static func showDeviceAlreadyAdded(deviceType: Int) -> FeedbackPageBLTNItem {
        let page = FeedbackPageBLTNItem(title: "rolleyAlreadyActivated".localized())
        page.appearance = getAppearance()
        
        page.descriptionText = "alreadyActivatedBody".localized()
        page.isDismissable = true
        
        return page
    }
    
    static func showDeviceNotFound(deviceType: Int) -> FeedbackPageBLTNItem{
        let page = FeedbackPageBLTNItem(title: "deviceScanCompletedTitle".localized())
        page.appearance = getAppearance()
        
        switch deviceType {
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            page.descriptionText = "deviceScanCompletedRolleyPlusBody".localized()
        case ConstantValues.PRODUCT_TYPE_ROLLEY:
            page.descriptionText = "deviceScanCompletedRolleyBody".localized()
        default:
            page.descriptionText = "deviceScanCompletedELF".localized()
        }
        
        page.isDismissable = true
        
        return page
    }
    
    static func showNotYourDevice(deviceType: Int) -> FeedbackPageBLTNItem {
        var page: FeedbackPageBLTNItem!
        switch (deviceType){
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            page = FeedbackPageBLTNItem(title: "addDeviceErrorRolleyPlus".localized())
        default:
            page = FeedbackPageBLTNItem(title: "addDeviceErrorRolley".localized())
        }
        page.descriptionText = "deviceAlreadyRegistered".localized()
        page.image = UIImage(named: "RLOR")
        page.appearance = getAppearance()
        page.isDismissable = true
        
        return page
    }
    
    
    static func showCannotConnectWithDevice(deviceType: Int, colorType: String) -> FeedbackPageBLTNItem {
        var page: FeedbackPageBLTNItem!
        switch (deviceType){
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            page = FeedbackPageBLTNItem(title: "addDeviceErrorRolleyPlus".localized())
        default:
            page = FeedbackPageBLTNItem(title: "addDeviceErrorRolley".localized())
        }
        page.descriptionText = "canNotFindThisRolley".localized()
        page.image = UIImage(named:  colorType)
        page.appearance = getAppearance()
        page.isDismissable = true
        
        return page
    }
    
    static func showWrongDeviceActivate(deviceType: Int) -> FeedbackPageBLTNItem {
        var page: FeedbackPageBLTNItem!
        page = FeedbackPageBLTNItem(title: "wrongDeviceTitle".localized())
        switch (deviceType){
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            let messageBody = String(format: NSLocalizedString("wrongDeviceMessage", comment: ""), arguments: ["Rolley+"])
            page.descriptionText = messageBody
        default:
            let messageBody = String(format: NSLocalizedString("wrongDeviceMessage", comment: ""), arguments: ["Rolley"])
            page.descriptionText = messageBody
        }
        
        page.appearance = getAppearance()
        page.isDismissable = true
        
        return page
    }
    
    
    
    
    static func showOTAUpdateBoard(delegate: BroadUIOTADelegate, fileURL: String) -> OTAUpdateBoardPage{
        let page = OTAUpdateBoardPage(title: "Updating...")
        page.isDismissable = false
        page.warningUILabel.text = "otpUpdateWarning".localized()
        page.descriptionText = "Download started..."
        page.appearance = getAppearance()
        page.actionHandler = { item in
            item.manager?.dismissBulletin()
            delegate.cancelButtonTapped()
        }
        
        page.presentationHandler = { item in
            delegate.isVisible(fileURL: fileURL)
        }
        
        return page
        
    }
    
    static func showScanBroadPage(deviceType: Int, delegate: BroadUIDelegate) -> ConnectToDeviceBroadPage{
        
        var page: ConnectToDeviceBroadPage!
        switch (deviceType){
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            page = ConnectToDeviceBroadPage(title: "Rolley+")
        case ConstantValues.PRODUCT_TYPE_ROLLEY:
            page = ConnectToDeviceBroadPage(title: "Rolley")
        default :
            page = ConnectToDeviceBroadPage(title: "ELF")
        }
        
        page.isDismissable = false
        
        page.descriptionText = "Scanning..."
        page.appearance = getAppearance()
        
        page.next = showDeviceConnectIntro(deviceType: deviceType, delegate: delegate)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            page.manager?.displayNextItem()
            let deviceConnectPage = page.next as! FeedbackPageBLTNItem
            deviceConnectPage.titleLabel.blink()
        }
        
        return page
        
    }
    
    
    private static func getAppearance() -> BLTNItemAppearance{
        let appearance = BLTNItemAppearance()
        if ThemeManager.currentThemeIndex == 1 {
            appearance.titleTextColor = UIColor.black
            appearance.descriptionTextColor = UIColor.black
            
        }else{
            appearance.titleTextColor = UIColor.white
            appearance.descriptionTextColor = UIColor.white
        }
        appearance.titleFontSize = 30
        appearance.descriptionFontSize = 18
        appearance.titleFontDescriptor = UIFontDescriptor(name: "SFProDisplay-regular", matrix: .identity)
        appearance.descriptionFontDescriptor = UIFontDescriptor(name: "SFProDisplay-light", matrix: .identity)
        
        return appearance
    }
}
extension BLTNTitleLabelContainer {
    func blink() {
        self.alpha = 0.0;
        UIView.animate(withDuration: 1, //Time duration you want,
            delay: 0.0,
            options: [.curveEaseInOut, .autoreverse, .repeat],
            animations: { [weak self] in self?.alpha = 1.0 },
            completion: { [weak self] _ in self?.alpha = 0.5 })
    }
}
extension UIView {
    
    func blink(duration: TimeInterval = 1, delay: TimeInterval = 0.0, alpha: CGFloat = 0.0) {
        UIView.animate(withDuration: duration, delay: delay, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {
            self.alpha = alpha
        })
    }
    
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}


