//
//  ConnectToDeviceBroadPage.swift
//  Scooterson
//
//  Created by deepansh jain on 8/14/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import BLTNBoard

class ConnectToDeviceBroadPage : BLTNPageItem{
    
    lazy var spinnerView = UIActivityIndicatorView(style: .whiteLarge)
    
    override func makeViewsUnderTitle(with interfaceBuilder: BLTNInterfaceBuilder) -> [UIView]? {
        spinnerView.color = .pastelOrange
        spinnerView.startAnimating()
        return [spinnerView]
    }
}

