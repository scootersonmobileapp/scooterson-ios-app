//
//  OTAUpdateBoardPage.swift
//  Scooterson
//
//  Created by deepansh jain on 7/23/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import BLTNBoard

class OTAUpdateBoardPage : BLTNPageItem{
    
    lazy var spinnerView = UIActivityIndicatorView(style: .whiteLarge)
    lazy var warningUILabel = UILabel()
    
    override func makeViewsUnderTitle(with interfaceBuilder: BLTNInterfaceBuilder) -> [UIView]? {
        spinnerView.color = .pastelOrange
        spinnerView.startAnimating()
        return [spinnerView]
    }
    
    override func makeViewsUnderDescription(with interfaceBuilder: BLTNInterfaceBuilder) -> [UIView]? {
        warningUILabel.textColor = .red
        warningUILabel.numberOfLines = 2
        warningUILabel.font = .light(size: 18)
        return [warningUILabel]
    }
    
}
