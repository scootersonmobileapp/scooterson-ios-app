//
//  DashBoardViewController.swift
//  Scooterson
//
//  Created by Deepansh on 6/1/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import CocoaMQTT
import CoreLocation


class DashBoardViewController: SCBaseDashboardViewController, RolleyBleDelegate, AuthenticationDelgate, CLLocationManagerDelegate {
    
    private var peripheral: CBPeripheral!
    private var isHeadLight: Bool = false
    private var pasLevel: Int32 = 0
    
    @IBOutlet var titleImageView: UIImageView!
    @IBOutlet var progressBars: ArcaProgressView!
    @IBOutlet var tripView: InfoView!
    @IBOutlet var levelView: InfoView!
    @IBOutlet var rangeView: InfoView!
    var lightView: InfoView!
    private var demoAnimationTimer: Timer!
    
    
    private var rolleyService: RolleyBleService!
    var maxSpeed = Double(45.0)
    var lastVoltage = 0
    var lastCurrent = 0
    var lastPower = 0.0
    var lastBatteryState: String = "NA"
    var gaugeTapCount = 0
    var pinState = 0
    
    //Constants
    let MAX_RANGE_LIMIT = 120
    let SHOW_SPEED = 0
    let SHOW_BATTERY = 1
    let SHOW_POWER = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
        
        self.device = tabBarController.device
        self.peripheral = tabBarController.peripheral
        setup_views()
        self.rolleyService = RolleyBleService()
        activateMQTTSC()
        progressBars.setProgressSpeed(to: Double(0.0), withAnimation: false, maxSpeed: maxSpeed)
        progressBars.setProgressPower(to: Double(0.0), withAnimation: false)
        progressBars.setProgressBattary(to: Double(0.0), withAnimation: false)
        
        //Location
        if device.type == ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS{
            locationManager = CLLocationManager()
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if lastBatteryState != "NA"{
            device.deviceParams?.lastBattery = lastBatteryState
            device.update()
        }
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        #if targetEnvironment(simulator)
        if !demoAnimationTimer.isValid {
            demoAnimationTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [weak self] (timer) in
                self?.progressBars.setProgressSpeed(to: Double.random(in: 0 ... 1), withAnimation: false, maxSpeed: self!.maxSpeed)
                self?.progressBars.setProgressPower(to: Double.random(in: 0 ... 1), withAnimation: false)
                self?.progressBars.setProgressBattary(to: Double.random(in: 0 ... 1), withAnimation: false)
            }
        }
        #endif
        tripView.bottomLabel.text = isImperial() ? "m_short".localized() : "km_short".localized()
        rangeView.bottomLabel.text = isImperial() ? "m_short".localized() : "km_short".localized()
        actionOnGauge()
        if locationManager != nil{
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
        }
        
    }
    
    // MARK: Location delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        sendTelemetryDouble(key: MQTTVariable.latitude.rawValue, value: (manager.location?.coordinate.latitude)!)
        sendTelemetryDouble(key: MQTTVariable.longitude.rawValue, value: (manager.location?.coordinate.longitude)!)
        sendTelemetry(key: MQTTVariable.isSlave.rawValue, value: !device.isOwner)
        if !device.isOwner{
            let user = SCUser.reStore()
            let name: String = "\((user?.firstName)!) \((user?.lastName)!)"
            sendTelemetry(key: MQTTVariable.slaveName.rawValue, value: name)
        }
        print("Rolley dashboard location updated")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            manager.startUpdatingLocation()
        }
        else {
            if status != .notDetermined{
                if device.isOwner {
                    showAlertForPermissionNotFound(title: "enableLocationTitle".localized(), message: "enableLocationMessage".localized(), actionButtoTitle: "enable".localized())
                }else {
                    showAlertForPermissionForSharedDevice(title: "enableLocationTitle".localized(), message: "enableLocationMessage".localized(), actionButtoTitle: "enable".localized())
                }
            }
        }
    }
    
    
    
    private func setup_views() {
        let smallScreen = 300 > self.view.frame.height/2 - 40
        let maxScreen =  400 < self.view.frame.height/2 - 40
        
        titleImageView = UIImageView()
        #if targetEnvironment(simulator)
        titleImageView.image = UIImage(named: "rolley2Logo")
        #else
        if device.type == ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS{
            titleImageView.theme_image = ["Rolley+white","Rolley+"]
        }
        else{
            titleImageView.theme_image = ["RolleyDarkLogo","RolleyLogo"]
        }
        
        #endif
        self.view.addSubview(titleImageView)
        titleImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(smallScreen ? 10 : maxScreen ? 50 : 30)
            //make.width.equalTo(134.0)
            make.height.equalTo(33.0)
            make.centerX.equalTo(self.view)
        }
        
        
        let sideLenght = smallScreen ? (self.view.frame.height - 100)/2 : maxScreen ? 320 : 300
        progressBars = ArcaProgressView(frame: CGRect(x: 0, y: 0, width: sideLenght, height: sideLenght))
        self.view.addSubview(progressBars)
        progressBars.progressBackgoundColor = .coolGrey
        progressBars.speedProgressForegroundColor = .pastelOrange
        progressBars.powerProgressForegroundColor = .dodgerBlue
        progressBars.batteryProgressForegroundColor = .greenblue
        progressBars.labelSize = smallScreen ? sideLenght/3.5 : 90
        progressBars.fontForTitleLabels = .bold(size: 15)
        progressBars.showSpeedOnLabel = true
        progressBars.snp.makeConstraints { (make) in
            make.width.height.equalTo(sideLenght)
            make.centerX.equalTo(self.view)
            make.top.equalTo(titleImageView.snp.bottom).offset(smallScreen ? 10 : maxScreen ? 50 : 30)
        }
        
        let showBattButton = UIButton(type: .custom)
        showBattButton.backgroundColor = .clear
        self.view.addSubview(showBattButton)
        showBattButton.snp.makeConstraints { (make) in
            make.edges.equalTo(progressBars)
            make.center.equalTo(progressBars)
        }
        showBattButton.addAction(for: .touchUpInside) { [weak self] in
            if(self?.pinState == 1){
                authenticationWithBiometricOrPhonePin(authicationDelgate: self!)
            }else{
                self?.actionOnGauge()
            }
        }
        
        tripView = InfoView(labels: ["trip".localized(), "0", isImperial() ? "m_short".localized() : "km_short".localized()], and: .charcoalGrey)
        setupTapOnTrip()
        
        //Head Light action item
        lightView = InfoView(image: UIImage(named: "light")!, onImage: nil, labelsOnOff: ["On", "Off"], and: .dodgerBlue, isOn: isHeadLight)
        lightView.button.addAction(for: .touchUpInside) { [weak self] in
            self?.isHeadLight = !self!.isHeadLight
            if self!.isConnectedWithRolley{
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                let lightVal: Int32 = self!.isHeadLight == true ? 1 : 0
                self!.rolleyService.sendCommandToRolley(command:APPSetHeadLight, data: lightVal)
            }
        }
        lightView.bottomLabel.text = isHeadLight ? "On" : "Off"
        lightView.setState(stateOn: isHeadLight)
        
        rangeView = InfoView(labels: ["range".localized(),"0", isImperial() ? "m_short".localized() : "km_short".localized()], and: .charcoalGrey)
        
        //Ui Contraints added
        let hStackView1 = UIStackView(arrangedSubviews: [tripView, lightView, rangeView])
        hStackView1.axis = .horizontal
        hStackView1.spacing = 10.0
        hStackView1.distribution = .equalSpacing
        let infoViewSideSize = 105.0
        
        tripView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        lightView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        rangeView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        
        //Pas Level Ui contorller
        let downButton = UIButton(type: .custom)
        downButton.layer.cornerRadius = 20
        downButton.theme_backgroundColor = [UIColor.dodgerBlue.toHexString(), UIColor.paleGrey.toHexString()]
        let image1 = UIImage(named: "smallArrowDown")?.withRenderingMode(.alwaysTemplate)
        downButton.theme_tintColor = [UIColor.charcoalGrey.toHexString(), UIColor.dodgerBlue.toHexString()]
        downButton.setImage(image1, for: .normal)
        downButton.addAction(for: .touchUpInside) { [weak self] in
            if(nil == PAS_NUM_CUR || local_pas_level<0 ){
                return;
            }
            if(local_pas_level == 0){
                self!.pasLevel=PAS_NUM_CUR[0]
            }else{
                var localPas = Int(local_pas_level)
                localPas -= 1
                self!.pasLevel = PAS_NUM_CUR[localPas]
            }
            print("Pass Level chage: \(self!.pasLevel)")
            self!.rolleyService.sendCommandToRolley(command:APPSetPASLevel, data: self!.pasLevel)
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        }
        
        levelView = InfoView(labels: ["","0", "0", "level".localized()],imageOnOFF: ["white-walking-scooter","grey-walking-scooter","blue-walking-scooter","blue-walking-scooter"] ,and: .charcoalGrey)
        
        let upButton = UIButton(type: .custom)
        upButton.layer.cornerRadius = 20
        upButton.theme_backgroundColor = [UIColor.dodgerBlue.toHexString(), UIColor.paleGrey.toHexString()]
        let image2 = UIImage(named: "smallArrowUp")?.withRenderingMode(.alwaysTemplate)
        upButton.theme_tintColor = [UIColor.charcoalGrey.toHexString(), UIColor.dodgerBlue.toHexString()]
        upButton.setImage(image2, for: .normal)
        upButton.addAction(for: .touchUpInside) { [weak self] in
            if(nil == PAS_NUM_CUR || local_pas_level >= PAS_NUM_LEN-1){
                return;
            }
            var localPas = Int(local_pas_level)
            localPas += 1
            self!.pasLevel =  PAS_NUM_CUR[localPas]
            print("Pass Level chage: \(self!.pasLevel)")
            self!.rolleyService.sendCommandToRolley(command:APPSetPASLevel, data: self!.pasLevel)
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        }
        
        let hStackView2 = UIStackView(arrangedSubviews: [downButton, levelView, upButton])
        hStackView2.spacing = 10.0
        hStackView2.distribution = .equalSpacing
        upButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        downButton.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        levelView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        
        
        let vStackView = UIStackView(arrangedSubviews: [hStackView1, hStackView2])
        vStackView.axis = .vertical
        vStackView.spacing = 10.0
        vStackView.distribution = .equalSpacing
        
        self.view.addSubview(vStackView)
        vStackView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-20)
        }
    }
    
    func onInfoReceived(command: Int32, value: String!) {
        print("Dashboard Rolley command received \(command) with value \(String(describing: value))")
        switch command {
        case EBoxInfSpeed:
            didGetSpeed(currentSpeedString: value)
        case EBoxInfremaindistance:
            didGetRange(rangeString: value)
        case EBoxInfTrip:
            let tripValue = self.rolleyService.getCanInfoForCertainParams(key: 11)
            didGetTrip(tripString: tripValue)
        case EBoxResSpeedLimit:
            maxSpeed = (value as NSString).doubleValue
        case EBoxInfPASLevel:
            didGetPASLevel(pasLevelSring: value)
        case EBoxInfBatteryCapacity:
            didGetBatNow(batteryString: value)
        case EBoxInfHeadLight:
            didGetHeadlight(lightStatusString: value)
        case EBoxInfOdo:
            didGetODO(odoString: value)
        case EBoxInfVoltage:
            lastVoltage = (value as NSString).integerValue
            updatePower()
        case EBoxInfCurrent:
            lastCurrent = (value as NSString).integerValue
            updatePower()
        case EBoxInfPINStatus:
            pinState = (value as NSString).integerValue
            print("Key feature at dash received: \(pinState)")
            if(pinState == 1){
                SCTabBarController.isDeviceAuthenticate = false
                progressBars.toggleBetweenLabelAndImageView(showLock: true)
                if !device.deviceParams!.isFirstTime{
                    authenticationWithBiometricOrPhonePin(authicationDelgate: self)
                }
            }else{
                SCTabBarController.isDeviceAuthenticate = true
                progressBars.toggleBetweenLabelAndImageView(showLock: false)
                progressBars.setProgressSpeed(to: 0, withAnimation: false, maxSpeed: maxSpeed)
            }
        case EBoxErrorCode:
            let errorCode = (value as NSString).integerValue
            print("Rolley error received: \(errorCode)")
            handleDriveTrainErrors(errorCode: errorCode)
            sendTelemetry(key: MQTTVariable.errorCode.rawValue, value: errorCode)
        case EBoxInfBMSCycleCount:
            sendTelemetry(key: MQTTVariable.batteryCycle.rawValue, value: value!)
        case EBoxInfBMSTemperature:
            sendTelemetry(key: MQTTVariable.batteryTemp.rawValue, value: value!)
        case EBoxBatterySerialNum:
            sendAttributes(key: MQTTVariable.batterySerial.rawValue, value: value!)
        case EBoxBatterySWVersion:
            sendAttributes(key: MQTTVariable.batterySoftwareVersion.rawValue, value: value!)
        case EBoxBatteryHWVersion:
            sendAttributes(key: MQTTVariable.batteryHardwareVersion.rawValue, value: value!)
        case EBoxControllerSWVersion:
            sendAttributes(key: MQTTVariable.controllerSoftwareVersion.rawValue, value: value!)
        case EBoxControllerSerialNum:
            sendAttributes(key: MQTTVariable.controllerSerial.rawValue, value: value!)
        case EBoxControllerHWVersion:
            sendAttributes(key: MQTTVariable.controllerHardwareVersion.rawValue, value: value!)
        case EBoxPanelSWVersion:
            sendAttributes(key: MQTTVariable.hMISoftwareVersion.rawValue, value: value!)
        case EBoxPanelSerialNum:
            sendAttributes(key: MQTTVariable.hMISerial.rawValue, value: value!)
        case EBoxPanelHWVersion:
            sendAttributes(key: MQTTVariable.hmiHardwareVersion.rawValue, value: value!)
        case EBoxInfBMSMaxChaInterval:
            sendTelemetry(key: MQTTVariable.batteryMaxChargeInterval.rawValue, value: value!)
        case EBoxInfBMSCurChaInterval:
            sendTelemetry(key: MQTTVariable.batteryCurrentChargeInterval.rawValue, value: value!)
        case APPSetAutoOff :
            sendAttributes(key: MQTTVariable.autoOff.rawValue, value: value!)
            if value == "1" && device.isOwner{
                //todo used by slave set to default fo user settings, if have
                self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 3)
            }else if !device.isOwner && value != "1"{
                self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 1)
            }
        case APPController_AuthPIN:
            if value == "0" && !device.deviceParams!.isKeyFeatureEnabled && pinState == 1 {
                let alert: UIAlertController = UIAlertController(title: "unlockRolleyTitle".localized(), message: "unlockRolleyMessage".localized(), preferredStyle: .alert)
                let action: UIAlertAction = UIAlertAction.init(title: "Unlock".localized(), style: UIAlertAction.Style.cancel) { (UIAlertAction) in
                    guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                    SCTabBarController.isDeviceAuthenticate = true
                    tabbarC.selectedIndex = 3
                    tabbarC.forceUpdateTabAction()

                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            
        default:
            break
            
        }
    }
    
    
    //Function to update the UI
    @objc private func didGetSpeed(currentSpeedString: String) {
        let currentSpeed = Double(currentSpeedString)
        print("Speed is: \(currentSpeed!)")
        
        //Update the power if set in the gauge
        self.progressBars.lastPowerState = String(format: "%.0f", lastPower)
        
        sendTelemetry(key: MQTTVariable.speed.rawValue, value: currentSpeed!)
        progressBars.setProgressSpeed(to: Double(currentSpeed!/(maxSpeed/100))/100, withAnimation: false, maxSpeed: maxSpeed)
        
    }
    
    @objc private func didGetRange(rangeString: String) {
        var range = (rangeString as NSString).integerValue
        if(isImperial()){
            range = Int(toMileFromKm(distance: (rangeString as NSString).doubleValue))
        }
        print("Range is: \(range)")
        if(range < self.MAX_RANGE_LIMIT){
            rangeView.mainLabel.text = String(format: "%d", range)
        }
        else{
            rangeView.mainLabel.text = "∞"
        }
    }
    
    @objc private func didGetTrip(tripString: String) {
        let trip = Float(tripString)
        print("Trip is: \(trip!)")
        if trip == 0.0 {
            tripView.mainLabel.text = "0"
        }
        else {
            if(isImperial()){
                let mileTrip = Float(toMileFromKm(distance: (tripString as NSString).doubleValue))
                tripView.mainLabel.text = String(format: "%.1f", mileTrip)
            }else{
                tripView.mainLabel.text = String(format: "%.1f", trip!)
            }
        }
        sendTelemetry(key: MQTTVariable.tripDistance.rawValue, value: tripString)
    }
    
    @objc private func didGetPASLevel(pasLevelSring:String) {
        switch pasLevelSring {
        case "助推档":
            levelView.setStateForLevel(isWalkingAssitanceSet: true, walkingAssistanceOn: false)
            levelView.setLabels(top: "walk".localized(), main: nil, bottom: "assitance".localized())
        case "助推中":
            levelView.setStateForLevel(isWalkingAssitanceSet: true, walkingAssistanceOn: true)
            levelView.setLabels(top: "walk".localized(), main: nil, bottom: "assitance".localized())
        default:
            levelView.setStateForLevel(isWalkingAssitanceSet: false, walkingAssistanceOn: false)
            let level = (pasLevelSring as NSString).integerValue
            levelView.setLabels(top: nil, main: String.init(format: "%d", level), bottom: nil)
            break
        }
        sendTelemetry(key: MQTTVariable.assistLevel.rawValue, value: pasLevelSring)
        
    }
    
    @objc private func didGetBatNow(batteryString: String) {
        let batNowCapacity = (batteryString as NSString).doubleValue
        let value = Double(batNowCapacity/100)
        sendTelemetry(key: MQTTVariable.batteryLevel.rawValue, value: batteryString)
        progressBars.setProgressBattary(to: value, withAnimation: false)
        lastBatteryState = batteryString
        //handling the right state of battery when shown on the gauage main
        if(progressBars.lastBatteryState != lastBatteryState){
            progressBars.lastBatteryState = lastBatteryState
            progressBars.updateBatteryOnMainLable()
        }
        
    }
    
    @objc private func didGetHeadlight(lightStatusString: String) {
        let light = (lightStatusString as NSString).integerValue
        print("Light is: \(light)")
        switch light {
        case 0:
            isHeadLight = false
        default:
            isHeadLight = true
        }
        lightView.bottomLabel.text = isHeadLight ? "On" : "Off"
        lightView.setState(stateOn: isHeadLight)
        sendTelemetry(key: MQTTVariable.lights.rawValue, value: isHeadLight.description)
        
    }
    @objc private func didGetODO(odoString: String) {
        sendTelemetry(key: MQTTVariable.odoReading.rawValue, value: odoString)
    }
    @objc private func updatePower() {
        lastPower = Double(lastVoltage * lastCurrent)
        progressBars.setProgressPower(to: lastPower, withAnimation: false)
    }
    
    func deviceStatus(status: Int32) {
        print("Rolley onDeviceStatusChanged \(peripheral.identifier.uuidString) with status \(status) ")
        isConnectedWithRolley = false
        switch (status) {
        case PL_BLE_STATUS_CONNECTED:
            break
        case PL_BLE_STATUS_READY:
            markTripStatus(isTripStarted: true)
            self.rolleyService.fetchDriveTrainInfo()
            if locationManager != nil {
                self.locationManager.startUpdatingLocation()
            }
            isConnectedWithRolley = true
            let isOwner = self.device.isOwner
            if isOwner{
                checkForFirstTime()
                setDeviceName()
                self.rolleyService.sendCommandToRolley(command: APPSetAutoDrive, data: device.deviceParams!.isSmartModeAccSet ? 1: 0)
                let autoOFF = self.rolleyService.getStringValueForAutoOff()
                if autoOFF == "1"{
                    self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 3)
                }
            }else{
                //shared device
                self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 1)
            }
        case PL_BLE_STATUS_RECONNECTING:
            //Dismiss when state moved to disconnected
            //todo show popup on the device screen that its been disconnected
            self.rolleyService.disconnect()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.rolleyService.disconnect()
                guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                tabbarC.moveToDeviceTab()
            }
            
            
        default:
            break
        }
        
    }
    
    
    func success() {
        //Can
        let key = self.device.deviceParams?.key
        if(key != nil){
            self.rolleyService.sendPinCommandToRolley(command: APPController_AuthPIN, pin: key!)
        }
    }
    
    func failed() {
        //Cannot
    }
    
    private var timer: Timer?
    private func actionOnGauge(){
        if(self.gaugeTapCount == self.SHOW_SPEED){
            self.progressBars.showSpeedOnLabel = false
            gaugeTapCount = SHOW_BATTERY
            if(lastBatteryState != "NA"){
                self.progressBars.lastBatteryState = self.lastBatteryState
            }else{
                self.progressBars.lastBatteryState = "0"
            }
            handleBatteryInfoOnGauage()
            
        }else if(self.gaugeTapCount == self.SHOW_BATTERY){
            timer?.invalidate()
            gaugeTapCount = SHOW_POWER
            self.progressBars.showPowerOnLabel = true
            self.progressBars.lastPowerState =  String(format: "%.0f", lastPower)
        }else {
            gaugeTapCount = SHOW_SPEED
            self.progressBars.showPowerOnLabel = false
            self.progressBars.showSpeedOnLabel = true
        }
        //
        self.progressBars.updateTapOntheMainLable()
        
    }
    private func handleBatteryInfoOnGauage(){
        timer = Timer.scheduledTimer(withTimeInterval: 5 , repeats: false, block: { (timer) in
            if(self.gaugeTapCount == self.SHOW_BATTERY){
                self.progressBars.showSpeedOnLabel = true
                self.gaugeTapCount = self.SHOW_SPEED
            }else{
                self.progressBars.showPowerOnLabel = true
            }
            self.progressBars.updateTapOntheMainLable()
            print("Last battery after timer expired: \(self.lastBatteryState)")
        })
    }
    
    
    private func checkForFirstTime(){
        let isFirtTime = device.deviceParams?.isFirstTime
        if(isFirtTime!){
            let savedKey  = device.deviceParams?.key
            if(savedKey != nil && savedKey!.count != 0){
                let resetKey = "\(savedKey!)0000"
                print("Key feature is going to be reset for the First Time with key: \(resetKey)")
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.rolleyService.sendPinCommandToRolley(command: APPController_SetPIN, pin: resetKey)
                }
            }
            
            //intilize dark mode
            updateDarkMode(valueSet: 0)
            
            device.deviceParams?.isFirstTime = false
            device.update()
        }
    }
    
    private func setDeviceName(){
        let name = getNameForRolley()
        rolleyService.changeRolleyName(name: name)
        device.name = name
        device.update()
    }
    
    private func getNameForRolley()->String{
        var deviceName = "Untitled"
        let user = SCUser.reStore()
        let firstName = user?.firstName
        let lastName = user?.lastName
        deviceName = "\(firstName!) \(lastName!)"
        if(deviceName.count >= 15){
            deviceName = firstName!
        }
        let numberOfAllDevices = UserDefaults.standard.integer(forKey: ContantsKey.activaatedDevicesCount.rawValue)
        let curruenDeviceCount = device.deviceParams?.deviceCount
        if(numberOfAllDevices > 1 && curruenDeviceCount! > 1){
            if(deviceName.count >= ConstantInt.maxFirstNameChar.rawValue){
                deviceName = firstName!
            }
            deviceName = "\(deviceName)(\(curruenDeviceCount!))"
            
        }
        return deviceName
    }
    
    private func handleDriveTrainErrors(errorCode: Int){
        let errorDic = DriveTrainErrors().getErrorDictionary()
        let errorType = errorDic[errorCode]
        if(errorType != nil){
            
            var message = errorType?.errorDeclaration
            message?.append("\n\n")
            message?.append("troubleShooting".localized())
            message?.append(errorType!.errorTroubleShooting)
            
            let alertTitle = "\("errorCodeTitle".localized()) \(errorCode)"
            let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "okay".localized(), style: .cancel, handler: {
                (alert: UIAlertAction!) in
                
            }))
            alert.addAction(UIAlertAction(title: "support".localized(), style: .default, handler: {
                (alert: UIAlertAction!) in
                self.createSupportTicket(subject: "Rolley Error code \(errorCode)", message: "User trigger this request", tags: ["iOS","rolleyHardwareError"])
            }))
            self.present(alert, animated: true)
        }
        
        
    }
    
    //Trip Tap button
    func setupTapOnTrip() {
        let touchDown = UILongPressGestureRecognizer(target:self, action: #selector(didTouchDown))
        touchDown.minimumPressDuration = 0
        self.tripView.addGestureRecognizer(touchDown)
    }
    
    @objc func didTouchDown(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            self.tripView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
            alertForTripReset()
        } else if gesture.state == .ended || gesture.state == .cancelled {
            self.tripView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        }
    }
    
    
    private func alertForTripReset(){
        let alert = UIAlertController(title: "resetTripTitle".localized(), message: "resetTripMessage".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "no".localized(), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
        }))
        alert.addAction(UIAlertAction(title: "yes".localized(), style: .destructive, handler: {
            (alert: UIAlertAction!) in
            self.tripView.mainLabel.text = "0"
            self.rolleyService.sendCommandToRolley(command: APPClsSinglelMile, data: 0)
        }))
        self.present(alert, animated: true)
    }
    
    
    func showAlertForPermissionForSharedDevice(title: String, message: String, actionButtoTitle: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "cancel".localized(), style: .cancel) { (UIAlertAction) in
            //dismissConnection and turnOffRolley
            self.rolleyService.sendCommandToRolley(command: APPController_PowerOnOff, data: 0)
        }
        let actionTurnOn: UIAlertAction = UIAlertAction.init(title: actionButtoTitle, style: .default) { (UIAlertAction) in
            let url = URL(string: UIApplication.openSettingsURLString)
            let app = UIApplication.shared
            app.open(url!, options: [:], completionHandler: nil)
        }
        alert.addAction(action)
        alert.addAction(actionTurnOn)
        self.present(alert, animated: true, completion: nil)
    }
    
}


