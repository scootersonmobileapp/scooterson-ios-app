//
//  DashBoardViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/1/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import CocoaMQTT
import SwiftTheme
import Solar
import CoreLocation

struct TripData {
    var complete: Bool!
    var batteryLevel: Float!
    var distance: Float!
}

class DashBoardViewControllerELF: SCBaseDashboardViewController, BTElfServiceDelegate, CLLocationManagerDelegate {
    
    private var btTools = BTElfService.shared
    private var currentLocation: CLLocationCoordinate2D!
    private var smartLightEnabled = UserDefaults.standard.bool(forKey: "SMART_LIGHT_ENABLED")
    public var tripData = TripData(complete: false, batteryLevel: 0.0, distance: 0.0)
    private var peripheral: CBPeripheral!
    private var isHeadLight: Bool = false {
        didSet {
            lightView.setState(stateOn: self.isHeadLight)
        }
    }
    private var isLocked: Bool = false {
        didSet {
            lockView.setState(stateOn: self.isLocked)
        }
    }
    private var isCruise: Bool = false {
        didSet {
            cruiseView.setState(stateOn: self.isCruise)
        }
    }
    
    private var pasLevel: Int = 0
    private var mqtt: CocoaMQTT!
    
    @IBOutlet var titleImageView: UIImageView!
    @IBOutlet var progressBars: ArcaProgressView!
    @IBOutlet var tripView: InfoView!
    @IBOutlet var modeView: InfoView!
    @IBOutlet var rangeView: InfoView!
    @IBOutlet var lockView: InfoView!
    @IBOutlet var cruiseView: InfoView!
    @IBOutlet var lightView: InfoView!
    private var demoAnimationTimer: Timer!
    private var smartLightTimer: Timer!
    
    var maxSpeed: Double! {
        get {
            return Double(BTElfService.shared.isImperial() ? 15.0 : 25.0)
        }
    }
    private var lastBatteryState = ""//: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup_views()
        guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
        btTools.delegate = self
        activateMQTT()
        self.device = tabBarController.device
        self.peripheral = tabBarController.peripheral
        #if targetEnvironment(simulator)
        lastBatteryState = "45"
        #else
//        setupNotifications()
        
        #endif
        
//        setup_views()
        #if targetEnvironment(simulator)
        #else
//        btTools.connectPeripheral(peripheral)
        #endif
    }
    
    // MARK: Location delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location?.coordinate
        smartLightActivate()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            manager.startUpdatingLocation()
        }
        else {
            showAlertWithButton(title: "enableLocationTitle".localized(), message: "enableLocationMessage".localized(), actionButtoTitle: "enable".localized())
        }
    }
    
    func showAlertWithButton(title: String, message: String, actionButtoTitle: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "cancel".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let actionTurnOn: UIAlertAction = UIAlertAction.init(title: actionButtoTitle, style: .default) { (UIAlertAction) in
            let url = URL(string: "App-Prefs:root=General")
            let app = UIApplication.shared
            app.open(url!, options: [:], completionHandler: nil)
        }
        alert.addAction(action)
        alert.addAction(actionTurnOn)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tripView.bottomLabel.text = btTools.isImperial() ? "miles".localized() : "km_short".localized()
        rangeView.bottomLabel.text = btTools.isImperial() ? "miles".localized() : "km_short".localized()
        progressBars.showSpeedOnLabel = true
        progressBars.setProgressSpeed(to: 0.0, withAnimation: true, maxSpeed: maxSpeed)
    }
    
    
    private func smartLightActivate() {
        if !CLLocationManager.locationServicesEnabled() { return }
        if smartLightEnabled {
            let solar = Solar(for: Date(), coordinate: currentLocation)
            smartLightTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [weak self] (timer) in
                if let twentyMinBeforeSunsetTIme = solar?.sunset?.addingTimeInterval(-1200) {
                    if twentyMinBeforeSunsetTIme.compare(Date()) == .orderedDescending {
                        if self?.peripheral?.state == CBPeripheralState.connected {
                            if !(self?.lightView.isOn)! {
                                self?.btTools.lightOnOff()
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isLocked = btTools.isLocked()
        isHeadLight = btTools.isHeadlight()
        isCruise = btTools.isCruise()
//        progressBars.setProgressSpeed(to: 0.01, withAnimation: true, maxSpeed: maxSpeed)
        #if targetEnvironment(simulator)
//        demoAnimationTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [weak self] (timer) in
//            self?.progressBars.setProgressSpeed(to: Double.random(in: 0 ... 1), withAnimation: false, maxSpeed: self!.maxSpeed)
//            self?.progressBars.setProgressPower(to: Double.random(in: 0 ... 1), withAnimation: false)
//        }
        #endif
        btTools.delegate = self
        UIApplication.shared.isIdleTimerDisabled = true
        if UserDefaults.standard.integer(forKey: ContantsKey.activaatedDevicesCount.rawValue) == 0 {
            UserDefaults.standard.set(true, forKey: "SMART_LIGHT_ENABLED")
            let locationManager = CLLocationManager()
            locationManager.requestWhenInUseAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    private func setup_views() {
        titleImageView = UIImageView()
//        #if targetEnvironment(simulator)
//        titleImageView.image = UIImage(named: "rolley2Logo")
//        #else
//        titleImageView.image = UIImage(named: String(format: "%@Logo", deviceName))
        titleImageView.contentMode = .center
        titleImageView.theme_image = ThemeImagePicker(images: UIImage(named: String(format: "%@DarkLogo", deviceName))!, UIImage(named: String(format: "%@Logo", deviceName))!)
//        #endif
        self.view.addSubview(titleImageView)
        titleImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(10)
            make.width.equalTo(134.0)
            make.height.equalTo(33.0)
            make.centerX.equalTo(self.view)
        }
        
        
        let smallScreen = 300 > self.view.frame.height/2 - 40
        let sideLenght = smallScreen ? (self.view.frame.height - 100)/2 : 300
        progressBars = ArcaProgressView(frame: CGRect(x: 0, y: 0, width: sideLenght, height: sideLenght))
        progressBars.threeArcs = false
        self.view.addSubview(progressBars)
        progressBars.progressBackgoundColor = .coolGrey
        progressBars.speedProgressForegroundColor = .barbiePink
        progressBars.powerProgressForegroundColor = .greenblue
        progressBars.labelSize = smallScreen ? sideLenght/3.5 : 90
        progressBars.fontForTitleLabels = .bold(size: 15)
        progressBars.showSpeedOnLabel = true
        progressBars.snp.makeConstraints { (make) in
            make.width.height.equalTo(sideLenght)
            make.centerX.equalTo(self.view)
            make.top.equalTo(titleImageView.snp.bottom).offset(smallScreen ? 10 : 40)
        }
        let showBattButton = UIButton(type: .custom)
        showBattButton.backgroundColor = .clear
        self.view.addSubview(showBattButton)
        showBattButton.snp.makeConstraints { (make) in
            make.edges.equalTo(progressBars)
            make.center.equalTo(progressBars)
        }
        showBattButton.addAction(for: .touchUpInside) { [weak self] in
            self?.progressBars.showSpeedOnLabel = false
            self?.progressBars.lastBatteryState = self!.lastBatteryState
            Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (timer) in
                self?.progressBars.showSpeedOnLabel = true
            })
        }
        
        #if DEBUG
        let testButton = UIButton(type: .custom)
        self.view.addSubview(testButton)
        testButton.backgroundColor = .red
        testButton.setTitleColor(.white, for: .normal)
        testButton.setTitle("TEST", for: .normal)
        testButton.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.width.equalTo(60)
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
        testButton.addAction(for: .touchUpInside) { [weak self] in
            self?.tripData = TripData(complete: false, batteryLevel: 0.2, distance: 1.0)
            self?.showChargeRemind()
        }
        #endif
        
        tripView = InfoView(labels: ["trip".localized(), "0.0", btTools.isImperial() ? "m_short".localized() : "km_short".localized()], and: .charcoalGrey)
        
        modeView = InfoView(labels: ["", "eco".localized(), "mode".localized()], and: .charcoalGrey)
        modeView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        modeView.mainLabel.textColor = .dodgerBlue
        modeView.mainLabel.adjustsFontSizeToFitWidth = true
        modeView.button.addAction(for: .touchUpInside) { [weak self] in
            self?.btTools.switchMode()
        }
        
        rangeView = InfoView(labels: ["range".localized(),"0",btTools.isImperial() ? "m_short".localized() : "km_short".localized()], and: .charcoalGrey)
        
        let hStackView1 = UIStackView(arrangedSubviews: [tripView, modeView, rangeView])
        hStackView1.axis = .horizontal
        hStackView1.spacing = 10.0
        hStackView1.distribution = .equalSpacing
        
        let infoViewSideSize = 105.0
        
        tripView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        modeView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        rangeView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        
        lockView = InfoView(image: UIImage(named: "lock")!, onImage: UIImage(named: "locked")!, labelsOnOff: ["On", "Off"], and: .dodgerBlue, isOn: isLocked)
        lockView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        lockView.button.addAction(for: .touchUpInside) { [weak self] in
            if self?.peripheral?.state == CBPeripheralState.connected {
                self?.btTools.lockUnlock()
            }
        }
        
        lightView = InfoView(image: UIImage(named: "headlightElf")!, onImage: nil, labelsOnOff: ["On", "Off"], and: .dodgerBlue, isOn: isHeadLight)
        lightView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        lightView.button.addAction(for: .touchUpInside) { [weak self] in
            if self?.peripheral?.state == CBPeripheralState.connected {
                self?.btTools.lightOnOff()
            }
        }
        
        cruiseView = InfoView(image: UIImage(named: "cruise")!, onImage: nil, labelsOnOff: ["On", "Off"], and: .dodgerBlue, isOn: isCruise)
        cruiseView.backgroundColor = .paleGrey
        cruiseView.button.addAction(for: .touchUpInside) { [weak self] in
            if self?.peripheral?.state == CBPeripheralState.connected {
                self?.btTools.setCruise()
            }
        }
        
        let hStackView2 = UIStackView(arrangedSubviews: [lockView, lightView, cruiseView])
        hStackView2.spacing = 10.0
        hStackView2.distribution = .equalSpacing
        lockView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        cruiseView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        lightView.snp.makeConstraints { (make) in
            make.height.width.equalTo(infoViewSideSize)
        }
        
        
        let vStackView = UIStackView(arrangedSubviews: [hStackView1, hStackView2])
        vStackView.axis = .vertical
        vStackView.spacing = 10.0
        vStackView.distribution = .equalSpacing
        
        self.view.addSubview(vStackView)
        vStackView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-20)
        }
    }
    
    
//    #if targetEnvironment(simulator)
//    #else
    
    // MARK: BTElfServiceDelegate
    func updateODO(odo:Float) {
        sendToMQTT(message: "{\"odoReading\":\"\(odo)\"}")
    }
    
    func updateTrip(trip:Float) {
        if trip == 0.0 {
            tripView.mainLabel.text = "0"
        }
        else {
            tripView.mainLabel.text = String(format: "%.1f", trip/1000.0)
            tripData.distance = trip/1000.0
        }
    }
    
    func updateBattery(battery: Double) {
        sendToMQTT(message: "{\"batteryLevel\":\"\(battery)\"}")
        lastBatteryState = "\(Int(battery))"
        progressBars.setProgressPower(to: battery/100, withAnimation: false)
        tripData.batteryLevel = Float(battery/100)
    }
    
    func updateSpeed(speed: UInt8) {
        sendToMQTT(message: "{\"speed\":\"\(speed)\"}")
        progressBars.setProgressSpeed(to: Double(Double(speed)/(maxSpeed/100))/100, withAnimation: false, maxSpeed: maxSpeed)
    }
    
    func lockStateIsChanged(lockState: Bool) {
        lockView.setState(stateOn: lockState)
    }
    
    func lightStateIsChanged(lightState: Bool) {
        lightView.setState(stateOn: lightState)
    }
    
    func cruiseStateIsChanged(cruiseState: Bool) {
        cruiseView.setState(stateOn: cruiseState)
    }
    
    func modeSwitchedTo(mode: ELFMode) {
        var text = ""
        switch mode {
        case .ELFModeECO:
            text = "eco".localized()
        case .ELFModeFUN:
            text = "fun".localized()
        case .ELFModeSPORT:
            text = "sport".localized()
        }
        modeView.mainLabel.text = text
    }
    
    func deviceDisconnected(peripheral: CBPeripheral) {
        self.tabBarController?.dismiss(animated: true, completion: nil)
        sendToMQTT(message: "{\"tripStatus\":\"false\"}")
        mqtt.disconnect()
        tripData.complete = true
        if btTools.chargerReminderIsOn {
            showChargeRemind()
        }
    }
    
    private func showChargeRemind() {
        var i = 0
        if tripData.batteryLevel < 0.25 && tripData.distance > 0 {
            Timer.scheduledTimer(withTimeInterval: 600, repeats: false, block: { (_) in
                Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (timer) in
                    if i == 60 { timer.invalidate(); return }
                    i += 1
                    if Reachability.isConnectedToWiFi {
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        
                        appDelegate?.scheduleNotification(notificationData: [
                            "title" : "ELF Charging Reminder",
                            "message" : "Currently, it has\(Int(self.tripData.batteryLevel * 100))% battery."
                            ])
                        timer.invalidate()
                    }
                })
            })
        }
    }
    
    func deviceConnected(peripheral: CBPeripheral) {
        // TODO: Need implement!        
    }
    
    private func updateInterface() {
        isCruise = btTools.isCruise()
        isLocked = btTools.isLocked()
    }
    
    // MARK: CocoaMQTT Delegate
    override func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        sendToMQTT(message: "{\"tripStatus\":\"true\"}")
    }
    
    override func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
         sendToMQTT(message: "{\"tripStatus\":\"false\"}")
    }
    
    // MARK: MQTT
    
    func activateMQTT() {
        let clientID = "ScootersoniOSApp-" + String(ProcessInfo().processIdentifier)
        API.shared.mqtt = CocoaMQTT(clientID: clientID, host: "thingsboard.scooterson.com", port: 1883)
        self.mqtt = API.shared.mqtt
        mqtt.username = device.credential?.accessToken
        mqtt.password = ""
        mqtt.keepAlive = 60
        mqtt.delegate = self
        if mqtt.connect() {
            print("MQTT connected")
        }
    }        
}

extension String {
    func presentAsImage(image: UIImage, atPoint point: CGPoint, bgColor: UIColor, fgColor: UIColor) -> UIImage {
        let textColor = bgColor
        let textFont :UIFont = .bold(size: 44)
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let rect = CGRect(origin: point, size: image.size)
        self.draw(in: rect, withAttributes: textFontAttributes)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
