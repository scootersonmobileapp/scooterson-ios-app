//
//  SCBaseDashboardViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 7/8/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Foundation
import CocoaMQTT
import CoreLocation

class SCBaseDashboardViewController: BaseViewController, CocoaMQTTDelegate {
    
    public var device:SCDevice!
    public var deviceName: String!
    private var mqttSC: CocoaMQTT!
    public var isConnectedWithRolley = false
    public var locationManager: CLLocationManager!
    
    func activateMQTTSC() {
        let clientID = "ScootersoniOSApp-" + String(ProcessInfo().processIdentifier)
        API.shared.mqtt = CocoaMQTT(clientID: clientID, host: "tb.scooterson.com", port: 1883)
        self.mqttSC = API.shared.mqtt
        mqttSC.username = device.credential?.accessToken
        mqttSC.password = ""
        mqttSC.keepAlive = 60
        mqttSC.delegate = self
        if mqttSC.connect() {
            print("MQTT connected")
            device.deviceParams?.lastSync = Date().currentTimeMillis()
            device.update()
        }
    }
    
    func sendTelemetry(key:String, value: Any){
        if(mqttSC != nil  && mqttSC.connState == .connected){
            let message = "{\"\(key)\":\"\(value)\"}"
            print("Mqtt: \(message)")
            mqttSC.publish("v1/devices/me/telemetry", withString: message, qos: .qos1)
        }
        
    }
    func sendTelemetryDouble(key:String, value: Double){
        if(mqttSC != nil  && mqttSC.connState == .connected){
            let message = "{\"\(key)\":\"\(value)\"}"
            print("Mqtt: \(message)")
            mqttSC.publish("v1/devices/me/telemetry", withString: message, qos: .qos1)
        }
        
    }
    func sendAttributes(key:String, value: Any){
        if(mqttSC != nil  && mqttSC.connState == .connected){
            let message = "{\"\(key)\":\"\(value)\"}"
            print("Mqtt: \(message)")
            mqttSC.publish("v1/devices/me/attributes", withString: message, qos: .qos1)
        }
        
    }
    
    public func markTripStatus(isTripStarted: Bool){
        sendTelemetry(key: MQTTVariable.tripStatus.rawValue, value: isTripStarted)
        if locationManager != nil {
            locationManager.stopUpdatingLocation()
        }
        if(!isTripStarted && mqttSC != nil){
            doChargingReminderThings(isTripStarted: isTripStarted)
            doSlaveRiderThings(isTripStarted: isTripStarted)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                API.shared.mqtt.disconnect()
                print("MQTT disconnected")
            }
        }
    }
    
    private func doChargingReminderThings(isTripStarted: Bool){
        if device.deviceParams!.isCharingReminderSet{
            let lastBattery:String  = (device.deviceParams?.lastBattery)!
            let deviceId: String = device.credential!.deviceID
            let jsonObject  = [MQTTVariable.tripStatus.rawValue : String(isTripStarted), MQTTVariable.batteryLevel.rawValue: lastBattery,
                                             "deviceId": deviceId ]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(jsonObject) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print("Mqtt charging reminder: \(jsonString)")
                    mqttSC.publish("v1/devices/me/telemetry", withString: jsonString, qos: .qos1)
                }
            }
        }
    }
    
    private func doSlaveRiderThings(isTripStarted: Bool){
        if !device.isOwner{
            let firstName = SCUser.reStore()?.firstName
            let deviceId: String = device.credential!.deviceID
            let jsonObject  = [MQTTVariable.tripStatus.rawValue : String(isTripStarted), MQTTVariable.isSlave.rawValue: "true", MQTTVariable.slaveName.rawValue : firstName, "deviceId": deviceId ]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(jsonObject) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print("Mqtt sharing ride reminder: \(jsonString)")
                    mqttSC.publish("v1/devices/me/telemetry", withString: jsonString, qos: .qos1)
                }
            }
        }
    }
    
    
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("MQTT check publish ask")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topics: [String]) {
        
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        
    }
    
    func mqttDidPing(_ mqtt: CocoaMQTT) {
        
    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        if isConnectedWithRolley{
            print("MQTT check disconneted")
            activateMQTTSC()
        }
        print("MQTT connection error \(String(describing: err))")
    }
}
