//
//  AddDeviceViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/14/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import CoreBluetooth
import SwiftTheme
import CoreGraphics
import BLTNBoard


struct deviceData {
    var type = Int()
    var deviceImage = String()
}


class AddDeviceViewController: BaseViewController, PLBleCallback,UITableViewDataSource, UITableViewDelegate, BTElfServiceDelegate, BroadUIDelegate{
    
    //Variables
    private var tableViewData =  [deviceData] ()
    private var addDevicesTableView: UITableView!
    private var scanTimer: Timer!
    private var deviceScanManager: BLTNItemManager!
    private var scanPage: FeedbackPageBLTNItem!
    
    //Contant
    var PRODUCT_TYPE_ELF = ConstantValues.PRODUCT_TYPE_ELF
    var PRODUCT_TYPE_ROLLEY = ConstantValues.PRODUCT_TYPE_ROLLEY
    var PRODUCT_TYPE_ROLLEY_PLUS = ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS
    
    
    override func viewDidLoad() {
        
        
        //Hide the line for the navigation bar from the top
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        tableViewData = [deviceData(type: PRODUCT_TYPE_ROLLEY_PLUS, deviceImage: "rolleyGhost"),
                         deviceData(type: PRODUCT_TYPE_ROLLEY, deviceImage: "rolleyGhost"),
                         deviceData(type: PRODUCT_TYPE_ELF,deviceImage: "elfGhost") ]
        
        setupViews()
        //Rolley Callback
        PLBleService.getInstance()?.ble_cb = self
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if scanTimer != nil {
            scanTimer.invalidate()
        }
        if deviceScanManager != nil && deviceScanManager.isShowingBulletin{
            deviceScanManager.dismissBulletin()
        }
    }
    
    @objc func didBecomeActive() {
        print("Add device did become active")
        if deviceScanManager != nil && deviceScanManager.isShowingBulletin{
            scanPage.titleLabel.blink()
        }
        
    }
    private func setupViews() {
        let headerView = setupTitleView()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        self.navigationItem.title = ""
        
        
        addDevicesTableView = UITableView()
        addDevicesTableView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        addDevicesTableView.theme_separatorColor =  [UIColor.paleGrey.toHexString(), UIColor.charcoalGrey.toHexString()]
        addDevicesTableView.dataSource = self
        addDevicesTableView.delegate = self
        addDevicesTableView.separatorInset = .zero
        addDevicesTableView.tableFooterView = UIView()
        self.view.addSubview(addDevicesTableView)
        addDevicesTableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo((headerView?.snp.bottom)!)
        }
        
        
        addDevicesTableView.reloadData()
        
    }
    func setupTitleView() -> UIView? {
        let titleView = UIView()
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 30)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = "addDevice".localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = tableViewData[indexPath.section].type
        let cell =  setupMainCell(type: type)
        cell.cellImageView.image = UIImage(named: tableViewData[indexPath.section].deviceImage)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0){
            return 180
        }else{
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !Reachability.isConnectedToInternet{
            showAlert(title: "noNetwork".localized(), message: "noNetworkBodyAddDeviceScreen".localized())
            return
        }
        self.deviceSelected(type: tableViewData[indexPath.section].type)
    }
    
    private func setupMainCell(type: Int) -> SCTableViewCell{
        let cell = SCTableViewCell(style: .default, reuseIdentifier: "nil")
        cell.selectionStyle = .none
        cell.isTapable = true
        let deviceImageView = UIImageView()
        cell.contentView.addSubview(deviceImageView)
        deviceImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(136.0)
            make.centerY.equalTo(cell.contentView)
            make.left.equalTo(cell.contentView).offset(40)
        }
        cell.cellImageView = deviceImageView
        
        let deviceNameImageView = UIImageView()
        cell.contentView.addSubview(deviceNameImageView)
        deviceNameImageView.contentMode = .left
        deviceNameImageView.snp.makeConstraints { (make) in
            make.width.equalTo(134.0)
            make.height.equalTo(33.0)
            make.centerY.equalTo(cell.contentView)
            make.left.equalTo(deviceImageView.snp.right).offset(40)
        }
        
        if(type == PRODUCT_TYPE_ROLLEY){
            deviceNameImageView.theme_image = ["RolleyDarkLogo","RolleyLogo"]
        }else if (type == PRODUCT_TYPE_ROLLEY_PLUS){
            deviceNameImageView.theme_image = ["Rolley+white","Rolley+"]
        }else{
            deviceNameImageView.theme_image = ["ELFDarkLogo","ELFLogo"]
        }
        return cell
    }
    
    var typeSelected: Int!
    private func deviceSelected(type:Int){
        switch type {
        case PRODUCT_TYPE_ROLLEY:
            self.runTimer(devicetype: type)
            typeSelected = type
            PLBleService.getInstance()?.ble_cb = self
            PLBleService.getInstance()?.scanDevices(true)
            
            scanPage = BoardUi.showDeviceConnectIntro(deviceType: PRODUCT_TYPE_ROLLEY, delegate: self)
            self.deviceScanManager = BoardUi.getDeviceManger(item: scanPage)
        case PRODUCT_TYPE_ROLLEY_PLUS:
            self.runTimer(devicetype: type)
            typeSelected = type
            PLBleService.getInstance()?.ble_cb = self
            PLBleService.getInstance()?.scanDevices(true)
            
            scanPage = BoardUi.showDeviceConnectIntro(deviceType: PRODUCT_TYPE_ROLLEY_PLUS, delegate: self)
            self.deviceScanManager  = BoardUi.getDeviceManger(item: scanPage)
        case PRODUCT_TYPE_ELF:
            //fake ELF loading screen
            self.runTimer(devicetype: type)
            scanPage = BoardUi.showDeviceConnectIntro(deviceType: PRODUCT_TYPE_ELF, delegate: self)
            self.deviceScanManager  = BoardUi.getDeviceManger(item: scanPage)
            typeSelected = type
        default:
            break;
        }
        deviceScanManager.showBulletin(above: self)
    }
    
    // ======================= Rolley setup function ==============================
    func onDeviceFound(_ peripheral: CBPeripheral!, data mData: [String : Any]!, rssi: Int32) {
        if((PLBleService.getInstance()?.isBafangCan(mData)) != false){
            print("Add Device Rolley found with following param", peripheral.identifier, peripheral.name as Any)
            let rolleyInfo: DeviceInfo = DeviceInfo(peripheral, data: mData, rssi: rssi)
            let deviceFound = SCDevice.reStore(vinCode: rolleyInfo.btAddr!)
            if(deviceFound == nil){
                claimDevice(deviceAddress: rolleyInfo.btAddr!, peripheral: peripheral)
                
            }else{
                // show dialog device already added in Devices
                deviceScanManager.displayNextItem()
            }
            
            self.scanTimer.invalidate()
            PLBleService.getInstance()?.scanDevices(false)
            PLBleService.getInstance()?.ble_cb = nil
        }
    }
    
    
    
    //======================= Device Activation Functions =============================
    private func claimDevice(deviceAddress: String ,peripheral: CBPeripheral?){
        let deviceModel = self.typeSelected == ConstantValues.PRODUCT_TYPE_ROLLEY ? "RL" : "RP"
        API.shared.claimDevice(deviceMacAddress: deviceAddress, deviceModel: deviceModel) { [weak self] (success, deviceCredential, typeInfo, key,errorDescription) in
            self?.deviceScanManager.dismissBulletin()
            if success {
                
                print("Add Device with ID: \(deviceCredential?.deviceID ?? "") and token: \(deviceCredential?.accessToken ?? "")")
                let rolleyParams = SCDeviceSettingsParams(lastBattery: nil, lastSync: nil, isShared: false, lastUpdateCheck: nil, isFirstTime: true, key: key, colorInfo: typeInfo!, deviceNumber: (self?.getDeviceNumber())!)
                let device = SCDevice(credential: deviceCredential!, btIdentifier: peripheral!.identifier, name: deviceAddress , vinCode: deviceAddress,type: self!.typeSelected, isOwner: true, deviceParams: rolleyParams)
                device.storeNewDevice()
                //show the dashboard
                self?.showDashboardForDevice(device: device, with: typeInfo!, peripheral: peripheral!)
            }
            else{
                if let errorDesc = errorDescription {
                    if errorDesc == "deviceAlreadyRegistered".localized() {
                        let item =  BoardUi.showNotYourDevice(deviceType: self!.typeSelected)
                        self?.deviceScanManager = BoardUi.getDeviceManger(item: item)
                        self?.deviceScanManager.showBulletin(above: self!)
                    }else if errorDesc == "stolenDevice" {
                        let stolenDeviceVC = StolenDeviceViewController()
                        let navVC = UINavigationController(rootViewController: stolenDeviceVC)
                        navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        navVC.modalTransitionStyle = .crossDissolve
                        self!.present(navVC, animated: true)
                        stolenDeviceVC.setMacForDeviceToConnect(deviceAddress: deviceAddress, peripheral: peripheral!)
                        
                    }else if errorDesc == "wrongDevice"{
                        let item =  BoardUi.showWrongDeviceActivate(deviceType: self!.typeSelected)
                        self?.deviceScanManager = BoardUi.getDeviceManger(item: item)
                        self?.deviceScanManager.showBulletin(above: self!)
                    }
                    else{
                        self?.showAlert(title: "addDeviceError".localized(), message: errorDesc.localized())
                    }
                }
                else {
                    self?.showAlert(title: "addDeviceError", message: "UNKNOWN_ERROR".localized())
                }
            }
        }
    }
    
    private func getDeviceNumber()-> Int{
        var deviceNumber = 0
        guard let rolleyCounter = UserDefaults.standard.stringArray(forKey: ContantsKey.prouctRolleyCounter.rawValue) else{ return 1 }
        
        for vincode in rolleyCounter {
            let device = SCDevice.reStore(vinCode: vincode)
            if device!.isOwner{
                deviceNumber += 1
            }
        }
        
        return deviceNumber
    }
    
    private func showDashboardForDevice(device: SCDevice?, with typeInfo:String, peripheral: CBPeripheral) {
        if typeInfo.contains("RL") || typeInfo.contains("RP"){
            let tabBarController = SCTabBarController(with: "Rolley", for: device, peripheral: peripheral , deviceType: .SCDeviceTypeRolley, deviceListController: self.navigationController?.viewControllers.first as? DeviceSelectionViewController)
            self.present(tabBarController, animated: true, completion: nil)
        }else{
            let tabBarController = SCTabBarController(with: "ELF", for: device, peripheral: peripheral,deviceType: .SCDeviceTypeELF, deviceListController:  self.navigationController?.viewControllers.first as? DeviceSelectionViewController)
            self.present(tabBarController, animated: true, completion: nil)
        }
    }
    
    
    //dialog Cancel action
    func cancelButtonTapped() {
        scanTimer.invalidate()
        //todo check if action taken on rolley
        if deviceScanManager != nil {
            deviceScanManager.dismissBulletin()
        }
        PLBleService.getInstance()?.scanDevices(false)
        PLBleService.getInstance()?.ble_cb = nil
    }
    /**
     Run timer for device lookup
     */
    private func runTimer(devicetype: Int) {
        scanTimer = Timer.scheduledTimer(withTimeInterval: 30, repeats: false) { [weak self] (timer) in
            if self?.deviceScanManager != nil{
                self?.deviceScanManager.push(item: BoardUi.showDeviceNotFound(deviceType: devicetype))
            }
            if  PLBleService.getInstance() != nil {
                PLBleService.getInstance()?.scanDevices(false)
            }
            
        }
    }
    
    
    
    
}
