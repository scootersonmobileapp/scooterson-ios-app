//
//  DeviceSelectionViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/19/19.
//  Copyright © 2019 Scooterson. All rights reserved.
//

import UIKit
import CoreBluetooth
import SwiftTheme
import BLTNBoard


enum SCDeviceType: Int {
    case SCDeviceTypeRolley
    case SCDeviceTypeELF
    case SCDeviceTypeBackPack
}

struct RawDevice {
    var peripheral: CBPeripheral?
    var scDevice: SCDevice
    var deviceType: Int
    var deviceParams: SCDeviceSettingsParams
}

class DeviceSelectionViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, BTElfServiceDelegate, PLBleCallback,BroadUIDelegate {
    
    private var devicesData: [RawDevice] = []
    private var selectedIndexPath: IndexPath!
    private var scanTimer: Timer!
    private var tryToActivate: Bool!
    @IBOutlet var devicesTableView: UITableView!
    @IBOutlet var authButton: UIButton!
    @IBOutlet var rolleyImg: UIImageView!
    private var isScanning: Bool!
    private var isPeripheralConnected: Bool!
    var btServices: BTElfService!
    private var buttonView: UIView!
    private var refreshControl: UIRefreshControl!
    private var scanPage: FeedbackPageBLTNItem!
    private var deviceScanManager: BLTNItemManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isScanning = false
        isPeripheralConnected = false
        setupViews()
        
        //Rolley setup
        PLBleService.getInstance()?.ble_cb = self
        // add notification observers
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    func cancelButtonTapped() {
        scanTimer.invalidate()
        //todo check if action taken on rolley
        if deviceScanManager != nil {
            deviceScanManager.dismissBulletin()
        }
        isScanning = false
        PLBleService.getInstance()?.scanDevices(false)
        PLBleService.getInstance()?.ble_cb = nil
    }
    
    private func addDevicesToTable() {
        devicesData.removeAll()
        
        guard let rolleyCounter = UserDefaults.standard.stringArray(forKey: ContantsKey.prouctRolleyCounter.rawValue) else{ return }
        
        for vincode in rolleyCounter {
            print("Device found with code \(vincode)")
            let device = SCDevice.reStore(vinCode: vincode)
            let deviceParameters = device?.deviceParams
            devicesData.append(RawDevice(peripheral: nil,scDevice: device!, deviceType: device!.type, deviceParams: deviceParameters!))
        }
        
        self.devicesTableView.reloadData()
        
    }
    
    @objc func didEnterBackground() {
        print("device did enter background")
        if  PLBleService.getInstance() != nil {
            PLBleService.getInstance()?.scanDevices(false)
        }
    }
    
    @objc func didBecomeActive() {
        print("device did become active")
        addDevicesToTable()
        getSharedDevice()
        checkForAppVersion()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("device view will appear")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        addDevicesToTable()
        getSharedDevice()
        checkForAppVersion()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if scanTimer != nil {
            scanTimer.invalidate()
        }
        if deviceScanManager != nil && deviceScanManager.isShowingBulletin {
            deviceScanManager.dismissBulletin()
        }
        PLBleService.getInstance()?.ble_cb = nil
    }
    
    
    private func runTimer(devicetype: Int) {
        scanTimer = Timer.scheduledTimer(withTimeInterval: 20, repeats: false) { [weak self] (timer) in
            if self?.deviceScanManager != nil{
                self?.deviceScanManager.push(item: BoardUi.showDeviceNotFound(deviceType: devicetype))
            }
            
            if  PLBleService.getInstance() != nil {
                PLBleService.getInstance()?.scanDevices(false)
            }
            if self?.btServices != nil {
                self?.btServices.manager.stopScan()
                self?.isScanning = false
                self!.isScanning = false
                self?.showAlert(title: "deviceScanCompletedTitle".localized(), message: "deviceScanCompletedELF".localized())
            }
        }
    }
    
    final func setupCells(tag: Int, device: RawDevice) -> SCTableViewCell {
        let deviceCell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        deviceCell.isTapable = true
        deviceCell.selectionStyle = .none
        
        //Cell componets
        let deviceImageView = UIImageView()
        deviceCell.contentView.addSubview(deviceImageView)
        deviceImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(136.0)
            make.centerY.equalTo(deviceCell.contentView)
            make.left.equalTo(deviceCell.contentView).offset(40)
        }
        
        deviceImageView.image = UIImage(named:  device.deviceParams.colorInfo)
        
        let deviceName = UILabel()
        deviceName.text = (device.scDevice.name != "nil" && device.scDevice.name?.count != 0) ? device.scDevice.name :  "My"
        deviceName.font = .light(size: 18)
        deviceName.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        deviceName.textAlignment = .left
        deviceCell.contentView.addSubview(deviceName)
        deviceName.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(20.0)
            make.right.equalTo(deviceCell.contentView).offset(-10.0)
            make.top.equalTo(deviceCell.contentView).offset(20.0)
            make.height.equalTo(30.0)
        }
        
        let deviceTypeImageView = UIImageView()
        deviceCell.contentView.addSubview(deviceTypeImageView)
        deviceTypeImageView.contentMode = .center
        deviceTypeImageView.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(20.0)
            make.top.equalTo(deviceName.snp.bottom)
            
        }
        if(device.deviceType == ConstantValues.PRODUCT_TYPE_ELF){
            deviceTypeImageView.theme_image = ["ELFDarkLogo","ELFLogo"]
        }else if (device.deviceType == ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS){
            deviceTypeImageView.theme_image = ["Rolley+white","Rolley+"]
        }else{
            deviceTypeImageView.theme_image = ["RolleyDarkLogo","RolleyLogo"]
        }
        
        
        
        let deviceBattery = UILabel()
        let batteryValue = device.deviceParams.lastBattery != nil ? "\(device.deviceParams.lastBattery!)%" : "NA"
        print("device is battery: \(batteryValue)")
        deviceBattery.text = "\("deviceBatteryInfo".localized()) \(batteryValue)"
        deviceBattery.font = .light(size: 18)
        deviceBattery.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        deviceBattery.textAlignment = .left
        deviceCell.contentView.addSubview(deviceBattery)
        deviceBattery.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(20.0)
            make.top.equalTo(deviceTypeImageView.snp.bottom)
            make.height.equalTo(30.0)
        }
        let deviceSharing = UILabel()
        print("device is shared: \(device.deviceParams.isShared)")
        let deviceSharingInfo = device.deviceParams.isShared == true ? "ON" : "OFF"
        deviceSharing.text = "\("deviceSharingInfo".localized()) \(deviceSharingInfo)"
        deviceSharing.font = .light(size: 18)
        deviceSharing.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        deviceSharing.textAlignment = .left
        deviceCell.contentView.addSubview(deviceSharing)
        deviceSharing.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(20.0)
            make.top.equalTo(deviceBattery.snp.bottom).offset(-10)
            make.height.equalTo(30.0)
        }
        let deviceLastSync = UILabel()
        let syncDay = getLastSync(lastTime: device.deviceParams.lastSync)
        deviceLastSync.text = "\("deviceLastSync".localized()) \(syncDay)"
        deviceLastSync.font = .light(size: 18)
        deviceLastSync.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        deviceLastSync.textAlignment = .left
        deviceCell.contentView.addSubview(deviceLastSync)
        deviceLastSync.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(20.0)
            make.top.equalTo(deviceSharing.snp.bottom).offset(-10)
            make.height.equalTo(30.0)
        }
        // Show button for sharing settings
        if(device.deviceParams.isKeyFeatureEnabled && device.scDevice.isOwner){
            let sharingSettingButton = UIButton()
            sharingSettingButton.backgroundColor = .dodgerBlue
            sharingSettingButton.setTitleColor(.white, for: .normal)
            sharingSettingButton.setTitleColor(.charcoalGrey, for: .highlighted)
            sharingSettingButton.layer.cornerRadius = 10.0
            sharingSettingButton.titleLabel?.font = .light(size: 18)
            sharingSettingButton.titleEdgeInsets = UIEdgeInsets.init(top:10, left: 10 ,bottom: 10 , right: 10 )
            sharingSettingButton.tag = tag
            sharingSettingButton.setTitle("sharing".localized(), for: .normal)
            sharingSettingButton.addTarget(self, action: #selector(openSharingScreen), for: .touchUpInside)
            deviceCell.contentView.addSubview(sharingSettingButton)
            sharingSettingButton.snp.makeConstraints { (make) in
                make.left.equalTo(deviceImageView.snp.right).offset(20.0)
                make.top.equalTo(deviceLastSync.snp.bottom).offset(10)
                make.bottom.equalTo(deviceCell.contentView).offset(-20)
                make.height.equalTo(40.0)
                make.width.equalTo(80.0)
                
            }
        }
        
        return deviceCell
    }
    @objc private func openSharingScreen(sender:UIButton) {
        let tag = sender.tag
        print("Device open sharing Screen with Tag \(tag)")
        let sharingVC = RolleySharingSettingsViewController()
        let backItem = UIBarButtonItem()
        backItem.title = "devices".localized()
        backItem.tintColor = .dodgerBlue
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(sharingVC, animated: true)
        sharingVC.setDeviceInfoWhenLauchFromDevice(device: self.devicesData[tag].scDevice)
    }
    
    private func setBottomButtonView() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 90))
        let addDeviceButton = UIButton(type: .custom)
        addDeviceButton.backgroundColor = .dodgerBlue
        addDeviceButton.setTitleColor(.white, for: .normal)
        addDeviceButton.setTitleColor(.charcoalGrey, for: .highlighted)
        addDeviceButton.layer.cornerRadius = 10.0
        addDeviceButton.setTitle("addDevice".localized(), for: .normal)
        addDeviceButton.addTarget(self, action: #selector(addDeviceScreen), for: .touchUpInside)
        view.addSubview(addDeviceButton)
        addDeviceButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.top.equalTo(view).offset(20)
            
        }
        buttonView = view
        
    }
    
    @objc private func addDeviceScreen() {
        let addDeviceVC = AddDeviceViewController()
        let backItem = UIBarButtonItem()
        backItem.title = "devices".localized()
        backItem.tintColor = .dodgerBlue
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(addDeviceVC, animated: true)
    }
    
    private func setupViews() {
        let headerView = setupTitleView()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        self.navigationItem.title = ""
        
        setBottomButtonView()
        self.view.addSubview(buttonView)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)),for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.pastelOrange
        
        devicesTableView = UITableView()
        devicesTableView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        devicesTableView.theme_separatorColor =  [UIColor.paleGrey.toHexString(), UIColor.charcoalGrey.toHexString()]
        devicesTableView.dataSource = self
        devicesTableView.delegate = self
        devicesTableView.separatorInset = .zero
        devicesTableView.addSubview(refreshControl)
        self.view.addSubview(devicesTableView)
        devicesTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.bottom.equalTo(buttonView.snp.top).offset(10)
            make.top.equalTo((headerView?.snp.bottom)!)
        }
        
        buttonView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.safeAreaLayoutGuide).offset(20.0)
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            if(isSmallScreen()){
                make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-30.0)
            }else{
                make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-10.0)
            }
            
            make.height.equalTo(60.0)
            
        }
        
        devicesTableView.reloadData()
        
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getSharedDevice()
        
    }
    
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    private func setupTitleView() -> UIView? {
        let titleView = UIView()
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 30)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = "devices".localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    
    
    private func getLastSync(lastTime: Double) ->String{
        var lastSyncTime = "never".localized()
        if(lastTime == 0) {
            return lastSyncTime
        }
        let differMilliSeconds =  Date().currentTimeMillis() -   lastTime
        let diffDays  = Int64(differMilliSeconds) / (24 * 60 * 60)
        print(" differnce time \(diffDays)")
        switch diffDays {
        case 0:
            lastSyncTime = "today".localized()
        case 1:
            lastSyncTime = "yesterday".localized()
        default:
            lastSyncTime = "\(diffDays) \("daysAgo".localized())"
        }
        
        return lastSyncTime
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard  !devicesData[indexPath.row].scDevice.isOwner else { /* Handle nil case */
            if  devicesData[indexPath.row].deviceParams.isKeyFeatureEnabled {
                return 230.0
            }else{
                return 180.0
            }
        }
        
        return 180.0
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect.zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devicesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return setupCells(tag: indexPath.row, device: devicesData[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        selectedIndexPath = indexPath
        let rawDevice = devicesData[indexPath.row]
        switch rawDevice.deviceType {
        case ConstantValues.PRODUCT_TYPE_ROLLEY:
            
            self.runTimer(devicetype: ConstantValues.PRODUCT_TYPE_ROLLEY)
            scanPage = BoardUi.showDeviceConnectIntro(deviceType: ConstantValues.PRODUCT_TYPE_ROLLEY, delegate: self)
            self.deviceScanManager = BoardUi.getDeviceManger(item: BoardUi.showScanBroadPage(deviceType: ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS, delegate: self))
            deviceScanManager.showBulletin(above: self)
            PLBleService.getInstance()?.ble_cb = self
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                PLBleService.getInstance()?.scanDevices(true)
            }
            
        case ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS:
            
            if !rawDevice.scDevice.isOwner && !rawDevice.deviceParams.isShared{
                //Master switched off the sharing show dialog to request
                showAlertForSharingSwitchedOffFromMaster(selectedDeviceID: rawDevice.scDevice.credential!.deviceID, name: rawDevice.scDevice.name!)
                return
            }else if !rawDevice.scDevice.isOwner{
                scanPage = BoardUi.showLocationIntro(name: rawDevice.scDevice.name!,deviceType: ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS, delegate: self)
                self.deviceScanManager = BoardUi.getDeviceManger(item: scanPage)
                deviceScanManager.showBulletin(above: self)
            }else {
                scanPage = BoardUi.showDeviceConnectIntro(deviceType: ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS, delegate: self)
                self.deviceScanManager = BoardUi.getDeviceManger(item: BoardUi.showScanBroadPage(deviceType: ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS, delegate: self))
                deviceScanManager.showBulletin(above: self)
                otherActionButtonTapped()
            }
            
        case ConstantValues.PRODUCT_TYPE_ELF:
            #if DEBUG
            runTimer(devicetype: ConstantValues.PRODUCT_TYPE_ELF)
            //activateDeviceAPICallForMCUID(mcuid: "95277890", peripheral: nil)
            #else
            btServices = BTElfService.shared
            btServices.delegate = self
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: { [weak self] in
                if self?.btServices.manager.state == .poweredOn {
                    if !self!.isScanning {
                        self!.isScanning = true
                        self?.runTimer(devicetype: ConstantValues.PRODUCT_TYPE_ELF)
                        self?.btServices.manager.scanForPeripherals(withServices: nil, options: nil)
                    }
                }
                else {
                    self?.showAlertWithButton(title: "Bluetooth Error", message: "Scooterson app will required bluetooth, to connect with \"ELF\"", actionButtoTitle: "turnOn".localized())
                }
            })
            #endif
            
        default:
            break
        }
    }
    /**
     Function been called from two place, Bulletin board sharing location.
     */
    func otherActionButtonTapped(){
        runTimer(devicetype: ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS)
        PLBleService.getInstance()?.ble_cb = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PLBleService.getInstance()?.scanDevices(true)
        }
        
    }
    
    
    func onDeviceFound(_ peripheral: CBPeripheral!, data mData: [String : Any]!, rssi: Int32) {
        if((PLBleService.getInstance()?.isBafangCan(mData)) != false){
            //Device found
            print("Rolley found with following param", peripheral.identifier, peripheral.name as Any)
            let device = self.devicesData[(self.selectedIndexPath.row)].scDevice
            self.devicesData[(self.selectedIndexPath.row)].peripheral = peripheral
            let rolleyInfo: DeviceInfo = DeviceInfo(peripheral, data: mData, rssi: rssi)
            if((device.credential?.accessToken.isEmpty) == true){//Check if we need to fetch the token for device in case of re registeration
                //Check for internet
                if !Reachability.isConnectedToInternet{
                    self.showAlert(title: "noNetwork".localized(), message: "noNetworkBodyDeviceFeteched".localized())
                    return
                }
                fetchDeviceCreds(device: device)
            }else if (PLBleService.getInstance() != nil){
                self.deviceScanManager.dismissBulletin()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    //check the rolley found is the samwe which user is trying to connect with.
                    if(rolleyInfo.btAddr == device.vinCode){
                        self.showDashboardForDevice(with: (self.devicesData[(self.selectedIndexPath.row)].deviceType), device: device )
                    }else{
                        let item =  BoardUi.showCannotConnectWithDevice(deviceType: device.type, colorType: device.deviceParams!.colorInfo)
                        self.deviceScanManager = BoardUi.getDeviceManger(item: item)
                        self.deviceScanManager.showBulletin(above: self)
                    }
                }
                
            }
            if self.scanTimer != nil{
                scanTimer.invalidate()
            }
            PLBleService.getInstance()?.scanDevices(false)
            PLBleService.getInstance()?.ble_cb = nil
        }
        
    }
    
    
    func showAlertWithButton(title: String, message: String, actionButtoTitle: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "cancel".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let actionTurnOn: UIAlertAction = UIAlertAction.init(title: actionButtoTitle, style: .default) { (UIAlertAction) in
            let url = URL(string: "App-Prefs:root=General")
            let app = UIApplication.shared
            app.open(url!, options: [:], completionHandler: nil)
        }
        alert.addAction(action)
        alert.addAction(actionTurnOn)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func fetchDeviceCreds(device: SCDevice){
        let deviceID = device.credential?.deviceID
        API.shared.getDeviceAcessToken(deviceID: deviceID!) { (success, token ,statusCode) in
            self.deviceScanManager.dismissBulletin()
            if success{
                device.credential?.accessToken = token!
                device.update()
                print("Device current state: \(String(describing: device.credential?.accessToken))")
                self.showDashboardForDevice(with: (self.devicesData[(self.selectedIndexPath.row)].deviceType), device: device)
            }else{
                self.showAlert(title: "addDeviceError".localized(), message: API.shared.errorDictionary[statusCode]!)
            }
        }
    }
    
    private func showDashboardForDevice(with deviceType: Int, device: SCDevice) {
        #if targetEnvironment(simulator)
        let tstDevice = SCDevice(credential: nil, btIdentifier: nil, typeInfo: "ELF", deviceAddress: "123456789EF")
        let tabBarController = SCTabBarController(with: "ELF", for: tstDevice, peripheral: nil, deviceType: devices[selectedIndexPath.row].deviceType!)
        self.present(tabBarController, animated: true, completion: nil)
        #else
        if deviceType == ConstantValues.PRODUCT_TYPE_ROLLEY || deviceType == ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS {
            let tabBarController = SCTabBarController(with: "Rolley", for: device , peripheral: self.devicesData[selectedIndexPath.row].peripheral!, deviceType: .SCDeviceTypeRolley, deviceListController: self)
            self.present(tabBarController, animated: true, completion: nil)
        }
        else {
            let tabBarController = SCTabBarController(with: "Elf", for: device, peripheral: self.devicesData[selectedIndexPath.row].peripheral!,deviceType: .SCDeviceTypeELF, deviceListController: self)
            self.present(tabBarController, animated: true, completion: nil)
        }
        #endif
    }
    // MARK: BTServiceDelegate
    func deviceConnected(peripheral: CBPeripheral) {
        var rawDevice = devicesData[selectedIndexPath.row]
        rawDevice.peripheral = peripheral
        isPeripheralConnected = true
        if scanTimer != nil {
            scanTimer.invalidate()
        }
        btServices.manager.stopScan()
        isScanning = false
        if let device = SCDevice.reStore(vinCode: rawDevice.scDevice.vinCode) {
            loaderHide()
            //            scanTimer.invalidate()
            //            btServices.manager.stopScan()
            //            isScanning = false
            tryToActivate = false
            //            if device.btIdentifier == peripheral.identifier {
            showDashboardForDevice(with: ConstantValues.PRODUCT_TYPE_ELF, device: device)
            //            }
            //            else {
            //                showAlert(title: "Error", message: "This device wasn't activated")
            //            }
        }
        else {
            tryToActivate = true
        }
    }
    
    func didGetMCUID(mcuid: String, for peripheral: CBPeripheral) {
        if tryToActivate {
            //activateDeviceAPICallForMCUID(mcuid: mcuid, peripheral: peripheral)
        }
    }
    
    func deviceDisconnected(peripheral: CBPeripheral) {
        print("Disconnected")
    }
    
    func managerDidChangeState(state: CBManagerState) {
        if state == .poweredOn {
            if !isScanning {
                runTimer(devicetype: ConstantValues.PRODUCT_TYPE_ELF)
                isScanning = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.btServices.manager.scanForPeripherals(withServices: nil, options: nil)
                })
            }
        }
        else {
            showAlertWithButton(title: "Bluetooth Error", message: "Scooterson app will required bluetooth, to connect with \"ELF\"", actionButtoTitle: "turnOn".localized())
        }
    }
    
    func appDisconneted(deviceType: Int){
        let deviceType = deviceType == ConstantValues.PRODUCT_TYPE_ROLLEY ? "Rolley" : "Rolley+"
        let messageBody = String(format: NSLocalizedString("displayOffMessageBody", comment: ""), arguments: [deviceType])
        showAlert(title: "displayOffTitle".localized(), message: messageBody)
    }
    
    private func getSharedDevice(){
        refreshControl.beginRefreshing()
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        API.shared.getSharedDevices(){[weak self](success, errorCode, sharedDevices) in
            self!.refreshControl.endRefreshing()
            
            if let rolleyCounter = UserDefaults.standard.stringArray(forKey: ContantsKey.prouctRolleyCounter.rawValue) {
                for vincode in rolleyCounter {
                    let device = SCDevice.reStore(vinCode: vincode)
                    let shareDevice = SharedDevice(deviceId: "", accessToken: "", keyFeaturePin: "", deviceType: "", deviceName: vincode,deviceOwnerCustomerUserName: "", status: 1)
                    if(!success && !device!.isOwner){
                        SCDevice.reMove(vinCode:vincode)
                    }else if !device!.isOwner && !(sharedDevices?.contains(shareDevice))!{
                        print("Device is removing \(vincode)")
                        SCDevice.reMove(vinCode:vincode)
                    }
                }
            }
            
            if success{
                for device in sharedDevices!{
                    let deviceFound = SCDevice.reStore(vinCode: device.deviceName)
                    if(deviceFound == nil){
                        let deviceCred = SCDeviceCredential(deviceID: device.deviceId, accessToken: device.accessToken)
                        let deviceSettingsParams = SCDeviceSettingsParams(lastBattery: nil, lastSync: nil, isShared:  device.status == 1, lastUpdateCheck: nil, isFirstTime: false, key: device.keyFeaturePin, colorInfo: device.deviceType, deviceNumber: 0)
                        let deviceObj = SCDevice(credential: deviceCred, btIdentifier: nil, name: device.deviceOwnerCustomerUserName , vinCode: device.deviceName, type: ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS, isOwner: false, deviceParams: deviceSettingsParams)
                        deviceObj.storeNewDevice()
                    }else{
                        //if device already there we will update the key and the token as this might changed
                        deviceFound?.deviceParams?.isShared = device.status == 1 // if shared is enabled from the master on his switch
                        deviceFound?.name = device.deviceOwnerCustomerUserName
                        deviceFound?.credential?.accessToken = device.accessToken
                        deviceFound?.deviceParams?.key = device.keyFeaturePin
                        deviceFound?.update()
                    }
                }
                
                
                self!.addDevicesToTable()
            } else if errorCode == 10 {
                // Device reset move to loginScreen
                self?.dismiss(animated: true, completion: {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "mainVC")
                    appDelegate.window?.rootViewController = UINavigationController(rootViewController: initialViewController)
                })
            }
            
        }
        addDevicesToTable()
    }
    
    private func showAlertForSharingSwitchedOffFromMaster(selectedDeviceID: String, name: String){
        let messageBody = String(format: NSLocalizedString("masterSharingTurnedOffMessage", comment: ""), arguments: [name])
        let alert: UIAlertController = UIAlertController(title: "masterSharingTurnedOffTitle".localized(), message: messageBody, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "okay".localized(), style: .cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let actionAsk: UIAlertAction = UIAlertAction.init(title: "aksMaster".localized(), style: .default) { (UIAlertAction) in
            self.askMaster(selectedDeviceID: selectedDeviceID, name: name)
        }
        
        alert.addAction(action)
        alert.addAction(actionAsk)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func askMaster(selectedDeviceID: String, name: String){
        API.shared.sharingAgainRequest(deviceId: selectedDeviceID) { (isDone) in
            if isDone{
                let messageBody = String(format: NSLocalizedString("requestSentMessage", comment: ""), arguments: [name])
                self.showToast(title:  "requestsent".localized(), message: messageBody)
            }else{
                let items =  ["reEnableSharingMasterRequestMessage".localized()]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                self.present(ac, animated: true)
            }
        }
    }
}

extension UIImage {
    func maskWithColor( color:UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        color.setFill()
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        let rect = CGRect(x: 0.0, y: 0.0, width: self.size.width, height: self.size.height)
        context.draw(self.cgImage!, in: rect)
        context.setBlendMode(CGBlendMode.sourceIn)
        context.addRect(rect)
        context.drawPath(using: CGPathDrawingMode.fill)
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()        
        return coloredImage!
    }
    
    func imageByMakingWhiteBackgroundTransparent() -> UIImage? {
        
        guard let image = UIImage(data: self.jpegData(compressionQuality: 1.0)!) else { return nil }
        let rawImageRef: CGImage = image.cgImage!
        let colorMasking: [CGFloat] = [222, 255, 222, 255, 222, 255]
        UIGraphicsBeginImageContext(image.size);
        
        let maskedImageRef = rawImageRef.copy(maskingColorComponents: colorMasking)
        UIGraphicsGetCurrentContext()?.translateBy(x: 0.0,y: image.size.height)
        UIGraphicsGetCurrentContext()?.scaleBy(x: 1.0, y: -1.0)
        UIGraphicsGetCurrentContext()?.draw(maskedImageRef!, in: CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return result
        
    }
}

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}
