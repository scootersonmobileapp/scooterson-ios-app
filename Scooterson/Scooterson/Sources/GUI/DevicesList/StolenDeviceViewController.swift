//
//  StolenDeviceViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 7/28/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import CocoaMQTT
import CoreLocation
import ZendeskCoreSDK
import SupportProvidersSDK

class StolenDeviceViewController: BaseViewController, RolleyBleDelegate, CLLocationManagerDelegate{
    
    private var deviceAddress: String!
    private var rolleyService: RolleyBleService!
    private var peripheral: CBPeripheral!
    private var mqttSC: CocoaMQTT!
    private var locationManager: CLLocationManager!
    private static var isLocationSent = false
    private var keyCreated = "0"
    private var isSet = false
    
    public func setMacForDeviceToConnect(deviceAddress: String, peripheral: CBPeripheral){
        self.deviceAddress = deviceAddress
        self.peripheral = peripheral
    }
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.rolleyService = RolleyBleService()
        getDeviceToken()
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        self.navigationItem.title = "Location needed"
        setupLocationPermissionView()
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    @objc func didBecomeActive() {
        print("Stolen did become active again")
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        rolleyService.disconnect()
        super.viewWillDisappear(animated)
    }
    
    // MARK: Location delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        sendTelemetryForStolenScooterWithLocation(latitude: (manager.location?.coordinate.latitude)!, longitude: (manager.location?.coordinate.longitude)!)
        if self.isSet {
            sendTelemetryLock()
        }
        print("Stolen location updated")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            manager.startUpdatingLocation()
            setupView()
        }
        else {
            if status != .notDetermined{
                showAlertForPermissionNotFound(title: "enableLocationTitle".localized(), message: "enableLocationMessage".localized(), actionButtoTitle: "enable".localized())
            }
        }
    }
    
    
    private func setupLocationPermissionView(){
        let infoView = setupInfoView(title: "enableLocationMessage".localized(), image: UIImage(named: "location_permission")!, textColor: nil)
        infoView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        self.view.addSubview(infoView)
        infoView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.top.equalTo(view).offset(5.0)
            make.height.equalTo(view)
        })
        let locationPersmissionButtton = UIButton(type: .custom)
        locationPersmissionButtton.backgroundColor = .dodgerBlue
        locationPersmissionButtton.setTitleColor(.white, for: .normal)
        locationPersmissionButtton.layer.cornerRadius = 10.0
        locationPersmissionButtton.setTitle("enableLocation".localized(), for: .normal)
        locationPersmissionButtton.addTarget(self, action: #selector(enableLocationAction), for: .touchUpInside)
        self.view.addSubview(locationPersmissionButtton)
        locationPersmissionButtton.snp.makeConstraints { (make) in
            make.left.equalTo(infoView).offset(20)
            make.right.equalTo(infoView).offset(-20)
            make.height.equalTo(50)
            make.bottom.equalTo(view.snp.bottom).offset(-40)
        }
    }
    @objc private func enableLocationAction(){
        let url = URL(string: UIApplication.openSettingsURLString)
        let app = UIApplication.shared
        app.open(url!, options: [:], completionHandler: nil)
    }
    
    
    private func setupView(){
        self.view.removeAllSubviews()
        let headerView = setupTitleView()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        self.navigationItem.title = ""
        
        let infoView = setupInfoView(title: "stolenWarning".localized(), image: UIImage(named: "RLOR")!, textColor: UIColor.red)
        self.view.addSubview(infoView)
        infoView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.top.equalTo((headerView?.snp.bottom)!).offset(5.0)
            make.height.equalTo(view)
        })
        
        let contactSupportButtton = UIButton(type: .custom)
        contactSupportButtton.backgroundColor = .dodgerBlue
        contactSupportButtton.setTitleColor(.white, for: .normal)
        contactSupportButtton.setTitleColor(.charcoalGrey, for: .highlighted)
        contactSupportButtton.layer.cornerRadius = 10.0
        contactSupportButtton.setTitle("stolenButttonText".localized(), for: .normal)
        contactSupportButtton.addTarget(self, action: #selector(contactSupportAction), for: .touchUpInside)
        self.view.addSubview(contactSupportButtton)
        contactSupportButtton.snp.makeConstraints { (make) in
            make.left.equalTo(infoView).offset(20)
            make.right.equalTo(infoView).offset(-20)
            make.height.equalTo(50)
            make.bottom.equalTo(view.snp.bottom).offset(-40)
        }
        
    }
    
    
    private func setupTitleView() -> UIView? {
        let titleView = UIView()
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 30)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .center
        titleLabel.text = "stolenRolley".localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    private func setupInfoView(title: String, image: UIImage, textColor: UIColor?) -> UIView{
        let infoView = UIView()
        infoView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        let imageView =  UIImageView()
        imageView.image = image
        infoView.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(infoView)
            make.top.equalTo(infoView).offset(20.0)
            make.height.equalTo(200)
            make.width.equalTo(200)
        }
        
        let settingHeadingLabel = UILabel()
        settingHeadingLabel.font = .bold(size: 18)
        if textColor != nil{
            settingHeadingLabel.textColor = textColor
        }else{
            settingHeadingLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        }
        settingHeadingLabel.textAlignment = .center
        settingHeadingLabel.numberOfLines = 4
        settingHeadingLabel.text = title
        infoView.addSubview(settingHeadingLabel)
        settingHeadingLabel.snp.makeConstraints { (make) in
            make.left.equalTo(infoView).offset(20)
            make.right.equalTo(infoView).offset(-20)
            make.centerX.equalTo(infoView)
            make.top.equalTo(imageView.snp.bottom).offset(20.0)
        }
        
        
        return infoView
        
    }
    
    @objc private func contactSupportAction(){
        createTicketForStolenDevice()
    }
    
    func deviceStatus(status: Int32) {
        switch (status) {
        case PL_BLE_STATUS_READY:
            print("Stolen device set lock")
            let randomInt = Int.random(in: 1000...9999)
            print("Stolen device Pin created: \(randomInt)")
            keyCreated = String(randomInt)
            let makekey  = "0000\(randomInt)"
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.rolleyService.sendPinCommandToRolley(command: APPController_SetPIN, pin: makekey)
            }
            break
        default:
            break
            
        }
    }
    func onInfoReceived(command: Int32, value: String!) {
        switch command {
        case EBoxInfPASLevel:
            sendTelemetryForStolenScooter()
        case APPController_SetPIN:
            if value == "1"{
                print("Stolen key is set \(value!)")
                self.isSet = true
                self.sendTelemetryLock()
            }
        default:
            break
        }
        
    }
    
    
    private func getDeviceToken(){
        //todo handle no connectivity
        let tbPhoneDeviceId = UserDefaults.standard.string(forKey: ContantsKey.tbPhoneDeviceId.rawValue)
        API.shared.getDeviceAcessToken(deviceID: tbPhoneDeviceId!) {[weak self] (success,token ,code) in
            if success {
                print("Stolen device phone token found \(String(describing: token))")
                self!.doMqtt(accessToken: token!)
            }
        }
        
    }
    
    private func doMqtt(accessToken: String){
        let clientID = "ScootersoniOSApp-" + String(ProcessInfo().processIdentifier)
        API.shared.mqtt = CocoaMQTT(clientID: clientID, host: "tb.scooterson.com", port: 1883)
        self.mqttSC = API.shared.mqtt
        mqttSC.username = accessToken
        mqttSC.password = ""
        mqttSC.keepAlive = 60
        if mqttSC.connect() {
            print("Stolen MQTT connected")
            //send data to the TB about the stolen scooter
            sendTelemetryForStolenScooter()
        }
    }
    
    private func sendTelemetryLock(){
        if(mqttSC != nil  && mqttSC.connState == .connected){
            let jsonObject  = [MQTTVariable.lock.rawValue : keyCreated]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(jsonObject) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print("Stolen Mqtt: \(jsonString)")
                    mqttSC.publish("v1/devices/me/attributes", withString: jsonString, qos: .qos1)
                }
            }
        }
    }
    private func sendTelemetryForStolenScooter(){
        let phoneNumber = SCUser.reStore()?.phone
        if(mqttSC != nil  && mqttSC.connState == .connected){
            let jsonObject  = [MQTTVariable.isStolen.rawValue : "true", MQTTVariable.deviceAddress.rawValue: deviceAddress, MQTTVariable.phoneNumber.rawValue: phoneNumber]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(jsonObject) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print("Stolen Mqtt: \(jsonString)")
                    mqttSC.publish("v1/devices/me/telemetry", withString: jsonString, qos: .qos1)
                }
            }
        }
    }
    private func sendTelemetryForStolenScooterWithLocation(latitude: Double, longitude: Double){
        let phoneNumber = SCUser.reStore()?.phone
        if(mqttSC != nil  && mqttSC.connState == .connected && !StolenDeviceViewController.isLocationSent){
            let jsonObject  = [MQTTVariable.isStolen.rawValue : "true", MQTTVariable.deviceAddress.rawValue: deviceAddress, MQTTVariable.phoneNumber.rawValue: phoneNumber, MQTTVariable.latitude.rawValue: String(latitude), MQTTVariable.longitude.rawValue: String(longitude), "notify": "true"]
            let encoder = JSONEncoder()
            if let jsonData = try? encoder.encode(jsonObject) {
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    print("Stolen Mqtt: \(jsonString)")
                    mqttSC.publish("v1/devices/me/telemetry", withString: jsonString, qos: .qos1)
                    self.locationManager.stopUpdatingLocation()
                    StolenDeviceViewController.isLocationSent = true
                }
            }
        }
    }
    
    private func createTicketForStolenDevice(){
        let phoneNumber = SCUser.reStore()?.phone
        self.loaderShow(withDelegate: nil)
        let identity =  Identity.createAnonymous(name: phoneNumber, email: nil)
        Zendesk.instance?.setIdentity(identity)
        let requestProvider = ZDKRequestProvider()
        let request = ZDKCreateRequest()
        request.subject = "Request to unblock disable rolley"
        request.requestDescription = "User trigger this request to unblock Rolley"
        
        requestProvider.createRequest(request) { (result, error) in
            DispatchQueue.main.async {
                self.loaderHide()
                if result != nil {
                    print(result ?? "unknown response")
                    self.showAlertForRequestSent(title: "", message: "We have received the request, our support team will contact you shortly")
                }
                else if error != nil {
                    print(error!.localizedDescription)
                }
            }
            
        }
    }
    
    private func showAlertForRequestSent(title: String, message: String) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction.init(title: "okay".localized(), style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension UIView {
    /// Remove all subview
    func removeAllSubviews() {
        subviews.forEach { $0.removeFromSuperview() }
    }
    
    /// Remove all subview with specific type
    func removeAllSubviews<T: UIView>(type: T.Type) {
        subviews
            .filter { $0.isMember(of: type) }
            .forEach { $0.removeFromSuperview() }
    }
}
