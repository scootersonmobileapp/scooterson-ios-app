//
//  AnalitycsViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/28/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit

class InsightsViewController: BaseTableViewController, BTElfServiceDelegate {

    private var headerViews: [UIView] = []
    private let btTools = BTElfService.shared
    private var odoCell: SCTableViewCell!
    private var timeCell: SCTableViewCell!
    private var tripCell: SCTableViewCell!
    private var imperial: Bool!
    
    override func viewDidLoad() {
        titleString = "insights".localized()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        super.viewWillAppear(animated)
        imperial = btTools.isImperial()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btTools.delegate = self
        getTripCount()
        getTripTotalTime()
    }
    
    func getTripCount() {
        let count = UserDefaults.standard.integer(forKey: btTools.scTotalTripCounts)
        tripCell.valueString = String(format: "%d %@", count, "totalTrips".localized())
        //        let divider = 1000
        //        tripCell.valueString = String(format: "%.2f %@", trip/Float(divider), imperial ? "ins_m".localized() : "ins_km".localized())
    }
    
    func getTripTotalTime() {
        let time = UserDefaults.standard.integer(forKey: btTools.scTotalTripTime)
        let lessWhenHour = time < 60
        let time4Str = lessWhenHour ? time : time/60
        timeCell.valueString = String(format: lessWhenHour ? "%d %@" : "%.1f %@", time4Str, lessWhenHour ? "ins_min".localized() : "ins_hr".localized())
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[0].count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    override func setupCells() {
        odoCell = self.setupDescriptionCell(withTitle: "totalDistance".localized(), withDescription: false)
        
        timeCell = self.setupDescriptionCell(withTitle: "totalTime".localized(), withDescription: true)
        timeCell.descriptionString = "insightDescription".localized()
        
        tripCell = self.setupDescriptionCell(withTitle: "totalTrip".localized(), withDescription: true)
        tripCell.descriptionString = "insightDescription".localized()
        
        cells = [[odoCell, timeCell, tripCell]]
    }
    
    // MARK: btTools delegate
    func deviceDisconnected(peripheral: CBPeripheral) {
        self.tabBarController?.dismiss(animated: true, completion: nil)
        self.sendToMQTT(message: "{\"tripStatus\":\"false\"}")
        API.shared.mqtt.disconnect()
    }
    
    func updateODO(odo: Float) {
        odoCell.valueString = String(format: "%d %@", Int(odo), imperial ? "ins_m".localized() : "ins_km".localized())
    }
}

