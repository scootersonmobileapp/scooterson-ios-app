//
//  RolleyDriveTrainViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/13/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
struct cellData {
    var opened = Bool()
    var title = String ()
    var sectionType = Int()
    var sectionInfo  = [(String, String)]()
}

enum RolleyDriveTrainInfoKeys: String {
    case batteryCycleCount = "batteryCycleCount"
    case battteryTemp = "battteryTemp"
    case serialNumber = "serialNumber"
    case softwareVersion = "softwareVersion"
    case hardwareVersion = "hardwareVersion"
    case currentChargeInterval = "currentChargeInterval"
    case maxChargeInterval = "maxChargeInterval"
    case fullCapacity = "fullCapacity"
    case cellNumber = "cellNumber"
    case serialCellCount = "serialCellCount"
    case parallelCellCount = "parallelCellCount"
    case soh = "stateOfHealth"
}



class RolleyDriveTrainViewController: BaseTableViewController, RolleyBleDelegate {
    
    private var deviceInfo: SCDevice!
    private var peripheral: CBPeripheral!
    private var rolleyService: RolleyBleService!
    var tableViewData =  [cellData] ()
    
    private var batteryInfoKeys = [RolleyDriveTrainInfoKeys.batteryCycleCount.rawValue.localized(), RolleyDriveTrainInfoKeys.battteryTemp.rawValue.localized(), RolleyDriveTrainInfoKeys.serialNumber.rawValue.localized() ,RolleyDriveTrainInfoKeys.softwareVersion.rawValue.localized(),RolleyDriveTrainInfoKeys.hardwareVersion.rawValue.localized(),RolleyDriveTrainInfoKeys.currentChargeInterval.rawValue.localized(),RolleyDriveTrainInfoKeys.maxChargeInterval.rawValue.localized(),RolleyDriveTrainInfoKeys.fullCapacity.rawValue.localized(),
                                   RolleyDriveTrainInfoKeys.cellNumber.rawValue.localized(),
                                   RolleyDriveTrainInfoKeys.serialCellCount.rawValue.localized(),
                                   RolleyDriveTrainInfoKeys.parallelCellCount.rawValue.localized(),RolleyDriveTrainInfoKeys.soh.rawValue.localized()
    ]
    private var processorInfoKeys = [RolleyDriveTrainInfoKeys.serialNumber.rawValue.localized() ,RolleyDriveTrainInfoKeys.softwareVersion.rawValue.localized(),RolleyDriveTrainInfoKeys.hardwareVersion.rawValue.localized()]
    private var displayInfoKeys = [RolleyDriveTrainInfoKeys.serialNumber.rawValue.localized() ,RolleyDriveTrainInfoKeys.softwareVersion.rawValue.localized(),RolleyDriveTrainInfoKeys.hardwareVersion.rawValue.localized()]
    
    private var batteryInfoValue = ["NA","NA","NA", "NA","NA","NA", "NA","NA","NA","NA","NA"]
    private var processorInfoValue = ["NA","NA","NA"]
    private var displayInfoValue = ["NA","NA","NA"]
    private var batteryDic: Dictionary <String,String>!
    private var processorDic: Dictionary <String,String>!
    private var displayDic: Dictionary <String,String>!
    
    private let SECTION_BATTERY = 1
    private let SECTION_PROCESSOR = 2
    private let SECTION_DISPLAY = 3
    
    
    
    override func viewDidLoad() {
        titleString = "insights".localized()
        guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
        self.peripheral = tabBarController.peripheral
        self.rolleyService = RolleyBleService()
        self.deviceInfo = tabBarController.device
        super.viewDidLoad()
        
        batteryInfoKeys = batteryInfoKeys.sorted(by: <)
        
        processorInfoKeys = processorInfoKeys.sorted(by: <)
        displayInfoKeys = displayInfoKeys.sorted(by:<)
        
        batteryDic = Dictionary(uniqueKeysWithValues: zip(batteryInfoKeys,batteryInfoValue))
        processorDic = Dictionary(uniqueKeysWithValues: zip(processorInfoKeys,processorInfoValue))
        displayDic = Dictionary(uniqueKeysWithValues: zip(displayInfoKeys,displayInfoValue))
        
        updateData()
        
    }
    
    private func updateData(){
        let sorted = batteryDic.sorted(by: { (keyVal1, keyVal2) -> Bool in
            keyVal1.key < keyVal2.key
        })
        let sorte2 = processorDic.sorted(by: { (keyVal1, keyVal2) -> Bool in
            keyVal1.key < keyVal2.key
        })
        let sorted3 = displayDic.sorted(by: { (keyVal1, keyVal2) -> Bool in
            keyVal1.key < keyVal2.key
        })
        
        self.tableViewData = [cellData(opened: false, title: "batttery".localized(), sectionType: SECTION_BATTERY,sectionInfo: sorted),
                              cellData(opened: false, title: "processor".localized(), sectionType: SECTION_PROCESSOR,sectionInfo: sorte2),
                              cellData(opened: false, title: "display".localized(),sectionType: SECTION_DISPLAY ,sectionInfo: sorted3)
        ]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loaderShow(withDelegate: nil)
        Timer.scheduledTimer(withTimeInterval: 8 , repeats: false, block: { (timer) in
            self.loaderHide()
        })
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true{
            return tableViewData[section].sectionInfo.count + 1
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cell =  setupTitleCell(withTitle: "", isOpen: tableViewData[indexPath.section].opened)
            cell.titleLabel.text = tableViewData[indexPath.section].title
            return cell
            
        }else{
            //let title = tableViewData[indexPath.section].sectionData[indexPath.row - 1]
            //            let dic = tableViewData[indexPath.section].sectionInfo
            //            let key = Array(dic.keys)[indexPath.row - 1]
            //            let cell = setupOneValuedCellCustom(withTtile: key, andText: Array(dic)[indexPath.row - 1].value)
            //            let cell = setupOneValuedCellCustom(withTtile: key, andText: Array(dic)[indexPath.row - 1].value)
            let element = tableViewData[indexPath.section].sectionInfo
            let keys = element[indexPath.row - 1]
            
            let cell = setupOneValuedCellCustom(withTtile: keys.0, andText: keys.1)
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 120
        }else{
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sections = IndexSet.init(integer: indexPath.section)
        if tableViewData[indexPath.section].opened == true{
            tableViewData[indexPath.section].opened = false
            tableView.reloadSections(sections, with: .none )
            
        }else{
            tableViewData[indexPath.section ].opened = true
            tableView.reloadSections(sections, with: .none )
        }
    }
    
    
    func deviceStatus(status: Int32) {
        print("Rolley insights driveTrain \(peripheral.identifier.uuidString) with status \(status) ")
        switch (status) {
            
        case PL_BLE_STATUS_READY:
            rolleyService.fetchDriveTrainInfo()
            
        case PL_BLE_STATUS_RECONNECTING:
            //Dismiss when state moved to disconnected
            //todo show popup on the device screen that its been disconnected
            self.rolleyService.disconnect()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.rolleyService.disconnect()
                guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                tabbarC.moveToDeviceTab()
            }
        default:
            break
        }
    }
    
    func onInfoReceived(command: Int32, value: String!) {
        var isUpated = false
        switch command {
        case EBoxInfBMSCycleCount:
            isUpated = true
            print("Insights battery cycle: \(value!)")
            batteryDic[RolleyDriveTrainInfoKeys.batteryCycleCount.rawValue.localized()] = value
            let pcell = self.rolleyService.getCanInfoForCertainParams(key: 53)
            let scell = self.rolleyService.getCanInfoForCertainParams(key: 52)
            batteryDic[RolleyDriveTrainInfoKeys.parallelCellCount.rawValue.localized()] = pcell
            batteryDic[RolleyDriveTrainInfoKeys.serialCellCount.rawValue.localized()] = scell
            
        case EBoxInfBMSTemperature:
            isUpated = true
            print("Insights battery temp: \(value!)")
            batteryDic[RolleyDriveTrainInfoKeys.battteryTemp.rawValue.localized()] = value
        case EBoxBatterySerialNum:
            isUpated = true
            batteryDic[RolleyDriveTrainInfoKeys.serialNumber.rawValue.localized()] = value
        case EBoxBatterySWVersion:
            isUpated = true
            print("Insights batttery SW info: \(value!)")
            batteryDic[RolleyDriveTrainInfoKeys.softwareVersion.rawValue.localized()] = value
        case EBoxBatteryHWVersion:
            isUpated = true
            print("Insights batttery SW info: \(value!)")
            batteryDic[RolleyDriveTrainInfoKeys.hardwareVersion.rawValue.localized()] = value
        case EBoxControllerSWVersion:
            isUpated = true
            processorDic[RolleyDriveTrainInfoKeys.softwareVersion.rawValue.localized()] = value
        case EBoxControllerSerialNum:
            isUpated = true
            processorDic[RolleyDriveTrainInfoKeys.serialNumber.rawValue.localized()] = value
        case EBoxControllerHWVersion:
            isUpated = true
            processorDic[RolleyDriveTrainInfoKeys.hardwareVersion.rawValue.localized()] = value
        case EBoxPanelHWVersion:
            isUpated = true
            displayDic[RolleyDriveTrainInfoKeys.hardwareVersion.rawValue.localized()] = value
        case EBoxPanelSWVersion:
            isUpated = true
            displayDic[RolleyDriveTrainInfoKeys.softwareVersion.rawValue.localized()] = value
        case EBoxPanelSerialNum:
            isUpated = true
            displayDic[RolleyDriveTrainInfoKeys.serialNumber.rawValue.localized()] = value
        case EBoxInfBMSMaxChaInterval:
            isUpated = true
            batteryDic[RolleyDriveTrainInfoKeys.maxChargeInterval.rawValue.localized()] = value
        case EBoxInfBMSCurChaInterval:
            isUpated = true
            batteryDic[RolleyDriveTrainInfoKeys.currentChargeInterval.rawValue.localized()] = value
        case EBoxBatteryCellNum:
            isUpated = true
            batteryDic[RolleyDriveTrainInfoKeys.cellNumber.rawValue.localized()] = value
        case EBoxInfBMSFullCapacity:
            isUpated = true
            batteryDic[RolleyDriveTrainInfoKeys.fullCapacity.rawValue.localized()] = value
        case EBoxSOH:
            isUpated = true
            batteryDic[RolleyDriveTrainInfoKeys.soh.rawValue.localized()] = value + " %"
        default:
            break
        }
        print("insight processor info \(processorDic!)")
        if(isUpated){
            self.updateData()
            mainTable.reloadData()
        }
    }
    
    func setupTitleCell(withTitle title:String, isOpen: Bool) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: "title")
        let titleLabel = UILabel()
        cell.titleLabel = titleLabel
        cell.titleLabel.text = title
        cell.isTapable = true
        cell.selectionStyle = .none
        cell.titleLabel.font = .regular(size: 24.0)
        cell.titleLabel.textAlignment = .left
        cell.titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        cell.titleLabel.text = title.localized()
        cell.contentView.addSubview(cell.titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-60.0)
            make.centerY.equalTo(cell.contentView)
            make.height.equalTo(30.0)
        }
        
        cell.cellImageView = UIImageView()
        if isOpen{
            cell.cellImageView.theme_image = ["collapse-arrow-white","collapse-arrow"]
        }else{
            cell.cellImageView.theme_image = ["expand-arrow-white","expand-arrow"]
        }
        
        cell.contentView.addSubview(cell.cellImageView)
        cell.cellImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(cell.contentView)
            make.right.equalTo(cell.contentView).offset(-20)
            
        }
        
        return cell
    }
    
    func setupOneValuedCellCustom(withTtile title:String, andText text:String) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.textColor = UIColor.pastelOrange
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(20.0)
        }
        
        let textLabel = UILabel()
        textLabel.font = .regular(size: 16.0)
        textLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        textLabel.textAlignment = .left
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.text = text
        textLabel.numberOfLines = 0
        cell.contentView.addSubview(textLabel)
        textLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(10.0)
        }
        textLabel.sizeToFit()
        return cell
    }
    
    
    
    
}
