//
//  RolleyInsightsViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/13/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit

class RolleyInsightsViewController: BaseTableViewController {
    
    private var TAG_RIDE_INFO  = 1
    private var TAG_DRIVETRAIN_INFO  = 2
    private var TAG_TUTORIAL_INFO  = 3
    
    override func viewDidLoad() {
        titleString = "insights".localized()
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //check if the rolley still connected. 
        let rolleyService = RolleyBleService.init()
        if rolleyService.isConnected{
            rolleyService.disconnect()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                rolleyService.disconnect()
                guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                tabbarC.moveToDeviceTab()
            }
        }
    }
    
    override func setupCells() {
        let rideInfoCell = self.setupDescriptionCellTypeBig(withTitle: "rideInformation".localized(), withDescription: true)
        rideInfoCell.descriptionString = "rideInformationDesc".localized()
        rideInfoCell.isTapable = true
        rideInfoCell.tag = TAG_RIDE_INFO
        
        let driveTrainInfoCell = self.setupDescriptionCellTypeBig(withTitle: "driveTrainInformation".localized(), withDescription: true)
        driveTrainInfoCell.descriptionString = "driveTrainInformationDesc".localized()
        driveTrainInfoCell.isTapable = true
        driveTrainInfoCell.tag = TAG_DRIVETRAIN_INFO
        
        let tutorialInfoCell = self.setupDescriptionCellTypeBig(withTitle: "tutorialInformation".localized(), withDescription: true)
        tutorialInfoCell.descriptionString = "tutorialInformationDesc".localized()
        tutorialInfoCell.isTapable = true
        tutorialInfoCell.tag = TAG_TUTORIAL_INFO
        
        cells = [[rideInfoCell, driveTrainInfoCell, tutorialInfoCell]]
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = cells[indexPath.section][indexPath.row]
        switch cell.tag {
        case TAG_RIDE_INFO:
            let rideVC = RolleyRideInsightsViewController()
            let backItem = UIBarButtonItem()
            backItem.title = "back".localized()
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(rideVC, animated: true)
        case TAG_TUTORIAL_INFO:
            break
        case TAG_DRIVETRAIN_INFO:
            let driveTrainVC = RolleyDriveTrainViewController()
            let backItem = UIBarButtonItem()
            backItem.title = "back".localized()
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(driveTrainVC, animated: true)
        default:
            break
        }
    }
}
