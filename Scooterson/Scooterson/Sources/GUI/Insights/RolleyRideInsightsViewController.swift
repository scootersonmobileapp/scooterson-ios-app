//
//  RolleyRideInsightsViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/13/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
class RolleyRideInsightsViewController: BaseTableViewController, RolleyBleDelegate {
    
    private var deviceInfo: SCDevice!
    private var peripheral: CBPeripheral!
    private var rolleyService: RolleyBleService!
    private var odoInfoCell : SCTableViewCell!
    private var imperial: Bool!
    
    override func viewDidLoad() {
        titleString = "insights".localized()
        guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
        self.peripheral = tabBarController.peripheral
        self.rolleyService = RolleyBleService()
        self.deviceInfo = tabBarController.device
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
        super.viewWillAppear(animated)
        imperial = isImperial()
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func setupCells() {
        odoInfoCell = setupProfileValueCell(withTtile: "totalDistance".localized(), andText: "fetching".localized())
        odoInfoCell.isTapable = false
        cells = [[odoInfoCell]]
    }
    
    
    func deviceStatus(status: Int32) {
         print("Rolley insights \(peripheral.identifier.uuidString) with status \(status) ")
        switch (status) {
        case PL_BLE_STATUS_RECONNECTING:
            //Dismiss when state moved to disconnected
            //todo show popup on the device screen that its been disconnected
            self.rolleyService.disconnect()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.rolleyService.disconnect()
                guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                tabbarC.moveToDeviceTab()
            }
        default:
            break
        }
    }
    
    func onInfoReceived(command: Int32, value: String!) {
        switch command {
        case EBoxInfOdo:
            let modVal = ((value! as NSString).doubleValue) + 1
            var reading = String(format: "%.2f", modVal)
            if imperial{
                let readingInMiles = toMileFromKm(distance: (reading as NSString).doubleValue)
                reading = "\(String(format: "%.2f", readingInMiles)) \("ins_m".localized())"
            }else{
                reading = "\(reading) \("ins_km".localized())"
            }
            
            self.odoInfoCell.infomationLabel.text = reading
            
        default:
            break
        }
    }
    
}
