//
//  AddNameVIewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/12/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit

class AddNameViewController: BaseTableViewController , UITextFieldDelegate{
    
    private var subTitleString: String!
    private var editedIndexPath: IndexPath!
    private var editable: Bool!
    private var buttonView: UIView!
    private var userData = SCUser.reStore()!
    private var firstNameCell: SCTableViewCell!
    private var lastNameCell: SCTableViewCell!
    private var hasSharedDevice = false
    
    
    override func viewDidLoad() {
        titleString = "addYourNameTitle".localized()
        
        editable = true
        
        super.viewDidLoad()
        
        setBottomButtonView()
        
        //Hide the line for the navigation bar from the top
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        if !editable {
            mainTable.tableFooterView = buttonView
        }
        self.loaderShow(withDelegate: nil)
        API.shared.getCustomerProfile { (success, userDataServer, statusCode) in
            self.loaderHide()
            if success {
                //self.userData = userDataServer!
                userDataServer?.store()
                self.userData = SCUser.reStore()!
                print("Profile name feteched:  \(String(describing: userDataServer?.name?.firstName))")
                self.firstNameCell.textField.text = self.userData.firstName
                self.lastNameCell.textField.text = self.userData.lastName
                
                if(self.userData.firstName!.count > 2){
                    self.mainTable.tableFooterView = self.buttonView
                }
            }
            else {
                if let userDataRestored = SCUser.reStore() {
                    self.userData = userDataRestored
                }
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func setHasSharedDevice(hasShared: Bool){
        self.hasSharedDevice = hasShared
    }
    
    
    private func setBottomButtonView() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: mainTable.frame.width, height: 90))
        let saveButton = UIButton(type: .custom)
        saveButton.backgroundColor = .dodgerBlue
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.setTitleColor(.charcoalGrey, for: .highlighted)
        saveButton.layer.cornerRadius = 10.0
        saveButton.setTitle(editable ? "save".localized() : "contact".localized(), for: .normal)
        saveButton.addTarget(self, action: #selector(checkFieldsAndSaveData), for: .touchUpInside)
        view.addSubview(saveButton)
        saveButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.bottom.equalTo(view)
        }
        buttonView = view
    }
    @objc private func checkFieldsAndSaveData() {
        userData.firstName = (cells[0][0].contentView.viewWithTag(1) as! UITextField).text// ?? userData.firstName
        userData.lastName = (cells[0][1].contentView.viewWithTag(2) as! UITextField).text// ?? userData.lastName
        if(userData.firstName!.count <= 2){
            self.showAlert(title: "nameNotEnterErrorTitle".localized(), message: "nameNotEnterErrorBody".localized())
            return
        }
        self.loaderShow(withDelegate: nil)
        API.shared.updateName(firstName: userData.firstName!, lastName: userData.lastName!) { (success, statusCode) in
            self.loaderHide()
            if success {
                self.userData.store()
                UserDefaults.standard.set(true, forKey: ContantsKey.keyOnBoardingStageAddName.rawValue)
                UserDefaults.standard.synchronize()
                
                var deviceVc: UIViewController!
                let numberOfAllDevicesActivated = UserDefaults.standard.integer(forKey: ContantsKey.activaatedDevicesCount.rawValue)
                if numberOfAllDevicesActivated > 0 || self.hasSharedDevice{
                    deviceVc = DeviceSelectionViewController()
                }else{
                    deviceVc = AddDeviceViewController()
                }
                
                let navVC = UINavigationController(rootViewController: deviceVc)
                navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(navVC, animated: true)
                
            }
            else {
                self.showAlert(title: "Error", message: API.shared.errorDictionaryProfile[statusCode!]!)
            }
        }
        
    }
    
    override func setupCells() {
        setupUI()
    }
    
    func setupUI() {
        firstNameCell = setupOneFieldCell(withTtile: "firstNameLabel".localized(), value: "", andPlaceHolder: "name.placeholder".localized(), tag: 1, mandatory: true)
        firstNameCell.textField.autocapitalizationType = .words
        firstNameCell.textField.autocorrectionType = .no
        firstNameCell.textField.keyboardType = .default
        lastNameCell = setupOneFieldCell(withTtile: "lastNameLabel".localized(), value: "", andPlaceHolder: "surname.palceholder".localized(), tag: 2, mandatory: true)
        lastNameCell.textField.autocapitalizationType = .words
        lastNameCell.textField.autocorrectionType = .no
        lastNameCell.textField.keyboardType = .default
        
        
        lastNameCell.textField.returnKeyType = .done
        firstNameCell.textField.returnKeyType = .next
        
        
        cells[0].append(firstNameCell)
        cells[0].append(lastNameCell)
        
        
    }
    
    
    func setupOneFieldCell(withTtile title:String, value: String?, andPlaceHolder text:String, tag: Int, mandatory: Bool) -> SCTableViewCell {
        editable = true
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.cellHeight = 130.0
        cell.selectionStyle = .none
        titleLabel.font = .light(size: 20.0)
        titleLabel.textColor = .pastelOrange
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(20.0)
        }
        
        let textField = UITextField()
        textField.font = .light(size: 20.0)
        textField.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        textField.theme_keyboardAppearance = [.dark, .light]
        textField.textAlignment = .left
        if let data = value, value?.count != 0 {
            textField.text = data
        }
        else {
            
            textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
            
        }
        textField.delegate = self
        textField.isMandatory = mandatory
        textField.tag = tag
        cell.contentView.addSubview(textField)
        textField.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(14.0)
            make.bottom.equalTo(cell.contentView).offset(-20.0)
        }
        cell.textField = textField
        return cell
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if(textField.tag == 1){
            return newLength <= ConstantInt.maxFirstNameChar.rawValue
        }else{
            return true
        }
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let textFieldRowCell = textField.superview?.superview as? UITableViewCell {
            editedIndexPath = mainTable.indexPath(for: textFieldRowCell)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag + 1 < cells[0].count + 1000 {
            textField.resignFirstResponder()
            if textField.text?.count == 0 {
                textField.text = nil
            }
            let nextTextField = self.view.viewWithTag(textField.tag + 1) as? UITextField
            nextTextField?.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
            mainTable.scrollToRow(at: IndexPath(item: 0, section: 0), at: .bottom, animated: true)
        }
        if textField.returnKeyType == .done {
            mainTable.tableFooterView = buttonView
            mainTable.reloadData()
        }
        return true
    }
    
}
