//
//  LaunchScreenViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/16/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import SwiftTheme

class LaunchViewController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    private var actionVc: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
            UIView.animate(withDuration: 0, animations: {
                //self.logoImageView.layer.anchorPoint = CGPoint(x: 0.518, y: 0.5)
                //            self.logoImageView.layer.transform = transform
                //self.logoImageView.transform = CGAffineTransform.identity.scaledBy(x: 100, y: 100)
            }) { (finished) in
                if finished {
                    self.decideTheNextVC()
                }
            }
        }
        
    }
private func decideTheNextVC(){
        let isAuthorize = UserDefaults.standard.bool(forKey: ContantsKey.autorized.rawValue)
        if isAuthorize {
            if(UserDefaults.standard.bool(forKey: ContantsKey.keyOnBoardingStageAddName.rawValue)){
                let numberOfAllDevicesActivated = UserDefaults.standard.integer(forKey: ContantsKey.activaatedDevicesCount.rawValue)
                if numberOfAllDevicesActivated > 0{
                    self.actionVc = DeviceSelectionViewController()
                    takeMeToVC()
                }else{
                    //Check is there is any shared device and modify the launch VC accordingly
                    API.shared.getSharedDevices {[weak self] (success, errorCode, sharedDevices) in
                        if success && sharedDevices!.count > 0{
                            self!.actionVc = DeviceSelectionViewController()
                            self!.takeMeToVC()
                        } else if errorCode == 10 {
                            self!.performSegue(withIdentifier: "showMain", sender: nil)
                        }else {
                            self!.actionVc = AddDeviceViewController()
                            self!.takeMeToVC()
                        }
                    }
                }
            }else{
                self.actionVc = AddNameViewController()
                takeMeToVC()
            }
        }
        else {
            self.performSegue(withIdentifier: "showMain", sender: nil)
        }
    }
    
    private func takeMeToVC(){
        let navVC = UINavigationController(rootViewController: self.actionVc)
        navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navVC.modalTransitionStyle = .crossDissolve
        self.present(navVC, animated: true)
    }
    
    public func doAction(deviceMac: String){
        let navVC = UINavigationController(rootViewController: DeviceSelectionViewController())
        navVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navVC.modalTransitionStyle = .crossDissolve
        self.present(navVC, animated: true) {
            let sharingVC = RolleySharingSettingsViewController()
            let backItem = UIBarButtonItem()
            backItem.title = "devices".localized()
            backItem.tintColor = .dodgerBlue
            navVC.navigationItem.backBarButtonItem = backItem
            navVC.pushViewController(sharingVC, animated: true)
            guard let deviceFound = SCDevice.reStore(vinCode: deviceMac) else { return }
            sharingVC.setDeviceInfoWhenLauchFromDevice(device: deviceFound)
        }
    }
    
    
}
