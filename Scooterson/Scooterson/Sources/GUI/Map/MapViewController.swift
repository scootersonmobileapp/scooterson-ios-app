//
//  MapViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/10/19.
//  Copyright © 2019 Scooterson. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftTheme

class MapViewController: BaseViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    private var locationManager: CLLocationManager!
    private var mapView: GMSMapView!
    private var deviceImageView: UIImageView!
    private var sharedWithWhomLabel: UILabel!
    private var scooterStateLabel: UILabel!
    private var timeLabel: UILabel!
    private var locationName: UILabel!
    private var imageButtonWithMutiAction: UIButton!
    private var imageButtonWithMultiActionName: UILabel!
    
    private var device: SCDevice!
    private var lastLocation: LastLocation!
    private var currentLocation: CLLocationCoordinate2D!
    private var scooterMarker: GMSMarker?
    private var scooterLatitude: Double!
    private var scooterLongitude: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = GMSMapView(frame: CGRect.zero)
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = false
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        view = mapView
        
        showScooterOnMap()
        checkLocationAccess()
        setupViews()
    }
    
    private func checkForDarkMode(){
        
        if ThemeManager.currentThemeIndex == 1 {return}
        
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkForDarkMode()
    }
    
    func checkLocationAccess() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    public func initalizeTheView(device: SCDevice, locationModel: LastLocation){
        self.lastLocation = locationModel
        self.device = device
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status.rawValue >= CLAuthorizationStatus.authorizedAlways.rawValue {
            locationManager.startUpdatingLocation()
        }else{
             showAlertForPermissionNotFound(title: "enableLocationTitle".localized(), message: "enableLocationMessage".localized(), actionButtoTitle: "enable".localized())
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.count > 0 {
            currentLocation = locations.first?.coordinate
            calculateScooterLocationTitle()
            manager.stopUpdatingLocation()
        }
        
    }
    
    private func calculateScooterLocationTitle(){
        let scooterCoordinate = CLLocation(latitude: scooterLatitude!, longitude: scooterLongitude!)
        let currentUserCoordinate = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
        let distanceInMeters = currentUserCoordinate.distance(from: scooterCoordinate)
        print("Location distance computed: \(distanceInMeters)")
        
        if isImperial() {
            scooterMarker!.title = String(format: "%.1f", toMileFromKm(distance: distanceInMeters/1000)) + "miAway".localized()
        }else{
            scooterMarker!.title = String(format: "%.1f", distanceInMeters/1000) + "kmAway".localized()
        }
        mapView.selectedMarker = scooterMarker
        if distanceInMeters <= 500{
            mapView.moveCamera(GMSCameraUpdate.zoom(to: 18))
        }else if distanceInMeters > 500 && distanceInMeters <= 5000{
            mapView.moveCamera(GMSCameraUpdate.zoom(to: 16))
        }else{
            mapView.moveCamera(GMSCameraUpdate.zoom(to: 14))
        }
        
    }
    
    private func showScooterOnMap(){
        scooterLatitude = Double (lastLocation.latitude.first!.value)
        scooterLongitude = Double(lastLocation.longitude.first!.value)
        let camera = GMSCameraPosition.camera(withLatitude: scooterLatitude!, longitude: scooterLongitude!, zoom: 14.0)
        mapView.camera = camera
        scooterMarker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: scooterLatitude!, longitude: scooterLongitude!))
        
        let selectedScooterImage = UIImage(named: device.deviceParams!.colorInfo)
        let markerView = UIImageView(image: selectedScooterImage!.addImagePadding(x: 30, y: 30))
        markerView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        markerView.theme_backgroundColor = [UIColor.white.toHexString(), UIColor.white.toHexString()]
        markerView.layer.cornerRadius = 25
        markerView.clipsToBounds = true
        mapView.clear()
        scooterMarker?.iconView = markerView
        scooterMarker!.map = mapView
        getGeoCodeName()
    }
    
    private func getGeoCodeName() {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: scooterLatitude , longitude: scooterLongitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler:
            {
                placemarks, error -> Void in
                
                // Place details
                guard let placeMark = placemarks?.first else { return }
                
                // Street address
                let address = "\(placeMark.subThoroughfare == nil ? "" : " \((placeMark.subThoroughfare ?? "")), ") \(placeMark.thoroughfare == nil ? "" : " \((placeMark.thoroughfare ?? "")), ")\(placeMark.subLocality == nil ? "" : " \((placeMark.subLocality ?? "")), ")\(placeMark.locality == nil ? "" : " \((placeMark.locality ?? "")), ")\(placeMark.administrativeArea ?? "")"
                print("Location address: \(address)")
                self.locationName.text = address
                
                
                
        })
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        imageButtonWithMutiAction.setImage(UIImage(named: "navigation"), for: .normal)
        imageButtonWithMultiActionName.text = "navigation".localized()
        mapView.selectedMarker = scooterMarker
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.imageButtonWithMutiAction.setImage(UIImage(named: "refresh"), for: .normal)
            self.imageButtonWithMultiActionName.text = "refresh".localized()
        }
        return true // or false as needed.
    }
    
    
    
    //================================================== Views ==================================================
    private func setupViews(){
        let headerView = setupTitleView()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        
        
        let markerButton = showMyLocationMarkerButton()
        self.view.addSubview(markerButton)
        markerButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            make.top.equalTo((headerView?.snp.bottom)!).offset(10.0)
            make.height.equalTo(40)
            make.width.equalTo(45)
        }
        
        let scooterMarkerButton = showScooterLocationMarkerButton()
        self.view.addSubview(scooterMarkerButton)
        scooterMarkerButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            make.top.equalTo(markerButton.snp.bottom).offset(10.0)
            make.height.equalTo(40)
            make.width.equalTo(45)
        }
        
        
        let lastSection = setupBottomView()
        self.view.addSubview(lastSection)
        lastSection.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.safeAreaLayoutGuide).offset(20.0)
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            
            make.height.equalTo(130.0)
            
        }
        
        
    }
    private func setupTitleView() -> UIView? {
        let titleView = UIView()
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 26)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = "location".localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    
    private func showMyLocationMarkerButton() -> UIButton{
        let myLocation = UIButton()
        myLocation.centerTextAndImage(spacing: 10)
        myLocation.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        myLocation.setImage(UIImage(named: "currentLocation"), for: .normal)
        myLocation.addTarget(self, action: #selector(actionMylocation), for: .touchUpInside)
        myLocation.layer.cornerRadius = 10.0
        return myLocation
    }
    
    @objc private func actionMylocation(){
        if(currentLocation != nil){
            mapView.moveCamera(GMSCameraUpdate.setTarget(currentLocation))
        }
    }
    
    private func showScooterLocationMarkerButton() -> UIButton{
        
        let scooterLocation = UIButton()
        scooterLocation.centerTextAndImage(spacing: 10)
        scooterLocation.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        scooterLocation.setImage(UIImage(named: "scootersLocation"), for: .normal)
        scooterLocation.addTarget(self, action: #selector(actionScooterLocation), for: .touchUpInside)
        scooterLocation.layer.cornerRadius = 10.0
        
        return scooterLocation
    }
    
    
    @objc private func actionScooterLocation(sender:UIButton){
        
        let scooterCoordinate = CLLocationCoordinate2D(latitude: scooterLatitude!, longitude: scooterLongitude!)
        mapView.moveCamera(GMSCameraUpdate.setTarget(scooterCoordinate))
        
    }
    
    
    private func setupBottomView() -> UIView{
        let view = UIView()
        view.layer.cornerRadius = 10
        view.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        deviceImageView = UIImageView()
        let selectedScooterImage = UIImage(named: device.deviceParams!.colorInfo)
        deviceImageView.image = selectedScooterImage!.addImagePadding(x: 30, y: 30)
        deviceImageView.layer.cornerRadius = 25
        deviceImageView.clipsToBounds = true
        deviceImageView.theme_backgroundColor = [UIColor.white.toHexString(), UIColor.paleGrey.toHexString()]
        view.addSubview(deviceImageView)
        deviceImageView.snp.makeConstraints { (make) in
            make.left.top.equalTo(view).offset(12)
            make.height.width.equalTo(50)
        }
        
        sharedWithWhomLabel = UILabel()
        sharedWithWhomLabel.font = .regular(size: 20)
        sharedWithWhomLabel.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        sharedWithWhomLabel.textAlignment = .left
        view.addSubview(sharedWithWhomLabel)
        sharedWithWhomLabel.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(8)
            make.right.equalTo(view).offset(-60)
            make.top.equalTo(view).offset(12)
        }
        
        scooterStateLabel = UILabel()
        scooterStateLabel.font = .bold(size: 16)
        scooterStateLabel.textAlignment = .left
        view.addSubview(scooterStateLabel)
        scooterStateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(8)
            make.top.equalTo(sharedWithWhomLabel.snp.bottom).offset(5)
        }
        
        timeLabel = UILabel()
        timeLabel.font = .light(size: 16)
        timeLabel.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        timeLabel.textAlignment = .left
        view.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(deviceImageView.snp.right).offset(8)
            make.top.equalTo(scooterStateLabel.snp.bottom).offset(5)
        }
        
        
        locationName = UILabel()
        locationName.font = .light(size: 16)
        locationName.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        locationName.textAlignment = .left
        locationName.text = ""
        view.addSubview(locationName)
        locationName.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(12)
            make.right.equalTo(view).offset(-12)
            make.bottom.equalTo(view).offset(-12)
        }
        
        imageButtonWithMutiAction = UIButton(type: .custom)
        imageButtonWithMutiAction.setImage(UIImage(named: "refresh"), for: .normal)
        imageButtonWithMutiAction.layer.cornerRadius = 2
        imageButtonWithMutiAction.addTarget(self, action: #selector(actionOnButton), for: .touchUpInside)
        view.addSubview(imageButtonWithMutiAction)
        imageButtonWithMutiAction.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-12)
            //make.height.equalTo(35)
            make.top.equalTo(view).offset(12)
            
        }
        
        imageButtonWithMultiActionName = UILabel()
        imageButtonWithMultiActionName.font = .light(size: 16)
        imageButtonWithMultiActionName.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        imageButtonWithMultiActionName.textAlignment = .center
        imageButtonWithMultiActionName.text = "refresh".localized()
        imageButtonWithMultiActionName.isHidden = true
        view.addSubview(imageButtonWithMultiActionName)
        imageButtonWithMultiActionName.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-12)
            make.top.equalTo(imageButtonWithMutiAction.snp.bottom).offset(8)
        }
        
        updateValue()
        return view
    }
    
    @objc private func actionOnButton(){
        if imageButtonWithMultiActionName.text == "refresh".localized(){
            imageButtonWithMutiAction.rotate()
            API.shared.getScooterLastLocation(deviceId: device!.credential!.deviceID) {(success, lastLocation ,statusCode) in
                self.imageButtonWithMutiAction.removeRotate()
                if success{
                    self.lastLocation = lastLocation
                    self.locationManager.startUpdatingLocation()
                    self.updateValue()
                    self.showScooterOnMap()
                }
            }
        }else{
            //show navigation action
            openMaps(latitude: scooterLatitude!, longitude: scooterLongitude!, title: "navigateToRolley".localized())
        }
    }
    
    private func updateValue(){
        let slaveName = self.lastLocation.slaveName[0].value != nil ?  "With \(self.lastLocation.slaveName[0].value!)" :  "withYourFriend".localized()
        sharedWithWhomLabel.text = self.lastLocation.isSlave[0].value == "false" ? "withYou".localized() : slaveName
        scooterStateLabel.textColor = lastLocation.tripStatus[0].value == "true" ? UIColor.greenblue : UIColor.pastelOrange
        scooterStateLabel.text = lastLocation.tripStatus[0].value == "true" ? "riding".localized() : "parked".localized()
        timeLabel.text = getStringForDateWithDay(time: (lastLocation.latitude[0].lastTime/1000))
    }
    
    private func openMaps(latitude: Double, longitude: Double, title: String?) {
        let application = UIApplication.shared
        let coordinate = "\(latitude),\(longitude)"
        let encodedTitle = title?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let handlers = [
            ("Apple Maps", "http://maps.apple.com/?q=\(encodedTitle)&ll=\(coordinate)"),
            ("Google Maps", "comgooglemaps://?q=\(coordinate)"),
            ("Waze", "waze://?ll=\(coordinate)"),
            ("Citymapper", "citymapper://directions?endcoord=\(coordinate)&endname=\(encodedTitle)")
            ]
            .compactMap { (name, address) in URL(string: address).map { (name, $0) } }
            .filter { (_, url) in application.canOpenURL(url) }
        
        guard handlers.count > 1 else {
            if let (_, url) = handlers.first {
                application.open(url, options: [:])
            }
            return
        }
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        handlers.forEach { (name, url) in
            alert.addAction(UIAlertAction(title: name, style: .default) { _ in
                application.open(url, options: [:])
            })
        }
        alert.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension UIImage {
    
    func addImagePadding(x: CGFloat, y: CGFloat) -> UIImage? {
        let width: CGFloat = size.width + x
        let height: CGFloat = size.height + y
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
        let origin: CGPoint = CGPoint(x: (width - size.width) / 2, y: (height - size.height) / 2)
        draw(at: origin)
        let imageWithPadding = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imageWithPadding
    }
}
extension UIView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
    func removeRotate(){
        self.layer.removeAllAnimations()
    }
}
extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        let writingDirection = UIApplication.shared.userInterfaceLayoutDirection
        let factor: CGFloat = writingDirection == .leftToRight ? 1 : -1
        
        self.imageEdgeInsets = UIEdgeInsets(top:  insetAmount*factor, left: insetAmount*factor, bottom: insetAmount*factor, right: insetAmount*factor)
        //self.titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount*factor, bottom: 0, right: -insetAmount*factor)
        self.contentEdgeInsets = UIEdgeInsets(top: insetAmount, left: 0, bottom: insetAmount, right: 0)
    }
}
