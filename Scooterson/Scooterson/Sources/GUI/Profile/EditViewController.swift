//
//  EditViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 6/13/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import SwiftTheme

class EditViewController: BaseTableViewController, UITextFieldDelegate {
    
    private var subTitleString: String!
    private var editedIndexPath: IndexPath!
    private var editable: Bool!
    private var isChecked: Bool!
    private var isUpdated: Bool!
    private var profileDataType: SCProfileDataType!
    private var userData = SCUser.reStore()!
    private var buttonView: UIView!
    private var tagSeries = 0
    
    //constant
    private let MAX_FIRST_NAME_CHAR = 10
    private let MAX_EMAIL_CHAR = 30
    
    
    override func viewDidLoad() {
        subTitleString = titleString
        titleString = "edit.".appending(subTitleString)
        self.tabBarController?.tabBar.isHidden = true
        isChecked = false
        isUpdated = false
        super.viewDidLoad()
        //        mainTable.snp.makeConstraints { (make) in
        //            make.left.right.top.equalTo(self.view)
        //            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(90)
        //        }
        setBottomButtonView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        if !editable {
            mainTable.tableFooterView = buttonView
        }
    }
    
    private func setBottomButtonView() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: mainTable.frame.width, height: 90))
        let saveButton = UIButton(type: .custom)
        saveButton.backgroundColor = .dodgerBlue
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.setTitleColor(.charcoalGrey, for: .highlighted)
        saveButton.layer.cornerRadius = 10.0
        saveButton.setTitle(editable ? "save".localized() : "contact".localized(), for: .normal)
        saveButton.addTarget(self, action: #selector(checkFieldsAndSaveData), for: .touchUpInside)
        view.addSubview(saveButton)
        saveButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.bottom.equalTo(view)
        }
        buttonView = view
    }
    
    @objc private func checkFieldsAndSaveData() {
        self.loaderShow(withDelegate: nil)
        switch profileDataType! {
        case .SCProfileDataTypeName:
            userData.firstName = (cells[0][0].contentView.viewWithTag(1000) as! UITextField).text// ?? userData.firstName
            userData.lastName = (cells[0][1].contentView.viewWithTag(1001) as! UITextField).text// ?? userData.lastName
            if(userData.firstName?.count == 0){
                self.showAlert(title: "nameNotEnterErrorTitle".localized(), message: "nameNotEnterErrorBody".localized())
                break
            }
            
            API.shared.updateName(firstName: userData.firstName!, lastName: userData.lastName!) { (success, statusCode) in
                self.loaderHide()
                if success {
                    self.userData.store()
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.showAlert(title: "Error", message: API.shared.errorDictionaryProfile[statusCode!]!)
                }
            }
        case .SCProfileDataTypeAddress:
            userData.addressLine1 = (cells[0][0].contentView.viewWithTag(2000) as? UITextField)?.text
            userData.addressLine2 = (cells[0][1].contentView.viewWithTag(2001) as? UITextField)?.text
            userData.zipcode = (cells[0][2].contentView.viewWithTag(2002) as? UITextField)?.text
            userData.city = (cells[0][3].contentView.viewWithTag(2003) as? UITextField)?.text
            userData.state = (cells[0][4].contentView.viewWithTag(2004) as? UITextField)?.text
            API.shared.updateAddress { (success, statusCode) in
                self.loaderHide()
                if success {
                    if success {
                        self.userData.store()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.showAlert(title: "Error", message: API.shared.errorDictionaryProfile[statusCode!]!)
                    }
                }
            }
        case .SCProfileDataTypeEmail:
            let emailEntered = (cells[0][0].contentView.viewWithTag(3000) as? UITextField)?.text// ?? userData.email
            if(emailEntered!.count == 0){
                self.loaderHide()
                self.showAlert(title: "emailNotEnteredTitle".localized(), message: "emailNotEnteredBody".localized())
                break
            }else if(!isValidEmail(emailEntered!)){
                self.loaderHide()
                self.showAlert(title: "emailWrongFormateTitle".localized(), message: "emailWrongFormateBody".localized())
                break
            }
            API.shared.updateEmail(email: emailEntered!) { (success, statusCode) in
                self.loaderHide()
                if success {
                    if success {
                        self.userData.email = emailEntered
                        self.userData.store()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.showAlert(title: "Error", message: API.shared.errorDictionaryProfile[statusCode!]!)
                    }
                }
            }
        default:
            return
        }
        
        
    }
    
    public func setupCellsForFields(fields:[(String, String, String?, Bool)], fieldProfieleDataType: SCProfileDataType, tagSeries: Int) {
        profileDataType = fieldProfieleDataType

        self.tagSeries = tagSeries
        var i = tagSeries
        
        for fieldData in fields {
            let (title, placeholder, value, mandatory) = fieldData
            let cell = setupOneFieldCell(withTtile: title, value: value, andPlaceHolder: placeholder, tag: i, mandatory: mandatory)
            i += 1
            switch profileDataType! {
            case .SCProfileDataTypeName:
                cell.textField.autocapitalizationType = .words
                cell.textField.autocorrectionType = .no
                cell.textField.keyboardType = .default
            case .SCProfileDataTypeEmail:
                cell.textField.autocapitalizationType = .none
                cell.textField.autocorrectionType = .no
                cell.textField.keyboardType = .emailAddress
            case .SCProfileDataTypeAddress:
                cell.textField.autocapitalizationType = .words
                cell.textField.autocorrectionType = .no
                cell.textField.keyboardType = .default
            case .SCProfileDataTypePhone:
                cell.textField.autocapitalizationType = .none
                cell.textField.autocorrectionType = .no
                cell.textField.keyboardType = .phonePad
            }
            if i == (fields.count+tagSeries) {
                cell.textField.returnKeyType = .done
            }
            else {
                cell.textField.returnKeyType = .next
            }
            cell.textField.addAction(for: .editingChanged) { [weak self] in 
                self?.isUpdated = true
            }
            cells[0].append(cell)
        }
        if let textField = self.view.viewWithTag(tagSeries) as? UITextField {
            textField.becomeFirstResponder()
        }
    }
    /**
                Make lenght  of the name field
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if(textField.tag == 1000){
            return newLength <= MAX_FIRST_NAME_CHAR
        }else if (textField.tag == 3000){
            return newLength <= MAX_EMAIL_CHAR
        }
        else{
            return true
        }
            
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    public func setupContactSupportCellwithText(text: String, andButtonTitle title: String) {
        editable = false
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.selectionStyle = .none
        cell.cellHeight = 140.0
        let label = UILabel()
        label.font = .light(size: 20)
        label.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        label.numberOfLines = 0
        label.textAlignment = .justified
        label.lineBreakMode = .byWordWrapping
        label.text = text
        cell.contentView.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.equalTo(cell.contentView).offset(20.0)
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.bottom.equalTo(cell.contentView).offset(-20.0)
        }
        cells[0].append(cell)
    }
    
    func setupOneFieldCell(withTtile title:String, value: String?, andPlaceHolder text:String, tag: Int, mandatory: Bool) -> SCTableViewCell {
        editable = true
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        cell.titleString = title
        let titleLabel = UILabel()
        cell.cellHeight = 130.0
        cell.selectionStyle = .none
        titleLabel.font = .light(size: 20.0)
        titleLabel.textColor = .pastelOrange
        titleLabel.textAlignment = .left
        titleLabel.text = title.localized()
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
            make.height.equalTo(20.0)
        }
        
        let textField = UITextField()
        textField.font = .light(size: 20.0)
        textField.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        textField.theme_keyboardAppearance = [.dark, .light]
        textField.textAlignment = .left
        if let data = value, value?.count != 0 {
            textField.text = data
        }
        else {
            if ThemeManager.currentThemeIndex == 0 {
                textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : UIColor.paleGrey])
            }
            else {
                textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
            }
            //            textField.placeholder = text
        }
        textField.delegate = self
        textField.isMandatory = mandatory
        textField.tag = tag
        cell.contentView.addSubview(textField)
        textField.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(14.0)
            make.bottom.equalTo(cell.contentView).offset(-20.0)
        }
        cell.textField = textField
        return cell
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let textFieldRowCell = textField.superview?.superview as? UITableViewCell {
            editedIndexPath = mainTable.indexPath(for: textFieldRowCell)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag + 1 < cells[0].count + tagSeries {
            textField.resignFirstResponder()
            if textField.text?.count == 0 {
                textField.text = nil
            }
            let nextTextField = self.view.viewWithTag(textField.tag + 1) as? UITextField
            nextTextField?.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
            mainTable.scrollToRow(at: IndexPath(item: 0, section: 0), at: .bottom, animated: true)
        }
        if isUpdated && textField.returnKeyType == .done {
            //            cells[editedIndexPath.section].append(buttonCell)
            mainTable.tableFooterView = buttonView
            mainTable.reloadData()
        }
        return true
    }
    
    @objc override func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                mainTable.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                mainTable.scrollToRow(at: editedIndexPath, at: .top, animated: true)
                mainTable.scrollIndicatorInsets = mainTable.contentInset
            }
        }
    }
}


extension UIViewController {
    private static var _myComputedProperty = [String:Bool]()
    
    var myComputedProperty:Bool {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UIViewController._myComputedProperty[tmpAddress] ?? false
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UIViewController._myComputedProperty[tmpAddress] = newValue
        }
    }
}

extension UITextField {
    private static var _isMandatory = [String:Bool]()
    
    var isMandatory:Bool {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UITextField._isMandatory[tmpAddress] ?? false
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UITextField._isMandatory[tmpAddress] = newValue
        }
    }
}
//UTIL function
func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}

