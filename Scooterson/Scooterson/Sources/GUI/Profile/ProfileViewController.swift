//
//  ProfileViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/28/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import ZendeskCoreSDK
import SupportProvidersSDK

public enum SCProfileDataType: Int {
    case SCProfileDataTypeName
    case SCProfileDataTypeAddress
    case SCProfileDataTypePhone
    case SCProfileDataTypeEmail
}

class ProfileViewController: BaseTableViewController {
    
    private var topCell: SCTableViewCell!
    private var userData: SCUser!
    
    override func viewDidLoad() {
        titleString = "profile".localized()
        if let userDataRestored = SCUser.reStore() {
            userData = userDataRestored
        }
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillAppear(animated)
        if let userDataRestored = SCUser.reStore() {
            userData = userDataRestored
            setupCells()
            mainTable.reloadData()
        }
    }
    
    override func setupCells() {
        setupTopCell()
        var addressText = "\(userData.addressLine1 ?? "")\n\(userData.addressLine2 ?? "")\n\(userData.city ?? "")\n\(userData.state ?? "")\n\(userData.zipcode ?? ""), \(userData.country)"
        if addressText.count == userData.country.count + 6 {
            addressText = userData.country
        }
        let addressCell = setupProfileValueCell(withTtile: "address", andText: addressText)
        addressCell.cellHeight = 200.0
        let mobileCell = setupProfileValueCell(withTtile: "mobile", andText: "+\(userData.phone)")
        
        let emailCell = setupProfileValueCell(withTtile: "email", andText: "\(userData.email ?? "add_email".localized())")
        if userData.email?.count == 0{
            emailCell.infomationLabel.text = "add_email".localized()
            emailCell.infomationLabel.theme_textColor = [UIColor.lightGray.toHexString(), UIColor.gray.toHexString()]
        }
        cells = [[topCell, addressCell, mobileCell, emailCell]]
    }
    
    func setupTopCell() {
        topCell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        topCell.cellHeight = 100.0
        topCell.isTapable = true
        topCell.selectionStyle = .none
        topCell.titleString = "name".localized()
        //        let photoImageView = UIImageView(image: UIImage(named: "big_noavatar"))
        //        photoImageView.layer.cornerRadius = 86/2
        //        photoImageView.clipsToBounds = true
        //        topCell.contentView.addSubview(photoImageView)
        //        photoImageView.snp.makeConstraints { (make) in
        //            make.width.height.equalTo(86)
        //            make.centerX.equalTo(topCell.contentView)
        //            make.top.equalTo(topCell.contentView).offset(30.0)
        //        }
        
        let nameLabel = UILabel()
        nameLabel.font = .regular(size: 22.0)
        nameLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.gunmetal.toHexString()]
        nameLabel.textAlignment = .center
        let fullName = "\(userData.firstName ?? "") \(userData.lastName ?? "")"
        nameLabel.text = fullName.count <= 1 ? "add_name".localized() : fullName
        topCell.contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(topCell.contentView)
            make.left.equalTo(topCell.contentView).offset(10)
            make.right.equalTo(topCell.contentView).offset(-10)
            make.top.equalTo(topCell.contentView).offset(30.0)
            make.height.equalTo(34)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let editViewController = EditViewController()
        editViewController.titleString = cells[indexPath.section][indexPath.row].titleString
        switch indexPath.row {
        case SCProfileDataType.SCProfileDataTypeName.rawValue:
            editViewController.setupCellsForFields(fields: [
                ("firstNameLabel".localized(), "name.placeholder".localized(), userData.firstName, true),
                ("lastNameLabel".localized(), "surname.palceholder".localized(), userData.lastName, true)
                ], fieldProfieleDataType: .SCProfileDataTypeName, tagSeries: 1000
            )
            let backItem = UIBarButtonItem()
            backItem.title = "cancel".localized()
            //dj todo learn
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(editViewController, animated: true)
        case SCProfileDataType.SCProfileDataTypeAddress.rawValue:
            editViewController.setupCellsForFields(fields: [
                ("addressLine1".localized(), "addressLine1.placeholder".localized(), userData.addressLine1, true),
                ("addressLine2".localized(), "addressLine2.placeholder".localized(), userData.addressLine2, false),
                ("zip".localized(), "zip.placeholder".localized(), userData.zipcode, true),
                ("city".localized(), "city.placeholder".localized(), userData.city, true),
                ("state".localized(), "state".localized(), userData.state, true),
                ], fieldProfieleDataType: .SCProfileDataTypeAddress, tagSeries: 2000
            )
            let backItem = UIBarButtonItem()
            backItem.title = "cancel".localized()
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(editViewController, animated: true)
        case SCProfileDataType.SCProfileDataTypeEmail.rawValue:
            editViewController.setupCellsForFields(fields: [
                ("email".localized(), "email.placeholder".localized(), userData.email, true)
                ], fieldProfieleDataType: .SCProfileDataTypeEmail, tagSeries: 3000
            )
            let backItem = UIBarButtonItem()
            backItem.title = "cancel".localized()
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(editViewController, animated: true)
        case SCProfileDataType.SCProfileDataTypePhone.rawValue:
            if SCUser.reStore()?.email != nil && SCUser.reStore()?.email != "" {
                let alert = UIAlertController(title: "changePhoneNumberDialogTitle".localized(), message: "changePhoneNumberDialogBody".localized(), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel , handler: nil))
                alert.addAction(UIAlertAction(title: "submit".localized(), style: .default, handler: {
                    (alert: UIAlertAction!) in
                    self.createTicketForNumberChange()
                    
                }))
                self.present(alert, animated: true)
                
            }
            else {
                showAlert(title: "emailMissingSupportRequestTitle".localized(), message: "emailMissingSupportRequestBody".localized())
            }
        default:
            return
        }
    }
    private func createTicketForNumberChange(){
        self.loaderShow(withDelegate: nil)
        let identity =  Identity.createAnonymous(name: userData.firstName, email: userData.email)
        Zendesk.instance?.setIdentity(identity)
        let requestProvider = ZDKRequestProvider()
        let request = ZDKCreateRequest()
        request.subject = "Request to change the phone number"
        request.requestDescription = "User trigger this request"
        
        requestProvider.createRequest(request) { (result, error) in
            DispatchQueue.main.async {
                self.loaderHide()
                if result != nil {
                    print(result ?? "unknown response")
                    self.showAlert(title: "", message: "We have received the request, our support team will contact you shortly")
                }
                else if error != nil {
                    print(error!.localizedDescription)
                }
            }
            
        }
    }
}
