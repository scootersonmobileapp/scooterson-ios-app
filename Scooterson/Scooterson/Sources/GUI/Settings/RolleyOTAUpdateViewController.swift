//
//  RolleyOTAUpdateViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/17/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import Foundation
import BLTNBoard
import Alamofire

class RolleyOTAUpdateViewController: BaseViewController, RolleyBleDelegate, BafangCanOtaCallback, BroadUIOTADelegate{
    
    private var headingUpdateLabel: UILabel!
    private var symstemInfoLabel: UILabel!
    private var updateTimeInfoLabel: UILabel!
    private var checkForUpdateButton: UIButton!
    private var page: OTAUpdateBoardPage!
    private var manager: BLTNItemManager!
    
    private var deviceInfo: SCDevice!
    private var peripheral: CBPeripheral!
    private var rolleyService: RolleyBleService!
    private var batterySW: String = "NA"
    private var controllerSW: String = "NA"
    private var panelSW: String = "NA"
    private var spinnerView: UIActivityIndicatorView!
    private var displayVersion = "NA"
    private var displayFileURL = "NA"
    private var processorVersion = "NA"
    private var processorFileURL = "NA"
    private var currentBattery = 0.0
    
    //Variable
    public var isForceUpdate = false
    
    override func viewDidLoad() {
        //Hide the line for the navigation bar from the top
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
        self.peripheral = tabBarController.peripheral
        self.rolleyService = RolleyBleService()
        self.deviceInfo = tabBarController.device
        
        super.viewDidLoad()
        setupView()
        checkForUpdate()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
        super.viewWillAppear(animated)
        checkForOTAUpdate { (displayVersion, displayFileURL, processorVersion, processorFileURL) in
            self.displayVersion = displayVersion
            self.displayFileURL = displayFileURL
            self.processorFileURL = processorFileURL
            self.processorVersion = processorVersion
        }
        
        
    }
    
    //============================== Set up Views =========================
    private func setupView(){
        let headerView = setupTitleView()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        self.navigationItem.title = ""
        let mainStatusView = setupStatusView()
        self.view.addSubview(mainStatusView)
        mainStatusView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.top.equalTo((headerView?.snp.bottom)!).offset(5.0)
            make.height.equalTo(450)
        })
        
        let lastSection = setBottomButton()
        self.view.addSubview(lastSection)
        lastSection.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.safeAreaLayoutGuide).offset(20.0)
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            if(isSmallScreen()){
                make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-30.0)
            }else{
                make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-10.0)
            }
            make.height.equalTo(60.0)
            
        }
    }
    
    
    private func setupTitleView() -> UIView? {
        let titleView = UIView()
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 30)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = "update".localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    
    private func setBottomButton() -> UIView{
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 90))
        checkForUpdateButton = UIButton(type: .custom)
        checkForUpdateButton.backgroundColor = .dodgerBlue
        checkForUpdateButton.setTitleColor(.white, for: .normal)
        checkForUpdateButton.setTitleColor(.charcoalGrey, for: .highlighted)
        checkForUpdateButton.layer.cornerRadius = 10.0
        if isForceUpdate {
             checkForUpdateButton.setTitle("unlockValidate".localized(), for: .normal)
        }else{
            checkForUpdateButton.setTitle("checkForUpdate".localized(), for: .normal)
        }
        checkForUpdateButton.addTarget(self, action: #selector(checkForUpdate), for: .touchUpInside)
        view.addSubview(checkForUpdateButton)
        checkForUpdateButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.bottom.equalTo(view)
        }
        return view
    }
    
    @objc private func checkForUpdate() {
        if checkForUpdateButton.titleLabel?.text == "updateNow".localized(){
            //todo check for battery
            let phoneLevel = UIDevice.current.batteryLevel
            print("OTA Phone level \(phoneLevel)")
            if currentBattery < 50{
                self.showAlert(title: "batteryLowWarningTitle".localized(), message: "batteryLowWarningMessage".localized())
                return
            }else if (phoneLevel < 0.3){
                self.showAlert(title: "batteryLowWarningTitle".localized(), message: "phonebatteryLowWarningMessage".localized())
                return
            }
            //SetAuto off to never ever
            self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 0xff)
            
            //update the scooter
            let actionType = self.validateOTAUpdateNeeded()
            let url = actionType == 1 ? processorFileURL : displayFileURL
            page = BoardUi.showOTAUpdateBoard(delegate: self, fileURL: url)
            manager = BoardUi.getDeviceManger(item: page)
            manager.showBulletin(above: self)
            return
        }else if checkForUpdateButton.titleLabel?.text == "unlockRolleyTitle".localized(){
            let url = processorFileURL
            page = BoardUi.showOTAUpdateBoard(delegate: self, fileURL: url)
            manager = BoardUi.getDeviceManger(item: page)
            manager.showBulletin(above: self)
        }
        spinnerView.isHidden = false
        spinnerView.startAnimating()
        headingUpdateLabel.text = "lookingForUpdate".localized()
        symstemInfoLabel.isHidden = true
        updateTimeInfoLabel.isHidden = true
        checkForUpdateButton.isHidden = true
        Timer.scheduledTimer(withTimeInterval: 10 , repeats: false, block: { (timer) in
            self.whenLookupForUpdateComplete(isUpdateNeeded: (self.validateOTAUpdateNeeded() > 0))
        })
    }
    
    private func doOTA(){
        
        BafangCanOtaHelper.getInstance()?.ota_cb = self
        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        documentsURL.appendPathComponent("scooterson_rolley.bin")
        print("OTA update file found url  \(documentsURL.path)")
        
        BafangCanOtaHelper.getInstance()?.setPatchFilePath(documentsURL.path)
        BafangCanOtaHelper.getInstance()?.start()
    }
    
    func onProgress(_ realSize: Int32, pathSize: Int32, percent: Int32) {
        self.page.descriptionLabel?.text = "Installing update...\(percent) %"
        
    }
    func onFinish(_ status: Int32) {
        if self.validateOTAUpdateNeeded() == 3 {
            isVisible(fileURL: processorFileURL)
            displayVersion = panelSW// to stop retry  loop
        }else{
            handleKey()
        }
        
    }
    
    private func handleKey(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.deviceInfo.deviceParams!.isKeyFeatureEnabled {
                let infoKey: String = (self.deviceInfo.deviceParams?.key)!
                let newKey  = "0000\(infoKey)"
                self.rolleyService.sendPinCommandToRolley(command: APPController_SetPIN, pin: newKey)
                print("OTA update setting key again with key \(infoKey)")
            }
            if self.manager.isShowingBulletin {
                self.manager.dismissBulletin()
            }
            //rest autoOFF
            self.whenLookupForUpdateComplete(isUpdateNeeded: false)
            self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 3)
            
            if self.isForceUpdate{
                self.rolleyService.sendCommandToRolley(command: APPController_PowerOnOff, data: 1)
                self.rolleyService.disconnect()
                guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                tabbarC.moveToDeviceTab()
            }
        }
        
    }
    
    private func whenLookupForUpdateComplete(isUpdateNeeded: Bool){
        
        spinnerView.isHidden = true
        spinnerView.stopAnimating()
        symstemInfoLabel.isHidden = false
        updateTimeInfoLabel.isHidden = false
        checkForUpdateButton.isHidden = false
        updateSytemInfo()
        deviceInfo.deviceParams?.lastUpdateCheck = Date().currentTimeMillis()
        deviceInfo.update()
        
        if isUpdateNeeded{
            headingUpdateLabel.text = "systemUpdateStatusHeader2".localized()
            checkForUpdateButton.setTitle("updateNow".localized(), for: .normal)
        }else if isForceUpdate {
              headingUpdateLabel.text = "unlockNow".localized()
             checkForUpdateButton.setTitle("unlockRolleyTitle".localized(), for: .normal)
        }else{
            checkForUpdateButton.setTitle("checkForUpdate".localized(), for: .normal)
            headingUpdateLabel.text = "systemUpdateStatusHeader".localized()
        }
        
    }
    
    
    private func setupStatusView() -> UIView{
        let statusView = UIView()
        statusView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        
        let updateImageView = UIImageView()
        updateImageView.image = UIImage(named: "blue-walking-scooter")
        statusView.addSubview(updateImageView)
        updateImageView.snp.makeConstraints{ (make) in
            make.left.equalTo(statusView).offset(20.0)
            make.top.equalTo(statusView).offset(20)
            make.height.equalTo(45.0)
            make.width.equalTo(45.0)
        }
        headingUpdateLabel = UILabel()
        headingUpdateLabel.font = .bold(size: 20.0)
        headingUpdateLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.black.toHexString()]
        headingUpdateLabel.textAlignment = .left
        headingUpdateLabel.text = "systemUpdateStatusHeader".localized()
        statusView.addSubview(headingUpdateLabel)
        headingUpdateLabel.snp.makeConstraints { (make) in
            make.left.equalTo(statusView).offset(20.0)
            make.right.equalTo(statusView).offset(-20.0)
            make.top.equalTo(updateImageView.snp.bottom).offset(10.0)
            make.height.equalTo(30.0)
        }
        
        symstemInfoLabel = UILabel()
        symstemInfoLabel.font = .regular(size: 18.0)
        symstemInfoLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.black.toHexString()]
        symstemInfoLabel.textAlignment = .left
        symstemInfoLabel.text = "Data"
        symstemInfoLabel.numberOfLines = 12
        statusView.addSubview(symstemInfoLabel)
        symstemInfoLabel.snp.makeConstraints { (make) in
            make.left.equalTo(statusView).offset(20.0)
            make.right.equalTo(statusView).offset(-20.0)
            make.top.equalTo(headingUpdateLabel.snp.bottom).offset(10.0)
            make.height.equalTo(250.0)
        }
        
        updateTimeInfoLabel = UILabel()
        updateTimeInfoLabel.font = .regular(size: 18.0)
        updateTimeInfoLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.black.toHexString()]
        updateTimeInfoLabel.textAlignment = .left
        updateTimeInfoLabel.numberOfLines = 2
        updateTimeInfoLabel.text = ""
        statusView.addSubview(updateTimeInfoLabel)
        updateTimeInfoLabel.snp.makeConstraints { (make) in
            make.left.equalTo(statusView).offset(20.0)
            make.right.equalTo(statusView).offset(-20.0)
            make.top.equalTo(symstemInfoLabel.snp.bottom).offset(isSmallScreen() ? 10.0 : 30.0)
            make.height.equalTo(50.0)
        }
        
        spinnerView = UIActivityIndicatorView(style: .whiteLarge)
        spinnerView.color = UIColor.dodgerBlue
        
        spinnerView.isHidden = true
        statusView.addSubview(spinnerView)
        spinnerView.snp.makeConstraints { (make) in
            make.centerX.equalTo(statusView)
            make.centerY.equalTo(statusView)
            make.height.equalTo(100.0)
        }
        
        return statusView
        
        
    }
    
    
    func deviceStatus(status: Int32) {
        if(self.rolleyService.isConnected){
            rolleyService.fetchDriveTrainInfo()
        }
    }
    
    
    func onInfoReceived(command: Int32, value: String!) {
        switch command {
        case EBoxControllerSWVersion:
            controllerSW = value
            print("OTA current version on processor \(String(describing: value))")
            
        case EBoxPanelSWVersion:
            panelSW = value
            print("OTA current version on display \(String(describing: value))")
            
        case EBoxBatterySWVersion:
            batterySW = value
        
        case EBoxInfBMSRemainCapacity:
            let batNowCapacity = (value as NSString).doubleValue
            self.currentBattery = Double(batNowCapacity/100)
        default:
            break
        }
        updateSytemInfo()
    }
    
    private func updateSytemInfo(){
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        var symtemInfo = "appVersion".localized()
        symtemInfo.append("\n")
        symtemInfo.append(appVersion!)
        symtemInfo.append("\n\n")
        symtemInfo.append("processorVersion".localized())
        symtemInfo.append("\n")
        symtemInfo.append(controllerSW)
        symtemInfo.append("\n\n")
        symtemInfo.append("displayVersion".localized())
        symtemInfo.append("\n")
        symtemInfo.append(panelSW)
        symtemInfo.append("\n\n")
        symtemInfo.append("batteryVersion".localized())
        symtemInfo.append("\n")
        symtemInfo.append(batterySW)
        
        
        symstemInfoLabel.text = symtemInfo
        var timeInfoString = "lastCheckForUpdate".localized()
        timeInfoString.append(" ")
        timeInfoString.append(getStringForDateWithDay(time: Date().currentTimeMillis()))
        
        updateTimeInfoLabel.text = timeInfoString
        
    }
    
    private func validateOTAUpdateNeeded() -> Int{
        
        if((displayVersion == "NA" && processorVersion == "NA") || (controllerSW.count < 3 || panelSW.count < 3)){
            return 0
        }
        
        let actionType = displayVersion != panelSW && processorVersion != controllerSW ? 3 : displayVersion != panelSW ?  2 : processorVersion != controllerSW ? 1 : 0
        print("OTA update validation action type  \(actionType)")
        return actionType
    }
    var downloadRequest: Alamofire.Request!
    func isVisible(fileURL: String) {
        downloadRequest = API.shared.downloadOTAFile(fileURL: fileURL) { (progess, isCompleted) in
            print("OTA update file downloaded with progress \(progess)")
            self.page.descriptionLabel?.text = "Downloading... \(Int(progess *  100)) %"
            if isCompleted{
                print("OTA update file download completed")
                self.doOTA()
            }
        }
    }
    
    
    func cancelButtonTapped() {
        BafangCanOtaHelper.getInstance()?.stop()
        downloadRequest.cancel()
        let data = [CAN_FW_CANCEL]
        let mData = Data(bytes: data, count: 1)
        PLBleService.getInstance()?.send_data_encry(mData, uuid: UUID_CAN_CONTROL, is_long_pkt: false)
        handleKey()
    }
    
    
    
}
