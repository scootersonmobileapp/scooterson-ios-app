//
//  RolleySettingsViewContoller.swift
//  Scooterson
//
//  Created by deepansh jain on 6/7/20.
//  Copyright © 2020 Deepansh Jain. All rights reserved.
//

import Foundation
import CoreLocation
import ZendeskCoreSDK
import SupportSDK
import SDKConfigurations

enum SectionsForRolley: Int {
    case SectionRolley
    case SectionGeneral
    case SectionSupport
    case SectionAbout
    static let count: Int = {
        var max: Int = 0
        while let _ = SectionsForRolley(rawValue: max) { max += 1 }
        return max
    }()
}

class RolleySettingsViewContoller: BaseTableViewController, RolleyBleDelegate, AuthenticationDelgate{
    
    private var loader: LoaderView!
    private var headerViews: [UIView] = []
    private var peripheral: CBPeripheral!
    private var rolleyService: RolleyBleService!
    private var autoOff: SCTableViewCell!
    private var keyFeature: SCTableViewCell!
    private var intelligentLight: SCTableViewCell!
    private var rolleyCell:[SCTableViewCell] = []
    private var generalCells: [SCTableViewCell] = []
    private var systemUpdate:SCTableViewCell!
    private var sharingCell :SCTableViewCell!
    private var deviceInfo: SCDevice!
    private var notificationCell: SCTableViewCell!
    private var darkModeCellIos13: SCTableViewCell!
    
    private var TAG_RATE_US  = 10000
    private var TAG_SYSTEM_UPDATE  = 10001
    private var TAG_SHARING  = 10002
    
    
    
    //Constant
    private var autoOffPicker = ["autoOff3Min".localized(), "autoOff9Min".localized(), "autoOffNever".localized()]
    private var darkModeIos13Picker = ["automatically".localized(),"On", "Off"]
    private let PICKER_TYPE_AUTO_OFF = 1
    private let PICKER_TYPE_DARK_MODE = 2
    private var isKeyRemovedAction = false
    private var currentKeyStatus: Bool = false
    private var lastUpdateCheckSubHeadingString = ""
    
    
    //Variable
    public var isForceUpdate = false
    
    override func viewDidLoad() {
        titleString = "settings".localized()
        guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
        self.peripheral = tabBarController.peripheral
        self.rolleyService = RolleyBleService()
        self.deviceInfo = tabBarController.device
        sectionHeaderViews()
        super.viewDidLoad()
        
        // add notification observers
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
    }
    
    // MARK: - Notification oberserver methods
    
    @objc func didBecomeActive() {
        print("settings did become active")
        handleNotificationStateOnResume()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
        super.viewWillAppear(animated)
        updateTheViewWithChangeInParams()
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return SectionsForRolley.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerViews[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = cells[indexPath.section][indexPath.row]
        if cell.expandable {
            UIView.animate(withDuration: 0.2, animations: {
                
                cell.expandedView.snp.updateConstraints { (update) in
                    update.height.equalTo(cell.expanded ? 0 : 120)
                }
                cell.expanded = !cell.expanded
                
                self.mainTable.reloadData()
                cell.isSelected = cell.expanded
            })
        }
        switch cell.tag {
        case TAG_RATE_US:
            //todo
            print("Rate us")
        case TAG_SYSTEM_UPDATE:
            if(isInternetCheckOkay(withMessageForAlert: "systemUpdateRequiredInternet".localized())){
                let addDeviceVC = RolleyOTAUpdateViewController()
                addDeviceVC.isForceUpdate = self.isForceUpdate
                let backItem = UIBarButtonItem()
                backItem.title = "settings".localized()
                backItem.tintColor = .dodgerBlue
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(addDeviceVC, animated: true)
            }
        case TAG_SHARING:
            
            if !deviceInfo.deviceParams!.isKeyFeatureEnabled{
                showAlert(title: "keyFeatureOffTitle".localized(), message: "keyFeatureOffMessageBody".localized())
                return
            }
            
            let sharingVC = RolleySharingSettingsViewController()
            let backItem = UIBarButtonItem()
            backItem.title = "settings".localized()
            backItem.tintColor = .dodgerBlue
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(sharingVC, animated: true)
        default:
            break
        }
    }
    
    private func updateTheViewWithChangeInParams() {
        //setting up UI
        
        if(deviceInfo.deviceParams?.lastUpdateCheck != nil){
            lastUpdateCheckSubHeadingString = "systemUpdateSubTitle".localized()
            lastUpdateCheckSubHeadingString.append(" ")
            lastUpdateCheckSubHeadingString.append(getStringForDateWithDay(time: (deviceInfo.deviceParams?.lastUpdateCheck)!))
        }
        
        
        //update the last check for sysmtem update
        systemUpdate.infomationLabel.text = lastUpdateCheckSubHeadingString
        sharingCell.titleLabel.text = "\("sharingTitle".localized())\(deviceInfo.deviceParams!.isShared ? "ON" :"OFF")"
        
        // Notification set
        print("settings Notification current state: \(isNotification())")
        notificationCell.switchControl.isOn = isNotification()
    }
    
    
    private func sectionHeaderViews() {
        let titles = [deviceInfo.type == ConstantValues.PRODUCT_TYPE_ROLLEY ? "Rolley".localized(): "Rolley+".localized(), "General".localized(), "Support".localized(), "About".localized()]
        for i in 0..<Section.count {
            let titleView = UIView()
            titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
            
            let titleLabel = UILabel()
            titleLabel.font = .bold(size: 16)
            titleLabel.textColor = .orange
            titleLabel.textAlignment = .left
            titleLabel.text = titles[i]
            titleView.addSubview(titleLabel)
            titleLabel.snp.makeConstraints { (make) in
                make.left.equalTo(titleView).offset(20)
                make.right.equalTo(titleView).offset(-20)
                make.centerY.equalTo(titleView)
            }
            headerViews.append(titleView)
        }
    }
    override func setupCells() {
        
        let darkModeCell = self.setupSwitchCell(withTitle: "darkMode".localized(), initValue: isDarkMode(), handler: {
            self.switchDarkMode()
        })
        
        darkModeCellIos13 = self.setupPickerCell(withTitle: "darkMode".localized())
        darkModeCellIos13.valueString = darkModeIos13Picker[darkModeIos13SetValue()]
        darkModeCellIos13.picker.tag = PICKER_TYPE_DARK_MODE
        
        
        
        notificationCell = self.setupSwitchCell(withTitle: "notification".localized(), initValue: isNotification(), handler: {
            setNotification()
            if !UserDefaults.standard.bool(forKey: ContantsKey.settingsNotificationPermission.rawValue) {
                self.checkNotificationSettingsAndSet()
            }
        })
        
        let currentUnit =  isImperial() == true ? 1 : 0
        let unitsCell = self.setupSegmentCell(withTitle: "units".localized(), values: ["kph".localized(),"mph".localized()], initValue: currentUnit, handler: {
            setImperial()
        })
        if #available(iOS 13.0, *) {
            generalCells = [notificationCell, darkModeCellIos13, unitsCell]
        }else{
            generalCells = [notificationCell, darkModeCell, unitsCell]
        }
        
        // Setup Support section
        let helpChat = self.setupButtonCell(withTitle: "supportSettings".localized(), buttonTitle: "getHelp".localized()) {
            
            let userData = SCUser.reStore()!
            if(userData.email != ""){
                let identity =  Identity.createAnonymous(name: userData.firstName, email: userData.email)
                Zendesk.instance?.setIdentity(identity)
                let config = RequestUiConfiguration()
                config.subject = "iOS Issue"
                config.tags = ["ios", "mobile"]
                let requestController = RequestUi.buildRequestUi(with: [config])
                self.navigationController?.pushViewController(requestController, animated: true)
            }else{
                print("Email not found")
                self.showOneButtonDialog(title: "emailMissingSupportRequestTitle".localized(), body: "emailMissingSupportRequestBody".localized())
            }
            
        }
        let supportCells:[SCTableViewCell] = [helpChat]
        
        let rateUs = self.setupJustHeaderCell(withTitle: "rateUs".localized(),tag: TAG_RATE_US)
        
        
        let privacyPolicy = self.setupButtonCell(withTitle: "privacyPolicy".localized(), buttonTitle: "read".localized()) {
            UIApplication.shared.open(URL(string: "https://scooterson.com/agreements/privacypolicy")!, options: [:], completionHandler: nil)
        }
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let versionCell = self.setupOneValuedCell(withTtile: "version".localized(), andText: appVersion!)
        let aboutCells = [rateUs,privacyPolicy,versionCell]
        
        
        keyFeature = self.setupSwitchCellWithSubTitle(withTitle: "keyFeature".localized(), subtitle:  "keyFeatureSummary".localized() ,initValue: deviceInfo.deviceParams!.isKeyFeatureEnabled, handler: {
            self.toggleKeyFeature()
        })
        
        intelligentLight = self.setupSwitchCellWithSubTitle(withTitle: "intelligentlight".localized(), subtitle:  "intelligentLightSummary".localized() ,initValue: getCurrentIntelligentLight(), handler: {
            self.toggleIntelligentLight(status: self.getCurrentIntelligentLight())
        })
        
        
        let chargingReminder = self.setupSwitchCellWithSubTitle(withTitle: "chargingReminder".localized(), subtitle:  "chargingReminderSummary".localized() ,initValue: deviceInfo.deviceParams!.isCharingReminderSet, handler: {
            self.toggleChangingReminder()
        })
        
        let SMA = self.setupSwitchCellWithSubTitle(withTitle: "smartModeAcceleration".localized(), subtitle:  "smartModeAccelerationSummary".localized() ,initValue: deviceInfo.deviceParams!.isSmartModeAccSet, handler: {
            self.toggleSMA()
        })
        
        
        autoOff = self.setupPickerCellWithSubTitle(withTitle: "autoOFF".localized(), subTitle: "autoOffSubTitle".localized())
        autoOff.valueString = autoOffPicker[getCurrentAutoOFFValue()]
        autoOff.picker.tag = PICKER_TYPE_AUTO_OFF
        
        sharingCell = self.setupDefaultSettingsCell(withTitle: "sharingTitle".localized(), subTitle: "sharingTitleSubTitle".localized(), tag: TAG_SHARING)
        
        
        systemUpdate = self.setupDefaultSettingsCell(withTitle: "systemUpdateTitle".localized(), subTitle: lastUpdateCheckSubHeadingString, tag: TAG_SYSTEM_UPDATE)
        
        
        rolleyCell = [keyFeature,intelligentLight,chargingReminder,SMA,autoOff,sharingCell,systemUpdate]
        
        if isForceUpdate{
            systemUpdate = self.setupDefaultSettingsCell(withTitle: "unlock".localized(), subTitle: lastUpdateCheckSubHeadingString, tag: TAG_SYSTEM_UPDATE)
            rolleyCell = [systemUpdate]
        }else if(deviceInfo.type == ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS){
            rolleyCell = [keyFeature,sharingCell,SMA,intelligentLight,chargingReminder,autoOff,systemUpdate]
        }else{
            rolleyCell = [keyFeature,intelligentLight,autoOff,systemUpdate]
        }
        if !deviceInfo.isOwner{
             rolleyCell = [intelligentLight]
        }
        
        
        cells = [rolleyCell,generalCells,supportCells,aboutCells]
        
    }
    
    private func getCurrentAutoOFFValue() -> Int{
        var rowValue = 0
        let value = rolleyService.getStringValueForAutoOff()
        print("Auto Off value from Rolley \(value)")
        if(value == "3"){
            rowValue = 0
        }else if (value == "9"){
            rowValue = 1
        }else{
            rowValue = 2
        }
        return rowValue
    }
    
    private func getCurrentIntelligentLight() -> Bool{
        var status = false
        let value = rolleyService.getStringValueForIntelligentLight()
        if(value == "5"){
            status = true
        }
        return status
    }
    
    
    private func toggleIntelligentLight(status: Bool){
        if(PLBleService.getInstance() != nil){
            if(status){
                self.rolleyService.sendCommandToRolley(command: APPSetLightSens, data:0)
            }else{
                self.rolleyService.sendCommandToRolley(command: APPSetLightSens, data:5)
            }
        }
    }
    
    private func toggleKeyFeature(){
        if(deviceInfo.deviceParams!.isKeyFeatureEnabled || currentKeyStatus){
            if deviceInfo.deviceParams!.isShared{
                self.keyFeature.switchControl.isOn = true
                showAlert(title: "sharingOnTitle".localized(), message: "sharingOnMessageBody".localized())
                return
            }
            isKeyRemovedAction = true
            authenticationWithBiometricOrPhonePin(authicationDelgate: self)
        }else{
            if(!isInternetCheckOkay(withMessageForAlert: "keyFeatureRequireInternet".localized())){
                self.keyFeature.switchControl.isOn = false
                return
            }
            loaderShow(withDelegate: nil)
            API.shared.getKeyForKeyFeature(deviceId: self.deviceInfo.credential!.deviceID ) { (success, info, statusCode) in
                if success{
                    self.loaderHide()
                    print("key feature Success")
                    self.deviceInfo.deviceParams?.key = info
                    self.deviceInfo.deviceParams?.isKeyFeatureEnabled = true
                    self.deviceInfo.update()
                    let newKey  = "0000\(info)"
                    self.rolleyService.sendPinCommandToRolley(command: APPController_SetPIN, pin: newKey)
                    self.showkeyFeatureDialog(title: "keyFeatureSet".localized(),body: "keyFeatureSetDialogBody".localized())
                }else{
                    self.loaderHide()
                    self.keyFeature.switchControl.isOn = false
                    //show error
                    if(statusCode == 500){
                        //todo
                        //Show contact  us dilaog
                        self.showOneButtonDialog(title: "Error \(statusCode)", body: info)
                    }else{
                        self.showOneButtonDialog(title: "Error \(statusCode)", body: info)
                    }
                }
            }
            
        }
    }
    
    
    
    //Mark: Rolley Connection factory func
    func deviceStatus(status: Int32) {
        switch (status) {
        case PL_BLE_STATUS_RECONNECTING:
            //Dismiss when state moved to disconnected
            //todo show popup on the device screen that its been disconnected
            self.rolleyService.disconnect()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.rolleyService.disconnect()
                guard let tabbarC: SCTabBarController =  self.tabBarController as? SCTabBarController  else {return}
                tabbarC.moveToDeviceTab()
            }
        default:
            break
        }
    }
    
    func onInfoReceived(command: Int32, value: String!) {
        switch command {
        case EBoxInfPINStatus:
            print("Key Feature in settings: \(value!)")
            let currentKeyStatus = Int(value)
            if(currentKeyStatus == 0){
                self.keyFeature.switchControl.isOn = false
                self.currentKeyStatus = false
            }else if (currentKeyStatus == 1){
                self.keyFeature.switchControl.isOn = true
                self.currentKeyStatus = true
            }else{
                self.keyFeature.switchControl.isOn = true
                self.currentKeyStatus = true
            }
        case APPController_SetPIN:
            print("Key Feature set status in settings: \(value!)")
            if value == "1" && isKeyRemovedAction{
                let key = self.deviceInfo.deviceParams?.key
                let resetKey = "\(key!)0000"
                print("Key feature pin \(resetKey)")
                self.deviceInfo.deviceParams?.key = "0000"
                self.deviceInfo.deviceParams?.isKeyFeatureEnabled = false
                self.deviceInfo.update()
                self.isKeyRemovedAction = false
            }else{
                self.failed()
            }
            
        case EBoxLightSensNum:
            intelligentLight.switchControl.isOn = getCurrentIntelligentLight()
            
        default:
            break;
        }
        
    }
    //Unit: Util for Alert
    private func showOneButtonDialog (title: String, body: String){
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "okay".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    private func showkeyFeatureDialog (title: String, body: String){
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "showMe".localized(), style: .default, handler: {
            (alert: UIAlertAction!) in
            authenticationWithBiometricOrPhonePin(authicationDelgate: self)
        }))
        alert.addAction(UIAlertAction(title: "okay".localized(), style: .default, handler: {
            (alert: UIAlertAction!) in
            print("Key Feature Autherication requested")
            let key = self.deviceInfo.deviceParams?.key
            self.rolleyService.sendPinCommandToRolley(command: APPController_AuthPIN, pin: key!)
        }))
        self.present(alert, animated: true)
        
    }
    
    
    
    
    // MARK: Picker view methods
    override func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag{
        case PICKER_TYPE_AUTO_OFF:
            return autoOffPicker.count
        case PICKER_TYPE_DARK_MODE:
            return darkModeIos13Picker.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        let label = UILabel()
        label.theme_textColor = [UIColor.dodgerBlue.toHexString(),UIColor.charcoalGrey.toHexString()]
        label.textAlignment = .center
        label.font = .regular(size: 20)
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        //IOS 14 doesnt return count 2 need to handle for ios <= 13
        if pickerView.subviews.count > 2{
            pickerView.subviews[2].theme_backgroundColor = [UIColor.white.toHexString(),UIColor.charcoalGrey.toHexString()]
            pickerView.subviews[1].theme_backgroundColor = [UIColor.white.toHexString(),UIColor.charcoalGrey.toHexString()]
        }
        
        switch pickerView.tag{
        case PICKER_TYPE_AUTO_OFF:
            label.text = autoOffPicker[row]
        case PICKER_TYPE_DARK_MODE:
            label.text = darkModeIos13Picker[row]
        default:
            break
        }
        
        
        return view
    }
    
    override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag{
        case PICKER_TYPE_AUTO_OFF:
            self.autoOff.valueString = autoOffPicker[row]
            if(autoOffPicker[row] == "autoOff3Min".localized()){
                self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 3)
            }else if (autoOffPicker[row] == "autoOff9Min".localized()){
                self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 9)
            }else{
                self.rolleyService.sendCommandToRolley(command: APPSetAutoOff, data: 0xff)
            }
        case PICKER_TYPE_DARK_MODE:
            self.darkModeCellIos13.valueString = darkModeIos13Picker[row]
            updateDarkMode(valueSet: row)
            break
        default:
            break
        }
        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return autoOffPicker[row]
    }
    func success() {
        let key = self.deviceInfo.deviceParams?.key
        if(isKeyRemovedAction){
            let resetKey = "\(key!)0000"
            self.rolleyService.sendPinCommandToRolley(command: APPController_SetPIN, pin: resetKey)
        }else{
            self.rolleyService.sendPinCommandToRolley(command: APPController_AuthPIN, pin: key!)
        }
    }
    
    func failed() {
        //Cannot
        if(isKeyRemovedAction){
            DispatchQueue.main.async() {
                self.isKeyRemovedAction = false
                self.keyFeature.switchControl.isOn = true
                self.showToast(title: "autheticationFailed".localized(), message: "autheticationFailedMessage".localized())
            }
        }
    }
    
    private func isInternetCheckOkay(withMessageForAlert message:String) -> Bool{
        if !Reachability.isConnectedToInternet{
            self.showAlert(title: "noNetwork".localized(), message: message)
            return false
        }else{
            return true
        }
        
    }
    
    private func toggleChangingReminder(){
        let isSet = !deviceInfo.deviceParams!.isCharingReminderSet
        sendAttributes(key: "chargingReminderSet", value: isSet)
        deviceInfo.deviceParams?.isCharingReminderSet = isSet
        deviceInfo.update()
    }
    
    private func toggleSMA(){
        let isSet = !deviceInfo.deviceParams!.isSmartModeAccSet
        deviceInfo.deviceParams?.isSmartModeAccSet = isSet
        deviceInfo.update()
        if (isSet){
            showAlert(title: "smaActiveTitle".localized(), message: "smaActiveMessageBody".localized())
        }else{
          self.rolleyService.sendCommandToRolley(command: APPSetAutoDrive, data: 0)
        }
    }
    
    private func checkNotificationSettingsAndSet() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                DispatchQueue.main.async {
                    self.showAlertForPermissionNotFound(title: "enableNotificationTitle".localized(), message: "enableNotificationMessage".localized(), actionButtoTitle: "settings".localized())
                }
            }
            
        }
    }
    private func handleNotificationStateOnResume(){
        //update noitification
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                DispatchQueue.main.async {
                    setNotification()
                    self.notificationCell.switchControl.isOn = false    
                }
            }else{
                UserDefaults.standard.set(true, forKey: ContantsKey.settingsNotificationPermission.rawValue)
                UserDefaults.standard.synchronize()
            }
            
        }
    }
    
    func sendAttributes(key:String, value: Any){
        let _MQTT = API.shared.mqtt
        if(_MQTT != nil  && _MQTT?.connState == .connected){
            let message = "{\"\(key)\":\"\(value)\"}"
            print("Mqtt: \(message)")
            _MQTT?.publish("v1/devices/me/attributes", withString: message, qos: .qos1)
        }
    }
    
}
