//
//  RolleySharingSettingsViewController.swift
//  Scooterson
//
//  Created by deepansh jain on 6/19/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import UIKit
import ContactsUI
import CoreLocation

class RolleySharingSettingsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, CNContactPickerDelegate {
    
    private var sharingSectionOneHeading: UILabel!
    private var sharingSectionOneSubHeading: UILabel!
    private var sharingSectionOneSwitchButton: UISwitch!
    private var sharingSectionTwoSubHeading: UILabel!
    private var contactTableView: UITableView!
    private var contactSectionHeading: UILabel!
    private var contactSection: UIView!
    private var spinnerView:  UIActivityIndicatorView!
    private var addContactButton: UIButton!
    
    private var deviceInfo: SCDevice!
    private var lastLocation: LastLocation!
    private var tableViewData =  [SharedContact] ()
    
    private var isShared: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        if(self.deviceInfo == nil){
            guard let tabBarController = self.tabBarController as? SCTabBarController else { return }
            self.deviceInfo = tabBarController.device
        }
        
        setupValues()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        //self.rolleyService.intializeService(getPeripheral: peripheral, delegate: self)
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        API.shared.getScooterLastLocation(deviceId: deviceInfo.credential!.deviceID) { (success, lastLocation ,statusCode) in
            if success{
                print("Location fetched successfull and last location \(lastLocation!)")
                let lastTime = getStringForDateWithDay(time: (((lastLocation?.latitude[0].lastTime)!)/1000))
                self.lastLocation = lastLocation
                self.sharingSectionTwoSubHeading.text = "lastLocatedAt".localized() + " " + lastTime
            }else{
                self.sharingSectionTwoSubHeading.text = "lastLocationFoundError".localized()
            }
        }
        //get contacts if already added before.
        getSharedContacts()
    }
    
    private func setupValues(){
        let param = deviceInfo.deviceParams
        if param != nil{
            self.isShared = param!.isShared
            print("Sharing device info found is: \(isShared)")
        }
        tableViewData = []
    }
    
    
    
    public func setDeviceInfoWhenLauchFromDevice(device: SCDevice){
        self.deviceInfo = device
    }
    
    
    private func setupView(){
        let headerView = setupTitleView()
        self.view.addSubview(headerView!)
        headerView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.height.equalTo(70)
        })
        self.navigationItem.title = ""
        
        let settingsView = setupSettingView()
        self.view.addSubview(settingsView)
        settingsView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.top.equalTo((headerView?.snp.bottom)!).offset(5.0)
            make.height.equalTo(200)
        })
        
        let lastSection = setupAddContactButton()
        self.view.addSubview(lastSection)
        lastSection.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.safeAreaLayoutGuide).offset(20.0)
            make.right.equalTo(self.view.safeAreaLayoutGuide).offset(-20.0)
            if(isSmallScreen()){
                make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-30.0)
            }else{
                make.bottom.equalTo(self.view.safeAreaLayoutGuide).offset(-10.0)
            }
            make.height.equalTo(60.0)
            
        }
        
        contactSection = setupContactSection()
        self.view.addSubview(contactSection)
        contactSection.snp.makeConstraints({ (make) in
            make.left.right.equalTo(self.view.safeAreaLayoutGuide)
            make.top.equalTo((settingsView.snp.bottom)).offset(5.0)
            make.bottom.equalTo(lastSection.snp.top).offset(5.0)
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData.count > 0 {
            contactSectionHeading.isHidden = false
            restoreTableViewFromBeenEmpty()
            if contactSection != nil{
                sharingSectionOneSubHeading.text =  "sharingSectionOneSubHeadingLableShared".localized()
                contactSection.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
            }
            
        }else{
            sharingSectionOneSubHeading.text =  "sharingSectionOneSubHeadingLableNotShared".localized()
            contactSectionHeading.isHidden = true
            setTableEmptyMessage("emptyTableViewMessage".localized())
            if contactSection != nil{
                contactSection.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
            }
        }
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contact = tableViewData[indexPath.row]
        print("Sharing element found: \(contact)")
        let cell =  setupMainCell(contact: contact)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            showAlertForDeletingContact(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "stop".localized()
    }
    
    private func showAlertForDeletingContact(indexPath: IndexPath){
        let contact = tableViewData[indexPath.row]
        let messagebody = String(format: NSLocalizedString("removeSharingMessage", comment: ""), arguments: [contact.name, contact.phoneNumber])
        let alert = UIAlertController(title: "removeSharingTitle".localized(), message: messagebody, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
        }))
        alert.addAction(UIAlertAction(title: "stop".localized(), style: .destructive, handler: {
            (alert: UIAlertAction!) in
            
            if !Reachability.isConnectedToInternet{
                self.showAlert(title: "noNetwork".localized(), message: "toRemoveContactPleaseConnectInternet".localized())
                return
            }
            self.loaderShow(withDelegate: nil)
            API.shared.removeSharedContact(deviceID: self.deviceInfo.credential!.deviceID, phoneNumber: contact.phoneNumber) { (success, statusCode) in
                self.loaderHide()
                if success{
                    self.tableViewData.remove(at: indexPath.row)
                    print("Row removed with size \(self.tableViewData.count)")
                    self.contactTableView.deleteRows(at: [indexPath], with: .fade)
                }else{
                    let message = "\("contactRemovalErrorMessage".localized()) \(statusCode)"
                    self.showAlert(title: "",    message: message)
                }
            }
            
        }))
        self.present(alert, animated: true)
    }
    
    
    
    
    // ================================ View =====================================
    
    private func setupMainCell(contact: SharedContact) -> SCTableViewCell {
        let cell = SCTableViewCell(style: .default, reuseIdentifier: nil)
        let titleLabel = UILabel()
        cell.selectionStyle = .none
        titleLabel.font = .regular(size: 20.0)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = contact.name
        cell.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.top.equalTo(cell.contentView).offset(20.0)
        }
        
        cell.descriptionLabel = UILabel()
        cell.descriptionLabel.font = .light(size: 16)
        cell.descriptionLabel.textAlignment = .left
        cell.descriptionLabel.text = "sharedOn".localized()+getStringForDateWithDay(time: contact.createdTime)
        cell.descriptionLabel.lineBreakMode = .byWordWrapping
        cell.descriptionLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        cell.contentView.addSubview(cell.descriptionLabel)
        cell.descriptionLabel.snp.makeConstraints { (make) in
            make.left.equalTo(cell.contentView).offset(20.0)
            make.right.equalTo(cell.contentView).offset(-20.0)
            make.top.equalTo(titleLabel.snp.bottom).offset(5.0)
            
        }
        
        
        return cell
        
    }
    
    
    
    private func setupTitleView() -> UIView? {
        let titleView = UIView()
        titleView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.black(size: 30)
        titleLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        titleLabel.textAlignment = .left
        titleLabel.text = "sharing".localized()
        titleView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView).offset(20)
            make.right.equalTo(titleView).offset(-20)
            make.centerY.equalTo(titleView)
        }
        return titleView
    }
    
    private func setupSettingView() -> UIView{
        let settingView = UIView()
        settingView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        
        let settingHeadingLabel = UILabel()
        settingHeadingLabel.font = .bold(size: 16)
        settingHeadingLabel.textColor = UIColor.pastelOrange
        settingHeadingLabel.textAlignment = .left
        settingHeadingLabel.text = "settings".localized()
        settingView.addSubview(settingHeadingLabel)
        settingHeadingLabel.snp.makeConstraints { (make) in
            make.left.equalTo(settingView).offset(20)
            make.right.equalTo(settingView).offset(-20)
            make.top.equalTo(settingView)
        }
        let sharingControllerView = getSharingSectionOneView()
        settingView.addSubview(sharingControllerView)
        sharingControllerView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(settingView)
            make.top.equalTo((settingHeadingLabel.snp.bottom))
            make.height.equalTo(90)
        })
        
        let sharingControllerSecondView = getSharingSectionSecondView()
        settingView.addSubview(sharingControllerSecondView)
        sharingControllerSecondView.snp.makeConstraints({ (make) in
            make.left.right.equalTo(settingView)
            make.top.equalTo((sharingControllerView.snp.bottom))
            make.height.equalTo(90)
        })
        
        let devidetLine = dividerLineView()
        settingView.addSubview(devidetLine)
        devidetLine.snp.makeConstraints({ (make) in
            make.left.right.equalTo(settingView)
            make.top.equalTo((sharingControllerSecondView.snp.bottom)).offset(4.0)
            make.height.equalTo(1)
        })
        
        
        return settingView
        
        
    }
    
    private func getSharingSectionOneView()-> UIView {
        let sharingControllerView = UIView()
        sharingSectionOneHeading = UILabel()
        sharingSectionOneHeading.font = .regular(size: 20)
        sharingSectionOneHeading.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        sharingSectionOneHeading.textAlignment = .left
        sharingSectionOneHeading.text =  isShared ? "sharingSectionOneHeadingLableShared".localized() : "sharingSectionOneHeadingLableNotShared".localized()
        sharingControllerView.addSubview(sharingSectionOneHeading)
        sharingSectionOneHeading.snp.makeConstraints { (make) in
            make.left.equalTo(sharingControllerView).offset(20)
            make.right.equalTo(sharingControllerView).offset(-20)
            make.top.equalTo(sharingControllerView).offset(20.0)
        }
        
        sharingSectionOneSubHeading = UILabel()
        sharingSectionOneSubHeading.font = .light(size: 16)
        sharingSectionOneSubHeading.numberOfLines = 2
        sharingSectionOneSubHeading.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        sharingSectionOneSubHeading.textAlignment = .left
        sharingSectionOneSubHeading.text =  isShared ? "sharingSectionOneSubHeadingLableShared".localized() : "sharingSectionOneSubHeadingLableNotShared".localized()
        sharingControllerView.addSubview(sharingSectionOneSubHeading)
        sharingSectionOneSubHeading.snp.makeConstraints { (make) in
            make.left.equalTo(sharingControllerView).offset(20)
            make.right.equalTo(sharingControllerView).offset(-80)
            make.top.equalTo(sharingSectionOneHeading.snp.bottom).offset(5.0)
        }
        
        sharingSectionOneSwitchButton = UISwitch()
        sharingSectionOneSwitchButton.onTintColor = .dodgerBlue
        sharingSectionOneSwitchButton.theme_tintColor = [UIColor.dodgerBlue.toHexString(), UIColor.charcoalGrey.toHexString()]
        sharingSectionOneSwitchButton.isOn = isShared
        sharingControllerView.addSubview(sharingSectionOneSwitchButton)
        sharingSectionOneSwitchButton.snp.makeConstraints { (make) in
            make.top.equalTo(sharingControllerView).offset(30.0)
            make.right.equalTo(sharingControllerView).offset(-20)
        }
        
        sharingSectionOneSwitchButton.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        
        //        sharingControllerView.tag = 1
        //        let tap = UILongPressGestureRecognizer(target:self, action: #selector(handleTap))
        //        tap.minimumPressDuration = 0
        //sharingControllerView.addGestureRecognizer(tap)
        
        return sharingControllerView
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        if !Reachability.isConnectedToInternet{
            showAlert(title: "noNetwork".localized(), message: "noInternetSharingToggle".localized())
            mySwitch.isOn = !value
            return
        }
        API.shared.toggleSharing(deviceID: deviceInfo.credential!.deviceID, isEnabled: value) { (success, errorCode) in
            if success{
                self.deviceInfo.deviceParams?.isShared = value
                self.deviceInfo.update()
                print("Sharing setting updated: \(String(describing: self.deviceInfo.deviceParams?.isShared))")
                self.isShared = value
                UIView.transition(with: self.sharingSectionOneHeading,duration: 0.25, options: .transitionCrossDissolve,animations: { [weak self] in
                    self!.sharingSectionOneHeading.text =  self!.isShared ? "sharingSectionOneHeadingLableShared".localized() : "sharingSectionOneHeadingLableNotShared".localized()
                    }, completion: nil)
                
                UIView.transition(with: self.addContactButton, duration: 0.3, options: .curveEaseInOut, animations: {
                    if value{
                        self.addContactButton.backgroundColor = .dodgerBlue
                    }else{
                        self.addContactButton.backgroundColor = .dodgerBlueDisable
                    }
                })
                
            }else{
                let errorMessageBody = String(format: NSLocalizedString("shareMessageErrorBody", comment: ""), arguments: [errorCode])
                self.showAlert(title: "error".localized(), message: errorMessageBody)
                mySwitch.isOn = !value
            }
        }
    }
    
    
    private func getSharingSectionSecondView() -> UIView{
        let sharingControllerView = UIView()
        let sharingSectionTwoHeading = UILabel()
        sharingSectionTwoHeading.font = .regular(size: 20)
        sharingSectionTwoHeading.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        sharingSectionTwoHeading.textAlignment = .left
        sharingSectionTwoHeading.text = "findMyRolley".localized()
        sharingControllerView.addSubview(sharingSectionTwoHeading)
        sharingSectionTwoHeading.snp.makeConstraints { (make) in
            make.left.equalTo(sharingControllerView).offset(20)
            make.right.equalTo(sharingControllerView).offset(-20)
            make.top.equalTo(sharingControllerView).offset(20.0)
        }
        
        sharingSectionTwoSubHeading = UILabel()
        sharingSectionTwoSubHeading.font = .light(size: 16)
        sharingSectionTwoSubHeading.theme_textColor =  [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        sharingSectionTwoSubHeading.textAlignment = .left
        sharingSectionTwoSubHeading.text = "fetching".localized()
        sharingControllerView.addSubview(sharingSectionTwoSubHeading)
        sharingSectionTwoSubHeading.snp.makeConstraints { (make) in
            make.left.equalTo(sharingControllerView).offset(20)
            make.right.equalTo(sharingControllerView).offset(-80)
            make.top.equalTo(sharingSectionTwoHeading.snp.bottom).offset(5.0)
        }
        
        let arrowNextImageView = UIImageView()
        arrowNextImageView.image = UIImage(named: "ArrowNext")
        sharingControllerView.addSubview(arrowNextImageView)
        arrowNextImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(sharingControllerView)
            make.right.equalTo(sharingControllerView).offset(-20)
        }
        
        
        sharingControllerView.tag = 2
        let tap = UILongPressGestureRecognizer(target:self, action: #selector(handleTap))
        tap.minimumPressDuration = 0
        sharingControllerView.addGestureRecognizer(tap)
        
        return sharingControllerView
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        let viewTapped = sender.view
        if sender.state == .began {
            viewTapped!.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
            switch viewTapped?.tag {
            case 1:
                self.sharingSectionOneSwitchButton.isOn = !self.sharingSectionOneSwitchButton.isOn
            case 2:
                if lastLocation == nil{
                    return
                }else if isLocationServiceIsEnabledAndPermissionGranted() == 1{
                    showAlertForPermissionNotFound(title: "enableLocationTitle".localized(), message: "enableLocationMessage".localized(), actionButtoTitle: "enable".localized())
                    return
                    
                }else if isLocationServiceIsEnabledAndPermissionGranted() == 0{
                    showAlert(title: "enableLocationTitle".localized(), message: "locationServiceIsOFF".localized())
                    return
                }
                
                
                let mapVC = MapViewController()
                mapVC.initalizeTheView(device: deviceInfo, locationModel: lastLocation)
                let backItem = UIBarButtonItem()
                backItem.title = "sharing".localized()
                backItem.tintColor = .dodgerBlue
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(mapVC, animated: true)
                break
            default:
                break
            }
            
        } else if sender.state == .ended || sender.state == .cancelled {
            viewTapped!.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        }
    }
    
    private func dividerLineView() -> UIView{
        let lineView = UIView(frame: CGRect(x: 0, y: 100, width: 320, height: 1.0))
        lineView.layer.borderWidth = 1.0
        lineView.layer.theme_borderColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        return lineView
    }
    
    private func setupContactSection() -> UIView{
        let contactView = UIView()
        contactView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.white.toHexString()]
        
        contactSectionHeading = UILabel()
        contactSectionHeading.font = .bold(size: 16)
        contactSectionHeading.textColor = UIColor.pastelOrange
        contactSectionHeading.textAlignment = .left
        contactSectionHeading.text = "contacts".localized()
        contactSectionHeading.isHidden = true
        contactView.addSubview(contactSectionHeading)
        contactSectionHeading.snp.makeConstraints { (make) in
            make.left.equalTo(contactView).offset(20)
            make.right.equalTo(contactView).offset(-20)
            make.top.equalTo(contactView).offset(5.0)
        }
        
        contactTableView = UITableView()
        contactTableView.theme_backgroundColor = [UIColor.gunmetal.toHexString(), UIColor.paleGrey.toHexString()]
        contactTableView.theme_separatorColor =  [UIColor.paleGrey.toHexString(), UIColor.charcoalGrey.toHexString()]
        contactTableView.dataSource = self
        contactTableView.delegate = self
        contactTableView.separatorInset = .zero
        contactTableView.tableFooterView = UIView()
        contactView.addSubview(contactTableView)
        contactTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contactView)
            make.top.equalTo((contactSectionHeading.snp.bottom)).offset(5.0)
            make.bottom.equalTo(contactView.snp.bottom)
        }
        contactTableView.isHidden = true
        
        
        spinnerView = UIActivityIndicatorView(style: .whiteLarge)
        spinnerView.color = UIColor.dodgerBlue
        spinnerView.isHidden = false
        contactView.addSubview(spinnerView)
        spinnerView.snp.makeConstraints { (make) in
            make.left.right.equalTo(contactView)
            make.top.equalTo((contactSectionHeading.snp.bottom)).offset(25.0)
            
        }
        spinnerView.startAnimating()
        
        return contactView
    }
    
    private func setupAddContactButton() -> UIView{
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 90))
        addContactButton = UIButton(type: .custom)
        if isShared{
            addContactButton.backgroundColor = .dodgerBlue
        }else{
            addContactButton.backgroundColor = .dodgerBlueDisable
        }
        addContactButton.setTitleColor(.white, for: .normal)
        addContactButton.setTitleColor(.charcoalGrey, for: .highlighted)
        addContactButton.layer.cornerRadius = 10.0
        addContactButton.setTitle("addContact".localized(), for: .normal)
        addContactButton.addTarget(self, action: #selector(addContactButtonAction), for: .touchUpInside)
        view.addSubview(addContactButton)
        addContactButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.right.equalTo(view).offset(-20)
            make.height.equalTo(50)
            make.top.equalTo(view).offset(20)
            
        }
        return view
    }
    private func setTableEmptyMessage(_ message: String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.theme_textColor = [UIColor.white.toHexString(), UIColor.charcoalGrey.toHexString()]
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = .light(size: 16)
        messageLabel.sizeToFit()
        
        contactTableView.backgroundView = messageLabel
    }
    
    func restoreTableViewFromBeenEmpty () {
        contactTableView.backgroundView = nil
    }
    
    
    
    //==================================================================== Contact add
    @objc private func addContactButtonAction(){
        if !Reachability.isConnectedToInternet{
            showAlert(title: "noNetwork".localized(), message: "toAddContactPleaseConnectInternet".localized())
            return
        }
        if !isShared{
            showAlert(title: "sharingOFFAddContactTitle".localized(), message: "sharingOFFAddContactMessage".localized())
            return
        }
        
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        picker.dismiss(animated: true, completion: nil)
        let name = CNContactFormatter.string(from: contact, style: .fullName)
        var numbers:[String]!
        for number in contact.phoneNumbers {
            let mobile = number.value.value(forKey: "digits") as? String
            if (mobile?.count)! > 7 {
                if numbers == nil{
                    numbers = [mobile!]
                }else{
                    numbers.append(mobile!)
                }
            }
        }
        
        if contact.phoneNumbers.count > 1{
            let title = "sharingWithTitle".localized() + name!
            let customAlert = AlertPickerView(nibName: "PickerAlert", bundle: nil)
            customAlert.setData(data: numbers, title: title, message: "mutiplePhoneNumberMessageBody".localized(),extraInfo: name!)
            customAlert.providesPresentationContextTransitionStyle = true
            customAlert.definesPresentationContext = true
            customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            customAlert.delegate = self
            self.present(customAlert, animated: true, completion: nil)
        }else{
            if(numbers == nil){return}
            if !numbers[0].contains("+"){
                let messageBody = String(format: NSLocalizedString("nameHasCountryCodeMissingMessage", comment: ""), arguments: [name!])
                showAlert(title: "nameHasCountryCodeMissingTitle".localized(), message: messageBody)
                print("Sharing contact picked doesnt have a country code \(numbers[0])")
                return
            }
            
            if hasContactExist(phoneNumber: numbers[0]){
                print("Sharing contact already exits for \(name!)")
                let messageBody = String(format: NSLocalizedString("alreadySharedMessageBody", comment: ""), arguments: [name!, numbers[0]])
                showAlert(title: "sharingWithTitle".localized(), message: messageBody)
                return
            }
            
            let title = "sharingWithTitle".localized()
            let messageBody = String(format: NSLocalizedString("confirmSharingMessageBody", comment: ""), arguments: [name!, numbers[0]])
            let alert = UIAlertController(title: title, message: messageBody, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "no".localized(), style: .cancel, handler: {
                (alert: UIAlertAction!) in
                
            }))
            alert.addAction(UIAlertAction(title: "confirm".localized(), style: .default, handler: {
                (alert: UIAlertAction!) in
                self.addContact(name: name!, phoneNumber: numbers[0])
            }))
            self.present(alert, animated: true)
        }
        
    }
    
    private func hasContactExist(phoneNumber: String) -> Bool{
        return tableViewData.contains(SharedContact(name: "String", phoneNumber: phoneNumber, createdTime: 0))
    }
    
    private func addContact(name: String, phoneNumber: String){
        self.showloaderOnSecondWindow(withDelegate: nil, withTitle:  "validating".localized())
        API.shared.shareDevice(deviceID: deviceInfo.credential!.deviceID, phoneNumber: phoneNumber) { [weak self] (success, code) in
            self!.loaderHide()
            if success{
                let items =  ["I have shared my Rolley+, now you can ride whenever you want. Open Scooterson app, it will be there"]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                self!.present(ac, animated: true)
                
                self!.tableViewData.append(SharedContact(name: name, phoneNumber: phoneNumber, createdTime: Date().currentTimeMillis()))
                self!.contactTableView.reloadData()
            }else{
                if code == 404{
                    self!.showAlertContactDoesntExist(name: name)
                } else if code == 409{
                    self!.showAlert(title: "cannotShare".localized(), message: "sharingContactSelfShareMessage".localized())
                }else{
                    let errorMessageBody = String(format: NSLocalizedString("shareMessageErrorBody", comment: ""), arguments: [code])
                    self!.showAlert(title: "error".localized(), message: errorMessageBody)
                }
            }
        }
        
    }
    
    private func showAlertContactDoesntExist(name: String){
        let title = String(format: NSLocalizedString("titleNotOnScooterson", comment: ""), arguments: [name])
        let attributedString = NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.font : UIFont.bold(size: 16)
        ])
        
        
        let alert = UIAlertController(title: "", message: "messageNotOnScooterson".localized(), preferredStyle: .alert)
        alert.setValue(attributedString, forKey: "attributedTitle")
        
        alert.addAction(UIAlertAction(title: "okay".localized(), style: .cancel, handler: {
            (alert: UIAlertAction!) in
            
        }))
        alert.addAction(UIAlertAction(title: "tellThem".localized(), style: .default, handler: {
            (alert: UIAlertAction!) in
            let items =  ["shareMessageNotOnScooterson".localized()]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            self.present(ac, animated: true)
            
        }))
        self.present(alert, animated: true)
    }
    
    //============================================ Location service  check ===========================
    private func isLocationServiceIsEnabledAndPermissionGranted()-> Int{
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                return 1
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                return 2
            @unknown default:
                return 0
            }
        } else {
            print("Location services are not enabled")
            return 0
        }
    }
    //============================= Get Shared contacts for this device ========================
    private func getSharedContacts (){
        API.shared.getSharedContacts(deviceID: deviceInfo.credential!.deviceID) { [weak self] (success, errorCode, sharedContacts) in
            self?.spinnerView.isHidden = true
            self?.contactTableView.isHidden = false
            if success{
                self?.tableViewData = sharedContacts!
                self?.contactTableView.reloadData()
            }
        }
    }
    
    
}
extension RolleySharingSettingsViewController: AlertPickerViewDelegate {
    
    func okButtonTapped(selectedItemName: String,selectedValue: String) {
        if !selectedValue.contains("+"){
            let messageBody = String(format: NSLocalizedString("nameHasCountryCodeMissingMessage", comment: ""), arguments: [selectedItemName])
            showAlert(title: "nameHasCountryCodeMissingTitle".localized(), message: messageBody)
            print("Sharing contact picked doesnt have a country code \(selectedValue)")
            return
        }
        
        if hasContactExist(phoneNumber: selectedValue){
            print("Sharing contact already exits for \(selectedItemName)")
            let messageBody = String(format: NSLocalizedString("alreadySharedMessageBody", comment: ""), arguments: [selectedItemName, selectedValue])
            self.showAlert(title: "sharingWithTitle".localized(), message: messageBody)
        }else{
            self.addContact(name: selectedItemName, phoneNumber: selectedValue)
        }
        
    }
    
    func cancelButtonTapped() {
        print("cancelButtonTapped")
    }
}


