//
//  SettingsViewController.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/19/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import UIKit
import CoreLocation
import SupportProvidersSDK
import SupportSDK
import SDKConfigurations

enum Section: Int {
    case SectionLight
    case SectionIneligent
    case SectionGeneral
    case SectionSupport
    static let count: Int = {
        var max: Int = 0
        while let _ = Section(rawValue: max) { max += 1 }
        return max
    }()
}

class SettingsViewController: BaseTableViewController, BTElfServiceDelegate {
    private var headerViews: [UIView] = []
    private let colorsForPicker = [
        "Merchant_Blue".localized(),
        "UnMellow_Yellow".localized(),
        "Pixelated_Grass".localized(),
        "Pink_Glamour".localized(),
        "Bright_Yarrow".localized(),
        "Tomato_Red".localized(),
        "Orange_Ville".localized(),
        "Turquoise".localized(),
        "Gloomy_Purple".localized(),
        "Winter_Green".localized()]
    private var monoLight: Bool = false
    private let btTools = BTElfService.shared
    private var footStepMonoColorCell: SCTableViewCell!
    private var lightCells:[SCTableViewCell] = []

    override func viewDidLoad() {
        titleString = "settings".localized()
        sectionHeaderViews()
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btTools.delegate = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerViews[section]
    }
    
    private func sectionHeaderViews() {
        let titles = ["light".localized(), "inteligent".localized(), "general".localized(), "support".localized()]
        for i in 0..<Section.count {
            let titleView = UIView()
            titleView.theme_backgroundColor = [UIColor.charcoalGrey.toHexString(), UIColor.paleGrey.toHexString()]
            
            let titleLabel = UILabel()
            titleLabel.font = UIFont.regular(size: 24)
            titleLabel.textColor = .barbiePink
            titleLabel.textAlignment = .left
            titleLabel.text = titles[i]
            titleView.addSubview(titleLabel)
            titleLabel.snp.makeConstraints { (make) in
                make.left.equalTo(titleView).offset(20)
                make.right.equalTo(titleView).offset(-20)
                make.centerY.equalTo(titleView)
            }
            headerViews.append(titleView)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = cells[indexPath.section][indexPath.row]
        if cell.expandable {
            UIView.animate(withDuration: 0.2, animations: {
                self.mainTable.beginUpdates()
                cell.expandedView.snp.updateConstraints { (update) in
                    update.height.equalTo(cell.expanded ? 0 : 120)
                }
                cell.expanded = !cell.expanded
                self.mainTable.endUpdates()
                cell.isSelected = cell.expanded
            })
        }
    }
    
    var lightModeSegmet: UISegmentedControl!
    var lightSegmentSwitched: Bool!
    
    override func setupCells() {
        // Setup General section
        let darkModeCell = self.setupSwitchCell(withTitle: "darkMode".localized(), initValue: isDarkMode(), handler: {
            self.switchDarkMode()
        })
        let notificationCell = self.setupSwitchCell(withTitle: "notification".localized(), initValue: isDarkMode(), handler: {
            self.switchNotification()
        })
        let unitsCell = self.setupSegmentCell(withTitle: "units".localized(), values: ["met".localized(), "imp".localized()], initValue: btTools.isImperial() ? 1 : 0) {
            self.switchUnits()
        }
        let generalCells = [notificationCell, darkModeCell, unitsCell]
        
        // Setup Light section
        let footstepLightTypeCell = self.setupSegmentCell(withTitle: "footstepLight".localized(), values: ["mono".localized(), "disco".localized()], initValue: btTools.isLightMono() ? 0 : 1) { [weak self] in
            self?.btTools.switchLightMode()
            self?.lightSegmentSwitched = true
            self?.setColorPatternCell()
        }
        lightModeSegmet = footstepLightTypeCell.segmetControl
        
        if btTools.isLightMono() {
            footStepMonoColorCell = self.setupPickerCell(withTitle: "footstepColor".localized())
            footStepMonoColorCell.valueString = colorsForPicker[footStepMonoColorCell.picker.selectedRow(inComponent: 0)]
        }
        else {
            footStepMonoColorCell = self.setupButtonCell(withTitle: "switchPattern".localized(), buttonTitle: "switch".localized(), handler: { [weak self] in
                self?.btTools.swithchFootstepPattern()
            })
        }
        
        lightCells = [footstepLightTypeCell, footStepMonoColorCell]
        
        // Setup Inteligent section
        let chargerReminder = self.setupSwitchCell(withTitle: "chargingReminder".localized(), initValue: btTools.chargerReminderIsOn, handler: {
            self.btTools.chargerReminderIsOn = !self.btTools.chargerReminderIsOn
        })
        
        let locationEnabled = CLLocationManager.locationServicesEnabled()
        let smartLightState = locationEnabled && UserDefaults.standard.bool(forKey: "SMART_LIGHT_ENABLED")

        
        let smartLight = self.setupSwitchCell(withTitle: "smartLight", initValue: smartLightState) {
            let currentState = UserDefaults.standard.bool(forKey: "SMART_LIGHT_ENABLED")
            UserDefaults.standard.set(!currentState, forKey: "SMART_LIGHT_ENABLED")
        }
        
        let ineligentCells:[SCTableViewCell] = [chargerReminder, smartLight]
        
        // Setup Support section
        let helpChat = self.setupButtonCell(withTitle: "help".localized(), buttonTitle: "getHelp".localized()) {
            let hcConfig = HelpCenterUiConfiguration()
            hcConfig.groupType = .category
            hcConfig.groupIds = [360006068831]
            let helpCenter = HelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [])
            self.navigationController?.pushViewController(helpCenter, animated: true)
        }
        
        let supportCells:[SCTableViewCell] = [helpChat]
        
        cells = [lightCells, ineligentCells, generalCells, supportCells]
    }
    
    private func setColorPatternCell() {
        cells[0].remove(at: 1)
        mainTable.deleteRows(at: [IndexPath(item: 1, section: 0)], with: .fade)
        if btTools.isLightMono() {
            footStepMonoColorCell = setupPickerCell(withTitle: "footstepColor".localized())
            let i = footStepMonoColorCell.picker.selectedRow(inComponent: 0)
            footStepMonoColorCell.valueString = colorsForPicker[i]
        }
        else {
            footStepMonoColorCell = setupButtonCell(withTitle: "switchPattern".localized(), buttonTitle: "switch".localized(), handler: { [weak self] in
                self?.btTools.swithchFootstepPattern()
            })
        }
        cells[0].append(footStepMonoColorCell)
        mainTable.insertRows(at: [IndexPath(item: 1, section: 0)], with: .fade)
    }
    
    private func switchNotification() {
        print("Notification chanhed")
    }
    
    private func switchChargerRemindOn() {
        
    }
    
    private func switchUnits() {
        print("Units chanhed")
        btTools.switchUnits()
    }
    
    // MARK: Picker view methods
    override func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    override func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colorsForPicker.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UIView()
        let label = UILabel()
        label.theme_textColor = [UIColor.dodgerBlue.toHexString(), UIColor.charcoalGrey.toHexString()]
        label.text = colorsForPicker[row]
        label.textAlignment = .center
        label.font = .regular(size: 20)
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        return view
    }
    
    override func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if btTools.isLightMono() {
            footStepMonoColorCell.valueString = colorsForPicker[row]
            btTools.setMonoColor(colorIndex: row)
        }
    }
    
    // MARK: btTools delegate
    func deviceDisconnected(peripheral: CBPeripheral) {
        self.tabBarController?.dismiss(animated: true, completion: nil)
        self.sendToMQTT(message: "{\"tripStatus\":\"false\"}")
        API.shared.mqtt.disconnect()
    }
    
    func deviceConnected(peripheral: CBPeripheral) {
        
    }    
}
