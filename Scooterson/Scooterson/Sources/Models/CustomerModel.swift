//
//  CustomerModel.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/21/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

struct SCCustomer: Codable {
    let customerID: String
    let customerAcc: String
    let enabled: Bool
    enum CodingKeys: String, CodingKey {
        case customerID = "customerId"
        case customerAcc = "sub"
        case enabled = "enabled"
    }
}
