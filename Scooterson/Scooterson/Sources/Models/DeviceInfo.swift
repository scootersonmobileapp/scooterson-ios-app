//
//  DeviceInfo.swift
//  Scooterson
//
//  Created by deepansh jain on 6/3/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import CoreBluetooth
import Foundation

class DeviceInfo: NSObject {
    var peripheral: CBPeripheral?
    var name: String?
    var btAddr: String?
    var rssi: Int32 = 0
    
    
    init(_ peripheral: CBPeripheral?, data mData: [String : Any?]?, rssi: Int32){
        self.peripheral = peripheral
        self.rssi = rssi
        self.name = mData?["kCBAdvDataLocalName"] as? String

            let manuData = mData?["kCBAdvDataManufacturerData"] as? Data
            if nil == manuData || (manuData?.count ?? 0) < 10 {
                btAddr = ""
            } else {
                let da  = manuData?.bytes
                print("BLE data found: \(String(describing: mData)) ")
              
                if nil == da {
                    return
                }
                
                let pos = 4
                btAddr =  String(format: "%02X:%02X:%02X:%02X:%02X:%02X", da![pos+5],da![pos+4], da![pos+3], da![pos+2], da![pos+1],da![pos])
                print("Ble address found", btAddr as Any)
                
            
            }

    }
 
}
