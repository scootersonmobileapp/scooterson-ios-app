//
//  DeviceModel.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/19/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

struct SCDeviceCredential: Codable {
    var deviceID: String
    var accessToken: String
    enum CodingKeys: String, CodingKey {
        case deviceID = "deviceId"
        case accessToken = "accessToken"
    }
}

struct SCDeviceSettingsParams: Codable {
    var lastBattery: String?
    var lastSync: Double
    var isShared: Bool
    var lastUpdateCheck : Double?
    var isFirstTime: Bool
    var key: String?
    var colorInfo: String
    var isCharingReminderSet: Bool
    var isSmartModeAccSet: Bool
    var deviceCount: Int //count of this device in the user phone used to name the device
    var isKeyFeatureEnabled: Bool
    init(lastBattery: String?, lastSync: Double?, isShared: Bool, lastUpdateCheck: Double?,isFirstTime: Bool,key: String?, colorInfo: String, deviceNumber: Int){
        self.lastBattery = lastBattery
        self.lastSync = (lastSync != nil) ? lastSync! : 0
        self.isShared = isShared
        self.lastUpdateCheck = lastUpdateCheck
        self.isFirstTime = isFirstTime
        self.key = key
        self.colorInfo = colorInfo
        self.isCharingReminderSet = false
        self.isSmartModeAccSet = false
        self.deviceCount = deviceNumber
        self.isKeyFeatureEnabled = false
    }
    
}


class SCDevice: Codable {
    public var credential: SCDeviceCredential?
    public var btIdentifier: UUID?
    public var name: String?
    public var vinCode: String
    public var type: Int
    public var isOwner: Bool
    public var deviceParams: SCDeviceSettingsParams?
    
    
    init(credential: SCDeviceCredential?, btIdentifier: UUID?, name: String, vinCode: String, type: Int, isOwner: Bool, deviceParams: SCDeviceSettingsParams) {
        self.credential = credential
        self.btIdentifier = btIdentifier
        self.vinCode = vinCode
        self.name = name
        self.type = type
        self.isOwner = isOwner
        self.deviceParams = deviceParams
    }
    
    func update() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            UserDefaults.standard.set(encoded, forKey: vinCode)
            UserDefaults.standard.synchronize()
        }
    }
    func storeNewDevice() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            UserDefaults.standard.set(encoded, forKey: vinCode)
            UserDefaults.incrementIntegerForKey(key: ContantsKey.activaatedDevicesCount.rawValue)
            saveDeviceMapInList(withValue: vinCode)
            UserDefaults.standard.synchronize()
        }
    }
    
    static func reStore(vinCode: String) -> SCDevice? {
        if let savedDevice = UserDefaults.standard.object(forKey: vinCode) as? Data {
            let decoder = JSONDecoder()
            if let device = try? decoder.decode(SCDevice.self, from: savedDevice) {
                return device
            }
        }
        return nil
    }
    
    static func reMove(vinCode: String) {
        UserDefaults.standard.removeObject(forKey: vinCode)
        UserDefaults.decrementIntegerForKey(key:  ContantsKey.activaatedDevicesCount.rawValue)
        //Removing it from the the vinCode map list
        let rolleyCounter = UserDefaults.standard.stringArray(forKey: ContantsKey.prouctRolleyCounter.rawValue)
        let rolleyCounterUpdated = rolleyCounter!.filter { $0 != vinCode }
        UserDefaults.standard.set(rolleyCounterUpdated, forKey: ContantsKey.prouctRolleyCounter.rawValue)
        
        
    }
    
    private func saveDeviceMapInList(withValue vincode:String){
        guard  let rolleyCounter = UserDefaults.standard.stringArray(forKey: ContantsKey.prouctRolleyCounter.rawValue) else{
            UserDefaults.standard.set([vincode], forKey: ContantsKey.prouctRolleyCounter.rawValue)
            return
        }
        var changedList = rolleyCounter
        changedList.append(vincode)
        UserDefaults.standard.set(changedList, forKey: ContantsKey.prouctRolleyCounter.rawValue)
        
    }
}

extension UserDefaults {
    static func incrementIntegerForKey(key: String) {
        let value = UserDefaults.standard.integer(forKey: key)
        UserDefaults.standard.set(value + 1, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func decrementIntegerForKey(key: String) {
        let value = UserDefaults.standard.integer(forKey: key)
        UserDefaults.standard.set(value - 1, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
