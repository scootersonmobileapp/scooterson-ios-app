//
//  DriveTrainErrors.swift
//  Scooterson
//
//  Created by deepansh jain on 6/15/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

struct ErrorType{
    var errorDeclaration = String()
    var errorTroubleShooting = String()
}

class DriveTrainErrors {
    
    private var errorDictionary: Dictionary <Int,ErrorType>
    
    init(){
        let errorType81 = ErrorType(errorDeclaration: "errorDec81".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType71 = ErrorType(errorDeclaration: "errorDec71".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType62 = ErrorType(errorDeclaration: "errorDec62".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType61 = ErrorType(errorDeclaration: "errorDec61".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType48 = ErrorType(errorDeclaration: "errorDec48".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType47 = ErrorType(errorDeclaration: "errorDec47".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType46 = ErrorType(errorDeclaration: "errorDec46".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType45 = ErrorType(errorDeclaration: "errorDec45".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType44 = ErrorType(errorDeclaration: "errorDec44".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType43 = ErrorType(errorDeclaration: "errorDec43".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType42 = ErrorType(errorDeclaration: "errorDec43".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType41 = ErrorType(errorDeclaration: "errorDec41".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType37 = ErrorType(errorDeclaration: "errorDec37".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType36 = ErrorType(errorDeclaration: "errorDec36".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType35 = ErrorType(errorDeclaration: "errorDec35".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType33 = ErrorType(errorDeclaration: "errorDec33".localized() ,errorTroubleShooting: "errorTSType1".localized())
        let errorType30 = ErrorType(errorDeclaration: "errorDec30".localized() ,errorTroubleShooting: "errorTSType1".localized())
        let errorType27 = ErrorType(errorDeclaration: "errorDec27".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType15 = ErrorType(errorDeclaration: "errorDec15".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType14 = ErrorType(errorDeclaration: "errorDec14".localized() ,errorTroubleShooting: "errorTSType2".localized())
        let errorType13 = ErrorType(errorDeclaration: "errorDec13".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType12 = ErrorType(errorDeclaration: "errorDec12".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType11 = ErrorType(errorDeclaration: "errorDec11".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType10 = ErrorType(errorDeclaration: "errorDec10".localized() ,errorTroubleShooting: "errorTSType2".localized())
        let errorType09 = ErrorType(errorDeclaration: "errorDec09".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType07 = ErrorType(errorDeclaration: "errorDec07".localized() ,errorTroubleShooting: "errorTSType3".localized())
        let errorType05 = ErrorType(errorDeclaration: "errorDec05".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        let errorType04 = ErrorType(errorDeclaration: "errorDec04".localized() ,errorTroubleShooting: "errorTSType4".localized())
        let errorType8 = ErrorType(errorDeclaration: "errorDec08".localized() ,errorTroubleShooting: "errorTSDefault".localized())
        
        
        errorDictionary = [81 : errorType81,
                           71 : errorType71,
                           62 : errorType62,
                           61 : errorType61,
                           48 : errorType48,
                           47: errorType47,
                           46 : errorType46,
                           8 : errorType8,
                           44 : errorType44,
                           45 : errorType45,
                           43 : errorType43,
                           42 : errorType42,
                           41 : errorType41,
                           37 : errorType37,
                           36 : errorType36,
                           35 : errorType35,
                           33 : errorType33,
                           30 : errorType30,
                           27 : errorType27,
                           15 : errorType15,
                           14 : errorType14,
                           13 : errorType13,
                           12 : errorType12,
                           11 : errorType11,
                           10 : errorType10,
                           9 : errorType09,
                           7 : errorType07,
                           5 : errorType05,
                           4 : errorType04
        ]
    }
    
    func getErrorDictionary() -> Dictionary<Int, ErrorType>{
        return errorDictionary
    }
    
}
