//
//  LastLocationModel.swift
//  Scooterson
//
//  Created by deepansh jain on 6/19/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import Foundation
struct LastLocation: Codable {
    let latitude: [Info]
    let longitude: [Info]
    let tripStatus: [Info]
    let isSlave: [Info]
    let slaveName: [InfoNullable]
    
    enum CodingKeys: String, CodingKey {
        case latitude = "latitude"
        case longitude = "longitude"
        case tripStatus = "tripStatus"
        case isSlave = "isSlave"
        case slaveName = "slaveName"
    }
    
    struct Info: Codable {
        let lastTime: Double
        let value: String
        
        enum CodingKeys: String, CodingKey {
            case lastTime = "ts"
            case value = "value"
        }
    }
    struct InfoNullable: Codable {
        let lastTime: Double
        let value: String?
        
        enum CodingKeys: String, CodingKey {
            case lastTime = "ts"
            case value = "value"
        }
    }
    
    struct InfoBool: Codable {
        let ts: Double
        let value: String?
        
        enum CodingKeys: String, CodingKey {
            case ts = "ts"
            case value = "value"
        }
    }
    
}
