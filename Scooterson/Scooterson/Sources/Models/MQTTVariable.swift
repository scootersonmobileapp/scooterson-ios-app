//
//  MQTTVariable.swift
//  Scooterson
//
//  Created by deepansh jain on 6/6/20.
//  Copyright © 2020 Anton Umnitsyn. All rights reserved.
//

import Foundation

enum MQTTVariable: String {
    case isSlave
    case speed
    case powerLevel
    case odoReading
    case batteryLevel
    case lights
    case assistLevel
    case speedLimit
    case longitude
    case latitude
    case tripStatus
    case tripDistance
    case remainDistance
    case errorCode
    case metric
    case lock
    case batteryCycle
    case batteryTemp
    case batterySerial
    case batterySoftwareVersion
    case batteryHardwareVersion
    case batteryMaxChargeInterval
    case batteryCurrentChargeInterval
    case hMISerial
    case hMISoftwareVersion
    case hmiHardwareVersion
    case controllerSerial
    case controllerSoftwareVersion
    case controllerHardwareVersion
    case autoOff
    case slaveName
    case isStolen
    case deviceAddress
    case phoneNumber
     
    
    
}
