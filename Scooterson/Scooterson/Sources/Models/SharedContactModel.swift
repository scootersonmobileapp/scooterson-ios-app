//
//  SharedContactModel.swift
//  Scooterson
//
//  Created by deepansh jain on 7/5/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import Foundation

struct SharedContact: Codable, Equatable {
    var name: String
    var phoneNumber: String
    var createdTime: Double
    
    static func ==(lhs:SharedContact, rhs:SharedContact) -> Bool { // Implement Equatable
        return lhs.phoneNumber == rhs.phoneNumber
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case phoneNumber
        case createdTime
    }
    
}
