//
//  SharedDevicesModel.swift
//  Scooterson
//
//  Created by deepansh jain on 7/8/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import Foundation
struct SharedDevice: Codable, Equatable{
    var deviceId: String
    //var claimSecretKey: String
    var accessToken: String
    var keyFeaturePin: String
    var deviceType: String
    var deviceName: String
    var deviceOwnerCustomerUserName: String
    var status: Int
    
    enum CodingKeys: String, CodingKey {
        case deviceId
        //case claimSecretKey
        case accessToken
        case keyFeaturePin
        case deviceType
        case deviceName
        case deviceOwnerCustomerUserName
        case status
        
    }
    static func ==(lhs:SharedDevice, rhs:SharedDevice) -> Bool { // Implement Equatable
           return lhs.deviceName == rhs.deviceName
    }
}
