//
//  TokenModel.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/21/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

struct SCToken: Codable {
    var token: String
    var refreshToken: String
    
    enum CodingKeys: String, CodingKey {
        case token
        case refreshToken
    }
    
    func store() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            UserDefaults.standard.set(encoded, forKey: "tokens")
            UserDefaults.standard.synchronize()
        }
    }
    
    static func reStore() -> SCToken? {
        if let savedTokens = UserDefaults.standard.object(forKey: "tokens") as? Data {
            let decoder = JSONDecoder()
            if let tokens = try? decoder.decode(SCToken.self, from: savedTokens) {
                return tokens
            }
        }
        return nil
    }
    
    static func clear() {
        UserDefaults.standard.removeObject(forKey: "tokens")
    }
}
