//
//  UserDataModel.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 7/2/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

struct SCUser: Codable {
    var firstName: String?
    var lastName: String?
    var addressLine1: String?
    var addressLine2: String?
    var city: String?
    var country: String
    var state: String?
    var zipcode: String?
    var email: String?
    var phone: String
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case addressLine1
        case addressLine2
        case city
        case country
        case state
        case zipcode
        case email
        case phone
    }
    
    func store() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            UserDefaults.standard.set(encoded, forKey: "userData")
            UserDefaults.standard.synchronize()
        }
    }
    
    static func reStore() -> SCUser? {
        if let savedData = UserDefaults.standard.object(forKey: "userData") as? Data {
            let decoder = JSONDecoder()
            if let data = try? decoder.decode(SCUser.self, from: savedData) {
                return data
            }
        }
        return nil
    }
}

struct SCUserName: Codable {
    var firstName: String?
    var lastName: String?
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
    }
    
    init() {
        let userData = SCUser.reStore()
        firstName = userData?.firstName
        lastName = userData?.lastName
    }
}

struct SCAddress: Codable {
    var addressLine1: String?
    var addressLine2: String?
    var city: String?
    var country: String
    var state: String?
    var zipcode: String?
    
    enum CodingKeys: String, CodingKey {
        case addressLine1
        case addressLine2
        case city
        case country
        case state
        case zipcode
    }
    
    init() {
        let userData = SCUser.reStore()
        addressLine1 = userData?.addressLine1
        addressLine2 = userData?.addressLine2
        city = userData?.city
        country = (userData?.country)!
        state = userData?.state
        zipcode = userData?.zipcode
    }
}
struct SCUserProfile: Codable{
    var addressLine1: String?
    var addressLine2: String?
    var city: String?
    var state: String?
    var zipcode: String?
    var email: String?
    var name: SCUserProfileNames?
    
    
    enum CodingKeys: String, CodingKey {
        
        case addressLine1 = "address"
        case addressLine2 = "address2"
        case city = "city"
        case state = "state"
        case zipcode = "zip"
        case email = "email"
        case name = "additionalInfo"
    }
    func store() {
        var userData = SCUser.reStore()
        userData?.addressLine1 = addressLine1
        userData?.addressLine2 = addressLine2
        userData?.city = city
        userData?.state = state
        userData?.email = email
        userData?.zipcode = zipcode
        userData?.firstName = name?.firstName
        userData?.lastName = name?.lastName
        userData?.store()
    }
    
    
}
struct SCUserProfileNames: Codable {
    let firstName: String
    let lastName: String
    
    enum CodingKeys: String, CodingKey {
        case firstName = "firstName"
        case lastName = "lastName"
    }
}
