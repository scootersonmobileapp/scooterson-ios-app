//
//  UserModel.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 6/15/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Realm
import RealmSwift

class User: Object, Decodable {
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        firstName = try container.decode(String.self, forKey: .firstName)
        lastName = try container.decode(String.self, forKey: .lastName)
        
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
}

class Address: Object, Decodable {
    @objc dynamic var addressLine1: String = ""
    @objc dynamic var addressLine2: String? = nil
    @objc dynamic var city: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var state: String? = nil
    @objc dynamic var zipcode: String = ""
    
    enum CodingKeys: String, CodingKey {
        case addressLine1
        case addressLine2
        case city
        case country
        case state
        case zipcode
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        addressLine1 = try container.decode(String.self, forKey: .addressLine1)
        addressLine2 = try? container.decode(String.self, forKey: .addressLine2)
        city = try container.decode(String.self, forKey: .city)
        country = try container.decode(String.self, forKey: .country)
        state = try? container.decode(String.self, forKey: .state)
        zipcode = try container.decode(String.self, forKey: .zipcode)
        
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
}
