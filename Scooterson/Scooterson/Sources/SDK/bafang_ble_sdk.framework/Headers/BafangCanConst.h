//
//  BTProtocol.h
//  BTTools
//
//  Created by 陈 曦 on 16/10/17.
//  Copyright © 2016年 bochengkang. All rights reserved.
//

#define    EBoxErrorCode      			0x09
#define    EBoxVersion                    0x0B
#define    EBoxInfMotorStatus 			0x37
#define    EBoxInfHeadLight   			0x40
#define    EBoxInfCurrent    			0x41
#define    EBoxInfVoltage    			0x42
#define    EBoxInfBatteryCapacity		0x43
#define    EBoxInfSpeed       			0x44
#define    EBoxInfTemperature             0x45
#define    EBoxInfOdo         			0x46
#define    EBoxInfTrip        			0x47
#define    EBoxInfMaxSpeed                0x48
#define    EBoxInfAverageSpeed            0x49

#define    EBoxInfPASLevel    			0x4A
#define    EBoxInfCadence     			0x4B
#define    EBoxInfMaintainMile            0x4C

#define    EBoxInfBatteryCapacity_AH      0x50
#define    EBoxInfBMSTemperature			0x60     //电池组内部温度
#define    EBoxInfBMSVoltage  			0x61     //电池总电压
#define    EBoxInfBMSCurrent   			0x62	    //实时电流
#define    EBoxInfBMSRelPercent			0x63	    //相对容量百分比
#define    EBoxInfBMSAbsPercent			0x64	    //绝对容量百分比
#define    EBoxInfBMSRemainCapacity   	0x65	    //剩余容量
#define    EBoxInfBMSFullCapacity     	0x66	    //满电容量
#define    EBoxInfBMSCycleCount       	0x67	    //循环次数
#define    EBoxInfBMSCurChaInterval   	0x68	    //当前充电间隔时间
#define    EBoxInfBMSMaxChaInterval   	0x69	    //最大充电间隔时间
#define    EBoxInfBMSNowCapacity      	0x6A
#define    EBoxInfCal                 	0x70	    //卡路里
#define    EBoxInfremaindistance      	0x71	    //剩余里程
#define    EBoxInfPASnum              	0x72	    //总档次
#define    EBoxResCurrentLimit            0x82
#define    EBoxResSpeedLimit          	0x87
#define    EBoxResWheelDiameter       	0x8b
#define    EBoxInfSensor_Model     		0xD1
#define    EBoxInfTouque           		0xD2
#define    EBoxInfTwist            		0xD3
#define    EBoxInfHeartRate        		0xD4
#define    EboxInfLockState        		0xFD
#define    EBoxResPASLevel         		0x89
#define    EBoxResHeadLight        		0xA3
#define    EBoxResDevice_Name      		0xA2
#define    EBoxInfPINStatus        		0xD5
#define    EBoxRes_SetPIN          		0xD6
#define    EBoxBackLightNum        		0xD7
#define    EBoxLightSensNum        		0xD8
#define    EBoxBatteryCellNum            	0xD9
#define    EBoxSensorInfo                 0x0160
#define    EBoxSensorHWVersion            0x016000
#define    EBoxSensorSWVersion            0x016001
#define    EBoxSensorModel                0x016002
#define    EBoxSensorSerialNum            0x016003
#define    EBoxControllerInfo             0x0260
#define    EBoxControllerHWVersion        0x026000
#define    EBoxControllerSWVersion        0x026001
#define    EBoxControllerModel            0x026002
#define    EBoxControllerSerialNum        0x026003
#define    EBoxPanelInfo                  0x0360
#define    EBoxPanelHWVersion             0x036000
#define    EBoxPanelSWVersion             0x036001
#define    EBoxPanelModel                 0x036002
#define    EBoxPanelSerialNum             0x036003
#define    EBoxBatteryInfo                0x0460
#define    EBoxBatteryHWVersion           0x046000
#define    EBoxBatterySWVersion           0x046001
#define    EBoxBatteryModel               0x046002
#define    EBoxBatterySerialNum           0x046003

#define    EBoxManPower					0xE6
#define    EBoxSOH                        0xE7
#define    EBoxControllerTemp             0xE8
#define    EBoxMotorTemp                  0xE9
#define    EBoxControllerPASLevel         0xEA
#define    EBoxControllerInfTrip          0xEB
#define    EBoxControllerPASNum           0xEC
#define    EBoxBLECanType                 0xED
#define    EBoxBLEInfOdo                  0xEE
#define    EBoxBLEInfTrip                 0xEF
#define    EBoxBLEMaxSpeed                0xF0
#define    EBoxBLEAverageSpeed            0xF1
#define    EBoxBLEMaintainMile            0xF2
#define    EBoxBLEAutoOff                 0xF3


#define    APPSetPASLevel          		0x89
#define    APPSetHeadLight         		0xA3
#define    APPGet_Version          		0xD5
#define    APPGetBMS_Info          		0xA1
#define    APPSetDevice_Name       		0xA2
#define    APPController_SetPIN    		0xA4
#define    APPController_AuthPIN   		0xA5
#define    APPController_ResetPIN  		0xA6
#define    APPNavigationData       		0xA7
#define    APPSetBackLight         		0xA8
#define    APPClsSinglelMile       		0xA9
#define    APPSetLightSens         		0xAB
#define    APPSetMaintainMile      		0xAC
#define    APPSetAutoOff           		0xAD
#define    APPSetSportMode          		0xAE
#define    APPSetECOMode            		0xAF
#define    APPController_PowerOnOff       0xB0
#define    APPSetAutoDrive                0xB1

// E-Box to APP command
//#define EBoxErrHall                    0x01
//#define EBoxErrThrottle                0x02
//#define EBoxErrController              0x03
//#define EBoxErrLowVoltage              0x04
//#define EBoxErrMotor                   0x05
//#define EBoxErrCurrentAbnormal         0x06
//#define EBoxErrBrake                   0x07
//#define EBoxErrCommunication           0x08
//#define EBoxErrorCode                  0x09
//
#define EBoxInfoBaseInfo               0x0b
//
////控制器工作状态
//#define EBoxInfMotorStatus             0x37
//
////#define EBoxTestResponse               0xD0
//#define EBoxInfHeadLight               0x40
//#define EBoxInfCurrent                 0x41
//#define EBoxInfVoltage                 0x42
//#define EBoxInfBatteryCapacity         0x43
//#define EBoxInfSpeed                   0x44
//#define EBoxInfTemperature             0x45
//#define EBoxInfOdo                     0x46
//#define EBoxInfTrip                    0x47
//
//#define EBoxInfPASLevel                0x4A
//#define EBoxInfWalkMode                0x4B
//#define EBoxInfCadence                 0x4B
//
//#define EBoxInfBatteryCapacity_AH      0x50
//
//
//// APP to E-BOX command
//#define APPSetACK                      0x80
//#define APPSetBatteryLowTh             0x81
//#define APPSetCurrentLimit             0x82
//#define EBoxResCurrentLimit            0x82
//#define APPSetPhaseMode                0x83
//#define APPSetMotorMagnetSteel         0x84
//#define APPSetSupplyVoltage            0x85
//#define APPSetBrakePercentage          0x86
//#define APPSetSpeedLimit               0x87
//#define EBoxResSpeedLimit              0x87
//#define APPSetSpeedRatio               0x88
//#define APPSetPASLevel                 0x89
//#define EBoxResPASLevel                0x89
//
//#define APPSetOdoReset                 0x8A
//#define APPSetWheelDiameter            0x8B
//#define EBoxResWheelDiameter           0x8B
//#define APPSetPasswordChangeReq        0x8C
//#define APPSetPASMode                  0x8D
//#define APPSetOfflineMode              0x8E
//#define APPSetCL                       0x8F
//
//#define APPSetBluetoothLocker          0x90
//#define APPSetPowerLocker              0x91
//#define APPSetChangeDeviceName         0x92
//#define APPSetResetPasswordReq         0x93
//#define APPSetWalkMode                 0x94
//#define APPSetBatteryCapacity          0x95
//#define APPSetDisconnectRequest        0x96
//#define APPSetDualDisplayMode          0x97//没有1.8.2
//#define APPSetSpeedSensor              0x99
//#define APPSetPASSensor                0x9A
//#define APPSetCCS                      0x9C
//
//#define APPSetDrivingMode              0xA0
//#define APPGetBMSInf                   0xA1
//#define APPSetHeadLight                0xA3
//#define APPResHeadLight                0xA3
//#define EBoxResHeadLight               0xA3
//
//#define EBoxInfSensor_Model            0xd1
//#define EBoxInfTouQue                  0xd2
//#define EBoxInfTwist                   0xd3
//#define EBoxInfoHeartRate              0xd4
//#define EBoxInfGetBaseInfo             0xd5

#pragma mark - other

#define CAN_TYPE_WITH_PANNEL            0
#define CAN_TYPE_WITHOUT_PANNEL         1






