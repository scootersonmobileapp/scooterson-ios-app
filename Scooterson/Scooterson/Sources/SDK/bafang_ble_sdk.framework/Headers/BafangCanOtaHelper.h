//
//  BafangCanOtaHelper.h
//  BFCanDemo
//
//  Created by Zhang Jonathan on 2019/12/18.
//  Copyright © 2019 Jonathan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define COMMAND_PREPARE_DOWNLOAD    1
#define COMMAND_DOWNLOAD            2
#define COMMAND_DATA                3
#define COMMAND_VERIFY              4

typedef NS_ENUM(NSInteger, FW_STATE_MACHINE){
	FW_STATE_IDLE = 1,
	FW_STATE_PREPARE_DOWNLOAD,
	FW_STATE_READY_FOR_DOWNLOAD,
	FW_STATE_DATA_TRANSFER,
	FW_STATE_VERIFICATION,
	FW_STATE_FINISH
};

@protocol BafangCanOtaCallback <NSObject>
@optional
-(void) onProgress:(int) realSize pathSize:(int)pathSize percent:(int) percent;
-(void) onFinish:(int) status;

@end

@interface BafangCanOtaHelper : NSObject

+(BafangCanOtaHelper *)getInstance;
@property (nonatomic, strong) NSFileHandle *mFileHandle;
@property (nonatomic, assign) FW_STATE_MACHINE mStateMachine;
@property (nonatomic, strong) id<BafangCanOtaCallback> ota_cb;

-(void)setPatchFilePath:(NSString *)path;
-(void)start;
-(void)stop;
-(void)postEvent:(int)msgType status:(int)status;
-(void)transitionTo:(FW_STATE_MACHINE)stateMachine status:(int)status;

@end

