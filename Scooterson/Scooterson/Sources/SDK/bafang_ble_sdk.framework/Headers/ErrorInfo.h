//
//  ErrorInfo.h
//  BFCanDemo
//
//  Created by Zhang Jonathan on 2019/11/28.
//  Copyright © 2019 Jonathan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorInfo : NSObject

+(instancetype)initInfo:(int)cmd_id str_info:(NSString *)str_info;
+(instancetype)initInfo:(int)cmd_id cmd_cnt:(int)cmd_cnt;

@property (nonatomic, assign) int cmd_id;
@property (nonatomic, assign) int cmd_cnt;
@property (nonatomic, assign) BOOL need_send;
@property (nonatomic, copy) NSString *str_info;

@end

