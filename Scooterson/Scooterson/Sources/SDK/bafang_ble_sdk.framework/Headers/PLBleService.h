//
//  PLBleService.h
//  BFCanDemo
//
//  Created by Zhang Jonathan on 2019/11/19.
//  Copyright © 2019年 Jonathan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "CanInfo.h"
#import "ErrorInfo.h"
#import "BleUtil.h"
#import "PLSign.h"
#import "PLFileManager.h"
#import "PLFileBrower.h"
#import "BafangCanOtaHelper.h"
#import "UartInfo.h"

#define NSLog(format, ...) do { \
fprintf(stderr, "<%s:%d> ", \
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], \
__LINE__); \
(NSLog)((format), ##__VA_ARGS__); \
} while (0)

#define BAFANG_SDK_VERSION @"bafang_sdk_lib 1.1.2"

@protocol PLBleCallback <NSObject>
@optional

-(void) onDeviceStatusChanged:(CBPeripheral *)peripheral status:(int) status;
-(void) onDeviceFound:(CBPeripheral *)peripheral data:(NSDictionary<NSString *,id> *)mData rssi:(int) rssi;
-(void) onCharFound:(NSMutableArray*) write read:(NSMutableArray *)read notify:(NSMutableArray *) notify;
-(void) onCharRead:(int) status data:(NSData*) mData uuid:(NSString *) uuid;
-(void) onDataReceived:(int) cmd value:(NSString *) value;
-(void) onDataSent:(NSData*) mData uuid:(NSString *) uuid;
-(void) onDeviceNameChanged:(BOOL) status;

@end

@interface PLBleService : NSObject

+(PLBleService *)getInstance;
-(void)initQueue;
-(void) scanDevices:(BOOL)start;
-(void) reconnect:(NSMutableArray *) write notify:(NSMutableArray *) notify read:(NSMutableArray *) read;
-(void) connect:(CBPeripheral *)peripheral write:(NSMutableArray *) write notify:(NSMutableArray *) notify read:(NSMutableArray *) read;
-(void) disconnect;
-(BOOL) isBafangCan:(NSDictionary<NSString *,id> *)advertisementData;
-(BOOL) isBafangUart:(NSDictionary<NSString *,id> *)advertisementData;
-(NSData *) gen_cmd_encry:(Byte) type data:(NSData *)mData;
-(int) send_data_encry:(NSData *) data uuid:(NSString *) uuid is_long_pkt:(BOOL) is_long_pkt;
-(void) process_can_data:(NSData *) mData;
-(void) process_can_cmd:(int) cmd data:(int) data pin:(NSData *)pin;
-(void) get_can_node_info:(int) can_node;
-(void) get_base_info;
-(void) get_battery_info;
-(void) get_init_data;
-(NSString *) get_error_info;
-(void) start_long_cmd_timer;
-(void) reset_long_cmd_list;
-(void)stop_fw_timer;

@property (nonatomic, strong) id<PLBleCallback> ble_cb;
@property (atomic) Boolean mLock;
@property (nonatomic, strong) NSMutableArray *base_cmd_list;
@property (nonatomic, strong) NSMutableArray *long_cmd_list;
@property (nonatomic, strong) NSMutableArray *can_info;
@property (nonatomic, strong) NSMutableArray *error_info;
@property (nonatomic, strong) NSArray *fw_error_info;
@property (nonatomic, strong) NSMutableArray *can_node_list;
@property (nonatomic, strong) NSMutableArray *can_node_ver;// 记录当前正在查询版本的节点
@property (nonatomic, strong) NSMutableArray *can_node_ack;
@property (nonatomic, strong) CBCharacteristic *char_name;

-(void)set_log_level:(int)level;
-(void)save_log_file:(NSString *)name;

// uart protocal
@property (nonatomic, strong) NSMutableArray *uart_info;
-(void) process_uart_cmd:(int) cmd data_t:(int) data_t;
-(void) send_uart_data:(NSData *) data;
-(NSData *) gen_uart_cmd:(Byte) type cmd_data:(Byte *)cmd_data len:(Byte) len;
-(NSMutableArray *) gen_uart_long_cmd:(Byte) type cmd_data:(Byte *)cmd_data len:(short) len;

@end

