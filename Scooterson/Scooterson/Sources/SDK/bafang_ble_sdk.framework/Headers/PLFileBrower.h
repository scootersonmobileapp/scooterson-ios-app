//
//  FileBrowser
//
//  Created by Steven Troughton-Smith on 18/06/2013.
//  Copyright (c) 2013 High Caffeine Content. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>

#define NOTIFY_FILE_PATH	@"NOTIFY_FILE_PATH"
#define ITEM_PATH			@"ITEM_PATH"

@interface PLFileBrower : UITableViewController <QLPreviewControllerDataSource>

- (id)initWithPath:(NSString *)path ext:(NSString *)ext;

@end
