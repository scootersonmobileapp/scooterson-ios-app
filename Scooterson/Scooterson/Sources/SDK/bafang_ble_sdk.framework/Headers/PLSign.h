//
//  PLSign.h
//  BFCanDemo
//
//  Created by Zhang Jonathan on 2019/11/11.
//  Copyright © 2019年 Jonathan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PL_BLE_STATUS_CONNECTED 		       0x0000
#define PL_BLE_STATUS_DISCONNECTED 	       0x0001
#define PL_BLE_STATUS_READY 		       0x0002
#define PL_BLE_STATUS_CHAR_ERR 		       0x0003
#define PL_BLE_STATUS_ENABLE_NOTIFY_ERR     0x0004
#define PL_BLE_STATUS_RECONNECTING          0x0005

#define PL_CONNECT_RETRY_MAX_TIMERS 	3

// can protocal
#define CAN_RAND1		    1
#define CAN_RAND2 		    2
#define CAN_FW_START 	    3
#define CAN_FW_CANCEL 	    4
#define CAN_FW_FAILED 	    5
#define CAN_FW_SUCCESS 	    6
#define CAN_FW_REQ_LEN	    7
#define CAN_GET_MTU	    8
#define CAN_RES_MTU	    9
#define CAN_SET_NAME	    0x10
#define CAN_ACK_NAME	    0x11
#define CAN_GET_ENCRY_CHANNEL   0x12
#define CAN_RES_ENCRY_CHANNEL   0x13


#define CAN_NODE_SENSOR 		0x01
#define CAN_NODE_CONTROLLER 	0x02
#define CAN_NODE_PANEL 			0x03
#define CAN_NODE_BATTERY 		0x04
#define CAN_NODE_BESST 			0x05
#define CAN_NODE_BLE 			0x13
#define CAN_NODE_ALL             0x1F

#define OPT_WRITE 		    0x00
#define OPT_READ 		    0x01
#define OPT_ACK_OK 	    0x02
#define OPT_ACK_ERROR	    0x03
#define OPT_LONG_START	    0x04
#define OPT_LONG_TX	    0x05
#define OPT_LONG_END	    0x06
#define OPT_WARNING	    0x07

#define UUID_UART_SERVICE		@"0000fff0-0000-1000-8000-00805f9b34fb"
#define UUID_UART_WRITE			@"0000fff3-0000-1000-8000-00805f9b34fb"
#define UUID_UART_NOTIFY		@"0000fff4-0000-1000-8000-00805f9b34fb"

#define UUID_CAN_SERVICE		@"49d554a6-76b1-11e9-8f9e-2a86e4085a59"
#define UUID_CAN_TRANSMIT_TX	@"49d5571c-76b1-11e9-8f9e-2a86e4085a59" // write can to mcu
#define UUID_CAN_TRANSMIT_RX	@"49d5571d-76b1-11e9-8f9e-2a86e4085a59" // notify can to app
#define UUID_CAN_FW			@"49d55ce4-76b1-11e9-8f9e-2a86e4085a59" // write FW
#define UUID_CAN_CONTROL		@"49d55e56-76b1-11e9-8f9e-2a86e4085a59" // write/notify control
#define UUID_DEV_SERVICE		@"00001800-0000-1000-8000-00805f9b34fb"
#define UUID_DEV_NAME			@"00002a00-0000-1000-8000-00805f9b34fb"
#define UUID_DEV_INFO_SERVICE 	@"0000180a-0000-1000-8000-00805f9b34fb"
#define UUID_DEV_FW_VERSION 	@"00002a28-0000-1000-8000-00805f9b34fb"

#define FILE_HMI 			0
#define FILE_CONTROLLER		1
#define FILE_BATTERY		2
#define FILE_SENSOR		3
#define FILE_BLE_DFU        4

#define STATUS_OK 					     0
#define STATUS_UNSUPPORTED_COMMAND	     1
#define STATUS_ILLEGAL_STATE		     2
#define STATUS_VERIFICATION_FAILED	     3
#define STATUS_INVALID_IMAGE		     4
#define STATUS_INVALID_IMAGE_SIZE	     5
#define STATUS_MORE_DATA			     6
#define STATUS_INVALID_APPID		     7
#define STATUS_INVALID_VERSION		     8
#define STATUS_DISCONNECT			     9
#define STATUS_ABORT					10
#define STATUS_TIMEOUT					11
#define STATUS_INVALID_DEVICE_ADDRESS	     12
#define STATUS_INVALID_FILE_PATH 		13
#define STATUS_IO_ERROR 				14
#define STATUS_NOT_BOND 				15
#define STATUS_UNKNOWN 				   0xFF

#define NOTIFY_FW_STATE			@"NOTIFY_FW_STATE"
#define ITEM_FW_MSG			@"ITEM_FW_MSG"
#define ITEM_FW_STATE_MACHINE	@"ITEM_FW_STATE_MACHINE"
#define ITEM_FW_STATUS			@"ITEM_FW_STATUS"

#define NOTIFY_BLE_STATE		@"NOTIFY_BLE_STATE"
#define ITEM_BLE_STATUS			@"ITEM_BLE_STATUS"

#define MSG_TRANSITION_TO	1
#define MSG_PROCESS_EVENT	2
#define MSG_TIMEOUT		3
#define MSG_QUIT 			4

#define FW_ABS_PATH		@"FW_ABS_PATH"
#define FW_PARENT_PATH		@"FW_PARENT_PATH"

#define DEVICE_TYPE_UART    0
#define DEVICE_TYPE_CAN     1

#define PL_LOG_NONE         1
#define PL_LOG_VERBOSE      2

extern int PAS_NUM_3[]; // 0x06 助推档 0x1f 助推中
extern int PAS_NUM_4[];
extern int PAS_NUM_5[];
extern int PAS_NUM_9[];
extern int PAS_NUM_LEN;
extern int *PAS_NUM_CUR;
extern Byte pas_detail[4];

extern Byte init_cmd_pkt[];
extern BOOL is_long_cmd_over;

extern Boolean speed_flag;
extern int pas_max_level;

extern int local_pas_level;
extern int local_pas_num;
extern int local_controller_pas_num;
extern int local_back_light;
extern int back_light_num;
extern int local_light_sens;
extern int light_sens_num;
extern int local_auto_off;
extern int local_headlight_onoff;
extern int local_sport_eco;
extern int local_cls_single_mile;

extern BOOL modify_name_once;
extern BOOL pas_level_once;
extern int boost_level;
extern int man_power_type;
extern int can_channel_encry;
extern Byte auto_drive;
extern int ble_can_type;

extern int mtu_size;
extern int INIT_REQUEST_LEN;
extern int request_len;
extern int request_last_len ;
extern int request_offset;
extern int fw_error_code;

extern int fw_update_stage;
extern int path_size;
extern int real_mtu;
extern int real_len;
extern UInt64 start_time;
extern UInt64 end_time;
extern int local_connection_status;
extern int pl_log_level;
extern BOOL is_can_device;

// uart protocal process
extern int data_cmd;
extern int data_sn;
extern Byte data_received[255];
extern int data_offset;
extern Byte data_checksum;

@interface PLSign : NSObject

@end

