//
//  ItemInfo.h
//  BafangTest
//
//  Created by yqf on 2017/7/12.
//  Copyright © 2017年 Jonathan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BafangCanConst.h"

@interface UartInfo : NSObject

@property (nonatomic, assign) int cmd_id;
@property (nonatomic, copy) NSString *str_info;
@property (nonatomic, copy) NSString *str_value;

-(instancetype)initInfo:(int)cmd_id str_info:(NSString *)str_info str_value:(NSString *)str_value;
+(instancetype)initInfo:(int)cmd_id str_info:(NSString *)str_info str_value:(NSString *)str_value;

-(void)update_value:(NSData *)data_t;


@end
