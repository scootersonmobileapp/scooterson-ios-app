//
//  API.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/19/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Foundation
import Alamofire
import JWTDecode
import CocoaMQTT

class API {
    
    public let errorDictionary: [Int:String] = [        
        201:"Created",
        204:"Can’t send token will return a code like TBTK[TB error code]",
        400:"plus(+) char in front of phone number [SCER32]",
        401:"Unauthorized",
        403:"Forbidden",
        404:"If the customer already exist but the phone is not the same [SCER65]",
        409:"Already sent sms [SCER84]",
        500:"Maybe a bug 🕷  at our side. We owe you a  beer 🍺 "
    ]
    
    public let errorDictionarySignUp: [Int:String] = [
        201:"Created",
        204:"Can’t send token will return a code like TBTK[TB error code]",
        400:"Looks like this device is already registered with us, please contact support",
        401:"Please enter the correct validation code.",
        403:"Forbidden",
        404:"If the customer already exist but the phone is not the same [SCER65]",
        409:"Already sent sms [SCER84]",
        500:"Maybe a bug 🕷  at our side. We owe you a  beer 🍺 "
    ]
    
    public let errorDictionaryProfile: [Int:String] = [
        0: "noNetworkDefaultMessage".localized(),
        201:"Created",
        204:"Can’t send token will return a code like TBTK[TB error code]",
        400:"Error code 400 TBTK",
        401:"Unauthorized",
        403:"Forbidden",
        404:"If the customer already exist but the phone is not the same [SCER65]",
        409:"Already sent sms [SCER84]",
        500:"Maybe a bug 🕷  at our side. We owe you a  beer 🍺 "
    ]
    
    
    
    //#if DEBUG
    #if targetEnvironment(simulator)
    let mainURLString: String = "http://localhost:8089/api/"
    #else
    let mainURLString: String = "https://middleware.scooterson.com:443/api/"
    let BASE_URL = "https://middleware.scooterson.com/"
    private let BASE_URL_TB = "https://tb.scooterson.com/"
    #endif
    
    static let shared = API()
    var authorized: Bool = UserDefaults.standard.bool(forKey: ContantsKey.autorized.rawValue)
    public var mqtt:CocoaMQTT!
    public var activatedDeviceList: [Any] = []
    private var repeatRequestCounter: Int = 0
    
    var tokens: SCToken!
    var customer: SCCustomer!
    //Contants URL
    private let GET_KEY_URL = "api/device/keyFeature?deviceId="
    private let TOKEN_REFRESH_URL = "api/tb/tokenRefresh"
    private let OTP_VERIFY_URL = "api/signup/"
    private let GET_DEVICE_TOKEN_URL = "api/device/getAccessToken/"
    private let URL_PROFILE_EMAIL = "api/tb/updateCustomerEmailAddress?email="
    private let URL_PROFILE_ADDRESS = "api/tb/updateCustomerAddress"
    private let URL_PROFILE_NAME = "api/tb/user/update?"
    private let URL_PROFILE_FETCH_OLD_DATA = "api/tb/getCustomerData"
    private let URL_SIGN_UP = "api/signup/"
    private let URL_ADD_DEVICE = "api/device/add/"
    private let URL_CLAIM_DEVICE = "api/device/claim?vinCode="
    private let URL_PUSH_TOKEN = "api/tb/updateCustomerPhoneDevicePushToken/"
    private let URL_SHARING_TOGGLE = "api/device/share/pause/"
    private let URL_SHARE_DEVICE = "api/device/share?granteePhoneNumber="
    private let URL_SHARED_CONTACT = "api/device/whoshare/"
    private let URL_SHARED_DEVICES = "api/device/getSharedDevices/"
    private let URL_SHARED_REMOVE_CONTACT = "api/device/removeForCustomer?shareDeviceId="
    private let URL_SHARED_REQUEST_DEVICE_AGAIN = "api/device/turnOnSharingRequest?deviceId="
    
    //TB URL
    private let URL_FETCH_TELEMETRY = "api/plugins/telemetry/DEVICE/"
    
    let uuid: String = UIDevice.current.identifierForVendor!.uuidString
    let PHONE_ID_IOS =  "1"
    
    private init() {}
    
    // MARK: OTP autentification
    func authorizeByPhone (phone: String, isModifyingPhone: Bool ,completionHandler: @escaping (_ success: Bool, _ status: Int?, _ errorCode: String?)->()) {
        if !Reachability.isConnectedToInternet{
            completionHandler(false, 0,nil)
            return
        }
        guard let requestString = "\(URL_SIGN_UP)\(phone)/\(PHONE_ID_IOS)\(uuid)".stringByAddingPercentEncodingForRFC3986() else {
            completionHandler(false, 0,nil)
            return
        }
        print("URL: \(BASE_URL)\nRequest string: \(requestString)")
        var method = HTTPMethod.post
        if isModifyingPhone {
             method = HTTPMethod.put
        }
        
        AF.request("\(BASE_URL)\(requestString)", method: method).responseString { (response) in
            print("Signup Result: \(response)")
            switch response.result {
            case .success:
                switch response.response?.statusCode {
                case 200:
                    completionHandler(true, 200, nil)
                default:
                    completionHandler(true, response.response?.statusCode, String(data: response.data!, encoding: .utf8))
                }
            case .failure(let error):
                if let data = response.data, let errorCode = String(data: data, encoding: .utf8) {
                    let statusCode = response.response?.statusCode
                    print("Signup obtained from the server failed with error code \(statusCode!)")
                    print("Signup error code: \(errorCode)")
                    if errorCode == "TWER34" {
                        completionHandler(false, 500,errorCode)
                    }
                }
                print("Something goes wrong: \(error.localizedDescription)")
                completionHandler(false, nil, nil)
            }
        }
    }
    
    /* Function to fetch the Key from the KeyFeature
     */
    func getKeyForKeyFeature(deviceId: String, completionHandler: @escaping (_ success: Bool, _ additionalinfo: String, _ errorCode: Int) -> Void){
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        let url = "\(BASE_URL)\(GET_KEY_URL)\(deviceId)"
        print("Key Feature key fetch url \(url)")
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON { [weak self] (response) in
            switch response.result {
            case .success:
                let key = String(decoding: response.data!, as: UTF8.self)
                print("Key Feature key obtained from the server success Data \(key)")
                completionHandler(true, key, 200)
                break
            case .failure:
                let statusCode = response.response?.statusCode
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                let errorinfo = self?.errorDictionary[statusCode!]
                print("Key Feature key obtained from the server failed with data \(errorCode)")
                print("Key Feature key obtained from the server failed with error code \(statusCode!)")
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.getKeyForKeyFeature(deviceId: deviceId,completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false,errorinfo!, statusCode!)
                    }
                }else {
                    if(!errorCode.contains("11")){
                        completionHandler(false, errorinfo!, statusCode!)
                    }
                }
                
                break
                
            }
        }
    }
    
    private func tokenCheckFailure(response: DataResponse<Any, AFError>) -> Bool {
        var result = false
        let errorCode = String(decoding: response.data!, as: UTF8.self)
        if(errorCode.contains("11")){
            result = true
            if refreshToken() {
                if repeatRequestCounter > 1{
                    //try is done
                    return false
                }
                if var repeatRequest = response.request {
                    repeatRequest.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
                    AF.request(repeatRequest).responseJSON { [weak self] (response) in
                        switch response.result {
                        case .success:
                            self?.repeatRequestCounter = 0
                            result = true
                        case .failure:
                            if (self?.tokenCheckFailure(response: response))! {
                                result = true
                            }
                        }
                    }
                }
            }
            return result
        }
        return result
    }
    
    func refreshToken()->Bool {
        
        let parameters = [
            "refreshToken"  : tokens.refreshToken,
            "token"         : tokens.token
        ]
        print("Token Parms \(parameters)")
        var result: Bool = false
        AF.request("\(BASE_URL)\(TOKEN_REFRESH_URL)", method: .post, parameters: parameters).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                switch response.response?.statusCode {
                case 200:
                    print("Token refresh Result: \(response)")
                    let decoder = JSONDecoder()
                    guard let data = response.data else { return }
                    do {
                        self?.tokens = try decoder.decode(SCToken.self, from: data)
                        self?.tokens.store()
                        self?.authorized = true
                        result = true
                    } catch let err {
                        print("Token refresh Decoding error", err)
                        result = false
                    }
                default:
                    self?.authorized = false
                }
            case .failure(let error):
                self!.repeatRequestCounter += 1
                print("Token refresh Something goes wrong: \(error)")
            }
            UserDefaults.standard.set(self?.authorized, forKey: "autorized")
            UserDefaults.standard.synchronize()
        })
        return result
    }
    
    
    func verifyOTP (phone: String, code: String, completionHandler: @escaping (_ success: Bool, _ statusCode : Int, _ errorCode: String?, _ hasSharedDevice: Bool)->Void) {
        var hasSharedDevice = false
        if !Reachability.isConnectedToInternet{
            completionHandler(false, 0,nil, hasSharedDevice)
            return
        }
        let tempURLForVerifyOTP = "\(BASE_URL)\(OTP_VERIFY_URL)\(phone)/\(PHONE_ID_IOS)\(uuid)/verify/\(code)"
        
        print("URL for Verify OTP: \(tempURLForVerifyOTP)")
        
        AF.request("\(tempURLForVerifyOTP)", method: .post).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                guard let data = response.data else { return }
                print("Verify OTP Result: \(String(decoding: data, as: UTF8.self))")
                do {
                    self?.tokens = try decoder.decode(SCToken.self, from: data)
                    self?.tokens.store()
                    self?.authorized = true
                } catch let err {
                    print("Verify OTP Decoding error", err)
                }
                if let resultDict = response.value as? [String: Any] {
                    let phoneId = resultDict["tbPhoneDeviceId"]
                    if phoneId != nil {
                        print("tbPhoneDeviceId: ",phoneId as Any)
                        UserDefaults.standard.set(phoneId, forKey: ContantsKey.tbPhoneDeviceId.rawValue)
                        UserDefaults.standard.synchronize()
                    }
                }
                do {
                    guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else{return}
                    if let dictionary = json as? [String: Any] {
                        print("Verify OTP  Json obtained for devices \(dictionary)")
                        if let devices = dictionary["tbDevices"] as? [String: Any] {
                            for (key, value) in devices {
                                var count = 1
                                let deviceCred = SCDeviceCredential(deviceID: key, accessToken: "")
                                if let device = value as? [String: Any] {
                                    
                                    let vinCode = device["name"] as! String
                                    let type = device["type"] as! String
                                    let keyFeaturePin =  device["keyFeaturePin"] as? String
                                    let deviceSettingsParams = SCDeviceSettingsParams(lastBattery: nil, lastSync: nil, isShared: false, lastUpdateCheck: nil, isFirstTime: true, key: keyFeaturePin, colorInfo: type, deviceNumber: count)
                                    var productType: Int!
                                    if(type.contains("RL")){
                                        productType = ConstantValues.PRODUCT_TYPE_ROLLEY
                                    }else if (type.contains("RP")){
                                        productType = ConstantValues.PRODUCT_TYPE_ROLLEY_PLUS
                                    }else{
                                        productType = ConstantValues.PRODUCT_TYPE_ELF
                                    }
                                    let deviceObj = SCDevice(credential: deviceCred, btIdentifier: nil, name: "" , vinCode: vinCode, type: productType, isOwner: true, deviceParams: deviceSettingsParams)
                                    deviceObj.storeNewDevice()
                                    count += 1
                                }
                            }
                        }
                        if let sharedDevice = dictionary["tbDevicesSharedWithMe"] as? NSArray {
                            print("Verify otp has shared device \(sharedDevice.count)")
                            hasSharedDevice = sharedDevice.count > 0
                            
                        }
                    }
                    
                }
                completionHandler(true, 200, nil, hasSharedDevice)
            case .failure:
                let statusCode = response.response?.statusCode
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                print("Verify OTP api error data \(errorCode)")
                print("Verify OTP api error code \(statusCode!)")
                completionHandler(false, statusCode!, errorCode, hasSharedDevice)
                self?.authorized = false
            }
            UserDefaults.standard.set(self?.authorized, forKey: "autorized")
            UserDefaults.standard.synchronize()
        })
    }
    
    func claimDevice(deviceMacAddress: String, deviceModel: String,completionHandler: @escaping (_ success: Bool, _ deviceCredential: SCDeviceCredential?,_ deviceTypeWithColor: String?, _ key: String?, _ errorDescription: String?)->Void){
        if tokens == nil{
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL
        urlCreated.append(URL_CLAIM_DEVICE)
        urlCreated.append(deviceMacAddress)
        urlCreated.append(deviceModel)
        urlCreated.append("RS")//PlaceHolder
        print("URL created for Claim Device request",urlCreated)
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                let statusCode = response.response?.statusCode
                print("Claim device success status code \(String(describing: statusCode))")
                if statusCode == 417 {
                     completionHandler(false, nil, nil,nil,"wrongDevice")
                }else{
                    let decoder = JSONDecoder()
                    guard let data = response.data else { return }
                    guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else{return}
                    print("Claim device response: ", json)
                    let dictionary = json as? [String: Any]
                    do {
                        let device: SCDeviceCredential = try decoder.decode(SCDeviceCredential.self, from: data)
                        let deviceTypeWithColor = dictionary!["deviceType"] as! String
                        guard let key = dictionary!["keyFeaturePin"] else{
                            print("Claim Device type without key : \(String(describing: deviceTypeWithColor))")
                            completionHandler(true,device,deviceTypeWithColor,nil,nil)
                            return
                            
                        }
                        print("Claim Device type: \(String(describing: deviceTypeWithColor)) and key: ",key)
                        completionHandler(true,device,deviceTypeWithColor, key as? String,nil)
                        
                        
                    } catch let err {
                        print("Decoding error", err)
                        completionHandler(false,nil,nil,nil,"Something happen with decoding")
                    }
                }
                break
            case .failure(let error):
                print("Claim Device error response: ", error)
                let statusCode = response.response?.statusCode
                print("Claim Device Api request failed with status code \(statusCode!)")
                guard let data = response.data else {
                    completionHandler(false,nil,nil,nil,nil)
                    return
                }
                let errorCode = String(decoding: data, as: UTF8.self)
                print("Claim Device Api request failed with error code \(errorCode)")
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.claimDevice(deviceMacAddress: deviceMacAddress,deviceModel: deviceModel,completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false, nil,nil, nil ,self?.errorDictionary[statusCode!]!)
                    }
                }else {
                    if (errorCode.contains("TBHW31")){
                        completionHandler(false, nil, nil,nil,"deviceAlreadyRegistered".localized())
                    }else if (errorCode.contains("TBHW32")){
                        completionHandler(false, nil, nil,nil,"stolenDevice")
                    }else{
                        completionHandler(false, nil, nil,nil, self?.errorDictionary[statusCode!]!)
                    }
                }
            }
        })
    }
    /*
     Get Token for the device using Device ID
     */
    func getDeviceAcessToken(deviceID: String ,completionHandler: @escaping (_ success: Bool, _ accessToken: String?,  _ statusCode : Int) -> Void){
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        
        let url = "\(BASE_URL)\(GET_DEVICE_TOKEN_URL)\(deviceID)"
        print("Device Token fetch url \(url)")
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON { [weak self] (response) in
            switch response.result {
            case .success:
                guard let json = try? JSONSerialization.jsonObject(with:  response.data!, options: []) else{return}
                if let dictionary = json as? [String: Any] {
                    if let token = dictionary["credentialsId"] as? String {
                        print("Device token is \(token)")
                        completionHandler(true,token,200)
                    }else{
                        completionHandler(false,nil, 500)
                    }
                    
                }
                
            case .failure:
                let statusCode = response.response?.statusCode
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                print("Key Feature key obtained from the server failed with data \(errorCode)")
                print("Key Feature key obtained from the server failed with error code \(statusCode!)")
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.getDeviceAcessToken(deviceID: deviceID,completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false,nil,statusCode!)
                    }
                }else {
                    if(!errorCode.contains("11")){
                        completionHandler(false,nil,statusCode!)
                    }
                }
                
            }
        }
        
    }
    
    
    
    
    func getCustomerProfile(completionHandler: @escaping (_ success: Bool, _ userData: SCUserProfile?, _ statusCode: Int?) -> Void) {
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        let url = "\(BASE_URL)\(URL_PROFILE_FETCH_OLD_DATA)"
        print("Profile old data fetched url: \(url)")
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON { [weak self] (response) in
            switch response.result {
            case .success:
                do {
                    print("Profile old data fetched: \(String(decoding: response.data!, as: UTF8.self))")
                    
                    let jsonDecoder = JSONDecoder()
                    let userData: SCUserProfile = try jsonDecoder.decode(SCUserProfile.self, from: response.data!)
                    print("Profile old data fetched: \(userData)")
                    completionHandler(true, userData, 200)
                }
                catch {
                    print("Profile old data fetch error")
                }
            case .failure:
                let statusCode = response.response?.statusCode
                print("Profile old data  from the server failed with error code \(statusCode!)")
                if(response.data == nil){
                    completionHandler(false, nil,statusCode)
                    break
                }
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                print("Profile old data from the server failed with data \(errorCode)")
                
                
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.getCustomerProfile(completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false, nil,statusCode)
                    }
                }else {
                    if(!errorCode.contains("11")){
                        completionHandler(false, nil,statusCode)
                    }
                }
                
            }
        }
    }
    
    func updateEmail(email: String ,completionHandler: @escaping (_ success: Bool, _ statusCode: Int?) -> Void) {
        if !Reachability.isConnectedToInternet{
            completionHandler(false, 0)
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        let url = "\(BASE_URL)\(URL_PROFILE_EMAIL)\(email)"
        print("Profile email url \(url)")
        var components = URLComponents(string: url)!
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "@", with: "%40")
        var request = URLRequest(url: components.url!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                completionHandler(true,200)
            case .failure:
                let statusCode = response.response?.statusCode
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                print("Profile email obtained from the server failed with data \(errorCode)")
                print("Profile email obtained from the server failed with error code \(statusCode!)")
                
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.updateEmail(email: email,completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false, statusCode)
                    }
                }else {
                    if(!errorCode.contains("11")){
                        completionHandler(false, statusCode)
                    }
                }
                
            }
        })
    }
    
    func updateName(firstName: String, lastName: String ,completionHandler: @escaping (_ success: Bool, _ statusCode: Int?) -> Void) {
        if !Reachability.isConnectedToInternet{
            completionHandler(false, 0)
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        
        let url = "\(BASE_URL)\(URL_PROFILE_NAME)firstName=\(firstName)&lastName=\(lastName)"
        let removingSpace = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print("Profile name url \(removingSpace!)")
        var request = URLRequest(url: URL(string: removingSpace!)!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                completionHandler(true, 200)
            case .failure:
                let statusCode = response.response?.statusCode
                //special case sending response code 200 in failure Unknown reason
                //todo later 
                if(statusCode == 200){
                    completionHandler(true, 200)
                    break
                }
                
                if(response.data == nil){
                    completionHandler(false, statusCode)
                    break
                }
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                print("Profile name obtained from the server failed with data \(errorCode)")
                print("Profile name obtained from the server failed with error code \(statusCode!)")
                
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.updateName(firstName: firstName, lastName: lastName, completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false, statusCode)
                    }
                }else {
                    if(!errorCode.contains("11")){
                        completionHandler(false, statusCode)
                    }
                }
            }
        })
    }
    
    func updateAddress(completionHandler: @escaping (_ success: Bool, _ statusCode: Int?) -> Void) {
        if !Reachability.isConnectedToInternet{
            completionHandler(false, 0)
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        let address = SCAddress()
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(address)
        let url = "\(BASE_URL)\(URL_PROFILE_ADDRESS)"
        print("Profile address URL \(url)")
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        request.httpBody = jsonData
        print("Profile address body \(String(decoding: jsonData, as: UTF8.self))")
        AF.request(request).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                completionHandler(true,200)
            case .failure:
                let statusCode = response.response?.statusCode
                let errorCode = String(decoding: response.data!, as: UTF8.self)
                print("Profile address obtained from the server failed with data \(errorCode)")
                print("Profile address obtained from the server failed with error code \(statusCode!)")
                
                if (self?.tokenCheckFailure(response: response))! {
                    if self!.repeatRequestCounter < 2 {
                        self?.updateAddress(completionHandler: completionHandler)
                    }
                    else {
                        completionHandler(false, statusCode)
                    }
                }else {
                    if(!errorCode.contains("11")){
                        completionHandler(false, statusCode)
                    }
                }
            }
        })
    }
    
    // MARK: Push token refresh
    func updatePushToken (token: String) -> Bool {
        return true
    }
    
    
    func checkResponse(response: DataResponse<Any, AFError>) -> String {
        //        if response.
        if let data = response.data, let errorCode = String(data: data, encoding: .utf8) {
            switch errorCode {
            case "TBHW31":
                return "DEVICE_ALREADY_REGISTER"
            default:
                return "UNKNOWN_ERROR"
            }
        }
        return "UNKNOWN_ERROR"
    }
    
    private func checkFailure(response: DataResponse<Any, AFError>) -> Bool {
        var result = false
        if let data = response.data, let errorCode = String(data: data, encoding: .utf8) {
            switch errorCode {
            case "TBTK11", "TBCS11":
                if refreshToken() {
                    if repeatRequestCounter > 1 {
                        logout()
                    }
                    if var repeatRequest = response.request {
                        repeatRequest.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
                        AF.request(repeatRequest).responseJSON { [weak self] (response) in
                            switch response.result {
                            case .success:
                                self?.repeatRequestCounter = 0
                                result = true
                            case .failure:
                                if (self?.checkFailure(response: response))! {
                                    result = true
                                }
                            }
                        }
                    }
                    #if DEBUG
                    print("Token refreshed")
                    #endif
                }
            default:
                print(errorCode)
            }
        }
        return result
    }
    
    private func logout() {
        SCToken.clear()
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Splash")
            UIApplication.shared.keyWindow?.rootViewController = controller
        })
    }
    
    
    func refreshToken(completionHandler: @escaping (_ success: Bool) -> Void) {
        
        let parameters = [
            "refreshToken"  : tokens.refreshToken,
            "token"         : tokens.token
        ]
        print("Token Parms \(parameters)")
        AF.request("\(BASE_URL)\(TOKEN_REFRESH_URL)", method: .post, parameters: parameters).responseJSON(completionHandler: { [weak self] (response) in
            switch response.result {
            case .success:
                switch response.response?.statusCode {
                case 200:
                    print("Token refresh Result: \(response)")
                    let decoder = JSONDecoder()
                    guard let data = response.data else { return }
                    do {
                        self?.tokens = try decoder.decode(SCToken.self, from: data)
                        self?.tokens.store()
                        self?.authorized = true
                        completionHandler(true)
                    } catch let err {
                        print("Token refresh Decoding error", err)
                        completionHandler(false)
                    }
                default:
                    self?.authorized = false
                }
            case .failure(let error):
                self!.repeatRequestCounter += 1
                print("Token refresh Something goes wrong: \(error)")
            }
            //todo ask what is this
            UserDefaults.standard.set(self?.authorized, forKey: "autorized")
            UserDefaults.standard.synchronize()
        })
        
    }
    
    
    
    
    
    public func getScooterLastLocation(deviceId: String,completionHandler: @escaping (_ success: Bool, _ lastLocation: LastLocation?  ,_ statusCode: Int?) -> Void){
        if !Reachability.isConnectedToInternet{
            completionHandler(false, nil ,0)
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL_TB
        urlCreated.append(URL_FETCH_TELEMETRY)
        urlCreated.append(deviceId)
        urlCreated.append("/values/timeseries?keys=latitude,longitude,tripStatus,isSlave,slaveName")
        print("Location fetched URL created \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("Bearer \(tokens.token)", forHTTPHeaderField: "X-Authorization")
        print("Location Token \("Bearer \(tokens.token)")")
        AF.request(request).responseJSON { [weak self] (response) in
            switch response.result {
            case .success:
                print("Location fetch result obtained: \(String(decoding: response.data!, as: UTF8.self))")
                let statusCode = response.response?.statusCode
                print("Location fetch result status code: \(String(describing: statusCode)) ")
                switch statusCode {
                case 401:
                    self!.refreshToken(){ (success) in
                        if success{
                            self!.getScooterLastLocation(deviceId: deviceId,completionHandler: completionHandler)
                        }else{
                            completionHandler(false, nil ,response.response?.statusCode)
                        }
                    }
                case 200:
                    guard let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) else{return}
                    if let params = try? JSONSerialization.data(withJSONObject: json) {
                        do {
                            let lastLocation = try JSONDecoder().decode(LastLocation.self, from: params)
                            completionHandler(true, lastLocation ,200)
                        } catch {
                            completionHandler(false, nil ,500)
                        }
                        
                    }
                    
                default:
                    completionHandler(false, nil ,response.response?.statusCode)
                }
                
            case .failure:
                
                completionHandler(false, nil ,response.response?.statusCode)
                break
            }
        }
        
    }
    
    public func sendPushToken(deviceToken: String, completionHandler: @escaping (_ success: Bool) -> Void){
        if !Reachability.isConnectedToInternet{
            completionHandler(false)
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
            if tokens == nil{//to handle token expire and user deleted.
                completionHandler(false)
                return
            }
        }
        
        var urlCreated = BASE_URL
        urlCreated.append(URL_PUSH_TOKEN)
        urlCreated.append(UserDefaults.standard.string(forKey: ContantsKey.tbPhoneDeviceId.rawValue)!)
        urlCreated.append("/pushToken/")
        urlCreated.append(deviceToken)
        print("Push token fetched URL created \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseString {(response) in
            let statusCode = response.response?.statusCode
            print("Push token is has status code \(String(describing: statusCode))")
            switch response.result {
            case .success:
                completionHandler(true)
            case .failure:
                completionHandler(false)
            }
            
        }
    }
    
    public func toggleSharing(deviceID: String, isEnabled: Bool ,completionHandler: @escaping (_ success: Bool, _ statusCode: Int) -> Void){
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL
        urlCreated.append(URL_SHARING_TOGGLE)
        urlCreated.append(deviceID)
        print("Sharing toggle \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        if isEnabled{
            request.httpMethod = HTTPMethod.post.rawValue
        }else{
            request.httpMethod = HTTPMethod.delete.rawValue
        }
        
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        print("Sharing toggle token \(tokens.token)")
        AF.request(request).responseString { (response) in
            let statusCode = response.response?.statusCode
            print("Sharing toggle is has status code \(String(describing: statusCode))")
            switch statusCode{
            case 200:
                completionHandler(true, 200)
            case 400:
                print("Sharing toggle 400 code \(String(describing: response.response?.debugDescription))")
                completionHandler(false, statusCode!)
            default:
                completionHandler(false, statusCode!)
                
                
            }
        }
    }
    
    public func shareDevice(deviceID: String, phoneNumber: String, completionHandler: @escaping (_ success: Bool, _ statusCode: Int) -> Void){
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL
        urlCreated.append(URL_SHARE_DEVICE)
        urlCreated.append(phoneNumber)
        urlCreated.append("&shareDeviceId=")
        urlCreated.append(deviceID)
        print("Sharing device \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.put.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseString {(response) in
            let statusCode = response.response?.statusCode
            print("Sharing device satus code \(String(describing: statusCode))")
            switch statusCode{
            case 200:
                completionHandler(true, 200)
            default:
                completionHandler(false, statusCode!)
            }
        }
    }
    
    public func getSharedContacts(deviceID: String, completionHandler: @escaping (_ success: Bool, _ statusCode: Int, _ sharedContacts: [SharedContact]?) -> Void){
        if !Reachability.isConnectedToInternet{
            completionHandler(false,0,nil)// no internet
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL
        urlCreated.append(URL_SHARED_CONTACT)
        urlCreated.append(deviceID)
        print("Sharing contacts fetched url \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseJSON {(response) in
            switch response.result {
            case .success:
                let statusCode = response.response?.statusCode
                print("Sharing contacts status code: \(String(describing: statusCode)) ")
                var sharedContacts =  [SharedContact] ()
                guard let data = response.data else { return }
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else{return}
                if let contacts = json as? NSArray {
                    for contact in contacts{
                        let params = try? JSONSerialization.data(withJSONObject:contact)
                        var cont = try! JSONDecoder().decode(SharedContact.self, from: params!)
                        cont.phoneNumber = "+\(cont.phoneNumber)"
                        cont.createdTime = cont.createdTime/1000
                        print("Sharing contacts found: \(cont.name)")
                        sharedContacts.append(cont)
                    }
                    completionHandler(true, 0, sharedContacts)
                }
                
                
            case.failure:
                let statusCode = response.response?.statusCode
                print("Sharing contacts status code in error: \(String(describing: statusCode)) ")
                completionHandler(false, statusCode!,nil)
                break
            }
        }
    }
    
    public func getSharedDevices(completionHandler: @escaping (_ success: Bool, _ statusCode: Int, _ sharedDevices: [SharedDevice]?) -> Void){
        if !Reachability.isConnectedToInternet{
            completionHandler(false,0,nil)// no internet
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
            if tokens == nil {
                performReset()
                completionHandler(false,10,nil)
                return
            }
        }
        let phoneSN = "\(uuid)".stringByAddingPercentEncodingForRFC3986()
        var urlCreated = BASE_URL
        urlCreated.append(URL_SHARED_DEVICES)
        urlCreated.append(phoneSN!)
        print("Sharing Devices fetched url \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        print("shared device token \(tokens.token)")
        AF.request(request).responseJSON {[weak self] (response) in
            switch response.result {
            case .success:
                let statusCode = response.response?.statusCode
                print("Sharing Devices status code: \(String(describing: statusCode)) ")
                var sharedDevices =  [SharedDevice] ()
                guard let data = response.data else { return }
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else{return}
                if let contacts = json as? NSArray {
                    for contact in contacts{
                        let params = try? JSONSerialization.data(withJSONObject:contact)
                        let cont = try! JSONDecoder().decode(SharedDevice.self, from: params!)
                        print("Sharing Devices fetched: \(cont.deviceName)")
                        sharedDevices.append(cont)
                    }
                    completionHandler(true, 0, sharedDevices)
                }else{
                    completionHandler(false, statusCode!, nil)
                }
                
            case.failure:
                let statusCode = response.response?.statusCode
                print("Sharing Devices status code in error: \(String(describing: statusCode)) ")
                if let data = response.data, let errorCode = String(data: data, encoding: .utf8) {
                    print("Sharing Devices status errorCode: \(errorCode) ")
                    if (self!.tokenCheckFailure(response: response)) {
                        if self!.repeatRequestCounter < 2 {
                            self!.getSharedDevices(completionHandler: completionHandler)
                        }else{
                            self!.performReset()
                            completionHandler(false, 10,nil)
                        }
                    }else{
                        if statusCode ==  404{
                            self!.performReset()
                            completionHandler(false, 10,nil)
                        }else{
                            completionHandler(false, statusCode!,nil)
                        }
                    }
                }
            }
        }
    }
    private func performReset(){
        UserDefaults.standard.set("token", forKey: ContantsKey.pushNotificationToken.rawValue)
        SCToken.clear()
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(false, forKey: ContantsKey.autorized.rawValue)
        UserDefaults.standard.synchronize()
        //remove SCdevice
        guard let rolleyCounter = UserDefaults.standard.stringArray(forKey: ContantsKey.prouctRolleyCounter.rawValue) else { return }
        for vincode in rolleyCounter{
            SCDevice.reMove(vinCode:vincode)
        }
    }
    
    public func removeSharedContact(deviceID: String, phoneNumber: String, completionHandler: @escaping (_ success: Bool, _ statusCode: Int) -> Void){
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL
        urlCreated.append(URL_SHARED_REMOVE_CONTACT)
        urlCreated.append(deviceID)
        urlCreated.append("&sharedCustomerPhoneNumber=")
        urlCreated.append(phoneNumber)
        print("Sharing contact remove url \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.delete.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseString {(response) in
            let statusCode = response.response?.statusCode
            switch statusCode{
            case 200:
                completionHandler(true, statusCode!)
            default:
                completionHandler(false, statusCode!)
            }
        }
        
    }
    
    
    public func downloadOTAFile(fileURL: String, completionHandler: @escaping (_ progress: Double, _ isCompleted: Bool) -> Void) -> Request{
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("scooterson_rolley.bin")
            return (documentsURL, [.removePreviousFile])
        }
        
       let downloadRequest =  AF.download(fileURL, to: destination).downloadProgress { (progress) in
            completionHandler(progress.fractionCompleted, false)
        }
        .responseData { (data) in
            completionHandler(1, true)
        }
        
        return downloadRequest
        
    }
    
    public func sharingAgainRequest(deviceId: String, completionHandler: @escaping (_ isSent: Bool) -> Void){
        if !Reachability.isConnectedToInternet{
            completionHandler(false)// no internet
            return
        }
        if tokens == nil {
            tokens = SCToken.reStore()
        }
        var urlCreated = BASE_URL
        urlCreated.append(URL_SHARED_REQUEST_DEVICE_AGAIN)
        urlCreated.append(deviceId)
        print("Sharing request again URL: \(urlCreated)")
        var request = URLRequest(url: URL(string: urlCreated)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue(tokens.token, forHTTPHeaderField: "X-Authorization")
        AF.request(request).responseString {[weak self] (response) in
            let statusCode = response.response?.statusCode
            guard let data = response.data else {
                completionHandler(false)
                return
                
            }
            print("Sharing request again response \(String(decoding: data, as: UTF8.self))")
            switch statusCode{
            case 200:
                completionHandler(true)
            case 400:
                //todo token expire retry to be validared
                self!.refreshToken(){ (success) in
                    if success{
                        self!.sharingAgainRequest(deviceId: deviceId,completionHandler: completionHandler)
                    }else{
                        completionHandler(false)
                    }
                }
            default:
                completionHandler(false)
            }
        }
        
    }
    
    
    
}

extension String {
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
    }
}

public class Reachability {
    static let shared = NetworkManager()
    static let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    static var isConnectedToWiFi: Bool {
        
        return reachabilityManager?.isReachableOnEthernetOrWiFi ?? false
    }
    
    static var isConnectedToInternet: Bool {
        return reachabilityManager?.isReachable ?? false
    }
    
    static func startNetworkReachabilityObserver() {
        Reachability.reachabilityManager?.startListening(onUpdatePerforming: { status in
            switch status {
            case .notReachable:
                print("The network is not reachable")
            case .unknown :
                print("It is unknown whether the network is reachable")
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
            case .reachable(.cellular):
                print("The network is reachable over the cellular connection")
            }
        })
    }
}

class NetworkManager {
    //shared instance
    static let shared = NetworkManager()
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    
}

