//
//  BTElfService.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 7/8/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol BTElfServiceDelegate {
    func deviceDisconnected(peripheral: CBPeripheral)
    func deviceConnected(peripheral: CBPeripheral)
    func managerDidChangeState(state: CBManagerState)
    
    func updateBattery(battery:Double)
    func updateSpeed(speed: UInt8)
    func updateODO(odo:Float)
    func updateTrip(trip:Float)
    func updateTripTime(time: Int)
    func modeSwitchedTo(mode: ELFMode)
    func lockStateIsChanged(lockState: Bool)
    func cruiseStateIsChanged(cruiseState: Bool)
    func lightStateIsChanged(lightState: Bool)
    func monoStateIsChanged(monoState: Bool)
    func unisStateIsChanged(imperial: Bool)
    func didGetMCUID(mcuid: String, for peripheral: CBPeripheral)
}

extension BTElfServiceDelegate {
    func deviceDisconnected(peripheral: CBPeripheral) {}
    func deviceConnected(peripheral: CBPeripheral) {}
    func managerDidChangeState(state: CBManagerState) {}
    
    func updateBattery(battery:Double) {}
    func updateSpeed(speed: UInt8) {}
    func updateODO(odo:Float) {}
    func updateTrip(trip:Float) {}
    func updateTripTime(time: Int) {}
    func modeSwitchedTo(mode: ELFMode) {}
    func lockStateIsChanged(lockState: Bool) {}
    func cruiseStateIsChanged(cruiseState: Bool) {}
    func lightStateIsChanged(lightState: Bool) {}
    func monoStateIsChanged(monoState: Bool) {}
    func unisStateIsChanged(imperial: Bool) {}
    func didGetMCUID(mcuid: String, for peripheral: CBPeripheral) {}
}

enum ELFDataHeader: String {
    case ELFDataHeaderFOOT_STEP =   "FF5519"
    case ELFDataHeaderMCU_ID =      "FF5502"
    case ELFDataHeaderODO =         "FF550C"
    case ELFDataHeaderTRIP =        "FF550B"
    case ELFDataHeaderSPEED =       "FF550A"
    case ELFDataHeaderGEAS =        "FF551F"
    case ELFDataHeaderCRUISE =      "FF551D"
    case ELFDataHeaderBATTERY =     "FF550D"
    case ELFDataHeaderLOCS =        "FF5517"
    case ELFDataHeaderBODY_TEMS =   "FF5511"
    case ELFDataHeaderTRIP_TIMS =   "FF5522"
    // TEst!
//    case ELFDataHeader_RANGE = "FF551204"
}

enum ELFMode: String {
    case ELFModeECO =   "FF551F010276"
    case ELFModeFUN =   "FF551F010377"
    case ELFModeSPORT = "FF551F010478"
}

let SCELFCodeLOCK = "FF551701026E"
let SCELFCodeUNLOCK = "FF551701016D"
let SCELFCodeECO = ELFMode.ELFModeECO.rawValue
let SCELFCodeFUN = ELFMode.ELFModeFUN.rawValue
let SCELFCodeSPORT = ELFMode.ELFModeSPORT.rawValue
let SCELFCodeGEAR = "FF551F010175"
let SCELFCodeSPEED_KM = "FF551801016E"
let SCELFCodeSPEED_MP = "FF551801026F"
let SCELFCodeCONNECT = "FF55010055"
let SCELFCodeCRUISE_ON = "FF551D010173"
let SCELFCodeCRUISE_OFF = "FF551D010274"
let SCELFCodeFOOT_STEP_LIGHT_MONO = "FF551901016F"
let SCELFCodeFOOT_STEP_LIGHT_RGB = "FF5519010270"
let SCELFCodeLIGHT_OFF = "FF5523010179"
let SCELFCodeLIGHT_ON = "FF552301027A"

let foolstepColors = [
//    "FF55150355efc474",
//    "FF55150381ececc5",
//    "FF55150374b9ff98",
//    "FF551503d63031a3",
//    "FF551503fdcb6ea2",
//    "FF5515036c5ce71b",
//    "FF551503e843932a",    
    "FF5515030652dda1",
    "FF551503ffff006a",
    "FF551503008000ec",
    "FF551503ff1431b0",
    "FF551503fdcb93c7",
    "FF551503ff00006b",
    "FF551503ffa50010",
    "FF55150300ffff6a",
    "FF5515038000806c",
    "FF5515037cfc00e4",
]

//let red = [85, 129, 116, 214, 253, 108, 232]
//let green = [239, 236, 185, 48, 203, 92, 67]
//let blue = [196, 236, 255, 49, 110, 231, 147]


class BTElfService: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {

    static let shared = BTElfService()
    public var delegate: BTElfServiceDelegate!
    public var manager: CBCentralManager!    
    private var device: CBPeripheral!
    private let serviceUUID = CBUUID(string: "7777")
    private let charasteristic1UUID = CBUUID(string: "00008888-0000-1000-8000-00805f9b34fb")
    private let charasteristic2UUID = CBUUID(string: "00008877-0000-1000-8000-00805f9b34fb")
    private var writeCharacteristic: CBCharacteristic!
    private var readCharacteristic: CBCharacteristic!
    private var connectTimer: Timer!
    private var locked: Bool!
    private var lightOn: Bool!
    private var cruiseOn: Bool!
    private var timerSecondsCount: Int8
    private var smartSuggestion = SCSMS()
    private var storedSpeed = 0.0
    private var storedMode = ELFMode.ELFModeECO
    public var chargerReminderIsOn: Bool! {
        set {
            UserDefaults.standard.set(newValue, forKey: "CHARGER_REMINDER_IS_ON")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: "CHARGER_REMINDER_IS_ON")
        }
    }
    private var currentMode: ELFMode!
    private var isMono: Bool! {
        set {
            UserDefaults.standard.set(newValue, forKey: scStoredLightState)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: scStoredLightState)
        }
    }
    private var imperial: Bool! {
        set {
            UserDefaults.standard.set(newValue, forKey: "SCMetricUnis")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: "SCMetricUnis")
        }
    }
    private var tripTime: Int = 0
    private var tripDistance: Int = 0
    
    public let scTotalTripTime = "SCTotalTripTime"
    public let scTotalTripCounts = "SCTotalTripCount"
    public let scStoredColorKey = "SCSotrdColorKey"
    public let scStoredLightState = "SCStoredLightState"
    
    override init() {
        timerSecondsCount = 0
        lightOn = false
        locked = false
        cruiseOn = false
        currentMode = .ELFModeECO
        super.init()
        manager = CBCentralManager(delegate: self, queue: nil)        
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if delegate != nil {
            delegate.managerDidChangeState(state: central.state)
        }
        if central.state == .poweredOn {
//            manager.scanForPeripherals(withServices: nil, options: nil)
            print("central state is powerOn")
        }
        else {
            switch central.state {
            case .unknown:
                print("central state is unknown")
            case .resetting:
                print("central state is reseting")
            case .unauthorized:
                print("central state is unathorised")
            case .unsupported:
                print("central state is unsupported")
            case .poweredOff:
                print("central state is power off")
            default:
                print("unknown state")
            }
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let name = advertisementData["kCBAdvDataLocalName"] as? String
        if name == "Scooter" { //"ELF" // Name of device to search!
            device = peripheral
            manager.connect(device, options: nil)
            manager.stopScan()
        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        device.delegate = self
        device.discoverServices(nil)
        delegate.deviceConnected(peripheral: peripheral)
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Device disconnected")
        connectTimer.invalidate()
        if tripDistance > 0 {
            let totalTime = UserDefaults.standard.integer(forKey: scTotalTripTime)
            UserDefaults.standard.set(totalTime + tripTime, forKey: scTotalTripTime)
            
            let totalTripsCount = UserDefaults.standard.integer(forKey: scTotalTripCounts)
            UserDefaults.standard.set(totalTripsCount + 1, forKey: scTotalTripCounts)
            
            UserDefaults.standard.synchronize()
            smartSuggestion.checkDataForSuggestion()
        }
        delegate.deviceDisconnected(peripheral: peripheral)
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        for service in services {
            if service.uuid.isEqual(serviceUUID) {
                print("*******************************")
                print(service)
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let charasteristics = service.characteristics else { return }
        for charasteristic in charasteristics {
            guard let data = Data(hexString: SCELFCodeCONNECT) else { return }
            
            if charasteristic.uuid.isEqual(charasteristic2UUID) {
                writeCharacteristic = charasteristic
                print("++++++++++++++++++++++++++++++++")
                print(charasteristic.description)
                
                device.writeValue(data, for: charasteristic, type: CBCharacteristicWriteType.withoutResponse)
                if isMono {
                    setMonoColor(colorIndex: UserDefaults.standard.integer(forKey: scStoredColorKey))
                }
                connectTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { [weak self] (timer) in
                    self?.device.writeValue(data, for: charasteristic, type: CBCharacteristicWriteType.withoutResponse)
                }
            }
            
            if charasteristic.uuid.isEqual(charasteristic1UUID) {
                readCharacteristic = charasteristic
                print("++++++++++++++++++++++++++++++++")
                print(charasteristic.description)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
//                    self?.device.setNotifyValue(true, for: charasteristic)
//                    self?.device.readValue(for: charasteristic)
//                }
                device.setNotifyValue(true, for: charasteristic)
                device.readValue(for: charasteristic)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if let errorWriting = error {
            print(errorWriting)
            return
        }
        print("Notification State is Updated")
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard let hString = characteristic.value?.hexEncodedString().uppercased() else { return }
        
        // Light state
        if hString == SCELFCodeLIGHT_ON {
            lightOn = true
            delegate.lightStateIsChanged(lightState: self.lightOn)
        }
        if hString == SCELFCodeLIGHT_OFF {
            lightOn = false
            delegate.lightStateIsChanged(lightState: self.lightOn)
        }
        
        // Read lock state
        if hString == SCELFCodeLOCK {
            self.locked = true
            delegate.lockStateIsChanged(lockState: self.locked)
        }
        if hString == SCELFCodeUNLOCK {
            self.locked = false
            delegate.lockStateIsChanged(lockState: self.locked)
        }
        
        // Read cruise state
        if hString == SCELFCodeCRUISE_ON {
            self.cruiseOn = true
            delegate.cruiseStateIsChanged(cruiseState: self.cruiseOn)
        }
        if hString == SCELFCodeCRUISE_OFF {
            self.cruiseOn = false
            delegate.cruiseStateIsChanged(cruiseState: self.cruiseOn)
        }
        
        // Read light mode
        if hString == SCELFCodeFOOT_STEP_LIGHT_RGB {
            delegate.monoStateIsChanged(monoState: self.isMono)
        }
        if hString == SCELFCodeFOOT_STEP_LIGHT_MONO {
            delegate.monoStateIsChanged(monoState: self.isMono)
        }
        
        // Read mode state
        if hString == SCELFCodeECO || hString == SCELFCodeFUN || hString == SCELFCodeSPORT {
            if let gettedMode = ELFMode(rawValue: hString) {
                if gettedMode != currentMode {
                    currentMode = gettedMode
                    delegate.modeSwitchedTo(mode: currentMode)
                    modeTimer.invalidate()
                    smartSuggestion.addTimeInMode(mode: currentMode, time: timerSecondsCount)
                    timerSecondsCount = 0
                }
            }
        }
        
        // Read units mode
        if hString == SCELFCodeSPEED_KM && imperial {
            delegate.unisStateIsChanged(imperial: self.imperial)
            imperial = false
        }
        if hString == SCELFCodeSPEED_MP && !imperial {
            delegate.unisStateIsChanged(imperial: self.imperial)
            imperial = true
        }
        
        // Read MCUID
//        if String(hString.prefix(6)) == ELFDataHeader.ELFDataHeaderMCU_ID.rawValue {
//            let preString = String(hString.dropFirst(8))
//            var dataString = String(preString.dropLast(2))
//            var mcuID = ""
//            while dataString.count >= 2 {
//                let charBytes = String(dataString.prefix(2))
//                dataString = String(dataString.dropFirst(2))
//                let char = String(charBytes.dropFirst())
//                mcuID += char
//            }
//            print("MCUID: \(mcuID)")
//        }
        
        parseData(headerString: String(hString.prefix(6)), hString: hString)
        #if DEBUG
        print(hString)
        #endif
    }
    
    
    private var modeTimer = Timer.init(timeInterval: 1, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: false)
    
    @objc func incrementCounter() {
        timerSecondsCount += 1
    }

    private func parseData (headerString: String, hString: String) {
        guard let header: ELFDataHeader = ELFDataHeader(rawValue: headerString) else { return }
        switch header {
        case .ELFDataHeaderMCU_ID:
            let preString = String(hString.dropFirst(8))
            var dataString = String(preString.dropLast(2))
            var mcuID = ""
            while dataString.count >= 2 {
                let charBytes = String(dataString.prefix(2))
                dataString = String(dataString.dropFirst(2))
                let char = String(charBytes.dropFirst())
                mcuID += char
            }
            delegate.didGetMCUID(mcuid: mcuID, for: device)
            print("MCUID: \(mcuID)")
        case .ELFDataHeaderSPEED:
            let preString = String(hString.dropFirst(8))
            let dataString = String(preString.prefix(4))
            let speed = Float(Int(dataString, radix: 16)!/1000)
            storedSpeed = Double(speed)
            delegate.updateSpeed(speed: UInt8(speed))
            if speed == 0 && storedSpeed != 0  && modeTimer.isValid {
                modeTimer.invalidate()
                smartSuggestion.addTimeInMode(mode: currentMode, time: timerSecondsCount)
                timerSecondsCount = 0
            }
        case .ELFDataHeaderBATTERY:
            let preString = String(hString.dropFirst(8))
            let dataString = String(preString.prefix(2))
            let battery = Double(Int(dataString, radix: 16)!)
            delegate.updateBattery(battery:battery)
        case .ELFDataHeaderODO:
            let preString = String(hString.dropFirst(8))
            let dataString = String(preString.dropLast(2))
            let odo = Float(Int(dataString, radix: 16)!/1000)
            delegate.updateODO(odo: odo)
        case .ELFDataHeaderTRIP:
            let preString = String(hString.dropFirst(8))
            let dataString = String(preString.dropLast(2))
            let trip = Float(Int(dataString, radix: 16)!)
            self.tripDistance = Int(trip)
            delegate.updateTrip(trip: trip)
        case .ELFDataHeaderTRIP_TIMS:
            let preString = String(hString.dropFirst(8))
            let dataString = String(preString.dropLast(2))
            let time = Int(dataString, radix: 16)!
            self.tripTime = time
            delegate.updateTripTime(time: time)
            
//        case .ELFDataHeaderCRUISE:
//            let preString = String(hString.dropFirst(8))
//            print("Cruise is: \(preString)")
//            cruiseOn = (preString == "0173")
//            delegate.cruiseStateIsChanged(cruiseState: cruiseOn)
//        case .ELFDataHeader_RANGE:
//            let preString = String(hString.dropFirst(8))
//            let dataString = String(preString.dropLast(2))
//            let range = Float(Int(dataString, radix: 16)!)
//            print("Range: \(range)")
        default:
           doSomething()
//            print(headerString)
        }
    }
    
    private func doSomething() {}

    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if let errorWriting = error {
            print(errorWriting)
            return
        }
        print("Data writed!")
    }
    
    // MARK: Public request values methods
    
    public func isLightMono() -> Bool {
        return UserDefaults.standard.bool(forKey: scStoredLightState)
    }
    
    public func isLocked() -> Bool {
        return self.locked
    }
    
    public func isHeadlight() -> Bool {
        return self.lightOn
    }
    
    public func isCruise() -> Bool {
        return self.cruiseOn
    }
    
    public func isImperial() -> Bool {
        return self.imperial
    }
    
    // MARK: Send data methods
    
    public func lightOnOff() {
        if device.state == .connected {
            guard let data = Data(hexString: lightOn ? SCELFCodeLIGHT_OFF : SCELFCodeLIGHT_ON) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
        }
    }
    
    public func lockUnlock() {
        if device.state == .connected {
            guard let data = Data(hexString: locked ? SCELFCodeUNLOCK : SCELFCodeLOCK) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
        }
    }
    
    public func switchMode() {
        if device.state == .connected {
            guard let data = Data(hexString: currentMode.rawValue) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
        }
    }
    
    public func switchLightMode() {
        if device.state == .connected {
            guard let data = Data(hexString: isMono ? SCELFCodeFOOT_STEP_LIGHT_RGB : SCELFCodeFOOT_STEP_LIGHT_MONO) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
            isMono = !isMono
            if isMono {
                let colorIndex = UserDefaults.standard.integer(forKey: scStoredLightState)
                guard let data = Data(hexString: foolstepColors[colorIndex]) else { return }
                device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
            }
        }
    }
    
    public func swithchFootstepPattern() {
        if device.state == .connected {
            guard let data = Data(hexString: SCELFCodeFOOT_STEP_LIGHT_RGB) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
        }
    }
    
    public func setMonoColor(colorIndex: Int) {
        if device.state == .connected {
            if isMono {
                guard let data = Data(hexString: SCELFCodeFOOT_STEP_LIGHT_MONO) else { return }
                device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
                guard let dataC = Data(hexString: foolstepColors[colorIndex]) else { return }
                device.writeValue(dataC, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
                UserDefaults.standard.set(colorIndex, forKey: scStoredColorKey)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    public func setCruise() {
        if device.state == .connected {
            let onoffStr = cruiseOn ? SCELFCodeCRUISE_OFF : SCELFCodeCRUISE_ON
            guard let data = Data(hexString: onoffStr) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
        }
    }
    
    public func switchUnits() {
        #if targetEnvironment(simulator)        
        imperial = !imperial
        #else
        if device.state == .connected {
            let units = !imperial ? SCELFCodeSPEED_MP : SCELFCodeSPEED_KM
            guard let data = Data(hexString: units) else { return }
            device.writeValue(data, for: writeCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
            imperial = !imperial
        }
        #endif
    }
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
    
    init?(hexString: String) {
        if hexString.count <= 0 { return nil }
        let len = hexString.count / 2
        var data = Data(capacity: len)
        for i in 0..<len {
            let j = hexString.index(hexString.startIndex, offsetBy: i*2)
            let k = hexString.index(j, offsetBy: 2)
            let bytes = hexString[j..<k]
            if var num = UInt8(bytes, radix: 16) {
                data.append(&num, count: 1)
            } else {
                return nil
            }
        }
        self = data
    }
}
