//
//  BTServices.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 4/18/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol BTServiceDelegate {
    func pairedPeripheral(peripheral: CBPeripheral)
    func unpairedPeripheral(peripheral: CBPeripheral)
    func cantConnect()
}

class BTServices: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {

    static let kBLEService_UUID: String = "7777"
    static let kBLE_Characteristic_uuid_Tx: String = "00008888-0000-1000-8000-00805f9b34fb"
    static let kBLE_Characteristic_uuid_Rx: String = "00008877-0000-1000-8000-00805f9b34fb"
    
    static let BLEService_UUID = CBUUID(string: kBLEService_UUID)
    static let BLE_Characteristic_uuid_Tx = CBUUID(string: kBLE_Characteristic_uuid_Tx)//(Property = Write without response)
    static let BLE_Characteristic_uuid_Rx = CBUUID(string: kBLE_Characteristic_uuid_Rx)
    
    static let shared = BTServices()
    private var btCentral: CBCentralManager!
    private var device: CBPeripheral!
    var delegate: BTServiceDelegate!
    var deviceConnected: Bool = false
    var deviceCBUUID: CBUUID!
    var txCharacteristic : CBCharacteristic?
    var rxCharacteristic : CBCharacteristic?
    var characteristicASCIIValue = NSString()
    
    override private init() {
        super.init()
        btCentral = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    func pairToDeviceWithUUID(cbUUIDString: String) {
        deviceCBUUID = CBUUID(string: cbUUIDString)
//        let services : [CBUUID] = [CBUUID(string: "00008888-0000-1000-8000-00805f9b34fb"), CBUUID(string: "00008877-0000-1000-8000-00805f9b34fb")]
        if btCentral.state == .poweredOn {
            btCentral.scanForPeripherals(withServices: nil, options: nil)
        }
        else {
            delegate.cantConnect()
        }
    }
    
    func disconnectDevice () {
        btCentral.cancelPeripheralConnection(device)
    }
    
    internal func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        delegate.cantConnect()
    }
    
    internal func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("central state is unknown")
        case .resetting:
            print("central state is reseting")
        case .unauthorized:
            print("central state is unathorised")
        case .unsupported:
            print("central state is unsupported")
        case .poweredOn:
            print("central state is power on")
        case .poweredOff:
            print("central state is power off")
        default:
            print("unknown state")
        }
    }
    
    internal func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let serviceName = advertisementData["kCBAdvDataLocalName"] as? String
        let connectable = advertisementData["kCBAdvDataIsConnectable"] as? Bool
        if  connectable! && serviceName == "Scooter"  {
            print("\(peripheral): \(advertisementData)")
            device = peripheral
            btCentral.stopScan()
            btCentral.connect(device, options: nil)
        }
    }
    
    internal func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        delegate.unpairedPeripheral(peripheral: peripheral)
        if peripheral == device {
            deviceConnected = false
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        delegate.pairedPeripheral(peripheral: peripheral)
        if peripheral == device {
            deviceConnected = true
            device.delegate = self
            device.discoverServices(nil)//[BTServices.BLEService_UUID])
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        print("*******************************************************")
        for service in services {
            print("Service: \(service)")
//            peripheral.discoverIncludedServices(nil, for: service)
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
        print("*******************************************************")
        print("Included service: \(service)")
//            peripheral.discoverIncludedServices(nil, for: service)
            peripheral.discoverCharacteristics(nil, for: service)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else { return }
        print("*******************************************************")
        print("Found \(characteristics.count) characteristics!")
        
        for characteristic in characteristics {
            //looks for the right characteristic
//            print("-----------------------")
//            print(characteristic)

            if characteristic.uuid.isEqual(BTServices.BLE_Characteristic_uuid_Rx)  {
                rxCharacteristic = characteristic
                
                //Once found, subscribe to the this particular characteristic...
                peripheral.setNotifyValue(true, for: rxCharacteristic!)
                // We can return after calling CBPeripheral.setNotifyValue because CBPeripheralDelegate's
                // didUpdateNotificationStateForCharacteristic method will be called automatically
                peripheral.readValue(for: characteristic)
                print("Rx Characteristic: \(characteristic.uuid)")
                peripheral.discoverDescriptors(for: characteristic)
//                let bytes:[UInt8] = [0xFF, 0x55, 0x01, 0x00, 0x55]
                //                let data = Data( {//String(Int("FF55010055", radix: 16)!, radix: 2).data(using: .ascii) {
//                let data = Data(fromHexEncodedString: "FF55010055")//"FF55010055".hexadecimal()//bytes.data
//                print(data!.description)
//                while peripheral.state == .connected {
//                    peripheral.writeValue(data!, for: rxCharacteristic!, type: .withResponse)
//                }
            }
            if characteristic.uuid.isEqual(BTServices.BLE_Characteristic_uuid_Tx){
                txCharacteristic = characteristic
                if let data = String(Int("FF55010055", radix: 16)!, radix: 2).data(using: .ascii) {
                    while peripheral.state == .connected {
                        peripheral.writeValue(data, for: rxCharacteristic!, type: .withResponse)
                    }
                }
//                peripheral.setNotifyValue(true, for: txCharacteristic!)
                print("Tx Characteristic: \(characteristic.uuid)")
//                peripheral.discoverDescriptors(for: characteristic)
//                let bytes:[UInt8] = [0xFF, 0x55, 0x01, 0x00, 0x55]
                //                let data = Data( {//String(Int("FF55010055", radix: 16)!, radix: 2).data(using: .ascii) {
                let data = "FF551F010377".hexadecimal()//bytes.data
                print(data!.description)
//                while peripheral.state == .connected {
                peripheral.writeValue(data!, for: characteristic, type: .withResponse)
//                }

            }
//            peripheral.discoverDescriptors(for: characteristic)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        print("*******************************************************")
        
        if error != nil {
            print("\(error.debugDescription)")
            return
        }
        if ((characteristic.descriptors) != nil) {
            
            for x in characteristic.descriptors!{
                if let descript = x as CBDescriptor? {
                    
                    if let data = "FF55010055".hexadecimal() {
                        while peripheral.state == .connected {
                            peripheral.writeValue(data, for: characteristic, type: .withResponse)
                        }
                    }
                    print("function name: DidDiscoverDescriptorForChar \(String(describing: descript.description))")
                    print("Rx Value \(String(describing: rxCharacteristic?.value))")
                    print("Tx Value \(String(describing: txCharacteristic?.value))")
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("==================================")
        print("Data was writen for characteristic\(characteristic.description)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Value was updated for characteristic: \(characteristic.description)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        print("==================================")
        print("Data was writen for descriptor\(descriptor.description)")
    }
}

extension String {
    /// Create `Data` from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a `Data` object. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.
    
    func hexadecimal() -> Data? {
        var data = Data(capacity: self.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, self.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            var num = UInt8(byteString, radix: 16)!
            data.append(&num, count: 1)
        }
        
        guard data.count > 0 else {
            return nil
        }
        
        return data
    }
}

extension Data {
    // Convert 0 ... 9, a ... f, A ...F to their decimal value,
    // return nil for all other input characters
    fileprivate func decodeNibble(_ u: UInt16) -> UInt8? {
        switch(u) {
        case 0x30 ... 0x39:
            return UInt8(u - 0x30)
        case 0x41 ... 0x46:
            return UInt8(u - 0x41 + 10)
        case 0x61 ... 0x66:
            return UInt8(u - 0x61 + 10)
        default:
            return nil
        }
    }
    
    init?(fromHexEncodedString string: String) {
        var str = string
        if str.count%2 != 0 {
            // insert 0 to get even number of chars
            str.insert("0", at: str.startIndex)
        }
        
        let utf16 = str.utf16
        self.init(capacity: utf16.count/2)
        
        var i = utf16.startIndex
        while i != str.utf16.endIndex {
            guard let hi = decodeNibble(utf16[i]),
                let lo = decodeNibble(utf16[utf16.index(i, offsetBy: 1, limitedBy: utf16.endIndex)!]) else {
                    return nil
            }
            var value = hi << 4 + lo
            self.append(&value, count: 1)
            i = utf16.index(i, offsetBy: 2, limitedBy: utf16.endIndex)!
        }
    }
}

extension String {
    
    /// Create `String` representation of `Data` created from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a String object from that. Note, if the string has any spaces, those are removed. Also if the string started with a `<` or ended with a `>`, those are removed, too.
    
    init?(hexadecimal string: String) {
        guard let data = string.hexadecimal() else {
            return nil
        }
        
        self.init(data: data, encoding: .utf8)
    }
    
    /// - parameter encoding: The `NSStringCoding` that indicates how the string should be converted to `NSData` before performing the hexadecimal conversion.
    
    /// - returns: `String` representation of this String object.
    
    func hexadecimalString() -> String? {
        return data(using: .utf8)?
            .hexadecimal()
    }
    
}

extension Data {
    
    /// Create hexadecimal string representation of `Data` object.
    
    /// - returns: `String` representation of this `Data` object.
    
    func hexadecimal() -> String {
        return map { String(format: "%02x", $0) }
            .joined(separator: "")
    }
}

extension Data {
    var bytes : [UInt8]{
        return [UInt8](self)
    }
}


extension Array where Element == UInt8 {
    var data : Data{
        return Data(self)
    }
}
