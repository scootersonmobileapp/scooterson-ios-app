//
//  RolleyBleService.swift
//  Scooterson
//
//  Created by deepansh jain on 6/7/20.
//  Copyright © 2020 Anton Umnitsyn. All rights reserved.
//

import Foundation

protocol RolleyBleDelegate {
    func deviceStatus(status: Int32)
    func onInfoReceived(command: Int32, value: String!)
}


class RolleyBleService: NSObject, PLBleCallback{
    private var peripheral: CBPeripheral!
    
    private var char_uuid_write: NSMutableArray = []
    private var char_uuid_read: NSMutableArray = []
    private var char_uuid_notify: NSMutableArray = []
    var isConnected = false
    var delegate: RolleyBleDelegate!
    
    
    override init() {
        super.init()
        char_uuid_write.add(UUID_CAN_TRANSMIT_TX)
        char_uuid_write.add(UUID_CAN_FW)
        char_uuid_write.add(UUID_CAN_CONTROL)
        char_uuid_notify.add(UUID_CAN_TRANSMIT_RX)
        char_uuid_notify.add(UUID_CAN_CONTROL)
        char_uuid_read.add(UUID_DEV_FW_VERSION)
        
    }
    
    func intializeService(getPeripheral: CBPeripheral, delegate: RolleyBleDelegate){
        self.peripheral = getPeripheral
        PLBleService.getInstance()?.ble_cb = self
        PLBleService .getInstance()?.connect(peripheral, write:char_uuid_write, notify: char_uuid_notify, read: char_uuid_read)
        self.delegate = delegate
    }
    
    func onDeviceStatusChanged(_ peripheral: CBPeripheral!, status: Int32) {
        print("Rolley onDeviceStatusChanged \(peripheral.identifier.uuidString) with status \(status) ")
        isConnected = false
        switch (status) {
        case PL_BLE_STATUS_CONNECTED:
            break
        case PL_BLE_STATUS_READY:
            isConnected = true
            break
        case PL_BLE_STATUS_RECONNECTING:
            //Dismiss when state moved to disconnected
            //todo show popup on the device screen that its been disconnected
            break
        default:
            break
        }
        delegate.deviceStatus(status: status)
    }
    
    func onDataReceived(_ cmd: Int32, value: String!) {
        delegate.onInfoReceived(command: cmd, value: value)
    }
    
    func sendCommandToRolley(command: Int32, data: Int32){
        if(PLBleService.getInstance() != nil){
            PLBleService.getInstance()?.process_can_cmd(command, data: data, pin: nil)
        }
    }
    
    func getStringValueForAutoOff()->String{
        var value = ""
        if(PLBleService.getInstance() != nil){
            let info: CanInfo = PLBleService.getInstance()?.can_info[47] as! CanInfo
            value = info.str_value
            print("Auto Off Can info: \(info.str_info!) with value \(value)")
        }
        return value
    }
    
    func getStringValueForIntelligentLight()->String{
        var value = ""
        if(PLBleService.getInstance() != nil){
            let info: CanInfo = PLBleService.getInstance()?.can_info[49] as! CanInfo
            value = info.str_value
            print("Intelligent light can info: \(info.str_info!) with value \(value)")
        }
        return value
    }
    
    func getCanInfoForCertainParams(key: Int)-> String{
        var value = "NA"
        if(PLBleService.getInstance() != nil){
            let info: CanInfo = PLBleService.getInstance()?.can_info[key] as! CanInfo
            value = info.str_value
            print("Can info: \(info.str_info!)")
        }
        return value
    }
    
    
    func sendPinCommandToRolley(command: Int32, pin: String){
        
        let dataPin = Data(pin.utf8)
        print("Key feature in pin command Rolley %@", dataPin)
        
        if(PLBleService.getInstance() != nil){
            PLBleService.getInstance()?.process_can_cmd(command, data: 0, pin: dataPin)
        }
    }
    
    func disconnect(){
        if(PLBleService.getInstance() != nil){
            self.delegate = nil
            PLBleService.getInstance()?.ble_cb = nil
            PLBleService.getInstance()?.disconnect()
            print("Rolley disconnected successfully")
        }
    }
    
    func changeRolleyName(name: String){
        let nameAsData = Data(name.utf8)
        if(PLBleService.getInstance() != nil){
            print("Name change request received %@ \(nameAsData)")
            let cmdEncrpted  = PLBleService.getInstance()?.gen_cmd_encry(UInt8(CAN_SET_NAME), data: nameAsData)
            PLBleService.getInstance()?.send_data_encry(cmdEncrpted, uuid: UUID_CAN_CONTROL, is_long_pkt: false)
        }
    }
    func onDeviceNameChanged(_ status: Bool) {
        print("Name change request status \(status)")
    }
    
    func fetchDriveTrainInfo(){
        if PLBleService.getInstance() != nil {
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                PLBleService.getInstance()?.get_base_info()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                PLBleService.getInstance()?.get_battery_info()
            }
        }
    }
    
}
