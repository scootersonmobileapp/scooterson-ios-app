//
//  SCSmartModeSugestion.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 8/8/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Foundation

class SCSMS {
    private var timeInMode: [ELFMode:Int8]!
    private var totalDistance: Double!
    private var totalTime: Int8!
    
    private let mode2Speed = [
        ELFMode.ELFModeECO: [1.0, 10.0],
        ELFMode.ELFModeFUN: [10.1, 15.0],
        ELFMode.ELFModeSPORT: [15.1, 25.0]
    ]
    
    
    init() {}
    
    func addTimeInMode(mode: ELFMode, time:Int8) {
        let sTime = timeInMode[mode] ?? 0
        timeInMode[mode] = sTime + time
    }
    
    func totalDistance(distance:Double) {
        totalDistance = distance
    }
    
    func totalTime(time:Int8) {
        totalTime = time
    }
    
    func checkDataForSuggestion() {
        if totalTime > 0 && totalDistance > 0 {
            let averageSpeed = totalDistance/Double(totalTime)
            var longestMode = ELFMode.ELFModeECO
            timeInMode.forEach { (mode, time) in
                if timeInMode[longestMode]! < time {
                    longestMode = mode
                }
            }
            if !(averageSpeed > (mode2Speed[longestMode]?[0])! && averageSpeed < (mode2Speed[longestMode]?[1])!) {
                prepareSuggestionMessage(mode: longestMode)
            }
        }
    }
    
    private func prepareSuggestionMessage(mode:ELFMode) {
        var recomendedMode: String
        switch mode {
        case .ELFModeFUN:
            recomendedMode = "ECO"
        case .ELFModeSPORT:
            recomendedMode = "FUN"
        case .ELFModeECO:
            recomendedMode = "ECO"
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MODE_SUGGESTION"), object: nil, userInfo: ["title": "modeSugestionTitle".localized(), "message": "modeSugestionMessage\(recomendedMode)".localized()])
    }
}
