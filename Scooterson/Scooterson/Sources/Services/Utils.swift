//
//  Utils.swift
//  Scooterson
//
//  Created by Anton Umnitsyn on 5/22/19.
//  Copyright © 2019 Anton Umnitsyn. All rights reserved.
//

import Foundation
import CoreTelephony

class Utils {
    static func firstCarrier() -> CTCarrier? {
        let carrierInfo = CTTelephonyNetworkInfo()
        if let carrier = (carrierInfo.serviceSubscriberCellularProviders?.values.first) {
            return carrier
        }
        return nil
    }
    
    static func localToUTC(date:Date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.string(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: dt)!
    }
    
    static func UTCToLocal(date:Date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.string(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm"
        
        return dateFormatter.date(from: dt)!
    }
}
