//
//  PermissionsManager.swift
//  Scooterson
//
//  Created by deepansh jain on 8/1/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//
import UIKit
import CoreLocation

class PermissionsManager {

    static let shared = PermissionsManager()

    let locationManager = CLLocationManager()


    func requestWhenInUseLocation() {
        locationManager.requestWhenInUseAuthorization()
    }

}
