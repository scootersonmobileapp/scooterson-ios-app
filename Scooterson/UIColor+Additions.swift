//
//  UIColor+Additions.swift
//  Scooterson
//
//  Generated on Zeplin. (5/15/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {
    func toHexString() -> String {
        
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        return String(format: "#%02X%02X%02X", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
    }
    
    @nonobjc class var coolGrey: UIColor {
        return UIColor(red: 145.0 / 255.0, green: 155.0 / 255.0, blue: 159.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var charcoalGrey: UIColor {
        return UIColor(red: 50.0 / 255.0, green: 62.0 / 255.0, blue: 65.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var dodgerBlue: UIColor {
        return UIColor(red: 52.0 / 255.0, green: 151.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var palePink: UIColor {
        return UIColor(red: 1.0, green: 204.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var gunmetal: UIColor {
        return UIColor(red: 86.0 / 255.0, green: 99.0 / 255.0, blue: 103.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var paleGrey: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 243.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var pastelOrange: UIColor {
        return UIColor(red: 1.0, green: 144.0 / 255.0, blue: 87.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var greenblue: UIColor {
        return UIColor(red: 35.0 / 255.0, green: 217.0 / 255.0, blue: 126.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var barbiePink: UIColor {
        return UIColor(red: 1.0, green: 52.0 / 255.0, blue: 130.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var dodgerBlueDisable: UIColor {
        return UIColor(red: 52.0 / 255.0, green: 151.0 / 255.0, blue: 253.0 / 255.0, alpha: 0.7)
    }
}
