//
//  ContentView.swift
//  watch Extension
//
//  Created by deepansh jain on 7/27/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
