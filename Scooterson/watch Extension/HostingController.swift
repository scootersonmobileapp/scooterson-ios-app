//
//  HostingController.swift
//  watch Extension
//
//  Created by deepansh jain on 7/27/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
