//
//  NotificationView.swift
//  watch Extension
//
//  Created by deepansh jain on 7/27/20.
//  Copyright © 2020 Scooterson. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
